package com.north.light.libpicblur;

import android.view.View;

/**
 * FileName: LibPicBlurCusApi
 * Author: lzt
 * Date: 2022/11/22 16:06
 * 毛玻璃实现类
 */
public interface LibPicBlurCusApi {

    /**
     * 开始模糊
     */
    public void startBlur(View parentView);


    /**
     * @param blurRadius 模糊半径 1-25
     */
    public void startBlur(View parentView, float blurRadius);

    /**
     * 四周圆角
     * */
    public void startBlur(View parentView, float blurRadius, int cornerRadius);


}
