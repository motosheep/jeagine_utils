package com.example.libpicblur;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.RelativeLayout;

/**
 * author:li
 * date:2022/10/29
 * desc:高斯模糊自定义跟布局
 */
@Deprecated
public class LibPicBlurRelativeView extends RelativeLayout {

    private static final String TAG = LibPicBlurRelativeView.class.getSimpleName();

    private EventListener eventListener;

    public LibPicBlurRelativeView(Context context) {
        super(context);
    }

    public LibPicBlurRelativeView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public LibPicBlurRelativeView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptHoverEvent(MotionEvent event) {
        Log.d(TAG, "onInterceptHoverEvent ");
        return super.onInterceptHoverEvent(event);
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        Log.d(TAG, "dispatchTouchEvent ");
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                Log.d(TAG, "dispatchTouchEvent ACTION_DOWN");
                if (eventListener != null) {
                    eventListener.down();
                }
                break;
            case MotionEvent.ACTION_MOVE:
                Log.d(TAG, "dispatchTouchEvent ACTION_MOVE");
                break;
            case MotionEvent.ACTION_UP:
                Log.d(TAG, "dispatchTouchEvent up");
                if (eventListener != null) {
                    eventListener.up();
                }
                break;
            case MotionEvent.ACTION_CANCEL:
                Log.d(TAG, "dispatchTouchEvent cancel");
                break;
        }
        return super.dispatchTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
//        int action = event.getAction();
//        switch (action) {
//            case MotionEvent.ACTION_DOWN:
//                Log.d(TAG, "onTouchEvent ACTION_DOWN");
//                if (eventListener != null) {
//                    eventListener.down();
//                }
//                break;
//            case MotionEvent.ACTION_MOVE:
//                Log.d(TAG, "onTouchEvent ACTION_MOVE");
//                break;
//            case MotionEvent.ACTION_UP:
//            case MotionEvent.ACTION_CANCEL:
//                Log.d(TAG, "onTouchEvent up or cancel");
//                break;
//        }
        return super.onTouchEvent(event);
    }


    public void setEventListener(EventListener listener) {
        this.eventListener = listener;
    }

    public interface EventListener {
        void down();

        void up();
    }
}
