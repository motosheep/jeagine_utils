package com.example.libpicblur;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.example.libpicblur.widget.LibPicBlurShapeBlurView;
import com.example.libpicblur.widget.LibPicColorView;

/**
 * author:li
 * date:2022/10/28
 * desc:高斯模糊自定义view
 */
public class LibPicBlurView extends RelativeLayout {

    private LibPicBlurShapeBlurView mBlurShapeBlurView;
    private LibPicColorView mPicColorView;

    public LibPicBlurView(Context context) {
        super(context);
        init();
    }

    public LibPicBlurView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public LibPicBlurView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        removeAllViews();
        View rootView = LayoutInflater.from(getContext()).inflate(R.layout.lib_pic_blur_root_view, null);
        addView(rootView);
        mBlurShapeBlurView = rootView.findViewById(R.id.lib_pic_blur_org_blur);
        mPicColorView = rootView.findViewById(R.id.lib_pic_blur_shadow);
    }

    protected int dp2px(int dpValue) {
        return (int) getContext().getResources().getDisplayMetrics().density * dpValue;
    }

    //外部调用------------------------------------------------------------------------------

    /**
     * 外部调用
     */
    public LibPicBlurShapeBlurView getBlurView() {
        return mBlurShapeBlurView;
    }

    /**
     * 设置自定义decorview
     */
    public void setDecorView(View decorView) {
        if (getBlurView() != null) {
            getBlurView().setmCusDecorView(decorView);
        }
    }


    /**
     * 设置Over layout颜色
     */
    public void setOverColor(int overColor) {
        if (getBlurView() == null) {
            return;
        }
        mPicColorView.postDelayed(new Runnable() {
            @Override
            public void run() {
                mPicColorView.setColor(overColor, dp2px(14), dp2px(14), 0, 0);
            }
        }, 80);
    }

    /**
     * 设置view的高度
     */
    public void setRootViewHeight(int height) {
        if (getBlurView() == null) {
            return;
        }
        getBlurView().setRootViewHeight(height);
    }

    /**
     * 设置身份标识
     */
    public void setIdentify(String key) {
        if (getBlurView() == null) {
            return;
        }
        getBlurView().setCurrentTag(key);
    }

    /**
     * 设置view在adapter中的位置
     */
    public void setViewPos(int key) {
        if (getBlurView() == null) {
            return;
        }
        getBlurView().setViewPos(key);
    }

    /**
     * 开始毛玻璃
     */
    public void startBlur() {
        if (getBlurView() == null) {
            return;
        }
        getBlurView().needBlur(true);
    }

    /**
     * 停止毛玻璃
     */
    public void stopBlur() {
        if (getBlurView() == null) {
            return;
        }
        getBlurView().needBlur(false);
    }

    /**
     * 暂停xx时间后恢复
     */
    public void resumeAfter(long time) {
        if (getBlurView() == null) {
            return;
        }
        getBlurView().resumeAfter(time);
    }

    /**
     * 显隐毛玻璃逻辑
     */
    public void hideBlurView(boolean hide) {
        if (getBlurView() != null) {
            getBlurView().setVisibility((hide) ? View.INVISIBLE : View.VISIBLE);
        }
    }
}
