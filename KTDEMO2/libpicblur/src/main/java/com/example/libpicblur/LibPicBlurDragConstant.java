package com.example.libpicblur;

import java.io.Serializable;

/**
 * FileName: LibPicBlurDragConstant
 * Author: lzt
 * Date: 2022/11/9 12:54
 * 拖动变量--！！！！重要，外部通过设置拖动参数变量。毛玻璃会根据这个变量进行是否模糊！！！！！
 */
public class LibPicBlurDragConstant implements Serializable {

    //是否拖动中
    private boolean isDrag = false;
    //当前拖动的位置
    private int dragPos = 0;

    private static class SingleHolder implements Serializable {
        static LibPicBlurDragConstant mInstance = new LibPicBlurDragConstant();
    }

    public static LibPicBlurDragConstant getInstance() {
        return SingleHolder.mInstance;
    }

    public void setDrag(boolean drag) {
        isDrag = drag;
    }

    public boolean isDrag() {
        return isDrag;
    }


    public int getDragPos() {
        return dragPos;
    }

    public void setDragPos(int dragPos) {
        this.dragPos = dragPos;
    }

}
