package com.north.light.libsvga;

/**
 * author:li
 * date:2022/12/25
 * desc:
 */
public interface SVGAStatusListener {
    void start();

    void pause();

    void finish();

    void repeat();
}
