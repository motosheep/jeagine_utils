package com.north.light.libsvga;

import com.opensource.svgaplayer.SVGAVideoEntity;

/**
 * author:li
 * date:2022/12/25
 * desc:
 */
public interface SVGALoadListener {
    void complete(SVGAVideoEntity entity);

    void error();
}
