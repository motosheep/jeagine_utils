package com.north.light.libsvga;

import android.content.Context;
import android.net.http.HttpResponseCache;

import com.opensource.svgaplayer.SVGACallback;
import com.opensource.svgaplayer.SVGADrawable;
import com.opensource.svgaplayer.SVGAImageView;
import com.opensource.svgaplayer.SVGAParser;
import com.opensource.svgaplayer.SVGAVideoEntity;
import com.opensource.svgaplayer.utils.log.SVGALogger;

import java.io.File;
import java.io.Serializable;
import java.util.List;

/**
 * author:li
 * date:2022/12/24
 * desc:
 */
public class SVGAAnimManager implements Serializable {
    private Context mContext;
    //缓存大小
    private final int CACHE_SIZE = 1024 * 1024 * 128;

    private static class SingleHolder {
        public static SVGAAnimManager mInstance = new SVGAAnimManager();
    }

    public static SVGAAnimManager getInstance() {
        return SingleHolder.mInstance;
    }


    /**
     * 初始化--必须调用
     */
    public void init(Context context) {
        this.mContext = context.getApplicationContext();
        try {
            SVGAParser.Companion.shareParser().init(context);
            File cacheDir = new File(context.getApplicationContext().getCacheDir(), "http");
            HttpResponseCache.install(cacheDir, CACHE_SIZE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        SVGALogger.INSTANCE.setLogEnabled(BuildConfig.DEBUG);
    }

    /**
     * 加载assets
     */
    public void loadAssets(Context context, String assetsName, SVGALoadListener listener) {
        loadAssets(context, assetsName, null, listener);
    }


    public <T extends SVGAImageView> void loadAssets(Context context, String assetsName, T svgaImageView,
                                                     SVGALoadListener listener) {
        loadAssets(context, assetsName, svgaImageView, listener, null);
    }

    //notice 如果外部传入status listener，意味着外部放弃了svga imageview的监听状态（set callback）
    public <T extends SVGAImageView> void loadAssets(Context context, String assetsName, T svgaImageView,
                                                     SVGALoadListener listener, SVGAStatusListener statusListener) {
        if (listener == null && svgaImageView == null) {
            return;
        }
        if (svgaImageView != null) {
            if (statusListener != null) {
                svgaImageView.setCallback(new SVGACallback() {
                    @Override
                    public void onPause() {
                        statusListener.pause();
                    }

                    @Override
                    public void onFinished() {
                        statusListener.finish();
                    }

                    @Override
                    public void onRepeat() {
                        statusListener.repeat();
                    }

                    @Override
                    public void onStep(int i, double v) {

                    }
                });
            } else {
                svgaImageView.setCallback(null);
            }
        }
        SVGAParser parser = new SVGAParser(context.getApplicationContext());
        parser.decodeFromAssets(assetsName, new SVGAParser.ParseCompletion() {
            @Override
            public void onComplete(SVGAVideoEntity svgaVideoEntity) {
                if (svgaImageView != null && svgaVideoEntity != null) {
                    SVGADrawable drawable = new SVGADrawable(svgaVideoEntity);
                    svgaImageView.setImageDrawable(drawable);
                    svgaImageView.startAnimation();
                    if (statusListener != null) {
                        statusListener.start();
                    }
                }
                if (listener != null) {
                    listener.complete(svgaVideoEntity);
                }
            }

            @Override
            public void onError() {
                if (listener != null) {
                    listener.error();
                }
            }
        }, new SVGAParser.PlayCallback() {
            @Override
            public void onPlay(List<? extends File> list) {

            }
        });
    }

}
