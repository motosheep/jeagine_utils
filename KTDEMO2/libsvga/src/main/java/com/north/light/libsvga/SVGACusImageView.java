package com.north.light.libsvga;

import android.content.Context;
import android.util.AttributeSet;

import com.opensource.svgaplayer.SVGAImageView;

/**
 * FileName: SVGACusImageView
 * Author: lzt
 * Date: 2022/12/27 09:32
 */
public class SVGACusImageView extends SVGAImageView {

    public SVGACusImageView(Context context) {
        super(context);
        init();
    }

    public SVGACusImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SVGACusImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        //设置detached后清空画布
        setClearsAfterDetached(true);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
