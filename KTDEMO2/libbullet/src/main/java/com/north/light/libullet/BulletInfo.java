package com.north.light.libullet;

import java.io.Serializable;

/**
 * FileName: BulletInfo
 * Author: lzt
 * Date: 2022/9/15 14:01
 * 弹幕信息
 */
public class BulletInfo implements Serializable {

    //类型：1内容 2空布局填充
    private int type = 1;

    //昵称
    private String nickName;

    //文字
    private String content;

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
