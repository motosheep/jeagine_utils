package com.north.light.libullet;

import android.animation.Animator;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.north.light.libbullet.R;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: BulletInfoAdapter
 * Author: lzt
 * Date: 2022/9/15 14:04
 * 弹幕adapter
 */
public class BulletInfoAdapter extends RecyclerView.Adapter<BulletInfoAdapter.BulletHolder> {

    //动画
    private BulletScaleInAnimation bulletScaleInAnimation = new BulletScaleInAnimation();

    //弹幕数据
    private List<BulletInfo> mData = new ArrayList<>();


    public List<BulletInfo> getData() {
        return mData;
    }

    public void setData(List<BulletInfo> mData) {
        this.mData = wrapperData(mData);
        notifyDataSetChanged();
    }

    public void clear() {
        this.mData = new ArrayList<>();
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NonNull BulletHolder holder) {
        super.onViewAttachedToWindow(holder);
        try {
            addAnimation(holder);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addAnimation(BulletHolder holder) throws Exception {
        holder.itemView.clearAnimation();
        for (Animator animator : bulletScaleInAnimation.getAddAnimators(holder.itemView)) {
            animator.setDuration(2000).start();
            animator.setInterpolator(new LinearInterpolator());
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    try {
                        removeAnimation(holder);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
        }
    }

    private void removeAnimation(BulletHolder holder) throws Exception {
        for (Animator animator : bulletScaleInAnimation.getRemoveAnimators(holder.itemView)) {
            animator.setDuration(5000).start();
            animator.setInterpolator(new LinearInterpolator());
        }
    }

    /**
     * 前后都填充五个空布局
     */
    private List<BulletInfo> wrapperData(List<BulletInfo> mData) {
        if (mData == null || mData.size() == 0) {
            return new ArrayList<>();
        }
        int insertSize = 7;
        for (int i = 0; i < insertSize; i++) {
            BulletInfo extra = new BulletInfo();
            extra.setType(2);
            if (i < insertSize - 3) {
                mData.add(0, extra);
            } else {
                mData.add(mData.size(), extra);
            }
        }
        return mData;
    }

    @Override
    public BulletHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_libbullet_info, parent, false);
        return new BulletHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BulletHolder holder, int position) {
        //设置数据
        BulletInfo bulletInfo = getData().get(position);
        if (bulletInfo == null) {
            return;
        }
        TextView tvContent = holder.mContent;
        if (tvContent != null && tvContent.getEditableText() != null) {
            tvContent.getEditableText().clear();
            tvContent.getEditableText().clearSpans();
            tvContent.setText(null);
        }
        //1内容 2空布局填充
        if (tvContent != null) {
            int type = bulletInfo.getType();
            switch (type) {
                case 1:
                    tvContent.setVisibility(View.VISIBLE);
                    String commentNickName = bulletInfo.getNickName();
                    String comment = bulletInfo.getContent();
//                    String content = commentNickName + "：" + comment;
//                    int index = content.indexOf(comment);
//                    SpannableStringBuilder span = new SpannableStringBuilder(content);
//                    span.setSpan(new ForegroundColorSpan(Color.parseColor("#8ce7fe")), 0, index, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
//                    tvContent.setText(span);


                    tvContent.setText((Html.fromHtml("<font color = '#8ce7fe'>" + commentNickName + "：</font>" + comment)));
                    break;
                case 2:
                    tvContent.setVisibility(View.INVISIBLE);
                    tvContent.setText(null);
                    break;
            }
        }
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class BulletHolder extends RecyclerView.ViewHolder {
        private TextView mContent;

        public BulletHolder(@NonNull View itemView) {
            super(itemView);
            mContent = itemView.findViewById(R.id.tvTextContent);
        }
    }
}
