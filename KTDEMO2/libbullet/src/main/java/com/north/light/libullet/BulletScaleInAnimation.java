package com.north.light.libullet;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.view.View;

/**
 * FileName: BulletBulletScaleInAnimation
 * Author: lzt
 * Date: 2022/9/15 14:38
 */
public class BulletScaleInAnimation {
    private static final float DEFAULT_SCALE_FROM = 0.7f;
    private final float mFrom;

    public BulletScaleInAnimation() {
        this(DEFAULT_SCALE_FROM);
    }

    public BulletScaleInAnimation(float from) {
        mFrom = from;
    }

    public Animator[] getAddAnimators(View view) {
//        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", mFrom, 1f);
//        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", mFrom, 1f);
        ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", 0.2f, 1f);
//        return new ObjectAnimator[]{scaleX, scaleY, alpha};
        return new ObjectAnimator[]{alpha};
    }

    public Animator[] getRemoveAnimators(View view) {
//        ObjectAnimator scaleX = ObjectAnimator.ofFloat(view, "scaleX", mFrom, 1f);
//        ObjectAnimator scaleY = ObjectAnimator.ofFloat(view, "scaleY", mFrom, 1f);
        ObjectAnimator alpha = ObjectAnimator.ofFloat(view, "alpha", 1f, 0.2f);
//        return new ObjectAnimator[]{scaleX, scaleY, alpha};
        return new ObjectAnimator[]{alpha};
    }
}
