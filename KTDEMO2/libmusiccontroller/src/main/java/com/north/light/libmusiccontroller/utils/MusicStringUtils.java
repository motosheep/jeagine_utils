package com.north.light.libmusiccontroller.utils;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * FileName: MusicStringUtils
 * Author: lzt
 * Date: 2022/10/17 11:23
 * 字符串工具栏
 */
public class MusicStringUtils implements Serializable {

    /**
     * 字符串转空
     */
    public static String trainNoNull(String org) {
        if (TextUtils.isEmpty(org)) {
            return "";
        }
        return org;
    }
}
