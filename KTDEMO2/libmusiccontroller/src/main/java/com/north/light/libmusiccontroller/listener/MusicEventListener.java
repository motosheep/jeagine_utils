package com.north.light.libmusiccontroller.listener;

import com.north.light.libmusiccontroller.bean.MusicPlayInfo;

import java.io.Serializable;

/**
 * author:li
 * date:2022/10/16
 * desc:音乐播放状态监听
 */
public interface MusicEventListener extends Serializable {


    public void status(MusicPlayInfo musicInfo);
}
