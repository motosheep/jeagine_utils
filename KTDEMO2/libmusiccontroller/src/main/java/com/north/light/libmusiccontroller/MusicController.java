package com.north.light.libmusiccontroller;

import com.north.light.libmusiccontroller.impl.MusicControllerOldApi;
import com.north.light.libmusiccontroller.impl.MusicStarryOldImpl;

import java.io.Serializable;

/**
 * FileName: MusicController
 * Author: lzt
 * Date: 2022/10/12 16:51
 * 音乐管理类--控制音乐播放操作逻辑
 */
public class MusicController implements Serializable {
    private MusicType mMusicPlayerType = MusicType.TYPE_STARRY;
    private MusicControllerOldApi mPlayerController;

    private static final class SingleHolder implements Serializable {
        static MusicController mInstance = new MusicController();
    }

    private static MusicController getInstance() {
        return SingleHolder.mInstance;
    }

    public MusicController() {
        if (mMusicPlayerType == MusicType.TYPE_STARRY) {
            mPlayerController = new MusicStarryOldImpl();
        } else {
            mPlayerController = new MusicStarryOldImpl();
        }
    }

    public MusicControllerOldApi getPlayerController() {
        return mPlayerController;
    }

    /**
     * 外部调用方法--获取播放对象
     */
    public static MusicControllerOldApi getMusicController() {
        return getInstance().getPlayerController();
    }


}
