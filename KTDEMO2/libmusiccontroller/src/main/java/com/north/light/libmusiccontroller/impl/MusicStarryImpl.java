//package com.north.light.libmusiccontroller.impl;
//
//import android.app.Application;
//import android.content.ComponentName;
//import android.content.ServiceConnection;
//import android.os.Handler;
//import android.os.IBinder;
//import android.os.Looper;
//import android.os.Message;
//import android.text.TextUtils;
//import android.util.Log;
//
//import androidx.annotation.NonNull;
//
//import com.north.light.libmusiccontroller.MusicStatus;
//import com.north.light.libmusiccontroller.bean.MusicPlayInfo;
//import com.north.light.libmusiccontroller.bean.MusicPlayObj;
//import com.north.light.libmusiccontroller.listener.MusicEventListener;
//import com.north.light.libmusiccontroller.listener.MusicProgressListener;
//import com.north.light.libmusiccontroller.utils.MusicStringUtils;
//import com.north.light.libmusicplay.OnPlayerEventListener;
//import com.north.light.libmusicplay.SongInfo;
//import com.north.light.libmusicplay.StarrySky;
//import com.north.light.libmusicplay.manager.PlaybackStage;
//
//import org.jetbrains.annotations.NotNull;
//
//import java.util.concurrent.CopyOnWriteArrayList;
//
///**
// * author:li
// * date:2022/10/16
// * desc:三方sdk impl
// */
//public class MusicStarryImpl implements MusicControllerApi {
//
//    private final CopyOnWriteArrayList<MusicEventListener> mEventListener = new CopyOnWriteArrayList<>();
//    private final CopyOnWriteArrayList<MusicProgressListener> mProgressListener = new CopyOnWriteArrayList<>();
//
//    private boolean mHadInit = false;
//
//    //进度条更新
//    private static final int HANDLER_PROGRESS = 0x0001;
//    private static final long HANDLER_PROGRESS_INTERVAL = 1000;
//
//    private Handler mUIHandler = new Handler(Looper.getMainLooper()) {
//        @Override
//        public void handleMessage(@NonNull Message msg) {
//            super.handleMessage(msg);
//            switch (msg.what) {
//                case HANDLER_PROGRESS:
//                    try {
//                        if (StarrySky.with().isPlaying()) {
//                            //播放器进度回调
//                            long duration = StarrySky.with().getDuration();
//                            long progress = StarrySky.with().getPlayingPosition();
//                            long buffered = StarrySky.with().getBufferedPosition();
//                            String songUrl = StarrySky.with().getNowPlayingSongUrl();
//                            notifyProgress(songUrl, progress, buffered, duration);
//                            Log.d("MusicStarryImpl", "progress:  " + songUrl + " pro: " + progress + " buff: " + buffered + " duration: " + duration);
//                        }
//                    } catch (Exception e) {
//                        e.printStackTrace();
//                    }
//                    mUIHandler.sendEmptyMessageDelayed(HANDLER_PROGRESS, HANDLER_PROGRESS_INTERVAL);
//                    break;
//            }
//        }
//    };
//
//    @Override
//    public void init(Application application) {
//        mUIHandler.removeCallbacksAndMessages(null);
//        //初始化--默认不显示通知栏
//        StarrySky.init(application)
//                .setOpenCache(false)
//                .setAutoManagerFocus(false)
//                .setNotificationSwitch(false)
//                .connServiceListener(new ServiceConnection() {
//                    @Override
//                    public void onServiceConnected(ComponentName name, IBinder service) {
////                        Log.d("MusicStarryImpl", "onServiceConnected ");
//                        initListener();
//                    }
//
//                    @Override
//                    public void onServiceDisconnected(ComponentName name) {
////                        Log.d("MusicStarryImpl", "onServiceDisconnected ");
//                    }
//                })
//                .apply();
//    }
//
//    /**
//     * 歌曲信息转换
//     */
//    private MusicPlayInfo trainSongInfo(PlaybackStage stage) {
//        SongInfo songInfo = stage.getSongInfo();
//        if (songInfo == null) {
//            return null;
//        }
//        long duration = StarrySky.with().getDuration();
//        long progress = StarrySky.with().getPlayingPosition();
//        MusicPlayInfo result = new MusicPlayInfo();
//        result.setDuration(duration);
//        result.setProgress(progress);
//        MusicStatus musicStatus;
//        switch (stage.getStage()) {
//            case PlaybackStage.BUFFERING:
//                musicStatus = MusicStatus.TYPE_BUFFERING;
//                break;
//            case PlaybackStage.ERROR:
//                musicStatus = MusicStatus.TYPE_ERROR;
//                break;
//            case PlaybackStage.PAUSE:
//                musicStatus = MusicStatus.TYPE_PAUSE;
//                break;
//            case PlaybackStage.PLAYING:
//                musicStatus = MusicStatus.TYPE_PLAYING;
//                break;
//            case PlaybackStage.SWITCH:
//                musicStatus = MusicStatus.TYPE_SWITCH;
//                break;
//            default:
//                musicStatus = MusicStatus.TYPE_IDLE;
//                break;
//        }
//        result.setStatus(musicStatus);
//        MusicPlayObj playObj = new MusicPlayObj();
//        playObj.setPlayUrl(songInfo.getSongUrl());
//        playObj.setAuthor(songInfo.getArtist());
//        playObj.setCover(songInfo.getSongCover());
//        playObj.setTitle(songInfo.getSongName());
//        result.setInfo(playObj);
//        return result;
//    }
//
//    private void initListener() {
//        StarrySky.with().addPlayerEventListener(new OnPlayerEventListener() {
//            @Override
//            public void onPlaybackStageChange(@NotNull PlaybackStage stage) {
//                Log.d("MusicStarryImpl", "onPlaybackStageChange: ");
//                MusicPlayInfo result = trainSongInfo(stage);
//                if (result == null) {
//                    return;
//                }
//                //数据回调
//                notifyStatus(result);
//            }
//        }, MusicStarryImpl.class.getSimpleName());
//        //开始进度条回调handler
//        mUIHandler.sendEmptyMessageDelayed(HANDLER_PROGRESS, HANDLER_PROGRESS_INTERVAL);
//        mHadInit = true;
//    }
//
//    @Override
//    public void release() {
//        if (!hadInit()) {
//            return;
//        }
//        StarrySky.with().stopMusic();
//        StarrySky.with().release();
//    }
//
//    @Override
//    public void play(MusicPlayObj music) {
//        if (!hadInit()) {
//            return;
//        }
//        if (!checkPlayObj(music)) {
//            return;
//        }
//        //执行播放逻辑
//        SongInfo starrySongInfo = new SongInfo();
//        //播放Url
//        starrySongInfo.setSongUrl(music.getPlayUrl());
//        starrySongInfo.setSongId(music.getPlayUrl());
//        //封面
//        starrySongInfo.setSongCover(MusicStringUtils.trainNoNull(music.getCover()));
//        //作者
//        starrySongInfo.setArtist(MusicStringUtils.trainNoNull(music.getAuthor()));
//        //标题
//        starrySongInfo.setSongName(MusicStringUtils.trainNoNull(music.getTitle()));
//        StarrySky.with().playMusicByInfo(starrySongInfo);
//    }
//
//    @Override
//    public void pause() {
//        if (!hadInit()) {
//            return;
//        }
//        StarrySky.with().pauseMusic();
//    }
//
//    @Override
//    public void resume() {
//        if (!hadInit()) {
//            return;
//        }
//        StarrySky.with().restoreMusic();
//    }
//
//    @Override
//    public void stop() {
//        if (!hadInit()) {
//            return;
//        }
//        StarrySky.with().stopMusic();
//    }
//
//    @Override
//    public void seekTo(long duration, boolean playWhenPause) {
//        if (!hadInit()) {
//            return;
//        }
//        StarrySky.with().seekTo(duration, playWhenPause);
//    }
//
//    @Override
//    public boolean checkPlayObj(MusicPlayObj musicPlayObj) {
//        if (musicPlayObj == null) {
//            return false;
//        }
//        if (TextUtils.isEmpty(musicPlayObj.getPlayUrl())) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public boolean hadInit() {
//        return mHadInit;
//    }
//
//    @Override
//    public MusicPlayInfo getCurrentInfo() {
//        if (!hadInit()) {
//            return null;
//        }
//        PlaybackStage playbackStage = StarrySky.with().playStage();
//        if (playbackStage == null) {
//            return null;
//        }
//        return trainSongInfo(playbackStage);
//    }
//
//    @Override
//    public boolean isPlaying() {
//        return StarrySky.with().isPlaying();
//    }
//
//    @Override
//    public boolean isBuffering() {
//        return StarrySky.with().isBuffering();
//    }
//
//    @Override
//    public boolean isIdle() {
//        return StarrySky.with().isIdle();
//    }
//
//    @Override
//    public boolean isPause() {
//        return StarrySky.with().isPaused();
//    }
//
//    @Override
//    public long getProgress() {
//        if (!hadInit()) {
//            return 0;
//        }
//        return StarrySky.with().getPlayingPosition();
//    }
//
//    @Override
//    public long getDuration() {
//        if (!hadInit()) {
//            return 0;
//        }
//        return StarrySky.with().getDuration();
//    }
//
//    @Override
//    public void setMusicStatusListener(MusicEventListener musicStatusListener) {
//        if (musicStatusListener == null) {
//            return;
//        }
//        mEventListener.add(musicStatusListener);
//    }
//
//    @Override
//    public void removeMusicStatusListener(MusicEventListener musicStatusListener) {
//        if (musicStatusListener == null) {
//            return;
//        }
//        mEventListener.remove(musicStatusListener);
//    }
//
//    @Override
//    public void setMusicProgressListener(MusicProgressListener progressListener) {
//        if (progressListener == null) {
//            return;
//        }
//        mProgressListener.add(progressListener);
//    }
//
//    @Override
//    public void removeMusicProgressListener(MusicProgressListener progressListener) {
//        if (progressListener == null) {
//            return;
//        }
//        mProgressListener.remove(progressListener);
//    }
//
//    /**
//     * 通知外部状态更新
//     */
//    private void notifyStatus(MusicPlayInfo info) {
//        if (mEventListener.size() == 0 || info == null) {
//            return;
//        }
//        for (MusicEventListener eventListener : mEventListener) {
//            eventListener.status(info);
//        }
//    }
//
//    /**
//     * 通知外部进度条更新
//     */
//    private void notifyProgress(String url, long progress, long buffered, long duration) {
//        if (mProgressListener.size() == 0) {
//            return;
//        }
//        for (MusicProgressListener progressListener : mProgressListener) {
//            progressListener.progressInfo(url, progress, buffered, duration);
//        }
//    }
//}
