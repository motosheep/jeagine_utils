package com.north.light.libmusiccontroller;

/**
 * author:li
 * date:2022/10/16
 * desc:播放状态
 * const val IDLE = "IDLE"            //初始状态，播完完成，停止播放都会回调该状态
 * const val PLAYING = "PLAYING"      //开始播放，播放中
 * const val SWITCH = "SWITCH"        //切歌
 * const val PAUSE = "PAUSE"          //暂停
 * const val BUFFERING = "BUFFERING"  //缓冲
 * const val ERROR = "ERROR"          //出错
 */
public enum MusicStatus {
    TYPE_IDLE(0, "IDLE"),
    TYPE_PLAYING(1, "PLAYING"),
    TYPE_SWITCH(2, "SWITCH"),
    TYPE_PAUSE(3, "PAUSE"),
    TYPE_BUFFERING(4, "BUFFERING"),
    TYPE_ERROR(5, "ERROR");

    private int type;
    private String desc;

    MusicStatus(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
