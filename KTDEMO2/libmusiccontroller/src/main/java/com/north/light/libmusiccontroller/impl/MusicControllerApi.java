//package com.north.light.libmusiccontroller.impl;
//
//import android.app.Application;
//
//import com.north.light.libmusiccontroller.bean.MusicPlayInfo;
//import com.north.light.libmusiccontroller.bean.MusicPlayObj;
//import com.north.light.libmusiccontroller.listener.MusicEventListener;
//import com.north.light.libmusiccontroller.listener.MusicProgressListener;
//
///**
// * author:li
// * date:2022/10/16
// * desc:音乐播放控制api
// */
//public interface MusicControllerApi {
//
//    /**
//     * 初始化
//     */
//    public void init(Application application);
//
//    /**
//     * 释放所有资源
//     */
//    public void release();
//
//    /**
//     * 播放相关
//     */
//    public void play(MusicPlayObj music);
//
//    /**
//     * 暂停播放
//     */
//    public void pause();
//
//    /**
//     * 恢复播放
//     */
//    public void resume();
//
//    /**
//     * 停止播放
//     */
//    public void stop();
//
//    /**
//     * 拖动进度条
//     */
//    public void seekTo(long duration, boolean playWhenPause);
//
//    /**
//     * 检查播放对象
//     */
//    public boolean checkPlayObj(MusicPlayObj musicPlayObj);
//
//    /**
//     * 是否已经初始化
//     */
//    public boolean hadInit();
//
//    /**
//     * 获取当前播放对象
//     */
//    public MusicPlayInfo getCurrentInfo();
//
//    /**
//     * 是否播放中
//     */
//    public boolean isPlaying();
//
//    /**
//     * 是否缓存中
//     */
//    public boolean isBuffering();
//
//    /**
//     * 是否空闲
//     */
//    public boolean isIdle();
//
//    /**
//     * 是否暂停
//     */
//    public boolean isPause();
//
//    /**
//     * 播放进度
//     * */
//    public long getProgress();
//
//    /**
//     * 总时间
//     * */
//    public long getDuration();
//
//    /**
//     * 设置监听
//     */
//    public void setMusicStatusListener(MusicEventListener musicStatusListener);
//
//    public void removeMusicStatusListener(MusicEventListener musicEventListener);
//
//    public void setMusicProgressListener(MusicProgressListener progressListener);
//
//    public void removeMusicProgressListener(MusicProgressListener progressListener);
//
//}
