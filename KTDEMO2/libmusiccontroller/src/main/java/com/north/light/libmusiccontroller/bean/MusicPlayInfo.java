package com.north.light.libmusiccontroller.bean;

import com.north.light.libmusiccontroller.MusicStatus;

import java.io.Serializable;

/**
 * author:li
 * date:2022/10/16
 * desc:音乐播放对象
 */
public class MusicPlayInfo implements Serializable {

    private MusicStatus status;
    private MusicPlayObj info;
    private long progress;
    private long duration;

    public MusicStatus getStatus() {
        return status;
    }

    public void setStatus(MusicStatus status) {
        this.status = status;
    }

    public MusicPlayObj getInfo() {
        return info;
    }

    public void setInfo(MusicPlayObj info) {
        this.info = info;
    }

    public long getProgress() {
        return progress;
    }

    public void setProgress(long progress) {
        this.progress = progress;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }
}
