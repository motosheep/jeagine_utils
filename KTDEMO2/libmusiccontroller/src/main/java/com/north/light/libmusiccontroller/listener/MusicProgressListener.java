package com.north.light.libmusiccontroller.listener;

import java.io.Serializable;

/**
 * author:li
 * date:2022/10/16
 * desc:音乐播放进度监听
 */
public interface MusicProgressListener extends Serializable {
    void progressInfo(String url, long progress, long buffered, long duration);
}
