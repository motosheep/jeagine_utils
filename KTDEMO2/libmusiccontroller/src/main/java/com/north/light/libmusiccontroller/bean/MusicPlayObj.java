package com.north.light.libmusiccontroller.bean;

import java.io.Serializable;

/**
 * author:li
 * date:2022/10/16
 * desc:音乐播放对象--外部传入
 */
public class MusicPlayObj implements Serializable {

    //播放Url--必须填写
    private String playUrl;
    //标题
    private String title;
    //作者
    private String author;
    //封面
    private String cover;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getPlayUrl() {
        return playUrl;
    }

    public void setPlayUrl(String playUrl) {
        this.playUrl = playUrl;
    }
}
