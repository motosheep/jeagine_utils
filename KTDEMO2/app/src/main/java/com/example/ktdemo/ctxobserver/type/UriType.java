package com.example.ktdemo.ctxobserver.type;

import java.io.Serializable;

/**
 * FileName: UriType
 * Author: lzt
 * Date: 2023/4/28 10:51
 */
public enum UriType implements Serializable {
    TYPE_PIC(1, "pic"),
    TYPE_VIDEO(2, "video"),

    ;

    private int type;
    private String desc;

    UriType(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
