package com.example.ktdemo;

import android.content.Context;
import android.text.TextUtils;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * FileName: LimitTextView
 * Author: lzt
 * Date: 2022/12/28 11:17
 */
public class LimitTextView extends androidx.appcompat.widget.AppCompatTextView {
    public LimitTextView(@NonNull Context context) {
        super(context);
    }

    public LimitTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public LimitTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    /**
     * 设置最大字体
     */
    public void setTextWithLimit(String text, int limit) {
        if (TextUtils.isEmpty(text)) {
            setText("");
            return;
        }
        if (text.length() <= limit) {
            setText(text);
            return;
        }
        String trainText = text.substring(0, limit) + "...";
        setText(trainText);
    }
}
