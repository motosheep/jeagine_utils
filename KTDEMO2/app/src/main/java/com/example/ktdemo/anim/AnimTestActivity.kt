package com.example.ktdemo.anim

import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.example.ktdemo.R

class AnimTestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_anim_test)

        val rippleView = findViewById<AnimRippleView>(R.id.rippleView)
        rippleView.start()

        Handler().postDelayed(Runnable { rippleView.stop() },5000)
        Handler().postDelayed(Runnable { rippleView.start() },7000)
    }
}