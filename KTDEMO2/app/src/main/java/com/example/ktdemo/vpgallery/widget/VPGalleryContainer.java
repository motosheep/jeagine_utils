package com.example.ktdemo.vpgallery.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import androidx.annotation.NonNull;
import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.example.ktdemo.vpgallery.VPGalleryMain;
import com.example.ktdemo.vpgallery.bean.VPGalleryInfo;
import com.example.ktdemo.vpgallery.cache.VPGalleryCacheImgUtils;
import com.example.ktdemo.vpgallery.cache.VPGalleryCacheListener;
import com.example.ktdemo.vpgallery.widget.impl.VPGalleryView;

import java.util.ArrayList;
import java.util.Map;

/**
 * FileName: VPGalleryContainer
 * Author: Administrator
 * Date: 2023/6/8 14:55
 * Description:
 */
public class VPGalleryContainer extends RelativeLayout {

    private VPGalleryView mViewPager;

    private ImageView mIvBlurBuildView;

    //是否已经初始化模糊view
    private boolean hadInitBlurView = false;

    private VPOverlayPageAdapter mAdapter;

    //普通数据
    private ArrayList<VPGalleryInfo> normalList = new ArrayList<>();

    public VPGalleryContainer(Context context) {
        super(context);
        init();
    }

    public VPGalleryContainer(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public VPGalleryContainer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mViewPager = new VPGalleryView(getContext());
        addView(mViewPager);

        mIvBlurBuildView = new ImageView(getContext());
        addView(mIvBlurBuildView);
        mIvBlurBuildView.setVisibility(View.INVISIBLE);
        mIvBlurBuildView.setScaleType(ImageView.ScaleType.CENTER_CROP);

        ViewGroup.LayoutParams vpParams = mViewPager.getLayoutParams();
        mViewPager.post(new Runnable() {
            @Override
            public void run() {
                vpParams.width = getMeasuredWidth();
                vpParams.height = getMeasuredHeight();
                mViewPager.setLayoutParams(vpParams);
            }
        });
    }


    /**
     * 更细毛玻璃大小尺寸后初始化数据
     */
    private void initBlurView(int width, int height) {
        if (mIvBlurBuildView == null) {
            return;
        }
        ViewGroup.LayoutParams vpParams = mIvBlurBuildView.getLayoutParams();
        vpParams.width = width;
        vpParams.height = height;
        mIvBlurBuildView.setLayoutParams(vpParams);
        //初始化数据
        VPGalleryCacheImgUtils.getInstance().startTask(getContext(), getAdapter().getList(), mIvBlurBuildView, new VPGalleryCacheListener() {
            @Override
            public void finish(Map<String, Bitmap> source, Map<String, Bitmap> bulletBit) {
                getViewPager().setAdapter(mAdapter);
            }

            @Override
            public void empty() {
                getViewPager().setAdapter(null);
            }

            @Override
            public Bitmap getBitmap(String url) throws Exception {
                return VPGalleryMain.getUrlBitmap(getContext(), url);
            }
        });
    }


    //外部调用---------------------------------------------------------------------------------


    /**
     * 设置adapter
     */
    public void setData(@NonNull ArrayList<VPGalleryInfo> galleryList) {
        normalList.clear();
        VPOverlayPageAdapter pageAdapter = new VPOverlayPageAdapter(getContext());
        normalList.addAll(galleryList);
        //获取item第一个控件，并且初始化模糊图片的view宽高
        if (mViewPager == null || hadInitBlurView) {
            return;
        }
        if (normalList.size() == 0) {
            getViewPager().setAdapter(null);
            return;
        }
        setAdapter(pageAdapter);
        getAdapter().setInitListener(new VPOverlayPageAdapter.InitListener() {
            @Override
            public void initFinish() {
                if (hadInitBlurView) {
                    return;
                }
                hadInitBlurView = true;
                try {
                    //获取宽高
                    View ivView = mViewPager.getChildAt(0).findViewById(R.id.ivCover);
                    if (ivView != null) {
                        int meaWidth = ivView.getMeasuredWidth();
                        int meaHeight = ivView.getMeasuredHeight();
                        LogUtil.d("setAdapter w: " + meaWidth + " h: " + meaHeight);
                        initBlurView(meaWidth, meaHeight);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
        getAdapter().setDataAndBindViewPager(getViewPager(), normalList);
        getViewPager().setAdapter(getAdapter());
    }

    private void setAdapter(VPOverlayPageAdapter mAdapter) {
        this.mAdapter = mAdapter;
    }

    public VPOverlayPageAdapter getAdapter() {
        return mAdapter;
    }

    /**
     * 获取vp
     */
    public VPGalleryView getViewPager() {
        return mViewPager;
    }

}
