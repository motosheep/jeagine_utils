package com.example.ktdemo.floatmenu;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;

import com.example.ktdemo.R;


/**
 * FileName: DoExamFloatMenu
 * Author: lzt
 * Date: 2022/7/26 17:43
 * 做题悬浮按钮--套题做题
 */
public class DoExamFloatMenu extends LinearLayout {

    private ImageView ivArrow;
    private RelativeLayout llContent;
    private LinearLayout llRoot;

    private LinearLayout llCommit, llAnalyse, llAnswerCard;
    /**
     * 当前开关状态--默认打开
     */
    private boolean mShowStatus = true;

    private SelectEventListener mListener;

    /**
     * 是否查看过解析
     */
    private boolean mSeeAnalyse;
    private ImageView ivAnalyse;

    //开关动画相关--------------------------------------------------------------------
    private boolean mInitAnim = false;
    //内容宽度
    private int mContentWidth = 0;
    //是否正在动画中
    private boolean mIsAnimDoing = false;


    public DoExamFloatMenu(Context context) {
        super(context);
        init();
    }

    public DoExamFloatMenu(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DoExamFloatMenu(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * 初始化
     */
    private void init() {
        setGravity(Gravity.CENTER);
        setOrientation(LinearLayout.VERTICAL);
        View rootView = LayoutInflater.from(getContext().getApplicationContext())
                .inflate(R.layout.view_do_exam_float_menu_layout, null);
        addView(rootView);

        ivArrow = rootView.findViewById(R.id.ivArrow);
        llContent = rootView.findViewById(R.id.llContent);


        llCommit = rootView.findViewById(R.id.llAnswerCommit);
        llAnalyse = rootView.findViewById(R.id.llAnalyse);
        llAnswerCard = rootView.findViewById(R.id.llAnswerCard);

        ivAnalyse = rootView.findViewById(R.id.ivAnalyse);
        llRoot = rootView.findViewById(R.id.llRoot);


        //初始开关状态
        mShowStatus = (llContent.getVisibility() == View.VISIBLE);


        //监听事件---------------------------------------------------------
        ivArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //开关功能
                toggle();
            }
        });
        llCommit.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.commit();
                }
            }
        });
        llAnalyse.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.analyse();
                }
                if (mSeeAnalyse) {
                    ivAnalyse.setImageResource(R.drawable.ic_do_exam_float_menu_analyse_open);
                } else {
                    ivAnalyse.setImageResource(R.drawable.ic_do_exam_float_menu_analyse_close);
                }
                mSeeAnalyse = !mSeeAnalyse;
            }
        });

        llAnswerCard.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.card();
                }
            }
        });

        //界面初始化完成时候会调用runnable
        post(new Runnable() {
            @Override
            public void run() {
                mInitAnim = true;
                mContentWidth = llContent.getMeasuredWidth();
            }
        });
    }

    /**
     * 更新开关状态
     */
    private void updateShowStatus(boolean show) {
        if (llContent == null) {
            return;
        }
        //content动画
        if (!mInitAnim) {
            llContent.setVisibility((show) ? View.VISIBLE : View.GONE);
            mShowStatus = (llContent.getVisibility() == View.VISIBLE);
            ivArrow.setImageResource((mShowStatus) ? R.drawable.ic_do_exam_float_menu_arrow_close :
                    R.drawable.ic_do_exam_float_menu_arrow_open);
        } else {
            if (mIsAnimDoing) {
                return;
            }
            mIsAnimDoing = true;
            if (show) {
                llRoot.animate().translationX(0).setDuration(300).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        mShowStatus = true;
                        ivArrow.setImageResource(R.drawable.ic_do_exam_float_menu_arrow_close);
                        mIsAnimDoing = false;
                    }
                }).start();
            } else {
                llRoot.animate().translationX(mContentWidth).setDuration(300).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        mShowStatus = false;
                        ivArrow.setImageResource(R.drawable.ic_do_exam_float_menu_arrow_open);
                        mIsAnimDoing = false;
                    }
                }).start();
            }
        }

    }

    //外部调用-------------------------------------------------------------------------------------

    /**
     * 获取开关状态
     */
    public boolean getShowStatus() {
        return mShowStatus;
    }


    /**
     * 打开
     */
    public void show() {
        if (llContent == null) {
            return;
        }
        updateShowStatus(true);
    }

    /**
     * 关闭
     */
    public void hide() {
        if (llContent == null) {
            return;
        }
        updateShowStatus(false);
    }

    /**
     * 开关切换
     */
    public void toggle() {
        if (getShowStatus()) {
            hide();
        } else {
            show();
        }
    }

    //监听----------------------------------------------------------------------


    public interface SelectEventListener {
        void commit();

        void analyse();

        void card();
    }

    public void setSelectEventListener(SelectEventListener listener) {
        this.mListener = listener;
    }

}
