package com.example.ktdemo.scrollview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ktdemo.LogUtil;

/**
 * FileName: ReboundEditText
 * Author: lzt
 * Date: 2023/3/10 08:24
 * 回弹edittext
 */
public class ReboundEditText extends androidx.appcompat.widget.AppCompatEditText {
    private float mDownViewX, mDownViewY;

    private float mLastMoveX, mLastMoveY;

    public ReboundEditText(@NonNull Context context) {
        super(context);
    }

    public ReboundEditText(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public ReboundEditText(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public boolean canScrollToTop(){
        return canScrollVertically(-1);
    }

    public boolean canScrollToBottom(){
        return canScrollVertically(1);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        boolean canScrollToTop = canScrollVertically(-1);
        boolean canScrollToBottom = canScrollVertically(1);
        LogUtil.d("canScrollToTop: " + canScrollToTop + " --canScrollToBottom: " + canScrollToBottom);
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mDownViewX = event.getX();
                mDownViewY = event.getY();
                mLastMoveX = event.getX();
                mLastMoveY = event.getY();
                LogUtil.d("ACTION_DOWN mDownViewX: " + mDownViewX + " --mDownViewY: " + mDownViewY);
                break;
            case MotionEvent.ACTION_MOVE:
                LogUtil.d("ACTION_MOVE mDownViewX: " + mLastMoveX + " --mDownViewY: " + mLastMoveY);
                //判断滑动方向
                if (event.getY() < mLastMoveY) {
                    //上拉
                    if (!canScrollToBottom) {
                        float interval = event.getY() - mDownViewY;
                        LogUtil.d("ACTION_MOVE 1 interval: " + interval);
                    }
                } else if (event.getY() > mLastMoveY) {
                    //下拉
                    if (!canScrollToTop) {
                        float interval = event.getY() - mDownViewY;
                        LogUtil.d("ACTION_MOVE 2 interval: " + interval);
                    }
                }
                mLastMoveX = event.getX();
                mLastMoveY = event.getY();
                break;
            case MotionEvent.ACTION_CANCEL:
            case MotionEvent.ACTION_UP:
                //恢复位置
                resetPos();
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 恢复到原来的位置
     */
    private void resetPos() {
//        setTranslationY(0);
    }
}
