package com.example.ktdemo.picbanner.banner;

import android.graphics.Bitmap;

/**
 * FileName: BannerPicSizeCallback
 * Author: lzt
 * Date: 2022/10/17 11:59
 * 图片尺寸回调
 */
public interface BannerPicSizeCallback {

    public void size(String url, int width, int height, Bitmap bitmap);
}
