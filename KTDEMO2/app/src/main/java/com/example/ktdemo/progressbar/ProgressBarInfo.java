package com.example.ktdemo.progressbar;

import android.graphics.Color;

import java.io.Serializable;

/**
 * FileName: ProgressBarInfo
 * Author: lzt
 * Date: 2023/3/16 19:17
 */
public class ProgressBarInfo implements Serializable {

    private int progressColor = Color.BLUE;
    private int progress = 0;

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }

    public int getProgressColor() {
        return progressColor;
    }

    public void setProgressColor(int progressColor) {
        this.progressColor = progressColor;
    }
}
