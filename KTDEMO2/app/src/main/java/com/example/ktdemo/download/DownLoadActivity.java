package com.example.ktdemo.download;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.north.light.libdownload.DownloadListener;
import com.north.light.libdownload.DownloadUtils;
import com.north.light.libdownload.utils.DownloadSaveUtils;

/**
 * 下载
 */
public class DownLoadActivity extends AppCompatActivity {
    //    private String urlPath = "https://v.jeagine.com/sv/2f6ac480-184426d80de/2f6ac480-184426d80de.mp4";
//    private String urlPath = "https://v.jeagine.com/s
//    v/311051a6-1843775dabb/311051a6-1843775dabb.mp4";
//    private String urlPath = "https://ucan.25pp.com/PPAssistant_pp_103.apk";
//    private String urlPath = "https://v.jeagine.com/sv/2f6ac480-184426d80de/2f6ac480-184426d80de.mp4";
//    private String urlPath = "http://jeagine-oss-test.oss-cn-shenzhen.aliyuncs.com/voteImg/e74e819edaa5458988a9750b3d12a166.jpg";
//    private String urlPath = "http://jeagine-oss-test.oss-cn-shenzhen.aliyuncs.com/voteImg/52a4d9c45fa54d48b508c8aea0c00ad3.jpg";
    private String urlPath = "https://v.jeagine.com/sv/59e7cf6d-18413145ca7/59e7cf6d-18413145ca7.mp3";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_down_load);
        String storePath = getExternalCacheDir().getAbsolutePath() + "/download/";
        new Thread(new Runnable() {
            @Override
            public void run() {
                DownloadUtils.getInstance().startDownLoad(false, DownLoadActivity.this,
                        storePath, urlPath, new DownloadListener() {
                            @Override
                            public void data(String link, long total, long current, float percent, boolean isFinish, String localPath, String fileName) {
                                if (isFinish) {
                                    LogUtil.d("download finish");
                                    //插入图库
//                                    boolean insertPic = DownloadSaveUtils.saveVideoToAlbum(DownLoadActivity.this, localPath);
//                                    if(insertPic){
//                                        DownloadSaveUtils.deleteFile(localPath);
//                                    }
//                                    LogUtil.d("download insertPic result: " + insertPic);
                                    boolean saveResult = DownloadSaveUtils.saveImgFileToAlbum(DownLoadActivity.this, localPath);
                                    LogUtil.d("download finish: saveResult " + saveResult);
                                }
                            }

                            @Override
                            public void error(String message) {
                                LogUtil.d("download error: " + message);
                            }

                            @Override
                            public void setProgress(String key, long pro) {
                                MMKVManager.getInstance().put(DownLoadActivity.this, key, pro);
                            }

                            @Override
                            public Long getProgress(String key) {
                                return MMKVManager.getInstance().getLong(DownLoadActivity.this, key);
                            }

                            @Override
                            public void noPermission() {
                                LogUtil.d("download noPermission");
                            }

                            @Override
                            public boolean checkNeedToDownload(String url, String localPath, String fileName) {
                                //检查文件是否需要下载
//                                boolean checkResult = !DownloadUtils.getInstance().checkVideoExistGallery(DownLoadActivity.this, fileName);
//                                LogUtil.d("download checkNeedToDownload localPath: " + localPath + " fileName: " + fileName);
//                                if (!checkResult) {
//                                    //不需要下载
//                                    LogUtil.d("download checkNeedToDownload 不需要下载");
//                                }
//                                return checkResult;
                                return true;
                            }
                        });
            }
        }).start();
    }

    @Override
    protected void onDestroy() {
        DownloadUtils.getInstance().stop(urlPath);
        super.onDestroy();
    }
}