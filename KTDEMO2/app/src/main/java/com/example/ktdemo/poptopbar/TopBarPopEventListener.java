package com.example.ktdemo.poptopbar;

import android.view.View;

/**
 * FileName: TopBarPopEventListener
 * Author: lzt
 * Date: 2022/10/24 11:12
 */
public interface TopBarPopEventListener {


    void init(View view);

    void dismiss();

}
