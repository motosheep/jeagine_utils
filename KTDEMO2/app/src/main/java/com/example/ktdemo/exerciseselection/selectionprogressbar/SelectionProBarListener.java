package com.example.ktdemo.exerciseselection.selectionprogressbar;

import android.view.View;

/**
 * author:li
 * date:2022/9/30
 * desc:进度条监听
 */
public interface SelectionProBarListener {

    //进度回调
    void progressChange(int progress, int max);

    //点击事件回调
    void click(View v, float x, float y);
}
