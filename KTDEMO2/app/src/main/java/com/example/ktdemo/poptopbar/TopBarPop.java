package com.example.ktdemo.poptopbar;

import android.content.Context;
import android.content.res.Resources;
import android.os.Handler;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.PopupWindow;

import com.example.ktdemo.R;

/**
 * FileName: TopBarPop
 * Author: lzt
 * Date: 2022/10/24 10:43
 * 顶部弹窗类
 */
public class TopBarPop {

    private TopBarPopEventListener mEventListener;
    private Handler mUIHandler = new Handler(Looper.getMainLooper());
    private static final long UI_DURATION = 5000;
    private PopupWindow mPopupWindow;

    public void show(Context context,
                     View showLocView,
                     int layoutId,
                     int marginStart,
                     int marginEnd,
                     int marginTop) {
        View contentView = LayoutInflater.from(context).inflate(layoutId, null);
        show(context, showLocView, contentView, marginStart, marginEnd, marginTop, UI_DURATION);
    }


    /**
     * 显示pop window
     *
     * @param showLocView 显示的父类view
     * @param showView    需要显示出屏幕上面的view
     * @param duration    显示时长，-1为长期显示
     */
    public void show(Context context,
                     View showLocView,
                     View showView,
                     int marginStart,
                     int marginEnd,
                     int marginTop,
                     long duration) {
        dismiss();
        mPopupWindow = null;
        DisplayMetrics dm = context.getResources().getDisplayMetrics();
        mPopupWindow = new PopupWindow(
                showView,
                dm.widthPixels - (dp2px(marginStart) + dp2px(marginEnd)),
                ViewGroup.LayoutParams.WRAP_CONTENT);
        notifyInit(showView);
        mPopupWindow.setOutsideTouchable(false);
        mPopupWindow.setTouchable(true);
        mPopupWindow.setBackgroundDrawable(null);
        mPopupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        mPopupWindow.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        mPopupWindow.setAnimationStyle(R.style.anim_top_bar_pop);  //自定义动画
        mPopupWindow.showAtLocation(showLocView, Gravity.TOP | Gravity.CENTER_HORIZONTAL,
                0, dp2px(marginTop)); //指定顶部位置
        mPopupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                notifyDismiss();
            }
        });
        if (duration != -1 && mUIHandler != null) {
            mUIHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dismiss();
                }
            }, duration);
        }
    }

    public void dismiss() {
        if (mPopupWindow != null && mPopupWindow.isShowing()) {
            mPopupWindow.dismiss();
        }
    }

    public void release() {
        if (mUIHandler != null) {
            mUIHandler.removeCallbacksAndMessages(null);
        }
        removeTopBarPopEventListener();
        mPopupWindow = null;
    }

    public static int dp2px(final float dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    //监听事件-------------------------------------------------------------------------------

    private void notifyDismiss() {
        if (this.mEventListener != null) {
            this.mEventListener.dismiss();
        }
    }

    private void notifyInit(View view) {
        if (this.mEventListener != null) {
            this.mEventListener.init(view);
        }
    }

    public void setTopBarPopEventListener(TopBarPopEventListener listener) {
        this.mEventListener = listener;
    }

    public void removeTopBarPopEventListener() {
        this.mEventListener = null;
    }
}
