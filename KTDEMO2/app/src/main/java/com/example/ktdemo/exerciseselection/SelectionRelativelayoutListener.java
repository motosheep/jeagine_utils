package com.example.ktdemo.exerciseselection;

/**
 * FileName: SelectionRelativelayoutListener
 * Author: lzt
 * Date: 2022/10/9 17:23
 * 监听
 */
public interface SelectionRelativelayoutListener {

    /**
     * 开始动画
     *
     * @param adapterPosition*/
    public void startDropPicAnim(int adapterPosition);

    /**
     * 完成动画
     *
     * @param adapterPosition*/
    public void endDropPicAnim(int adapterPosition);
}
