package com.example.ktdemo.ctxobserver.bean;

import android.net.Uri;

import com.example.ktdemo.ctxobserver.type.UriType;

import java.io.Serializable;

/**
 * FileName: ObserverChangeInfo
 * Author: lzt
 * Date: 2023/4/28 10:56
 */
public class ObserverChangeInfo implements Serializable {

    //类型
    private UriType type;
    //是否自身改变
    private boolean selfChange;
    //uri
    private Uri uri;

    public ObserverChangeInfo(UriType type, boolean selfChange, Uri uri) {
        this.type = type;
        this.selfChange = selfChange;
        this.uri = uri;
    }

    public UriType getType() {
        return type;
    }

    public void setType(UriType type) {
        this.type = type;
    }

    public boolean isSelfChange() {
        return selfChange;
    }

    public void setSelfChange(boolean selfChange) {
        this.selfChange = selfChange;
    }

    public Uri getUri() {
        return uri;
    }

    public void setUri(Uri uri) {
        this.uri = uri;
    }


    @Override
    public String toString() {
        return "ObserverChangeInfo{" +
                "type=" + type +
                ", selfChange=" + selfChange +
                ", uri=" + uri +
                '}';
    }
}
