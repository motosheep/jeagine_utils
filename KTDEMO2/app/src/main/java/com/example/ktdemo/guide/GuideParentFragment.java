package com.example.ktdemo.guide;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.ktdemo.R;
import com.example.ktdemo.guide.base.GuideBaseFragment;
import com.example.ktdemo.guide.impl.GuideAFragment;
import com.example.ktdemo.guide.impl.GuideBFragment;
import com.example.ktdemo.guide.impl.GuideCFragment;
import com.example.ktdemo.guide.impl.GuideDFragment;

/**
 * FileName: GuilaParentFragment
 * Author: lzt
 * Date: 2023/6/30 14:38
 * Description:
 */
public class GuideParentFragment extends GuideBaseFragment {

    private ViewPager vpContent;

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_image_guide_parent;
    }

    @Override
    protected void initViews(View root) {
        vpContent = root.findViewById(R.id.vpContent);
        vpContent.setOffscreenPageLimit(4);
        vpContent.setAdapter(new FragmentStatePagerAdapter(getChildFragmentManager()) {
            @NonNull
            @Override
            public Fragment getItem(int position) {
                if (position == 0) {
                    return GuideAFragment.newInstance(R.mipmap.ic_guilde_1);
                } else if (position == 1) {
                    return GuideBFragment.newInstance(R.mipmap.ic_guilde_2);
                } else if (position == 2) {
                    return GuideCFragment.newInstance(R.mipmap.ic_guilde_3);
                } else {
                    return GuideDFragment.newInstance(R.mipmap.ic_guilde_4);
                }
            }

            @Override
            public int getCount() {
                return 4;
            }
        });
    }

    /**
     * 取消
     */
    public void dismiss() {
        getChildFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
    }

    /**
     * 下一个
     */
    public void next() {
        vpContent.setCurrentItem(vpContent.getCurrentItem() + 1);
    }

    /**
     * 完成
     */
    public void finish() {
        getChildFragmentManager().beginTransaction().remove(this).commitAllowingStateLoss();
    }
}
