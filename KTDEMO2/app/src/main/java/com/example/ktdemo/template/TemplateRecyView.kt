package com.example.ktdemo.template

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import com.example.ktdemo.LogUtil

/**
 * FileName: TemplateRecyView
 * Author: lzt
 * Date: 2023/7/24 16:12
 * Description:
 */
class TemplateRecyView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : RecyclerView(context, attrs) {


    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        super.onMeasure(widthSpec, heightSpec)
        val widthSize = getMeasuredWidth()
        val heightSize = getMeasuredHeight()
        LogUtil.d("test ddd: ${widthSize} --- ${heightSize}")
    }
}