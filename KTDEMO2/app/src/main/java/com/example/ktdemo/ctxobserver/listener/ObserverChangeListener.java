package com.example.ktdemo.ctxobserver.listener;

import androidx.annotation.NonNull;

import com.example.ktdemo.ctxobserver.bean.ObserverChangeInfo;

/**
 * FileName: ObserverChangeListener
 * Author: lzt
 * Date: 2023/4/28 10:53
 * 变动通知监听
 */
public interface ObserverChangeListener {

    void change(@NonNull ObserverChangeInfo info);
}
