package com.example.ktdemo.vpgallery.bean;

import java.io.Serializable;

/**
 * FileName: VPGalleryInfo
 * Author: Administrator
 * Date: 2023/6/8 15:20
 * Description:
 */
public class VPGalleryInfo implements Serializable {
    private String cover;

    public VPGalleryInfo(String cover) {
        this.cover = cover;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }
}
