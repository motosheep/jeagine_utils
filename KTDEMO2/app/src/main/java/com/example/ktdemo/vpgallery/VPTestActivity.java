package com.example.ktdemo.vpgallery;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.example.ktdemo.R;
import com.example.ktdemo.vpgallery.bean.VPGalleryInfo;
import com.example.ktdemo.vpgallery.widget.VPGalleryContainer;

import java.util.ArrayList;

public class VPTestActivity extends AppCompatActivity {

    private VPGalleryContainer viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vptest);

        viewPager = findViewById(R.id.vpContent);

        ArrayList<VPGalleryInfo> testList = new ArrayList<VPGalleryInfo>();
        for (int i = 0; i < 9; i++) {
            int rest = i % 4;
            if (rest == 0) {
                testList.add(new VPGalleryInfo("https://img2.baidu.com/it/u=2902535988,3737440102&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=550"));
            } else if (rest == 1) {
                testList.add(new VPGalleryInfo("https://c-ssl.dtstatic.com/uploads/item/202005/02/20200502185802_FuFU2.thumb.1000_0.jpeg"));
            } else if (rest == 2) {
                testList.add(new VPGalleryInfo("https://img2.baidu.com/it/u=1825179598,4172885953&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500"));
            } else {
                testList.add(new VPGalleryInfo("https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fc-ssl.duitang.com%2Fuploads%2Fitem%2F201712%2F18%2F20171218085616_FKGar.thumb.1000_0.jpeg&refer=http%3A%2F%2Fc-ssl.duitang.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=auto?sec=1688800969&t=0d7830d377878fe835b6f7797a37d1ea"));
            }
        }
        viewPager.setData(testList);


    }
}