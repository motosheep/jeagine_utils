package com.example.ktdemo.exerciseselection.text;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * FileName: SelectionAnimHotView
 * Author: lzt
 * Date: 2022/10/27 15:35
 * 热度动画控件
 */
class SelectionAnimHotBaseView extends View {
    private final ConcurrentHashMap<String, ValueAnimator> animMap = new ConcurrentHashMap<>();

    public SelectionAnimHotBaseView(Context context) {
        super(context);
    }

    public SelectionAnimHotBaseView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectionAnimHotBaseView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    protected void stopAnimAndRemoveCallbacks() {
        for (Map.Entry<String, ValueAnimator> entry : animMap.entrySet()) {
            entry.getValue().cancel();
        }
        animMap.clear();
    }

    protected void startValueAnimator(String tag, ValueAnimator valueAnimator) {
        ValueAnimator oldValueAnimator = animMap.get(tag);
        if (oldValueAnimator != null) {
            oldValueAnimator.cancel();
            animMap.remove(tag);
        }
        animMap.put(tag, valueAnimator);
        valueAnimator.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        try {
            stopAnimAndRemoveCallbacks();
        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onDetachedFromWindow();
    }

    protected ValueAnimator getValueAnimator(String tag) {
        return animMap.get(tag);
    }

    protected int dp2px(int dpValue) {
        return (int) getContext().getResources().getDisplayMetrics().density * dpValue;
    }
}
