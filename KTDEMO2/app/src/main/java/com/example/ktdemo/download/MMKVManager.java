package com.example.ktdemo.download;

import android.content.Context;
import android.text.TextUtils;

import com.tencent.mmkv.MMKV;

import java.io.Serializable;

/**
 * FileName: MMKVManager
 * Author: lzt
 * Date: 2022/4/25 8:44
 */
public class MMKVManager implements Serializable {
    private static final String TAG = MMKVManager.class.getSimpleName();
    private MMKV mMMKVObj;
    //是否初始化标识
    private boolean mIsInit = false;
    //数据库名字
    private static final String DB_NAME = "jeaginemmkv";

    private static final class SingleHolder {
        public static MMKVManager mInstance = new MMKVManager();
    }

    public static MMKVManager getInstance() {
        return SingleHolder.mInstance;
    }

    /**
     * 初始化--只需要调用一次
     */
    private void init(String dbName, Context context) {
        if (mIsInit) {
            return;
        }
        try {
            MMKV.initialize(context.getApplicationContext());
            //多进程
            mMMKVObj = MMKV.mmkvWithID(dbName, MMKV.MULTI_PROCESS_MODE);
            mIsInit = true;
        } catch (Exception e) {
            mIsInit = false;
        }
    }

    /**
     * 放入数据
     */
    public synchronized <T> boolean put(Context context, String key, T t) {
        if (TextUtils.isEmpty(key)) return false;
        try {
            init(DB_NAME, context);
            if (t instanceof Integer) {
                mMMKVObj.encode(key, (Integer) t);
            } else if (t instanceof Long) {
                mMMKVObj.encode(key, (Long) t);
            } else if (t instanceof Boolean) {
                mMMKVObj.encode(key, (Boolean) t);
            } else if (t instanceof Float) {
                mMMKVObj.encode(key, (Float) t);
            } else if (t instanceof String) {
                mMMKVObj.encode(key, (String) t);
            } else {
                return false;
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * 删除数据
     */
    public synchronized boolean clear(Context context, String key) {
        if (TextUtils.isEmpty(key)) return false;
        try {
            init(DB_NAME, context);
            mMMKVObj.removeValueForKey(key);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public synchronized String getString(Context context, String key) {
        if (TextUtils.isEmpty(key)) return null;
        try {
            init(DB_NAME, context);
            return mMMKVObj.decodeString(key, "");
        } catch (Exception e) {
            return "";
        }
    }

    public synchronized int getInt(Context context, String key) {
        if (TextUtils.isEmpty(key)) return -1;
        try {
            init(DB_NAME, context);
            return mMMKVObj.decodeInt(key, -1);
        } catch (Exception e) {
            return -1;
        }
    }

    public synchronized long getLong(Context context, String key) {
        if (TextUtils.isEmpty(key)) return -1L;
        try {
            init(DB_NAME, context);
            return mMMKVObj.decodeLong(key, -1L);
        } catch (Exception e) {
            return -1L;
        }
    }


    public synchronized float getFloat(Context context, String key) {
        if (TextUtils.isEmpty(key)) return -1f;
        try {
            init(DB_NAME, context);
            return mMMKVObj.decodeFloat(key, -1f);
        } catch (Exception e) {
            return -1f;
        }
    }

    public synchronized boolean getBoolean(Context context, String key) {
        if (TextUtils.isEmpty(key)) return false;
        try {
            init(DB_NAME, context);
            return mMMKVObj.getBoolean(key, false);
        } catch (Exception e) {
            return false;
        }
    }
}
