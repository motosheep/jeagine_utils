package com.example.ktdemo.staggle.frist;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: StaggleAdapter
 * Author: lzt
 * Date: 2023/5/8 15:39
 */
public class StaggleAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public final static int TYPE_FIRST_ITEM = 0x0001;
    public final static int TYPE_SECOND_ITEM = 0x0002;
    public final static int TYPE_OTHER_ITEM = 0x0003;

    private List<StaggleBean> mData = new ArrayList<>();

    @Override
    public int getItemViewType(int position) {
        return mData.get(position).getType();
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_FIRST_ITEM:
                View rootView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staggle_first_child,
                        parent, false);
                return new StaggleFirstHolder(rootView);
            case TYPE_SECOND_ITEM:
                View rootView2 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staggle_second_child,
                        parent, false);
                return new StaggleSecondHolder(rootView2);
            default:
                View rootView3 = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_staggle_normal_child,
                        parent, false);
                return new StaggleDefaultHolder(rootView3);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof StaggleDefaultHolder) {
            StaggleBean index = mData.get(position);
            ConstraintLayout clRoot = ((StaggleDefaultHolder) holder).clRoot;
            if (position == 0) {
                ViewGroup.LayoutParams rootParams = clRoot.getLayoutParams();
                rootParams.height = 600;
                clRoot.setLayoutParams(rootParams);
            } else {
                ViewGroup.LayoutParams rootParams = clRoot.getLayoutParams();
                rootParams.height = 250;
                clRoot.setLayoutParams(rootParams);
            }
            ((StaggleDefaultHolder) holder).tvTitle.setText(index.getTitle());
        }
    }


    public void setmData(List<StaggleBean> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    class StaggleFirstHolder extends RecyclerView.ViewHolder {

        public StaggleFirstHolder(@NonNull View itemView) {
            super(itemView);
        }
    }


    class StaggleSecondHolder extends RecyclerView.ViewHolder {

        public StaggleSecondHolder(@NonNull View itemView) {
            super(itemView);
        }
    }


    class StaggleDefaultHolder extends RecyclerView.ViewHolder {
        private ConstraintLayout clRoot;
        private TextView tvTitle;

        public StaggleDefaultHolder(@NonNull View itemView) {
            super(itemView);
            clRoot = itemView.findViewById(R.id.clRoot);
            tvTitle = itemView.findViewById(R.id.tvTitle);
        }
    }


}
