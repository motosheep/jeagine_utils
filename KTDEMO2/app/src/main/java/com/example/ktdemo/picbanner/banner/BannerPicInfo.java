package com.example.ktdemo.picbanner.banner;

import java.io.Serializable;

/**
 * FileName: BannerPicInfo
 * Author: lzt
 * Date: 2022/10/17 11:49
 * 图片信息
 */
public class BannerPicInfo implements Serializable {

    //图片url
    private String picUrl;

    //横屏图片起始距离占比
    private float horTopRate = 0.12f;

    //横屏图片宽高比
    private float horWHRate = (4 * 1.0f / 3 * 1.0f);

    //竖屏图片顶部占比
    private float verTopRate = 0f;

    //竖屏图片底部占比
    private float verBottomRate = 1f;

    public float getVerTopRate() {
        return verTopRate;
    }

    public void setVerTopRate(float verTopRate) {
        this.verTopRate = verTopRate;
    }

    public float getVerBottomRate() {
        return verBottomRate;
    }

    public void setVerBottomRate(float verBottomRate) {
        this.verBottomRate = verBottomRate;
    }

    public String getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(String picUrl) {
        this.picUrl = picUrl;
    }


    public float getHorTopRate() {
        return horTopRate;
    }

    public void setHorTopRate(float horTopRate) {
        this.horTopRate = horTopRate;
    }

    public float getHorWHRate() {
        return horWHRate;
    }

    public void setHorWHRate(float horWHRate) {
        this.horWHRate = horWHRate;
    }
}
