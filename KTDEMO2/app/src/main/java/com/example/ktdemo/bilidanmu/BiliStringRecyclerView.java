package com.example.ktdemo.bilidanmu;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;
import com.north.light.libbilibilidfm.DoExamCusBulletInfo;
import com.north.light.libbilibilidfm.DoExamCusBulletViewNew;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


/**
 * @Author: lzt
 * @CreateDate: 2021/7/30 10:30
 * @Version: 1.0
 * @Description:用于CoordinatorLayout，为了填充默认数据list的recyclerview
 */
public class BiliStringRecyclerView extends RecyclerView {


    public BiliStringRecyclerView(@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        new PagerSnapHelper().attachToRecyclerView(this);
    }

    /**
     * 设置数据
     */
    public void initData() {
        CoorAdapter adapter = new CoorAdapter();
        List<String> da = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            da.add(i + "");
        }
        adapter.mData = da;
        this.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static class CoorAdapter extends Adapter<CoorAdapter.CoorVH> {
        private List<String> mData = new ArrayList<>();

        @NotNull
        @Override
        public CoorVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new CoorVH(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.activity_bilidanmu_item,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull CoorVH holder, int position) {
            String data = mData.get(position);
            holder.mContent.setText(data);
            holder.bliView.setIdentify(String.valueOf(position));
//            if (position % 6 == 0) {
//                holder.bliView.setData(new ArrayList<>());
//            } else {
//                new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        List<DoExamCusBulletInfo> list = new ArrayList<>();
//                        list.add(new DoExamCusBulletInfo("这是222" + position, 1300));
//                        holder.bliView.setData(list);
//                        holder.bliView.addBullet(new DoExamCusBulletInfo("这是23" + position, 2000, true));
//                        holder.bliView.addBullet(new DoExamCusBulletInfo("这是24" + position, 2200));
//                        holder.bliView.addBullet(new DoExamCusBulletInfo("这是25" + position, 2300, true));
//                    }
//                }, 2000);
//            }
            List<DoExamCusBulletInfo> list = new ArrayList<>();
            list.add(new DoExamCusBulletInfo("这是111" + position, 300,false));
            list.add(new DoExamCusBulletInfo("这是222" + position, 1300,false));
            list.add(new DoExamCusBulletInfo("这是333" + position, 2300,false));
            list.add(new DoExamCusBulletInfo("这是444" + position, 3300,false));
            list.add(new DoExamCusBulletInfo("这是555" + position, 4300,false));
            holder.bliView.setData(list);
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23A" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23B" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23C" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23D" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23E" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是23F" + position, 2000, true));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是24" + position, 2200));
//            holder.bliView.addBullet(new DoExamCusBulletInfo("这是25" + position, 2300, true));

            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.bliView.seek(0);
                }
            }, 3000);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.bliView.seek(1000);
                }
            }, 4000);
            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
                @Override
                public void run() {
                    holder.bliView.seek(2000);
                }
            }, 5000);

//            new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    holder.bliView.setShow(false, true);
//                    new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            holder.bliView.setShow(true, true);
//
//                        }
//                    }, 2000);
//                }
//            }, 4000);
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public static class CoorVH extends ViewHolder {
            private TextView mContent;
            private DoExamCusBulletViewNew bliView;

            public CoorVH(@NonNull @NotNull View itemView) {
                super(itemView);
                mContent = itemView.findViewById(R.id.activity_coor_adapter_item_txt);
                bliView = itemView.findViewById(R.id.bliView);
            }
        }
    }
}
