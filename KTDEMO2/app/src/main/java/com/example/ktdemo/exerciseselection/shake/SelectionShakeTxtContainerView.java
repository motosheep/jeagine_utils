package com.example.ktdemo.exerciseselection.shake;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;


/**
 * FileName: SelectionShakeTxtContainerView
 * Author: lzt
 * Date: 2022/10/10 09:11
 * 文字抖动view
 */
public class SelectionShakeTxtContainerView extends LinearLayout {
    private static final int TIME_DURATION = 200;
    private static final int TIME_INTERVAL = 4000;
    private Handler mUIHandler = new Handler(Looper.getMainLooper());


    private boolean needShake;
    private int position;
    private boolean alphaAnim;

    public SelectionShakeTxtContainerView(Context context) {
        super(context);
        init();
    }

    public SelectionShakeTxtContainerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SelectionShakeTxtContainerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDetachedFromWindow() {
        hide();
        super.onDetachedFromWindow();
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    private void init() {

    }

    //外部调用----------------------------------------------


    /**
     * @param needShake 是否需要抖动
     */
    public void show(int position, boolean alphaAnim, boolean needShake, boolean immediately) {
        this.needShake = needShake;
        this.alphaAnim = alphaAnim;
        this.position = position;
        if (immediately) {
            start();
        }
    }

    /**
     * 开始动画
     */
    public void start() {
        //透明度
        postDelayed(new Runnable() {
            @Override
            public void run() {
                if (alphaAnim) {
                    setAlpha(0);
                    setVisibility(View.VISIBLE);
                    animate().alpha(1).setDuration(TIME_DURATION).start();
                } else {
                    setAlpha(1);
                    setVisibility(View.VISIBLE);
                }
                if (needShake) {
                    //抖动
                    animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            animate().rotation(16f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            animate().rotation(0f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                                @Override
                                                public void run() {
                                                    //后续会一直抖动
                                                    shakeAllTime();
                                                }
                                            }).start();
                                        }
                                    }).start();
                                }
                            }).start();
                        }
                    }).start();
                }
            }
        }, 20);
    }

    /**
     * 一直抖动
     */
    private void shakeAllTime() {
        clearAnimation();
        mUIHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        animate().rotation(16f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                    @Override
                                    public void run() {
                                        animate().rotation(0f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                            @Override
                                            public void run() {
                                                //后续会一直抖动
                                                shakeAllTime();
                                            }
                                        }).start();
                                    }
                                }).start();
                            }
                        }).start();
                    }
                }).start();
            }
        }, TIME_INTERVAL);
    }


    public void hide() {
        clearAnimation();
//        setVisibility(View.INVISIBLE);
        mUIHandler.removeCallbacksAndMessages(null);
    }
}
