package com.example.ktdemo.blur;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;
import com.example.ktdemo.blur.blurwidget.BlurAlignBottomBean;
import com.example.ktdemo.blur.blurwidget.BlurAlignBottomView;
import com.example.ktdemo.blur.newblur.BlurUtils;
import com.north.light.libmusicplayerx.utils.KtLogUtil;
import com.north.light.libpicblur.LibPicBlurCusView;
import com.north.light.libpicblur.LibPicBlurViewShotUtils;

public class CusBlurActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus_blur);
        RelativeLayout relativeLayout = findViewById(R.id.root);
        LibPicBlurCusView blurCusView = findViewById(R.id.blurV);
//        blurCusView.startBlur(relativeLayout, 20f, 20);

        ImageView mLeftIv = findViewById(R.id.leftIv);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mLeftIv.setImageResource(R.mipmap.ic_launcher);
            }
        }, 2000);
        Bitmap sr = BitmapFactory.decodeResource(getResources(),
                R.drawable.ic_do_exam_float_menu_arrow_close);

        KtLogUtil.d("dd");

        ImageView thirdImg = findViewById(R.id.thirdImg);
        BlurAlignBottomView ivBlurView = findViewById(R.id.ivBlurView);
        thirdImg.post(new Runnable() {
            @Override
            public void run() {
                LibPicBlurViewShotUtils.viewSnapShot(thirdImg, new LibPicBlurViewShotUtils.ViewSnapListener() {
                    @Override
                    public void success(Bitmap bitmap) {
                        Bitmap blurBitmap = BlurUtils.blur(CusBlurActivity.this, 26, bitmap);
                        BlurAlignBottomBean blurAlignBottomBean = new BlurAlignBottomBean();
                        blurAlignBottomBean.setSrcBitmap(blurBitmap);
                        blurAlignBottomBean.setBlurHeight(300);
                        blurAlignBottomBean.setRadius(80f);
                        ivBlurView.setBitmap(blurAlignBottomBean);
                        KtLogUtil.d("LibPicBlurViewShotUtils success");
                    }

                    @Override
                    public void failed(String message) {
                        KtLogUtil.d("LibPicBlurViewShotUtils failed");
                    }
                });
            }
        });


//        NewbieGuide.with(this)
//                .setLabel("guide1")
//                .alwaysShow(true)//总是显示，调试时可以打开
//                .addGuidePage(
//                        GuidePage.newInstance()
//                        .setEverywhereCancelable(false)
//                        .addHighLight(mLeftIv)
//                        .setLayoutRes(R.layout.view_guide_simple))
//                .show();
    }
}