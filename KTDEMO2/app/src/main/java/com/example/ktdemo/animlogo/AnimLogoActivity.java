package com.example.ktdemo.animlogo;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;


/**
 * 动画控件
 */
public class AnimLogoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anim_logo);
        Button test = findViewById(R.id.btTest);
        ImageView ivAnim = findViewById(R.id.ivAnim);
        final boolean[] switchTag = {false};
        test.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DoExamAnimUtils().startAnim(ivAnim,
                        switchTag[0],
                        !switchTag[0],
                        dp2px(50));
                switchTag[0] = !switchTag[0];
            }
        });

    }

    public static int dp2px(final float dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}