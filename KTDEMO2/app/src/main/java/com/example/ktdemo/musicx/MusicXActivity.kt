package com.example.ktdemo.musicx

import android.os.Bundle
import android.os.Handler
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.ktdemo.R
import com.north.light.libmusicplayerx.ipc.IPCPxMusic

//http://music.163.com/song/media/outer/url?id=447925558.mp3
class MusicXActivity : AppCompatActivity() {
    val url = "http://music.163.com/song/media/outer/url?id=447925558.mp3"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_xactivity)
//        PXMusic.getPlayer().init(this)
//        PXMusic.getPlayer().setEventListener(object : LibPXMusicEventListener {
//            override fun play(url: String?) {
//                LogUtil.d("mx event: play$url")
//            }
//
//            override fun pause(url: String?) {
//                LogUtil.d("mx event: pause$url")
//            }
//
//            override fun resume(url: String?) {
//                LogUtil.d("mx event: resume$url")
//            }
//
//            override fun stop(url: String?) {
//                LogUtil.d("mx event: stop$url")
//            }
//
//            override fun finish(url: String?) {
//                LogUtil.d("mx event: finish$url")
//            }
//
//            override fun needToInit() {
//                LogUtil.d("mx event: needToInit")
//            }
//        })
//        PXMusic.getPlayer().setProgressListener(object : LibPXMusicProgressListener {
//            override fun progress(url: String?, current: Int, total: Int) {
//                LogUtil.d("mx event: progress $url current $current total $total")
//            }
//
//            override fun buffering(url: String?, current: Int, total: Int) {
//                LogUtil.d("mx event: buffering $url current $current total $total")
//            }
//        })
        IPCPxMusic.getInstance().init(this)
    }

    fun play(view: View) {
        IPCPxMusic.getInstance().play(url)
    }

    fun pause(view: View) {
//        PXMusic.getPlayer().pause()
        IPCPxMusic.getInstance().pause()
    }

    fun resume(view: View) {
//        PXMusic.getPlayer().resume()
        IPCPxMusic.getInstance().resume()
    }

    fun stop(view: View) {
//        PXMusic.getPlayer().stop()
        IPCPxMusic.getInstance().stop()
    }

    fun release(view: View) {
//        PXMusic.getPlayer().release()
        IPCPxMusic.getInstance().release()
    }

    fun seek(view: View) {
//        PXMusic.getPlayer().seekTo(220285)
        IPCPxMusic.getInstance().seekTo(220285)
    }

}