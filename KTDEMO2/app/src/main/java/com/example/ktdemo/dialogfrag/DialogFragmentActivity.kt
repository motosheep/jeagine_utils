package com.example.ktdemo.dialogfrag

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.appcompat.app.AppCompatActivity
import com.example.ktdemo.R

class DialogFragmentActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_dialog_fragment)

        val cusDialog = CusDialogFragment()
        cusDialog.setHeight(500)
        cusDialog.show(supportFragmentManager, "cus")

        Handler(Looper.getMainLooper()).postDelayed(
            Runnable {
                cusDialog.dismissAllowingStateLoss()
            },
            5000
        )
    }
}