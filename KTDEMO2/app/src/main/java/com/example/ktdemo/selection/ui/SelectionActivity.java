package com.example.ktdemo.selection.ui;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.example.ktdemo.exerciseselection.text.SelectionAnimHotInfo;
import com.example.ktdemo.exerciseselection.text.SelectionAnimHotView;
import com.example.ktdemo.selection.SelectionAnimInfo;
import com.example.ktdemo.selection.SelectionRelativelayout;
import com.example.ktdemo.selection.SelectionRelativelayoutListener;
import com.example.ktdemo.selection.SelectionRootView;
import com.example.ktdemo.selection.adapter.SelectionAdapter;
import com.example.ktdemo.selection.selectionprogressbar.SelectionProBarInfo;
import com.example.ktdemo.selection.selectionprogressbar.SelectionProHorizontalBarView;
import com.example.ktdemo.selection.selectionprogressbar.SelectionProVerticalBarView;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

public class SelectionActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection);

        SelectionAdapter adapter = new SelectionAdapter();

        SelectionRelativelayout selectionRelativelayout = findViewById(R.id.rlContent);
        selectionRelativelayout.setSelectionRelativelayoutListener(new SelectionRelativelayoutListener() {
            @Override
            public void startAnim(int adapterPosition) {
                LogUtil.d("Anim pos start: " + adapterPosition);
                //更新adapter位置的透明度
                adapter.updateAlpha(adapterPosition, false);
            }

            @Override
            public void endAnim(int adapterPosition) {
                LogUtil.d("Anim pos end: " + adapterPosition);
                //逐渐显示透明度并且进行动画
                adapter.updateAlpha(adapterPosition, true);
            }
        });


        adapter.setOnInfoListener(new SelectionAdapter.OnInfoListener() {
            @Override
            public void clickInfo(SelectionRootView view, SelectionAnimInfo info, int position) {
                //做动画
                selectionRelativelayout.startAnim(info, position);
            }
        });
        List<String> da = new ArrayList<>();
        for (int i = 0; i < 13; i++) {
            da.add(i + "");
        }
        adapter.setmData(da);

        selectionRelativelayout.getRecyclerView().setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
        selectionRelativelayout.getRecyclerView().setAdapter(adapter);

        //进度条
        SelectionProHorizontalBarView horizontalBarView = findViewById(R.id.horiBar);
        SelectionProBarInfo horizontal = new SelectionProBarInfo();
        horizontal.setStatus(2);
        horizontal.setProgress(20);
        horizontalBarView.setProgress(horizontal);


        SelectionProVerticalBarView verBar = findViewById(R.id.verBar);
        SelectionProBarInfo verBarInfo = new SelectionProBarInfo();
        verBarInfo.setStatus(2);
        verBarInfo.setProgress(95);
        verBar.setProgress(verBarInfo);


        SelectionAnimHotView hotView = findViewById(R.id.hotView);
        hotView.setAnimListener(new SelectionAnimHotView.AnimEventListener() {

            @Override
            public void finish(SelectionAnimHotInfo info) {
                LogUtil.d("selection finish: " + (new Gson().toJson(info)));
                if (info != null && info.getIdentify() == -1) {
                    SelectionAnimHotInfo hotInfo = new SelectionAnimHotInfo();
                    hotInfo.setIdentify(1);
                    hotInfo.setTextRes(R.drawable.ic_do_exam_correct_add);
                    hotView.start(hotInfo);
                }
            }

            @Override
            public void start(SelectionAnimHotInfo info) {
                LogUtil.d("selection start: " + (new Gson().toJson(info)));
            }
        });
        hotView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hotView.start(new SelectionAnimHotInfo());
            }
        });
    }
}