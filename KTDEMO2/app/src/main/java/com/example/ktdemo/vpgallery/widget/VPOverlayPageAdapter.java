package com.example.ktdemo.vpgallery.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;
import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.example.ktdemo.vpgallery.bean.VPGalleryInfo;
import com.example.ktdemo.vpgallery.blur.VPBlurAlignBottomBean;
import com.example.ktdemo.vpgallery.blur.VPBlurAlignBottomView;
import com.example.ktdemo.vpgallery.memory.VPBlurBitmapMemory;
import com.example.ktdemo.vpgallery.widget.impl.VPGalleryTransformer;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;

public class VPOverlayPageAdapter extends PagerAdapter {
    protected Context context;
    private ArrayList<VPGalleryInfo> list;
    private boolean isLooper = true;
    private InitListener mListener;

    public VPOverlayPageAdapter(Context context) {
        this.context = context;
    }

    public void setDataAndBindViewPager(ViewPager vp, ArrayList<VPGalleryInfo> list) {
        setDataAndBindViewPager(vp, list, 4);
    }

    public void setDataAndBindViewPager(ViewPager vp, ArrayList<VPGalleryInfo> source, int offsetLimit) {
        this.list = source;
        if (list != null && list.size() > 0) {
            vp.setOffscreenPageLimit(offsetLimit);
            VPGalleryTransformer vpGalleryTransformer = new VPGalleryTransformer(this, vp, 60, 110);
            vp.setPageTransformer(true, vpGalleryTransformer);
        }
    }

    public ArrayList<VPGalleryInfo> getList() {
        return list;
    }

    public void setLooper(boolean looper) {
        isLooper = looper;
    }

    public boolean isLooper() {
        return isLooper;
    }

    @Override
    public int getCount() {
        if (null == list)
            return 0;
        if (list.size() == 1) {
            return 1;
        }
        if (!isLooper()) {
            return list.size();
        }
        return Integer.MAX_VALUE;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        final int p = position % list.size();
        String detailData = list.get(p).getCover();
        View view = LayoutInflater.from(context).inflate(R.layout.item_learning_stage, container, false);
        if (null == view) {
            throw new RuntimeException("you should set a item layout");
        }
        view.setTag(detailData);
        if (detailData != null) {
            setData(view, detailData, position);
        }
        container.addView(view);
        return view;
    }

    /**
     * 更新item
     */
    public void updateBlurItem(View rootView) {
        if (rootView == null) {
            return;
        }
        ImageView ivCover = rootView.findViewById(R.id.ivCover);
        VPBlurAlignBottomView ivBlurView = rootView.findViewById(R.id.ivBlurView);
        if (ivCover == null || ivBlurView == null) {
            return;
        }
        Object tag = rootView.getTag();
        if (tag instanceof String) {
            String url = (String) tag;
            Bitmap blurBitmap = VPBlurBitmapMemory.getInstance().getBlur(url);
            if (blurBitmap != null) {
                VPBlurAlignBottomBean blurInfo = new VPBlurAlignBottomBean();
                blurInfo.setBlurHeight(120);
                blurInfo.setRadius(40);
                blurInfo.setSrcBitmap(blurBitmap);
                ivBlurView.post(new Runnable() {
                    @Override
                    public void run() {
                        ivBlurView.setBitmap(blurInfo);
                    }
                });
                LogUtil.d("updateBlurItem use memory");
            }
        }
    }

    private void setData(View view, String detailData, int position) {
        TextView tvTitle = view.findViewById(R.id.tvTitle);
        ImageView ivCover = view.findViewById(R.id.ivCover);
        tvTitle.setText("No." + detailData);
        String url = (String) view.getTag();
        //只读取内存中的数据
        Bitmap bitmap = VPBlurBitmapMemory.getInstance().getSource(url);
        if (bitmap != null) {
            ivCover.setImageBitmap(bitmap);
            LogUtil.d("VPOverlayPageAdapter setData bit");
            updateBlurItem(view);
        } else {
            LogUtil.d("VPOverlayPageAdapter setData empty");
        }
        if (position == 0 && this.mListener != null) {
            ivCover.post(new Runnable() {
                @Override
                public void run() {
                    mListener.initFinish();
                }
            });
        }
    }

    @Override
    public void destroyItem(ViewGroup container, int position, @NotNull Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    public void setInitListener(InitListener listener) {
        this.mListener = listener;
    }

    public interface InitListener {
        void initFinish();
    }
}