package com.example.ktdemo.bilidanmu;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;
import com.north.light.libbilibilidfm.DoExamCusBulletInfo;
import com.north.light.libbilibilidfm.DoExamCusBulletView;

import java.util.ArrayList;
import java.util.List;

public class BiliDanMuCusActivity extends AppCompatActivity {
    private DoExamCusBulletView bulletView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bili_dan_mu_cus);
        bulletView = findViewById(R.id.bullet);
        List<DoExamCusBulletInfo> doExamList = new ArrayList<>();
//        for (int i = 0; i < 30; i++) {
//            doExamList.add(new DoExamCusBulletInfo("这是一条弹幕我是底" + i, i * 200));
//        }
        bulletView.addBullet(new DoExamCusBulletInfo("这是一条弹幕我是底0", 200));
        bulletView.addBullet(new DoExamCusBulletInfo("这是一条弹幕我是底1", 200));
        bulletView.addBullet(new DoExamCusBulletInfo("这是一条弹幕我是底2", 200));
        bulletView.addBullet(new DoExamCusBulletInfo("这是一条弹幕我是底3", 200));
        bulletView.addBullet(new DoExamCusBulletInfo("这是一条弹幕我是底4", 200));



//        bulletView.addBullet(new DoExamCusBulletInfo("这是一条弹幕我是底1", 200));

//        bulletView.setData(doExamList);
    }
}