package com.example.ktdemo.guide.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;

/**
 * FileName: GuildeBottomImageView
 * Author: lzt
 * Date: 2023/6/30 14:09
 * Description:
 */
public class GuideBottomImageView extends NestedScrollView {

    private ImageView imageView;

    public GuideBottomImageView(@NonNull Context context) {
        super(context);
        init();
    }

    public GuideBottomImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GuideBottomImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public boolean dispatchTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return false;
    }

    private void init() {
        imageView = new ImageView(getContext());
        addView(imageView);
        imageView.setAdjustViewBounds(true);
    }

    public void setImageResource(int res) {
        imageView.setImageResource(res);
        post(new Runnable() {
            @Override
            public void run() {
                fullScroll(FOCUS_DOWN);
            }
        });
    }

}
