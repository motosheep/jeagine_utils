package com.example.ktdemo.staggle.second

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ktdemo.R


class GridSecondActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_grid_second)

        val adapter = GridSecondAdapter()
        val rvContent = findViewById<RecyclerView>(R.id.rvContent)
        rvContent.layoutManager = GridLayoutManager(this, 2, GridLayoutManager.HORIZONTAL, false)
        rvContent.adapter = adapter

        adapter.setData(arrayListOf("1", "1", "1", "1", "1", "1", "1", "1", "1", "1"))
    }
}