package com.example.ktdemo.draghelper.widget

import android.widget.RelativeLayout

/**
 * FileName: DragLayoutEventListener
 * Author: lzt
 * Date: 2023/3/31 15:53
 */
interface DragLayoutEventListener {

    fun toTop()
    fun toMiddle()
    fun toBottom()
    fun inflateView(
        topContainer: RelativeLayout, middleContainer: RelativeLayout,
        bottomContainer: RelativeLayout
    )

}