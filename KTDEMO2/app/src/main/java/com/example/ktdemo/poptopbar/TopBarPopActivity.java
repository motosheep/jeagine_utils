package com.example.ktdemo.poptopbar;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;

public class TopBarPopActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_bar_pop);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                ConstraintLayout rlRoot = findViewById(R.id.rlRoot);
                TopBarPop topBarPop = new TopBarPop();

                topBarPop.setTopBarPopEventListener(new TopBarPopEventListener() {
                    @Override
                    public void init(View view) {
                        LogUtil.d("topBarPop init");
                        TopBarPopSlideLayout slideLayout = view.findViewById(R.id.slideRoot);
                        slideLayout.setOnSwipeEventListener(new TopBarPopSlideLayout.OnSwipeEventListener() {
                            @Override
                            public void down() {
                                LogUtil.d("topBarPop TopBarPopSlideLayout down");
                            }

                            @Override
                            public void up() {
                                LogUtil.d("topBarPop TopBarPopSlideLayout up");
                                topBarPop.dismiss();
                            }
                        });
                    }

                    @Override
                    public void dismiss() {
                        LogUtil.d("topBarPop dismiss");
                    }
                });
                topBarPop.show(TopBarPopActivity.this, rlRoot, R.layout.layout_top_bar_pop_exam,
                        20, 20, 40);
            }
        }, 1000);


    }
}