package com.example.ktdemo.template

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.ktdemo.R
import com.north.light.libmediatemplate.widget.TemplateVerticalTextView


class TemplateActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_template)
//
//        val ivRo : AndroidRotateTextView =findViewById(R.id.ivRo)
//        ivRo.setOnClickListener {
//            KtLogUtil.d("dsfasdfasdf")
//        }

        val tv1: TemplateVerticalTextView = findViewById(R.id.tv1)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tv1.getContentView().text = "你好\uD83D\uDE0F\uD83C\uDF1Ahh\n" +
                    "了\uD83C\uDF7A\uD83D\uDC16\uD83D\uDC16\uD83D\uDC16得那我"
        }, 2000)
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            tv1.getContentView().text = "一杯一盏有\n乾"
//        }, 4000)
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            tv1.getContentView().text = "一杯一盏有乾坤一杯一盏有乾"
//        }, 6000)
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            tv1.getContentView().text = "一杯一盏有乾\n坤一盏有乾\n坤一盏有乾"
//        }, 8000)
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            tv1.getContentView().text = "一杯一盏有乾坤一盏有乾"
//        }, 10000)
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            tv1.getContentView().text = "一杯一盏有乾坤一盏有乾一杯一盏有乾坤一盏有乾"
//        }, 12000)
//        Handler(Looper.getMainLooper()).postDelayed(Runnable {
//            tv1.getContentView().text = "一杯\n一盏\n有乾坤\n一盏有乾\n一杯一盏\n有乾\n坤一盏\n有乾"
//        }, 14000)


        val etText: EditText = findViewById(R.id.etText)
        val btConfirm: Button = findViewById(R.id.btConfirm)
        btConfirm.setOnClickListener {
            val tex = etText.text.toString()
            if(tex.isNullOrBlank()){
                return@setOnClickListener
            }
            tv1.getContentView().text = "$tex"
        }

    }
}