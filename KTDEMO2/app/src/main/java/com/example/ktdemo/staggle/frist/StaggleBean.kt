package com.example.ktdemo.staggle.frist

import java.io.Serializable

/**
 * FileName: StaggleBean
 * Author: lzt
 * Date: 2023/5/10 09:21
 */
class StaggleBean : Serializable {

    var type = 0
    var title: String? = null
}