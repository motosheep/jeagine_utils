package com.example.ktdemo.exerciseselection.ui;

import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.VideoView;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.example.ktdemo.exerciseselection.selectionprogressbar.v2.SelectionProBarInfoV2;
import com.example.ktdemo.exerciseselection.selectionprogressbar.v2.SelectionProHorizontalBarViewV2;
import com.example.ktdemo.exerciseselection.selectionprogressbar.v2.SelectionProVerticalBarViewV2;
import com.example.ktdemo.selection.SelectionViewShotUtils;
import com.example.libpicblur.LibPicBlurView;

public class SelectionProgressActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selection_progress);

        SelectionProHorizontalBarViewV2 proHorizontalBarViewV2 = findViewById(R.id.pbHor);
        SelectionProBarInfoV2 info = new SelectionProBarInfoV2();
        info.setStatus(2);
        info.setProgress(100);
        proHorizontalBarViewV2.setProgress(info, true);

        SelectionProHorizontalBarViewV2 proHorizontalBarViewV21 = findViewById(R.id.pbHor1);
        SelectionProBarInfoV2 info1 = new SelectionProBarInfoV2();
        info1.setStatus(0);
        info1.setProgress(30);
        proHorizontalBarViewV21.setProgress(info1, true);

        SelectionProHorizontalBarViewV2 proHorizontalBarViewV22 = findViewById(R.id.pbHor2);
        SelectionProBarInfoV2 info2 = new SelectionProBarInfoV2();
        info2.setStatus(1);
        info2.setProgress(50);
        proHorizontalBarViewV22.setProgress(info2, true);

        SelectionProVerticalBarViewV2 pbVer1 = findViewById(R.id.pbVer1);
        SelectionProVerticalBarViewV2 pbVer2 = findViewById(R.id.pbVer2);
        SelectionProVerticalBarViewV2 pbVer3 = findViewById(R.id.pbVer3);
        pbVer1.setProgress(info, true);
        pbVer2.setProgress(info1, true);
        pbVer3.setProgress(info2, true);

        LinearLayout llContentRoot = findViewById(R.id.llContentRoot);
        llContentRoot.postDelayed(new Runnable() {
            @Override
            public void run() {
                SelectionViewShotUtils.viewSnapshot(llContentRoot, new SelectionViewShotUtils.ViewSnapListener() {
                    @Override
                    public void success(Bitmap bitmap) {

                    }

                    @Override
                    public void failed(String message) {

                    }
                });
            }
        },3000);


//        VideoView vVideoView = findViewById(R.id.vVideoView);
//        vVideoView.setMediaController(new MediaController(this));
//        vVideoView.setVideoURI(Uri.parse("http://121.199.61.174/testhtml5.mp4"));
//        vVideoView.start();
    }
}