package com.example.ktdemo.exerciseselection;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;
import android.view.animation.LinearInterpolator;

import androidx.annotation.Nullable;


/**
 * FileName: SelectionAnimView
 * Author: lzt
 * Date: 2022/10/9 17:10
 * 选项动画view
 */
public class SelectionAnimView extends SelectionBaseView {

    private SelectionAnimInfo mSelectionInfo;
    //动画时长
    private long CONSTANT_TIME_ANIM = 400;
    //下落距离
    private static final int DISTANCE_DOWN = 250;
    private static final String STRING_ANIM = "STRING_ANIM";
    //旋转角度
    private static final int ROTATE_MAX_AGREE = 8;
    //最大透明度的值
    private static final int ROTATE_MAX_ALPHA = 255;

    //绘制的Y轴坐标
    private static int mDrawY = 0;
    //绘制的透明度
    private static int mDrawAlpha = 0;
    //图片旋转的角度
    private static int mDrawBitmapRotate = 0;

    //是否绘制
    private boolean mIsDrawing = false;

    //画笔
    private Paint mPaint;

    //监听
    private AnimEventListener mAnimListener;


    public SelectionAnimView(Context context) {
        super(context);
        initParams();
    }

    public SelectionAnimView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initParams();
    }

    public SelectionAnimView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initParams();
    }

    /**
     * 初始化参数
     */
    private void initParams() {
        //禁用硬件加速
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(1);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(dp2px(12));
    }

    /**
     * 开始动画
     */
    public void startAnim(SelectionAnimInfo selectionAnimInfo) {
        if (selectionAnimInfo == null || selectionAnimInfo.getBitmap() == null) {
            this.mSelectionInfo = null;
            postInvalidate();
            return;
        }
        this.mSelectionInfo = selectionAnimInfo;
        this.mIsDrawing = true;
        int startY = (int) this.mSelectionInfo.getStartY();
        //开始做动画
        ValueAnimator valueAnimator = getValueAnimator(STRING_ANIM);
        if (valueAnimator != null) {
            valueAnimator.cancel();
        }
        int total = startY + DISTANCE_DOWN;
        valueAnimator = ValueAnimator.ofInt(startY, total);
        valueAnimator.setInterpolator(new LinearInterpolator());
        valueAnimator.setDuration(CONSTANT_TIME_ANIM);
        valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                mDrawY = (int) animation.getAnimatedValue();
                float percent = 1 - (mDrawY * 1.0f / total * 1.0f);
                mDrawAlpha = (int) (ROTATE_MAX_ALPHA * percent);
                mDrawBitmapRotate = (int) (ROTATE_MAX_AGREE * (1 - percent));
                postInvalidate();
            }
        });
        valueAnimator.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                mIsDrawing = false;
                notifyFinish();
            }
        });
        notifyStart();
        startValueAnimator(STRING_ANIM, valueAnimator);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mIsDrawing) {
            drawBitmap(canvas);
        }
    }


    /**
     * 绘制bitmap
     */
    private void drawBitmap(Canvas canvas) {
        if (mSelectionInfo == null) {
            return;
        }
        Bitmap drawBitmap = mSelectionInfo.getBitmap();
        if (drawBitmap == null || drawBitmap.isRecycled()) {
            return;
        }
        Bitmap rotateBitmap = adjustPhotoRotation(drawBitmap, mDrawBitmapRotate);
        if (rotateBitmap == null) {
            return;
        }
        canvas.save();
        mPaint.setAlpha(mDrawAlpha);
        float startX = mSelectionInfo.getStartX();
        canvas.drawBitmap(rotateBitmap, startX, mDrawY, mPaint);
        canvas.restore();
    }


    //监听事件---------------------------------------------------------------

    public interface AnimEventListener {
        void start();

        void finish();
    }


    public void setAnimEventListener(AnimEventListener listener) {
        this.mAnimListener = listener;
    }

    private void notifyStart() {
        if (this.mAnimListener != null) {
            this.mAnimListener.start();
        }
    }

    private void notifyFinish() {
        if (this.mAnimListener != null) {
            this.mAnimListener.finish();
        }
    }


}
