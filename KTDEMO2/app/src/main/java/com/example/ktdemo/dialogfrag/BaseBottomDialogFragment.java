package com.example.ktdemo.dialogfrag;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.coordinatorlayout.widget.CoordinatorLayout;

import com.app.hubert.guide.util.ScreenUtils;
import com.example.ktdemo.R;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

/**
 * FileName: BaseDialogFragment
 * Author: lzt
 * Date: 2022/8/30 17:33
 * change by lzt 20221020 抽离默认宽高初始化方法
 */
public abstract class BaseBottomDialogFragment extends BottomSheetDialogFragment {

    private int DEFAULT_WIDTH = WindowManager.LayoutParams.MATCH_PARENT;//宽
    private int DEFAULT_HEIGHT = WindowManager.LayoutParams.MATCH_PARENT;//高
    private int DEFAULT_GRAVITY = Gravity.BOTTOM;//位置


    private BottomSheetBehavior<FrameLayout> bottomSheetBehavior;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mView = inflater.inflate(getLayoutId(), container, false);
        if (getDialog() != null && getDialog().getWindow() != null) {
            getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_NOTHING);
        }
        initViews(mView);
        return mView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onDismiss(@NonNull DialogInterface dialog) {
        super.onDismiss(dialog);
        dismissDialog();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        BottomSheetDialog mDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        if (null != mDialog) {//初始化
            mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            mDialog.setCanceledOnTouchOutside(canceledOnTouchOutside());
            mDialog.setCancelable(cancelable());
            Window window = mDialog.getWindow();
            if (null != window) {
                window.getDecorView().setPadding(0, 0, 0, 0);
                window.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                WindowManager.LayoutParams lp = window.getAttributes();
                lp.width = defWidth();
                lp.height = defHeight();
                lp.gravity = DEFAULT_GRAVITY;
                lp.windowAnimations = android.R.style.Animation_InputMethod;
                window.setAttributes(lp);
                window.setDimAmount(dimAmount());
            }
            mDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    return !cancelable();
                }
            });
        }
        return mDialog;
    }


    @Override
    public void onStart() {
        super.onStart();
        initSlide();
    }

    /**
     * 主线程执行
     */
    protected void runInUI(Runnable runnable) {
        if (runnable == null || getActivity() == null) {
            return;
        }
        getActivity().runOnUiThread(runnable);
    }

    /**
     * 初始化滑动交互
     */
    private void initSlide() {
        if (!canSlide()) {
            return;
        }
        try {
            if (getDialog() != null && getDialog() instanceof BottomSheetDialog) {
                int peekHeight = getPeekHeight() -
                        ScreenUtils.getStatusBarHeight(getContext());
                BottomSheetDialog dialog = (BottomSheetDialog) getDialog();
                //把windows的默认背景颜色去掉，不然圆角显示不见
                dialog.getWindow().findViewById(R.id.design_bottom_sheet).setBackground(new ColorDrawable(Color.TRANSPARENT));
                //获取dialog的根部局
                FrameLayout bottomSheet = dialog.getDelegate().findViewById(R.id.design_bottom_sheet);
                //获取根部局的LayoutParams对象
                CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomSheet.getLayoutParams();
                layoutParams.height = peekHeight;
                //修改弹窗的最大高度，不允许上滑（默认可以上滑）
                bottomSheet.setLayoutParams(layoutParams);
                bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet);
                //peekHeight即弹窗的最大高度
                bottomSheetBehavior.setPeekHeight(peekHeight);
                //初始为展开状态
                bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
                //能否滑动的时候隐藏
                bottomSheetBehavior.setHideable(hideAble());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        dismissDialog();
    }

    protected void dismissDialog() {

    }

    /**
     * 弹窗高度
     * 子类可重写该方法返回peekHeight
     *
     * @return height
     */
    protected int getPeekHeight() {
        return getResources().getDisplayMetrics().heightPixels;
    }

    protected float dimAmount() {
        return 0.6f;
    }

    protected int defWidth() {
        return DEFAULT_WIDTH;
    }

    protected int defHeight() {
        return DEFAULT_HEIGHT;
    }


    /**
     * 能否滑动
     */
    protected boolean canSlide() {
        return true;
    }

    /**
     * 设置位置
     *
     * @param gravity
     */
    public void setGravity(int gravity) {
        DEFAULT_GRAVITY = gravity;
    }

    /**
     * 设置宽
     *
     * @param width
     */
    public void setWidth(int width) {
        DEFAULT_WIDTH = width;
    }

    /**
     * 设置高
     *
     * @param height
     */
    public void setHeight(int height) {
        DEFAULT_HEIGHT = height;
    }

    /**
     * 设置点击返回按钮是否可取消
     */
    public boolean cancelable() {
        return true;
    }

    /**
     * 设置点击外部是否可取消
     */
    public boolean canceledOnTouchOutside() {
        return true;
    }

    public boolean hideAble() {
        return true;
    }

    /**
     * 设置布局
     *
     * @return
     */
    protected abstract int getLayoutId();

    /**
     * 初始化Views
     *
     * @param v
     */
    protected abstract void initViews(View v);


}

