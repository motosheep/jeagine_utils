package com.example.ktdemo.selection;

import java.io.Serializable;

/**
 * FileName: SelectionRelativelayoutApi
 * Author: lzt
 * Date: 2022/10/9 17:21
 * 做题实现api
 */
public interface SelectionRelativelayoutApi extends Serializable {

    /**
     * 获取recyclerview
     */
    public SelectionRecyclerview getRecyclerView();

    /**
     * 获取动画 view
     */
    public SelectionAnimView getAnimView();

    /**
     * 设置监听
     */
    public void setSelectionRelativelayoutListener(SelectionRelativelayoutListener listener);

    /**
     * 移除监听
     */
    public void removeSelectionRelativelayoutListener();

    /**
     * 传入图片，开始做动画
     */
    public void startAnim(SelectionAnimInfo info,int adapterPosition);

}
