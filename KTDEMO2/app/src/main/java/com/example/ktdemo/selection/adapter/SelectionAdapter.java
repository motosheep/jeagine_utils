package com.example.ktdemo.selection.adapter;

import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.example.ktdemo.selection.SelectionAnimInfo;
import com.example.ktdemo.selection.SelectionRootView;
import com.example.ktdemo.selection.SelectionViewShotUtils;
import com.example.ktdemo.selection.selectionprogressbar.SelectionProBarInfo;
import com.example.ktdemo.selection.selectionprogressbar.SelectionProHorizontalBarView;
import com.example.ktdemo.selection.shake.SelectionShakeAnimTextView;
import com.example.ktdemo.selection.shake.SelectionShakeTxtContainerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: SelectionAdapter
 * Author: lzt
 * Date: 2022/10/9 17:40
 */
public class SelectionAdapter extends RecyclerView.Adapter<SelectionAdapter.CoorVH> {
    private List<String> mData = new ArrayList<>();

    public static final String UPDATE_ALPHA = "UPDATE_ALPHA";
    public boolean showRoot = false;


    public void setmData(List<String> mData) {
        this.mData = mData;
        notifyDataSetChanged();
    }

    @NotNull
    @Override
    public CoorVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new CoorVH(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.activity_selection_item_txt,
                parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CoorVH holder, int position, @NonNull @NotNull List<Object> payloads) {
        super.onBindViewHolder(holder, position, payloads);
        if (payloads == null || payloads.size() == 0) {
            return;
        }
        for (Object obj : payloads) {
            if (!(obj instanceof String)) {
                return;
            }
        }
        //至此，payloads的对象全部都是string类型
        String key = (String) payloads.get(0);
        switch (key) {
            case UPDATE_ALPHA:
                if (showRoot) {
                    holder.rlRoot.show();
                } else {
                    holder.rlRoot.hide();
                }
                break;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull CoorVH holder, int position) {
        String data = mData.get(position);
        holder.mContent.setText(data);
        holder.progressInfo.setProgress(new SelectionProBarInfo());

        holder.tvAnim.setAnimText("80");
        if (position % 3 == 1) {
            holder.clAnim.show(true);
        }

        holder.rlRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                float touchX = v.getX();
                float touchY = v.getY();
                float width = v.getMeasuredWidth();
                float height = v.getMeasuredHeight();
                LogUtil.d("touch x: " + touchX + "\ttouch y: " + touchY + "\twidth: " + width
                        + "\theight: " + height + "\tpos: " + position);
                SelectionViewShotUtils.viewSnapshot(holder.rlRoot, new SelectionViewShotUtils.ViewSnapListener() {
                    @Override
                    public void success(Bitmap bitmap) {
                        SelectionAnimInfo selectionAnimInfo = new SelectionAnimInfo(bitmap, touchX, touchY, width, height);
                        if (mListener != null) {
                            mListener.clickInfo(holder.rlRoot, selectionAnimInfo, position);
                        }
                    }

                    @Override
                    public void failed(String message) {
                        LogUtil.d("failed message");
                    }
                });
            }
        });
        holder.rlRoot.setShowHideEventListener(new SelectionRootView.ShowHideEventListener() {
            @Override
            public void entireShow() {
                //显示出来了--
                LogUtil.d("root view adapter entire show");
            }

            @Override
            public void startShow() {
                LogUtil.d("root view adapter start show");
                SelectionProBarInfo horizontal = new SelectionProBarInfo();
                horizontal.setStatus(position % 3);
                horizontal.setProgress(20);
                holder.progressInfo.setProgress(horizontal);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public static class CoorVH extends RecyclerView.ViewHolder {
        private TextView mContent;
        private LinearLayout llRoot;
        private SelectionRootView rlRoot;
        private SelectionProHorizontalBarView progressInfo;
        private SelectionShakeAnimTextView tvAnim;
        private SelectionShakeTxtContainerView clAnim;


        public CoorVH(@NonNull @NotNull View itemView) {
            super(itemView);
            mContent = itemView.findViewById(R.id.activity_coor_adapter_item_txt);
            llRoot = itemView.findViewById(R.id.llRoot);
            rlRoot = itemView.findViewById(R.id.rlRoot);
            progressInfo = itemView.findViewById(R.id.progressInfo);
            tvAnim = itemView.findViewById(R.id.tvAnim);
            clAnim = itemView.findViewById(R.id.clAnim);
        }
    }


    //更新透明度
    public void updateAlpha(int pos, boolean showRoot) {
        this.showRoot = showRoot;
        notifyItemChanged(pos, UPDATE_ALPHA);
    }

    private OnInfoListener mListener;

    public interface OnInfoListener {
        void clickInfo(SelectionRootView view, SelectionAnimInfo info, int position);
    }

    public void setOnInfoListener(OnInfoListener listener) {
        this.mListener = listener;
    }


}