package com.example.ktdemo.sliderecyclerview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.LogUtil;

/**
 * FileName: SlideTabRecyclerview
 * Author: lzt
 * Date: 2023/2/15 14:07
 * 滑动recyclerview
 */
public class SlideTabRecyclerview extends RecyclerView {


    public SlideTabRecyclerview(@NonNull Context context) {
        super(context);
        init();
    }

    public SlideTabRecyclerview(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlideTabRecyclerview(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        addOnScrollListener(scrollListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        removeOnScrollListener(scrollListener);
        super.onDetachedFromWindow();
    }

    private OnScrollListener scrollListener = new OnScrollListener() {

        @Override
        public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
            super.onScrollStateChanged(recyclerView, newState);
        }

        @Override
        public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);
            try {
                updateLinearLayoutManager(recyclerView, dx, dy);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    /**
     * LinearLayoutManager更新事件
     */
    private void updateLinearLayoutManager(RecyclerView recyclerView, int dx, int dy) throws Exception {
        LayoutManager layoutManager = recyclerView.getLayoutManager();
        if (layoutManager instanceof LinearLayoutManager) {
            int firstCompleteVisiblePos = ((LinearLayoutManager) layoutManager).findFirstCompletelyVisibleItemPosition();
            int firstVisiblePos = ((LinearLayoutManager) layoutManager).findFirstVisibleItemPosition();
            View firstVisibleView = layoutManager.findViewByPosition(firstVisiblePos);
            View firstCompleteView = layoutManager.findViewByPosition(firstCompleteVisiblePos);

            int lastCompleteVisiblePos = ((LinearLayoutManager) layoutManager).findLastCompletelyVisibleItemPosition();
            int lastVisiblePos = ((LinearLayoutManager) layoutManager).findLastVisibleItemPosition();
            View lastVisibleView = layoutManager.findViewByPosition(lastVisiblePos);
            View lastCompleteView = layoutManager.findViewByPosition(lastCompleteVisiblePos);

            //向上为负数，向下为正数
            if (((LinearLayoutManager) layoutManager).getOrientation() == LinearLayoutManager.VERTICAL) {
                //垂直滑动
                //上层
                if (firstVisibleView != null) {
                    float marginTop = firstVisibleView.getTop();
                    float viewHeight = firstVisibleView.getMeasuredHeight();
                    if (viewHeight != 0) {
                        if (firstCompleteView != null) {
                            firstCompleteView.setAlpha(1f);
                        }
                        if (marginTop <= 0) {
                            float alpha = (viewHeight - Math.abs(marginTop)) / viewHeight;
                            firstVisibleView.setAlpha(alpha);
                        }
                    }
                }
                //下层
                if (lastVisibleView != null) {
                    float marginBottom = lastVisibleView.getBottom();
                    float parentHeight = recyclerView.getMeasuredHeight();
                    float viewHeight = lastVisibleView.getMeasuredHeight();
                    float showHeight = parentHeight - marginBottom;
                    if (viewHeight != 0) {
                        if (lastCompleteView != null) {
                            lastCompleteView.setAlpha(1f);
                        }
                        if (Math.abs(showHeight) <= viewHeight) {
                            float alpha = (viewHeight - Math.abs(showHeight)) / viewHeight;
                            lastVisibleView.setAlpha(alpha);
                        }
                    }
                }
            } else {
                //水平滑动
                //左边
                if (firstVisibleView != null) {
                    float marginStart = firstVisibleView.getLeft();
                    float viewHeight = firstVisibleView.getMeasuredWidth();
                    if (viewHeight != 0) {
                        if (firstCompleteView != null) {
                            firstCompleteView.setAlpha(1f);
                        }
                        if (marginStart <= 0) {
                            float alpha = (viewHeight - Math.abs(marginStart)) / viewHeight;
                            firstVisibleView.setAlpha(alpha);
                        }
                    }
                }
                //右边
                if (lastVisibleView != null) {
                    float marginEnd = lastVisibleView.getRight();
                    float parentWidth = recyclerView.getMeasuredWidth();
                    float viewWidth = lastVisibleView.getMeasuredWidth();
                    float showWidth = parentWidth - marginEnd;
                    if (viewWidth != 0) {
                        if (lastCompleteView != null) {
                            lastCompleteView.setAlpha(1f);
                        }
                        if (Math.abs(showWidth) <= viewWidth) {
                            float alpha = (viewWidth - Math.abs(showWidth)) / viewWidth;
                            lastVisibleView.setAlpha(alpha);
                        }
                    }
                }
            }
            LogUtil.d("firstPos: " + firstVisiblePos + " comPos: " + firstCompleteVisiblePos);
            LogUtil.d("dx: " + dx + " dy: " + dy);

        }
    }
}
