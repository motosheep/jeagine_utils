package com.example.ktdemo.bitmap;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.Log;

/**
 * FileName: ImgDownloadWaterUtils
 * Author: lzt
 * Date: 2023/3/2 13:45
 */
public class ImgDownloadWaterUtils {
    /**
     * 下载图片添加水印--右下角
     */
    public Bitmap addWater(String filePath, Bitmap snapBitmap) throws Exception {
        Bitmap srcBitmap = BitmapFactory.decodeFile(filePath);
        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();
        int snapWidth = snapBitmap.getWidth();
        int snapHeight = snapBitmap.getHeight();

        int srcShortLine = Math.min(srcWidth, srcHeight);
        float scale = 4f;
        int snapLongLine = Math.max(snapHeight, snapWidth);
        float rate = 1f;
        if (srcShortLine / scale < snapLongLine) {
            rate = (srcShortLine / scale * 1.0f / snapLongLine);
        } else {
            rate = (snapLongLine * 1.0f / srcShortLine / scale);
        }
        Matrix matrix = new Matrix();
        matrix.postScale(rate, rate);
        Bitmap trainSnapBitmap = Bitmap.createBitmap(snapBitmap, 0, 0, snapWidth, snapHeight,
                matrix, true);

        //绘制水印
        Bitmap resultBitmap = Bitmap.createBitmap(srcWidth, srcHeight, Bitmap.Config.ARGB_8888);
        Canvas resultCanvas = new Canvas(resultBitmap);
        resultCanvas.drawBitmap(srcBitmap, 0, 0, new Paint());
        resultCanvas.drawBitmap(trainSnapBitmap,
                srcWidth - trainSnapBitmap.getWidth() - (srcShortLine / 21f),
                srcHeight - trainSnapBitmap.getHeight() - (srcShortLine / 21f),
                new Paint());
        resultCanvas.save();
        resultCanvas.restore();

        if (srcBitmap != null && !srcBitmap.isRecycled()) {
            srcBitmap.recycle();
        }
        if (trainSnapBitmap != null && !trainSnapBitmap.isRecycled()) {
            trainSnapBitmap.recycle();
        }


        return resultBitmap;
    }


    /**
     * 下载图片添加水印--右下角
     */
    public Bitmap addWater(Bitmap srcBitmap, String userTips, String appName, Bitmap logo) throws Exception {
        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();


        Bitmap resultBitmap = Bitmap.createBitmap(srcWidth, srcHeight, Bitmap.Config.ARGB_8888);
        Canvas resultCanvas = new Canvas(resultBitmap);
        resultCanvas.drawBitmap(srcBitmap, 0, 0, new Paint());

        int canvasWidth = resultCanvas.getWidth();
        int canvasHeight = resultCanvas.getHeight();

        Paint mPaint = new Paint();
        mPaint.setStyle(Paint.Style.FILL);
        mPaint.setStrokeWidth(1);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setColor(Color.WHITE);

        int shortLength = Math.min(canvasWidth, canvasHeight);
        int bottomTextSize = (int) (shortLength * 1.0f / 51);
        int appTextSize = (int) (shortLength * 1.0f / 28);
        //底部边距
        int bottomMargin = shortLength / 20;
        //右侧边距
        int endMargin = shortLength / 20;

        mPaint.setShadowLayer(2, 2, 2, Color.BLACK);

        mPaint.setTextSize(dp2px(bottomTextSize));

        if (!TextUtils.isEmpty(userTips)) {
            //底部文字宽度
            float bottomTextWidth = mPaint.measureText(userTips);
            //绘制底部文字
            resultCanvas.drawText(userTips, srcWidth - bottomTextWidth - endMargin,
                    srcHeight - getTextDescent((int) mPaint.getTextSize()) - bottomMargin, mPaint);

            //获取底部文字高度
            int bottomTipsHeight = (int) getTextHeight((int) mPaint.getTextSize());
            mPaint.setTextSize(dp2px(appTextSize));
            int appTextDescent = (int) getTextDescent((int) mPaint.getTextSize());
            float appTextWidth = mPaint.measureText(appName);
            float appStartX = srcWidth - appTextWidth - endMargin;
            float appStartY = srcHeight - bottomTipsHeight - appTextDescent - bottomMargin;
            resultCanvas.drawText(appName, appStartX, appStartY, mPaint);

            //加入图片--需要缩放到指定大小后，进行绘制
            if (logo != null) {
                float targetHeight = (int) getTextHeight((int) mPaint.getTextSize());
                int logoWidth = logo.getWidth();
                int logoHeight = logo.getHeight();
                float rate = targetHeight / logoHeight;
                Matrix matrix = new Matrix();
                matrix.postScale(rate, rate);
                Bitmap trainLogoBitmap = Bitmap.createBitmap(logo, 0, 0, logoWidth, logoHeight,
                        matrix, true);
                //trainLogoBitmap.getHeight() / 7.0f--视乎图片内边距进行微调
                float logoStartX = appStartX - trainLogoBitmap.getWidth();
                float logoStartY = srcHeight - bottomTipsHeight - bottomMargin -
                        trainLogoBitmap.getHeight() + trainLogoBitmap.getHeight() / 7.0f;
                resultCanvas.drawBitmap(trainLogoBitmap, logoStartX, logoStartY, new Paint());
            }
        }


        resultCanvas.save();
        resultCanvas.restore();

        return resultBitmap;
    }

    private int dp2px(int dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }

    /**
     * 获取字体底边距
     */
    private float getTextDescent(int size) {
        try {
            Paint paint = new Paint();
            paint.setTypeface(Typeface.DEFAULT);
            paint.setTextSize(size);
            Paint.FontMetrics fm = paint.getFontMetrics();
            return fm.descent;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 获取字体底边距
     */
    private float getTextHeight(int size) {
        try {
            Paint paint = new Paint();
            paint.setTypeface(Typeface.DEFAULT);
            paint.setTextSize(size);
            Paint.FontMetrics fm = paint.getFontMetrics();
            float height1 = fm.descent - fm.ascent;
            float height2 = fm.bottom - fm.top + fm.leading;
            float height3 = fm.bottom - fm.top;
            Log.d("BulletPaintUtils", "getTextHeight: height1 " + (height1) + " height2: " + height2 + " height3: " + height3);
            return height2;
        } catch (Exception e) {
            return 0;
        }
    }
}
