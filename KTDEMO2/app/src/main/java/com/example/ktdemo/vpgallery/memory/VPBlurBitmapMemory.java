package com.example.ktdemo.vpgallery.memory;

import android.graphics.Bitmap;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * FileName: VPBlurBitmapMemory
 * Author: Administrator
 * Date: 2023/6/8 15:53
 * Description:
 */
public class VPBlurBitmapMemory implements Serializable {
    private ConcurrentHashMap<String, Bitmap> picBlurInfo = new ConcurrentHashMap<>();

    private ConcurrentHashMap<String, Bitmap> picSourceInfo = new ConcurrentHashMap<>();


    private static class SingleHolder implements Serializable {
        public static VPBlurBitmapMemory mInstance = new VPBlurBitmapMemory();
    }

    public static VPBlurBitmapMemory getInstance() {
        return SingleHolder.mInstance;
    }


    public void putBlur(String url, Bitmap bitmap) {
        if (url == null || bitmap == null) {
            return;
        }
        picBlurInfo.put(url, bitmap);
    }

    public void clearBlur() {
        picBlurInfo.clear();
    }

    public Bitmap getBlur(String url) {
        if (url == null) {
            return null;
        }
        return picBlurInfo.get(url);
    }

    public void putSource(String url, Bitmap bitmap) {
        if (url == null || bitmap == null) {
            return;
        }
        picSourceInfo.put(url, bitmap);
    }

    public void clearSource() {
        picSourceInfo.clear();
    }

    public void addSource(ConcurrentHashMap<String, Bitmap> info) {
        if (info == null) {
            return;
        }
        for (Map.Entry<String, Bitmap> entry : info.entrySet()) {
            picSourceInfo.put(entry.getKey(), entry.getValue());
        }
    }

    public Bitmap getSource(String url) {
        if (url == null) {
            return null;
        }
        return picSourceInfo.get(url);
    }

    public ConcurrentHashMap<String, Bitmap> getPicBlurInfo() {
        return picBlurInfo;
    }

    public ConcurrentHashMap<String, Bitmap> getPicSourceInfo() {
        return picSourceInfo;
    }
}
