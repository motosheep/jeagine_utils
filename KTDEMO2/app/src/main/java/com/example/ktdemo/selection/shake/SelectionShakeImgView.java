package com.example.ktdemo.selection.shake;

import android.content.Context;
import android.util.AttributeSet;

/**
 *
 **/
public class SelectionShakeImgView extends androidx.appcompat.widget.AppCompatImageView {
    //缩小的时间
    private int TIME_REDUCE = 100;
    //放大的时间
    private int TIME_ADD = 100;


    public SelectionShakeImgView(Context context) {
        this(context, null);
    }

    public SelectionShakeImgView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SelectionShakeImgView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initParams();
    }

    private void initParams() {

    }

    @Override
    protected void onDetachedFromWindow() {
        clearAnimation();
        super.onDetachedFromWindow();
    }

    /**
     * 设置动画资源
     */
    public void setResWithAnim(int resId) {
        int totalTime = TIME_ADD * 2 + TIME_REDUCE * 2;
        animate().alpha(0).setDuration(1).withEndAction(new Runnable() {
            @Override
            public void run() {
                setImageResource(resId);
                animate().alpha(1).setDuration(totalTime).start();
            }
        }).start();
        animate().scaleX(0.8f).scaleY(0.8f).setDuration(TIME_ADD).withEndAction(new Runnable() {
            @Override
            public void run() {
                animate().scaleX(1f).scaleY(1f).setDuration(TIME_REDUCE).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        animate().scaleX(0.9f).scaleY(0.9f).setDuration(TIME_ADD).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                animate().scaleX(1f).scaleY(1f).setDuration(TIME_REDUCE).start();
                            }
                        }).start();
                    }
                }).start();
            }
        }).start();
    }

    /**
     * 不带动画地设置资源
     */
    public void setRes(int resId) {
        setImageResource(resId);
    }

}
