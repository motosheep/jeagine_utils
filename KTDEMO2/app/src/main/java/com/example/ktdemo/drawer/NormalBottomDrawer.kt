package com.example.ktdemo.drawer

import android.content.Context
import android.util.AttributeSet
import com.example.ktdemo.R

/**
 * FileName: NormalBottomDrawer
 * Author: lzt
 * Date: 2022/6/8 11:37
 * 普通样式的底部抽屉drawer
 */
class NormalBottomDrawer(context: Context?, attrs: AttributeSet?) :
    BaseBottomDrawerWidget(context, attrs) {

    override fun bgColor(): Int {
        return R.color.black
    }

    override fun canCancelInner(): Boolean {
        return true
    }

    override fun heightRate(): Float {
        return 1.6f
    }
}