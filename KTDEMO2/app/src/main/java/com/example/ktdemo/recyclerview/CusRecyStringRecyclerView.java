package com.example.ktdemo.recyclerview;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


/**
 * @Author: lzt
 * @CreateDate: 2021/7/30 10:30
 * @Version: 1.0
 */
public class CusRecyStringRecyclerView extends RecyclerView {


    public CusRecyStringRecyclerView(@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        LinearLayoutManager linearLayoutManager=
                new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, true);
        linearLayoutManager.setStackFromEnd(true);
        setLayoutManager(linearLayoutManager);
    }

    /**
     * 设置数据
     */
    public void initData() {
        CoorAdapter adapter = new CoorAdapter();
        List<String> da = new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            da.add(i + "");
        }
        adapter.mData = da;
        this.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static class CoorAdapter extends Adapter<CoorAdapter.CoorVH> {
        private List<String> mData = new ArrayList<>();

        @NotNull
        @Override
        public CoorVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new CoorVH(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.activity_cus_recy_test_item,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull CoorVH holder, int position) {
            String data = mData.get(position);

        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public static class CoorVH extends ViewHolder {

            public CoorVH(@NonNull @NotNull View itemView) {
                super(itemView);
            }
        }
    }
}
