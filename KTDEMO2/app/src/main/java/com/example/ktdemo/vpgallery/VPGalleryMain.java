package com.example.ktdemo.vpgallery;

import android.content.Context;
import android.graphics.Bitmap;
import androidx.annotation.NonNull;
import com.bumptech.glide.Glide;

import java.io.Serializable;

public class VPGalleryMain implements Serializable {


    /**
     * 外部调用时，需要重写获取网络bitmap的方法
     */
    public static Bitmap getUrlBitmap(@NonNull Context context, @NonNull String url) throws Exception {
        return Glide.with(context).asBitmap().load(url).submit().get();
    }
}
