package com.example.ktdemo

import android.app.Application
import com.example.ktdemo.ctxobserver.CtxObserverManager
import com.example.ktdemo.ctxobserver.bean.ObserverChangeInfo
import com.example.ktdemo.ctxobserver.listener.ObserverChangeListener
import com.north.light.libmusicplayerx.utils.KtLogUtil
import com.north.light.libmusicplayerx.utils.KtLogUtil.d

/**
 * FileName: App
 * Author: lzt
 * Date: 2023/3/9 12:42
 */
class App : Application() {
    override fun onCreate() {
        super.onCreate()
        d("app onCreate")



    }
}