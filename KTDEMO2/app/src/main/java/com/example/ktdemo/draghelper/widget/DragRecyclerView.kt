package com.example.ktdemo.draghelper.widget

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.north.light.libmusicplayerx.utils.KtLogUtil.d

/**
 * FileName: DragRecyclerView
 * Author: lzt
 * Date: 2023/3/31 11:03
 */
open class DragRecyclerView : RecyclerView {
    private var mListener: ScrollEventListener? = null

    //触摸变量
    private var mTouchDownX = 0
    private var mTouchDownY = 0
    private var mTouchMoveX = 0
    private var mTouchMoveY = 0

    //顶部是否完全显示
    private var mCompleteShowTopView = false

    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    private fun init() {
        addOnScrollListener(object : OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                //上拉就是负数，下拉就是正数
                val layoutManager = recyclerView.layoutManager
                if (layoutManager is LinearLayoutManager) {
                    val isCompleteShowFirst = (dy < 0 && getCompleteShowPos() == 0)
                    if (isCompleteShowFirst) {
                        //上拉到顶了
//                        mListener?.completeShowTop()
                    }
                }
            }
        })
    }

    /**
     * 获取第一个完全可见的位置
     * */
    private fun getCompleteShowPos(): Int {
        val currentLayoutManager = layoutManager
        if (currentLayoutManager is LinearLayoutManager) {
            return currentLayoutManager.findFirstCompletelyVisibleItemPosition()
        }
        return -1
    }

    /**
     * 第一个项目是完全可见
     * */
    private fun firstCompleteShow(): Boolean {
        return getCompleteShowPos() == 0
    }

    fun setScrollEventListener(listener: ScrollEventListener) {
        mListener = listener
    }

    override fun onTouchEvent(e: MotionEvent?): Boolean {
        val action = e?.action ?: return super.onTouchEvent(e)
        when (action) {
            MotionEvent.ACTION_DOWN -> {
                d("DragRecyclerView onTouchEvent: ACTION_DOWN")
                mTouchDownX = e.x.toInt()
                mTouchDownY = e.y.toInt()
                mCompleteShowTopView = false
                mListener?.completeShowTop(mCompleteShowTopView)
            }
            MotionEvent.ACTION_UP -> {
                d("DragRecyclerView onTouchEvent: ACTION_UP")
            }
            MotionEvent.ACTION_MOVE -> {
                d("DragRecyclerView onTouchEvent: ACTION_MOVE")
                mCompleteShowTopView = firstCompleteShow()
                if (firstCompleteShow()) {
                    d("DragRecyclerView onTouchEvent: 第一个item完成可见")
                    val currentY = e.y.toInt()
                    if (currentY > mTouchDownY) {
                        //下拉
                        //回调滑动距离
                        val interval = (currentY - mTouchDownY)
                        d("DragRecyclerView onTouchEvent: 第一个item完成可见 下拉: $interval")
                        mListener?.completeShowTop(mCompleteShowTopView)
                    } else if (currentY < mTouchDownY) {
                        //上拉
                        d("DragRecyclerView onTouchEvent: 第一个item完成可见 上拉")
                        mListener?.completeShowTop(false)
                    }
                    mTouchMoveX = e.x.toInt()
                    mTouchMoveY = e.y.toInt()
                }

            }
            MotionEvent.ACTION_CANCEL -> {
                d("DragRecyclerView onTouchEvent: ACTION_CANCEL")
            }
        }
        return super.onTouchEvent(e)
    }

    override fun onInterceptTouchEvent(e: MotionEvent?): Boolean {
        d("DragRecyclerView onInterceptTouchEvent: ${e?.action}")
        return super.onInterceptTouchEvent(e)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        d("DragRecyclerView dispatchTouchEvent: ${ev?.action}")
        return super.dispatchTouchEvent(ev)
    }

}

interface ScrollEventListener {
    fun completeShowTop(complete: Boolean)
}
