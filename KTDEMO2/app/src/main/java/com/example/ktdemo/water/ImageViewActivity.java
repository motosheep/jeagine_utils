package com.example.ktdemo.water;

import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;
import com.north.light.libltobserver.LTObserver;

public class ImageViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);

        RotateImageView rotate = findViewById(R.id.rotate);
        rotate.setImageResource(R.mipmap.ic_launcher);
        rotate.setOnAnimationListener(new RotateImageView.AnimationListener() {
            @Override
            public void start() {
                Log.d("rotate", "rotate view start");
            }

            @Override
            public void stop() {
                Log.d("rotate", "rotate view stop");
            }

            @Override
            public void repeat() {
                Log.d("rotate", "rotate view repeat");
            }
        });
        rotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rotate.isRotating()) {
                    rotate.setAutoRepeat(false);
                    rotate.stopAnim();
                } else {
                    rotate.setAutoRepeat(true);
                    rotate.startAnim();
                }
            }
        });

        WaterRippleView ripple = findViewById(R.id.ripple);
        ripple.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(ripple.isDrawing()){
                    ripple.stop();
                }else{
                    ripple.resume();
                }
            }
        });
        ripple.start(70 / 2, 3);
    }
}