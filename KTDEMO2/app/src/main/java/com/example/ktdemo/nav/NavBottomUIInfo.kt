package com.example.ktdemo.nav

import android.graphics.Bitmap
import java.io.Serializable

/**
 * FileName: NavigationUIInfo
 * Author: lzt
 * Date: 2022/5/10 15:05
 */
class NavBottomUIInfo : Serializable {
    var identify = NavBottomUIInfo::class.hashCode()
    var nameSel: String? = null
    var nameUnSel: String? = null
    var nameSelColor: Int = 0
    var nameUnSelColor: Int = 0
    var imgSel: Bitmap? = null
    var imgUnSel: Bitmap? = null

    //尺寸相关
    var nameTxSize = 12f
    var imgWH = 34
    var interval = 4

    //开关相关
    //文字是否显示
    var txtSwitch = true
    var imgSwitch = true
}