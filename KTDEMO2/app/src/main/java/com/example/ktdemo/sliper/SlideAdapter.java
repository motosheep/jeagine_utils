package com.example.ktdemo.sliper;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;
import com.north.light.libsliderecyclerview.SlideBaseAdapter;

import java.util.List;

/**
 * author:li
 * date:2022/9/17
 * desc:
 */
public class SlideAdapter extends SlideBaseAdapter<SlideAdapter.CoorVH, SlideObj> {

    @NonNull
    @Override
    public CoorVH onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new CoorVH(LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_slide_info, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull CoorVH holder, int position) {
        SlideObj data = getDataList().get(position);
        holder.mContent.setText(data.getTitle());
    }


    public void setData(List<SlideObj> mData) {
        setDataList(mData);
    }

    public void addData(List<SlideObj> mData) {
        addDataList(mData);
    }


    @Override
    public int getItemCount() {
        return getDataList().size();
    }

    public static class CoorVH extends RecyclerView.ViewHolder {
        private TextView mContent;

        public CoorVH(@NonNull View itemView) {
            super(itemView);
            mContent = itemView.findViewById(R.id.activity_coor_adapter_item_txt);
        }
    }
}
