package com.example.ktdemo.floatmenu;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.example.ktdemo.R;

public class FloatMenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_float_menu);
    }
}