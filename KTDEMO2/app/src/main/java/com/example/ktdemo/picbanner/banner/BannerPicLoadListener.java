package com.example.ktdemo.picbanner.banner;

import android.widget.ImageView;

/**
 * FileName: BannerPicLoadListener
 * Author: lzt
 * Date: 2022/10/17 11:51
 * 图片加载监听
 */
public interface BannerPicLoadListener {

    /**
     * 加载图片，并返回是否横竖屏标识
     */
    public void load(ImageView img, String url, BannerPicSizeCallback picSizeCallback);
}
