package com.example.ktdemo.vpgallery.cache;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import com.example.ktdemo.LogUtil;
import com.example.ktdemo.vpgallery.bean.VPGalleryInfo;
import com.example.ktdemo.vpgallery.memory.VPBlurBitmapMemory;
import com.example.ktdemo.vpgallery.utils.VPBlurUtils;
import com.example.ktdemo.vpgallery.utils.VPViewShotUtils;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 图片数据缓存Utils
 */
public class VPGalleryCacheImgUtils implements Serializable {

    private static class SingleHolder implements Serializable {
        public static VPGalleryCacheImgUtils mInstance = new VPGalleryCacheImgUtils();
    }

    public static VPGalleryCacheImgUtils getInstance() {
        return SingleHolder.mInstance;
    }


    /**
     * 开始任务
     */
    public void startTask(Context context, List<VPGalleryInfo> list,
                          @NonNull ImageView blurSnapView,
                          @NonNull VPGalleryCacheListener listener) {
        if (list == null || list.size() == 0) {
            listener.empty();
            return;
        }
        Map<String, String> urlMap = new HashMap<>();
        for (VPGalleryInfo info : list) {
            String url = info.getCover();
            if (!TextUtils.isEmpty(url)) {
                urlMap.put(url, url);
            }
        }
        if (urlMap.size() == 0) {
            listener.empty();
            return;
        }
        ConcurrentHashMap<String, Bitmap> picSourceInfo = new ConcurrentHashMap<>();
        AtomicInteger counter = new AtomicInteger(0);
        new Thread(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<String, String> entry : urlMap.entrySet()) {
                    String url = entry.getValue();
                    LogUtil.d("vp initData url: " + url);

                    try {
                        final Bitmap resource = listener.getBitmap(url);
                        counter.incrementAndGet();
                        if (resource != null) {
                            picSourceInfo.put(url, resource);
                        }
                        if (counter.get() == urlMap.size()) {
                            //完成
                            if (urlMap.size() == picSourceInfo.size()) {
                                //数据完整--进行设置到view且获取模糊
                                VPBlurBitmapMemory.getInstance().clearSource();
                                VPBlurBitmapMemory.getInstance().addSource(picSourceInfo);

                                //初始化毛玻璃
                                initBlurBitmap(picSourceInfo, blurSnapView, listener);
                            } else {
                                //数据不完整
                                startTask(context, list, blurSnapView, listener);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        counter.incrementAndGet();
                        if (counter.get() == urlMap.size()) {
                            //数据不完整
                            startTask(context, list, blurSnapView, listener);
                        }
                    }
                }
            }
        }).start();
    }

    /**
     * 毛玻璃初始化
     */
    private void initBlurBitmap(ConcurrentHashMap<String, Bitmap> info, @NonNull ImageView mIvBlurBuildView,
                                @NonNull VPGalleryCacheListener listener) {
        VPBlurBitmapMemory.getInstance().clearBlur();
        mIvBlurBuildView.post(new Runnable() {
            @Override
            public void run() {
                for (Map.Entry<String, Bitmap> entry : info.entrySet()) {
                    Bitmap bt = entry.getValue();
                    if (bt == null) {
                        continue;
                    }
                    mIvBlurBuildView.setImageBitmap(bt);
                    try {
                        Bitmap snapBt = VPViewShotUtils.viewSnapshot(mIvBlurBuildView);
                        final Bitmap blurBitmap = VPBlurUtils.blur(mIvBlurBuildView.getContext(), 23, snapBt);
                        VPBlurBitmapMemory.getInstance().putBlur(entry.getKey(), blurBitmap);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //已经完成了，重新设置adapter
                listener.finish(VPBlurBitmapMemory.getInstance().getPicSourceInfo(),
                        VPBlurBitmapMemory.getInstance().getPicSourceInfo());
            }
        });
    }


}
