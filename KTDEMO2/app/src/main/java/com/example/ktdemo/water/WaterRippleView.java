package com.example.ktdemo.water;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: WaterRippleView
 * Author: lzt
 * Date: 2022/10/19 15:06
 * 播放器水波纹控件
 */
public class WaterRippleView extends View {
    //画笔
    private Paint mPaint;


    //是否需要绘制
    private boolean needDrawing = false;
    //开始半径
    private int mStartRadius = 0;
    //结束半径
    private int mEndRadius = 0;
    //半径间距
    private float mRadiusInterval = 0f;
    //绘制数量
    private int mDrawCount = 3;

    //圆的半径数组
    private List<Integer> mRadiusList = new ArrayList<>();

    public WaterRippleView(Context context) {
        super(context);
        init();
    }

    public WaterRippleView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WaterRippleView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(1);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setTextAlign(Paint.Align.LEFT);
        mPaint.setTextSize(dp2px(12));
        mPaint.setColor(Color.RED);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        drawCircle(canvas);
    }

    public boolean isDrawing() {
        return needDrawing;
    }

    private void drawCircle(Canvas canvas) {
        //计算整理圆的半径集合
        List<Integer> drawList = new ArrayList<>();
        for (int i = 0; i < mRadiusList.size(); i++) {
            Integer radius = mRadiusList.get(i);
            if (radius <= mEndRadius) {
                drawList.add(radius);
            }
        }
        //判断是否需要添加一个数据了
        if (drawList.size() != 0) {
            Integer radius = drawList.get(0);
            if (radius != null && radius >= (mStartRadius + mRadiusInterval)) {
                drawList.add(0, (int) (radius - mRadiusInterval));
            }
        }

        mRadiusList.clear();

        //开始绘制
        int x = getMeasuredWidth() / 2;
        int y = getMeasuredHeight() / 2;
        for (int i = 0; i < drawList.size(); i++) {
            Integer radius = drawList.get(i);
            if (radius != null) {
                //画笔透明度计算
                float percent = (radius * 1.0f - mStartRadius) / (mRadiusInterval * mDrawCount);
                mPaint.setAlpha((int) (255 * (1 - percent)));
                canvas.drawCircle(x, y, radius, mPaint);

                //更新radius
                radius = radius + 2;
                mRadiusList.add(radius);
            }
        }

        if (needDrawing) {
            postInvalidateDelayed(20);
        }
    }


    protected int dp2px(int dpValue) {
        return (int) getContext().getResources().getDisplayMetrics().density * dpValue;
    }

    //外部调用--------------------------------------------------------------------------------

    /**
     * @param startRd 开始的半径
     * @param count   水波纹数量
     */
    public void start(int startRd, int count) {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                int startR = startRd;
                int endR = getMeasuredWidth() / 2;
                if (endR <= startR) {
                    return;
                }
                needDrawing = true;
                mStartRadius = startR;
                mEndRadius = endR;
                mDrawCount = count;
                //计算所有数组
                mRadiusList.clear();
                mRadiusInterval = ((mEndRadius - mStartRadius) * 1.0f) / (count * 1.0f);
                for (int i = 0; i < count; i++) {
                    mRadiusList.add((int) (mStartRadius + (i * mRadiusInterval)));
                }
                postInvalidate();
            }
        }, 20);
    }

    /**
     * 恢复
     */
    public void resume() {
        postDelayed(new Runnable() {
            @Override
            public void run() {
                needDrawing = true;
                postInvalidate();
            }
        }, 20);
    }

    /**
     * 停止
     */
    public void stop() {
        needDrawing = false;
        postInvalidate();
    }

}
