package com.example.ktdemo.nav

import android.graphics.Color
import androidx.annotation.DrawableRes
import com.example.ktdemo.R
import java.io.Serializable

/**
 * FileName: NavigationPointInfo
 * Author: lzt
 * Date: 2022/5/11 15:07
 * 红点数据
 */
class NavBottomUnreadInfo : Serializable {

    enum class MESSAGE_TYPE {
        POINT, NUMBER
    }

    //消息类型
    var msgType: MESSAGE_TYPE = MESSAGE_TYPE.POINT

    //消息数量
    var msgCount: Int = 0

    //消息背景色
    @DrawableRes
    var msgBgColor: Int = R.drawable.ic_launcher_foreground

    //消息文字颜色
    var msgTxColor: Int = Color.WHITE

    //消息文字大小
    var msgTxSize: Float = 12f
}