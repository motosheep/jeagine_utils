package com.example.ktdemo.exerciseselection.selectionprogressbar.v2;


import com.example.ktdemo.R;

import java.io.Serializable;

/**
 * Author: lzt
 * Date: 2022/7/7 9:06
 * 自定义进度条参数
 * !!!注意status的值!!!
 * change by lzt 20221021 增加一个外部传入的高度值参数
 */
public class SelectionProBarInfoV2 implements Serializable {
    //进度条默认背景色--resource color
    private int bgColor = R.color.color_selection_progress_default_bg_v2;
    //status == 1的背景色
    private int unSelBgColor = R.color.color_selection_progress_boarder_v2;
    //status == 2的背景色
    private int selectBgColor = R.color.color_selection_progress_boarder_v2;
    private int selectFillBgColor = R.color.color_selection_progress_sel_bg_v2;
    //进度条颜色--status==2
    private int progressSelColor = R.color.color_selection_progress_sel_v2;
    //进度条颜色--status==1
    private int progressUnSelColor = R.color.color_selection_progress_un_sel_v2;
    //发光区域颜色--status==2
    private int selectShadowColor = R.color.color_selection_progress_shadow_v2;
    //发光区域颜色--status==2
    private int unSelectShadowColor = R.color.color_selection_progress_shadow_v2;
    //发光区域半径--px
    private int shadowRadius = 12;
    //进度条圆角
    private int radius = 6;
    //圆角修正
    private int radiusSplit = 1;
    //进度--最大100
    private int progress = 0;
    //状态：0默认状态(只有背景,不会绘制进度条) 1非选中(有进度) 2选中(有进度)
    private int status = 0;
    //边框粗细--px
    private int boardSize = 4;


    //自定义参数-------------------------------------------------------------------


    //自定义高度
    private int mCusHeight = -1;


    public int getRadiusSplit() {
        return radiusSplit;
    }

    public void setRadiusSplit(int radiusSplit) {
        this.radiusSplit = radiusSplit;
    }

    public int getBoardSize() {
        return boardSize;
    }

    public void setBoardSize(int boardSize) {
        this.boardSize = boardSize;
    }

    public int getSelectFillBgColor() {
        return selectFillBgColor;
    }

    public void setSelectFillBgColor(int selectFillBgColor) {
        this.selectFillBgColor = selectFillBgColor;
    }

    public int getShadowRadius() {
        return shadowRadius;
    }

    public void setShadowRadius(int shadowRadius) {
        this.shadowRadius = shadowRadius;
    }

    public int getSelectShadowColor() {
        return selectShadowColor;
    }

    public void setSelectShadowColor(int selectShadowColor) {
        this.selectShadowColor = selectShadowColor;
    }

    public int getUnSelectShadowColor() {
        return unSelectShadowColor;
    }

    public void setUnSelectShadowColor(int unSelectShadowColor) {
        this.unSelectShadowColor = unSelectShadowColor;
    }

    public int getmCusHeight() {
        return mCusHeight;
    }

    public void setmCusHeight(int mCusHeight) {
        this.mCusHeight = mCusHeight;
    }

    public int getCusHeight() {
        return mCusHeight;
    }

    public void setCusHeight(int mCusHeight) {
        this.mCusHeight = mCusHeight;
    }

    public int getProgressUnSelColor() {
        return progressUnSelColor;
    }

    public void setProgressUnSelColor(int progressUnSelColor) {
        this.progressUnSelColor = progressUnSelColor;
    }

    public int getUnSelBgColor() {
        return unSelBgColor;
    }

    public void setUnSelBgColor(int unSelBgColor) {
        this.unSelBgColor = unSelBgColor;
    }

    public int getSelectBgColor() {
        return selectBgColor;
    }

    public void setSelectBgColor(int selectBgColor) {
        this.selectBgColor = selectBgColor;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }


    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public int getProgressSelColor() {
        return progressSelColor;
    }

    public void setProgressSelColor(int progressColor) {
        this.progressSelColor = progressColor;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

}
