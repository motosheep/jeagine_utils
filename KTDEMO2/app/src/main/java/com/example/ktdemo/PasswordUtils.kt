package com.example.ktdemo

import android.text.TextUtils
import java.io.Serializable
import java.util.regex.Pattern

/**
 * FileName: PasswordUtils
 * Author: lzt
 * Date: 2022/8/18 16:02
 */
class PasswordUtils : Serializable {
    enum class PWD_TYPE {
        WEEK, NORMAL, STRONG
    }

    companion object {

        @JvmStatic
        fun getInstance(): PasswordUtils {
            return SingleHolder.instance
        }
    }

    private object SingleHolder : Serializable {
        val instance = PasswordUtils()
    }

    //Z = 字母 S = 数字 T = 特殊字符
    fun checkPassword(passwordStr: String): PWD_TYPE {
        if (TextUtils.isEmpty(passwordStr)) {
            return PWD_TYPE.WEEK
        }
        val regexZ = "\\d*".toRegex()
        val regexS = "[a-zA-Z]+".toRegex()
        val regexT = "\\W+$".toRegex()
        val regexZT = "\\D*".toRegex()
        val regexST = "[\\d\\W]*".toRegex()
        val regexZS = "\\w*".toRegex()
        val regexZST = "[\\w\\W]*".toRegex()
        if (passwordStr.matches(regexZ)) {
            return PWD_TYPE.WEEK
        }
        if (passwordStr.matches(regexS)) {
            return PWD_TYPE.WEEK
        }
        if (passwordStr.matches(regexT)) {
            return PWD_TYPE.WEEK
        }
        if (passwordStr.matches(regexZT)) {
            return PWD_TYPE.NORMAL
        }
        if (passwordStr.matches(regexST)) {
            return PWD_TYPE.NORMAL
        }
        if (passwordStr.matches(regexZS)) {
            return PWD_TYPE.NORMAL
        }
        return if (passwordStr.matches(regexZST)) {
            PWD_TYPE.STRONG
        } else PWD_TYPE.WEEK
    }

    /**
     * 是否包括中文
     *
     * @param content
     * @return
     */
    private fun isHasChinese(content: String?): Boolean {
        val regEx = "[\u4e00-\u9fa5]"
        val pattern = Pattern.compile(regEx)
        val matcher = pattern.matcher(content)
        while (matcher.find()) {
            return true
        }
        return false
    }


    /**
     * 判断密码输入是否合适
     * 必须只含有特殊字符、数字和字母不含中文，6-20位
     */
    fun judge(pwd: String?): Boolean {
        if (pwd.isNullOrBlank()) {
            return false
        }
        val regex = "^(?!.*[！·（）{}【】“”：；，》￥、。‘’——\\s-……%\\n])(?=.*[a-zA-Z])(?=.*\\d)(?=.*[~!@#\$%^&*()_+`\\-={}:\\\";'<>?,.\\/])[^\\u4e00-\\u9fa5]{6,20}\$".toRegex()
        return regex.matches(pwd)
    }


}