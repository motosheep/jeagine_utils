package com.example.ktdemo.span;

import com.example.ktdemo.span.view.SpanViewInfo;

/**
 * author:li
 * date:2022/10/23
 * desc:
 */
public class SpanExtInfo extends SpanViewInfo {
    private String ext = "yes";

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }
}
