package com.example.ktdemo.picbanner.indicate;

import com.example.ktdemo.R;

import java.io.Serializable;

/**
 * FileName: IndicateInfo
 * Author: lzt
 * Date: 2022/10/18 13:58
 * 指示器信息
 */
public class IndicateInfo implements Serializable {

    //选中背景
    private int selBgRes = R.drawable.shape_indeicate_sel_bg;

    //没有选中背景
    private int unSelBgRes = R.drawable.shape_indeicate_un_sel_bg;

    //控件长度--宽高一致
    private int size = 40;

    //总个数
    private int total = 0;

    //间距
    private int interval = 10;

    public int getInterval() {
        return interval;
    }

    public void setInterval(int interval) {
        this.interval = interval;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSelBgRes() {
        return selBgRes;
    }

    public void setSelBgRes(int selBgRes) {
        this.selBgRes = selBgRes;
    }

    public int getUnSelBgRes() {
        return unSelBgRes;
    }

    public void setUnSelBgRes(int unSelBgRes) {
        this.unSelBgRes = unSelBgRes;
    }
}
