package com.example.ktdemo.sharemotion;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.app.ActivityOptionsCompat;
import androidx.core.util.Pair;

import com.example.ktdemo.R;

/**
 * 共享元素
 */
public class ShareMotionActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.activity_share_motion);


    }

    public void jump(View view) {
        Intent intent = new Intent(this, ShareMotionDetailActivity.class);
        @SuppressWarnings("unchecked")
        ActivityOptionsCompat activityOptions = ActivityOptionsCompat.makeSceneTransitionAnimation(
                this, new Pair<>(findViewById(R.id.ivImg), "ddd")
        );
        ActivityCompat.startActivity(this, intent, activityOptions.toBundle());
    }
}