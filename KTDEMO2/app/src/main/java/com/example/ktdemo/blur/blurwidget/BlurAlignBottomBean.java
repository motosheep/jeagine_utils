package com.example.ktdemo.blur.blurwidget;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * FileName: BlurAlignBottomBean
 * Author: lzt
 * Date: 2023/5/10 13:03
 * 底部模糊传入对象
 */
public class BlurAlignBottomBean implements Serializable {

    //模糊对象bitmap
    private Bitmap srcBitmap;
    //模糊图片需要显示高度
    private int blurHeight;
    //圆角
    private float radius = 0f;

    public boolean check(){
        if(srcBitmap == null || srcBitmap.isRecycled()){
            return false;
        }
        return true;
    }

    public Bitmap getSrcBitmap() {
        return srcBitmap;
    }

    public void setSrcBitmap(Bitmap srcBitmap) {
        this.srcBitmap = srcBitmap;
    }

    public int getBlurHeight() {
        return blurHeight;
    }

    public void setBlurHeight(int blurHeight) {
        this.blurHeight = blurHeight;
    }

    public float getRadius() {
        return radius;
    }

    public void setRadius(float radius) {
        this.radius = radius;
    }
}
