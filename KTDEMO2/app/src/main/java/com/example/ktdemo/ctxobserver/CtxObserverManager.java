package com.example.ktdemo.ctxobserver;

import android.content.Context;
import android.os.Handler;

import com.example.ktdemo.ctxobserver.api.ContentObserverApi;
import com.example.ktdemo.ctxobserver.listener.ObserverChangeListener;
import com.example.ktdemo.ctxobserver.media.MediaPicContentObserver;
import com.example.ktdemo.ctxobserver.media.MediaVideoContentObserver;

import java.io.Serializable;

/**
 * FileName: CtxObserverManager
 * Author: lzt
 * Date: 2023/4/28 10:49
 * 观察者管理类
 */
public class CtxObserverManager implements Serializable {


    /**
     * 获取图片观察者
     */
    public static ContentObserverApi getPicObserver(Context context,
                                                    Handler handler,
                                                    ObserverChangeListener listener) {
        return new MediaPicContentObserver(context, handler, listener);
    }

    /**
     * 获取视频观察者
     */
    public static ContentObserverApi getVideoObserver(Context context,
                                                      Handler handler,
                                                      ObserverChangeListener listener) {
        return new MediaVideoContentObserver(context, handler, listener);
    }


}
