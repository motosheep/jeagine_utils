package com.example.ktdemo.scrollviewtest;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;

import com.example.ktdemo.R;

public class ScrollviewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrollview_test);

        NestedScrollView nsv = findViewById(R.id.nsvTest);
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup.LayoutParams nsvP = nsv.getLayoutParams();
                nsvP.height = 600;
                nsv.setLayoutParams(nsvP);
            }
        }, 2000);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(4000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        ViewGroup.LayoutParams nsvP = nsv.getLayoutParams();
                        nsvP.height = ViewGroup.LayoutParams.WRAP_CONTENT;
                        nsv.setLayoutParams(nsvP);
                    }
                });
            }
        }).start();
    }
}