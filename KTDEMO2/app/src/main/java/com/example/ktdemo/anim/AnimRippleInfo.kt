package com.example.ktdemo.anim

import android.graphics.Color
import java.io.Serializable

/**
 * FileName: AnimRippleInfo
 * Author: lzt
 * Date: 2023/3/13 10:24
 */
class AnimRippleInfo : Serializable {

    //圆圈颜色
    var circleColor = Color.BLUE

    //水波纹颜色
    var rippleColor = Color.BLUE

    //中心圆大小占比
    var circleRadiusRate = 0.6f


}