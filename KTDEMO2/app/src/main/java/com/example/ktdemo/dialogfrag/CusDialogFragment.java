package com.example.ktdemo.dialogfrag;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ktdemo.R;

import org.jetbrains.annotations.NotNull;

/**
 * FileName: CusDialogFragment
 * Author: lzt
 * Date: 2022/8/30 17:34
 */
public class CusDialogFragment extends BaseDialogFragment {


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_cus_dialog;
    }

    @Override
    protected void initViews(View v) {
        Log.d("dd", "dd initViews");

    }



    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.d("dd", "dd onDestroyView");

    }


}
