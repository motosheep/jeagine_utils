package com.example.ktdemo.exerciseselection.selectionprogressbar;



import com.example.ktdemo.R;

import java.io.Serializable;

/**
 * Author: lzt
 * Date: 2022/7/7 9:06
 * 自定义进度条参数
 * !!!注意status的值!!!
 * change by lzt 20221021 增加一个外部传入的高度值参数
 */
public class SelectionProBarInfo implements Serializable {
    //进度条默认背景色--resource color
    private int bgColor = R.color.color_selection_progress_default_bg;
    //status == 1的背景色
    private int unSelBgColor = R.color.color_selection_progress_un_sel_bg;
    //status == 2的背景色
    private int selectBgColor = R.color.color_selection_progress_sel_bg;
    //进度条颜色--status==2
    private int progressSelColor = R.color.color_selection_progress_sel;
    //进度条颜色--status==1
    private int progressUnSelColor = R.color.color_selection_progress_un_sel;
    //水波纹颜色
    private int rippleColor = R.color.color_selection_progress_sel;
    //进度条圆角
    private int radius = 8;
    //进度--最大100
    private int progress = 0;
    //状态：0默认状态(只有背景,不会绘制进度条) 1非选中(有进度) 2选中(有进度)
    private int status = 0;
    //选中情况下，点击的坐标
    private float selectTouchX = 0;
    private float selectTouchY = 0;

    //自定义高度
    private int mCusHeight = -1;

    public int getCusHeight() {
        return mCusHeight;
    }

    public void setCusHeight(int mCusHeight) {
        this.mCusHeight = mCusHeight;
    }

    public int getProgressUnSelColor() {
        return progressUnSelColor;
    }

    public void setProgressUnSelColor(int progressUnSelColor) {
        this.progressUnSelColor = progressUnSelColor;
    }

    public int getRippleColor() {
        return rippleColor;
    }

    public void setRippleColor(int rippleColor) {
        this.rippleColor = rippleColor;
    }

    public float getSelectTouchX() {
        return selectTouchX;
    }

    public void setSelectTouchX(float selectTouchX) {
        this.selectTouchX = selectTouchX;
    }

    public float getSelectTouchY() {
        return selectTouchY;
    }

    public void setSelectTouchY(float selectTouchY) {
        this.selectTouchY = selectTouchY;
    }

    public int getUnSelBgColor() {
        return unSelBgColor;
    }

    public void setUnSelBgColor(int unSelBgColor) {
        this.unSelBgColor = unSelBgColor;
    }

    public int getSelectBgColor() {
        return selectBgColor;
    }

    public void setSelectBgColor(int selectBgColor) {
        this.selectBgColor = selectBgColor;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getProgress() {
        return progress;
    }

    public void setProgress(int progress) {
        this.progress = progress;
    }


    public int getBgColor() {
        return bgColor;
    }

    public void setBgColor(int bgColor) {
        this.bgColor = bgColor;
    }

    public int getProgressSelColor() {
        return progressSelColor;
    }

    public void setProgressSelColor(int progressColor) {
        this.progressSelColor = progressColor;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

}
