package com.example.ktdemo.guide.impl;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.ktdemo.R;
import com.example.ktdemo.guide.base.GuideBaseFragment;
import com.example.ktdemo.guide.widget.GuideBottomImageView;

/**
 * FileName: GuideDFragment
 * Author: lzt
 * Date: 2023/6/30 14:32
 * Description:
 */
public class GuideDFragment extends GuideBaseFragment {

    public static GuideDFragment newInstance(int resId) {
        GuideDFragment fragment = new GuideDFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUILDER_STR_RES_ID, resId);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_image_guide_d;
    }

    @Override
    protected void initViews(View root) {
        Bundle bundle = getArguments();
        int resId = R.mipmap.ic_guilde_4;
        if (bundle != null) {
            resId = getArguments().getInt(BUILDER_STR_RES_ID, R.mipmap.ic_guilde_4);
        }
        TextView tvFinish = root.findViewById(R.id.tvFinish);
        GuideBottomImageView vImage = root.findViewById(R.id.vImage);
        vImage.setImageResource(resId);
        tvFinish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
