package com.example.ktdemo.span;

import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.example.ktdemo.TestObj;
import com.example.ktdemo.span.view.RoundBackgroundColorSpan;
import com.example.ktdemo.span.view.SpanTextView;
import com.example.ktdemo.span.view.SpanTextViewListener;
import com.example.ktdemo.span.view.SpanViewInfo;
import com.google.gson.Gson;
import com.north.light.libltobserver.LTObserver;

import java.util.ArrayList;
import java.util.List;

public class SpanTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_span_test);


        SpanTextView tvSpan = findViewById(R.id.tvSpan);

        List<SpanViewInfo> sInfo = new ArrayList<>();

        SpanExtInfo pSpan0 = new SpanExtInfo();
        SpanViewInfo.PicInfo picInfo0 = new SpanViewInfo.PicInfo();
        picInfo0.setWidth(60);
        picInfo0.setHeight(60);
        picInfo0.setPicId(R.drawable.video_icon_like1_pre);
        pSpan0.setType(1);
        pSpan0.setPic(picInfo0);
        sInfo.add(pSpan0);


        //文字
        SpanViewInfo tSpan = new SpanViewInfo();
        SpanViewInfo.TxtInfo txtInfo = new SpanViewInfo.TxtInfo();
        txtInfo.setText("加粗哈哈 默认文字哈哈 默认文字");
        txtInfo.setColor(getResources().getColor(R.color.purple_500));
        txtInfo.setSize(50);
        txtInfo.setBold(true);
        tSpan.setType(0);
        tSpan.setText(txtInfo);
        sInfo.add(tSpan);


        SpanViewInfo sSpan = new SpanViewInfo();
        SpanViewInfo.TxtInfo txtInfo2 = new SpanViewInfo.TxtInfo();
        txtInfo2.setText("第二个哈哈 默认文字哈哈  默认文文");
        txtInfo2.setColor(getResources().getColor(R.color.purple_200));
        txtInfo2.setSize(30);
        sSpan.setType(0);
        sSpan.setText(txtInfo2);
        sInfo.add(sSpan);


        //图片
        SpanViewInfo pSpan = new SpanViewInfo();
        SpanViewInfo.PicInfo picInfo = new SpanViewInfo.PicInfo();
        picInfo.setWidth(80);
        picInfo.setHeight(80);
        picInfo.setPicId(R.drawable.video_icon_like1_pre);
        pSpan.setType(1);
        pSpan.setPic(picInfo);
        sInfo.add(pSpan);


        SpanViewInfo sSpan3 = new SpanViewInfo();
        SpanViewInfo.TxtInfo txtInfo3 = new SpanViewInfo.TxtInfo();
        txtInfo3.setText("加粗开始第三个哈哈 默认文字哈哈  默认文文默认文字哈\n");
        txtInfo3.setColor(getResources().getColor(R.color.purple_200));
        txtInfo3.setSize(20);
        txtInfo3.setBold(true);
        sSpan3.setType(0);
        sSpan3.setText(txtInfo3);
        sInfo.add(sSpan3);

        tvSpan.setInfo(sInfo);

        tvSpan.setSpanTextViewListener(new SpanTextViewListener() {
            @Override
            public void LongClick(View v) {
                LogUtil.d("span tv:  长按事件");
            }

            @Override
            public <T extends SpanViewInfo> void click(View v, T info) {
                LogUtil.d("span tv:  点击事件： " + (new Gson().toJson(info)));
            }
        });


        SpannableString spanString = new SpannableString("答案：A");
        RoundBackgroundColorSpan roundBackgroundColorSpan = new
                RoundBackgroundColorSpan(
                getResources().getColor(R.color.white)
                , getResources().getColor(R.color.black), 8, 28, 30, true);
        spanString.setSpan(roundBackgroundColorSpan, 0, spanString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);

        tvSpan.append(spanString);

        tvSpan.append("后面有人");

    }

}