package com.example.ktdemo.water;

import android.animation.ValueAnimator;
import android.content.Context;
import android.util.AttributeSet;
import android.view.animation.LinearInterpolator;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;


/**
 * FileName: RotateImageView
 * Author: lzt
 * Date: 2022/10/19 13:42
 * 自动转动的圆形ImageView--调用startAnim,stopAnim时候，必须调用setAutoRepeat，否则有意想不到的效果
 */
public class RotateImageView extends androidx.appcompat.widget.AppCompatImageView {

    private AnimationListener mListener;
    private int mRotateAngle = 0;

    //是否旋转中
    private boolean isRotating = false;

    //是否自动循环
    private boolean autoRepeat = true;

    //循环一次的时间
    private final long duration = 8000;

    public RotateImageView(@NonNull Context context) {
        super(context);
        init();
    }

    public RotateImageView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public RotateImageView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
    }


    public void setAutoRepeat(boolean autoRepeat) {
        this.autoRepeat = autoRepeat;
    }

    //旋转相关-------------------------------------------------------------------------------------


    public boolean isRotating() {
        return isRotating;
    }

    public void startAnim(boolean autoRepeat) {
        setAutoRepeat(autoRepeat);
        startAnim();
    }

    /**
     * 开始旋转
     */
    public void startAnim() {
        clearAnimation();
        if (mRotateAngle >= 360) {
            mRotateAngle = mRotateAngle % 360;
        }
        setRotation(mRotateAngle);
        animate().rotation(mRotateAngle + 360).setDuration(duration)
                .setInterpolator(new LinearInterpolator())
                .setUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        mRotateAngle = (int) getRotation();
                    }
                })
                .withStartAction(new Runnable() {
                    @Override
                    public void run() {
                        isRotating = true;
                        if (mListener != null) {
                            mListener.start();
                        }
                    }
                })
                .withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        if (autoRepeat) {
                            isRotating = false;
                            if (mListener != null) {
                                mListener.repeat();
                            }
                            startAnim();
                        } else {
                            isRotating = false;
                            if (mListener != null) {
                                mListener.stop();
                            }
                        }
                    }
                });
    }


    public void stopAnim(boolean autoRepeat) {
        setAutoRepeat(autoRepeat);
        stopAnim();
    }

    /**
     * 停止旋转
     */
    public void stopAnim() {
        animate().cancel();
        clearAnimation();
        isRotating = false;
        setRotation(mRotateAngle);
        if (mListener != null) {
            mListener.stop();
        }
    }

    public void release() {
        setAutoRepeat(false);
        stopAnim();
        removeAnimationListener();
        isRotating = false;
    }


    @Override
    protected void onDetachedFromWindow() {
        release();
        super.onDetachedFromWindow();
    }


    /**
     * 设置监听
     */
    public void setOnAnimationListener(AnimationListener listener) {
        this.mListener = listener;
    }

    /**
     * 移除监听
     */
    public void removeAnimationListener() {
        this.mListener = null;
    }

    /**
     * 动画监听
     */
    public interface AnimationListener {
        void start();

        void stop();

        void repeat();
    }

}
