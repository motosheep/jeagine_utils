package com.example.ktdemo.sliper;

/**
 * FileName: SlideObj
 * Author: lzt
 * Date: 2022/9/19 10:07
 */
public class SlideObj {

    private String title = "da";

    public SlideObj(String value) {
        this.title = value;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
