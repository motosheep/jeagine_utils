package com.example.ktdemo.exerciseselection;

import android.content.Context;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

/**
 * FileName: SelectionRelativelayout
 * Author: lzt
 * Date: 2022/10/9 17:09
 * 做题选项根布局--内含recyclerview以及相关的动画view
 */
public class SelectionRelativelayout extends ConstraintLayout implements SelectionRelativelayoutApi {

    private SelectionRecyclerview mRecyclerview;
    private SelectionAnimView mDropPicAnimView;

    //监听事件
    private SelectionRelativelayoutListener mListener;

    //传入的adapter position
    private int mAdapterPosition;


    public SelectionRelativelayout(Context context) {
        super(context);
        init();
    }

    public SelectionRelativelayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SelectionRelativelayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * 初始化控件
     */
    private void init() {
        mRecyclerview = new SelectionRecyclerview(getContext());
        mDropPicAnimView = new SelectionAnimView(getContext());

        addView(mRecyclerview);
        LayoutParams recyclerviewParams = (LayoutParams) mRecyclerview.getLayoutParams();
        recyclerviewParams.topToTop = LayoutParams.PARENT_ID;
        recyclerviewParams.bottomToBottom = LayoutParams.PARENT_ID;
        recyclerviewParams.startToStart = LayoutParams.PARENT_ID;
        recyclerviewParams.endToEnd = LayoutParams.PARENT_ID;
        recyclerviewParams.width = 0;
        recyclerviewParams.height = LayoutParams.WRAP_CONTENT;
        mRecyclerview.setLayoutParams(recyclerviewParams);
        mRecyclerview.setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        addView(mDropPicAnimView);
        LayoutParams animParams = (LayoutParams) mDropPicAnimView.getLayoutParams();
        animParams.topToTop = LayoutParams.PARENT_ID;
        animParams.bottomToBottom = LayoutParams.PARENT_ID;
        animParams.startToStart = LayoutParams.PARENT_ID;
        animParams.endToEnd = LayoutParams.PARENT_ID;
        animParams.width = 0;
        animParams.height = 0;
        mDropPicAnimView.setLayoutParams(animParams);


        //监听事件
        mDropPicAnimView.setAnimEventListener(null);
        mDropPicAnimView.setAnimEventListener(new SelectionAnimView.AnimEventListener() {
            @Override
            public void start() {
                notifyDropPicAnimStart(mAdapterPosition);
            }

            @Override
            public void finish() {
                notifyDropPicAnimEnd(mAdapterPosition);
            }
        });
    }


    @Override
    public SelectionRecyclerview getRecyclerView() {
        return mRecyclerview;
    }

    @Override
    public SelectionAnimView getAnimView() {
        return mDropPicAnimView;
    }


    @Override
    public void startDropPicAnim(SelectionAnimInfo info, int adapterPosition) {
        if (info == null || mDropPicAnimView == null) {
            return;
        }
        this.mAdapterPosition = adapterPosition;
        this.mDropPicAnimView.startAnim(info);
    }

    //监听相关-----------------------------------------------------------------------------


    @Override
    public void setSelectionDropPicListener(SelectionRelativelayoutListener listener) {
        this.mListener = listener;
    }

    @Override
    public void removeSelectionDropPicListener() {
        this.mListener = null;
    }

    private void notifyDropPicAnimStart(int adapterPosition) {
        if (this.mListener != null) {
            this.mListener.startDropPicAnim(adapterPosition);
        }
    }

    private void notifyDropPicAnimEnd(int adapterPosition) {
        if (this.mListener != null) {
            this.mListener.endDropPicAnim(adapterPosition);
        }
    }
}
