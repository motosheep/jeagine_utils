package com.example.ktdemo.animlogo;

import android.view.View;
import android.widget.ImageView;

import java.io.Serializable;

/**
 * FileName: DoExamAnimUtils
 * Author: lzt
 * Date: 2022/8/2 上午 11:14
 */
public class DoExamAnimUtils implements Serializable {

    public DoExamAnimUtils() {
    }


    /**
     * 传入状态和控件
     */
    public void startAnim(ImageView view, boolean oldVis, boolean latestVis, int translationY) {
        if (view == null) {
            return;
        }
        if (oldVis == latestVis) {
            return;
        }
        view.clearAnimation();
        view.post(new Runnable() {
            @Override
            public void run() {
                //初始化状态
                view.setVisibility((oldVis) ? View.VISIBLE : View.INVISIBLE);
                //开始做动画
                if (latestVis) {
                    view.setVisibility(View.VISIBLE);
                    view.animate().translationY(-translationY).withEndAction(new Runnable() {
                        @Override
                        public void run() {

                        }
                    }).start();
                } else {
                    view.animate().translationY(0).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            view.setVisibility(View.INVISIBLE);
                        }
                    }).start();
                }
            }
        });
    }
}
