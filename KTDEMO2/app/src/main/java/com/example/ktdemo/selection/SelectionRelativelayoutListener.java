package com.example.ktdemo.selection;

/**
 * FileName: SelectionRelativelayoutListener
 * Author: lzt
 * Date: 2022/10/9 17:23
 * 监听
 */
public interface SelectionRelativelayoutListener {

    /**
     * 开始动画
     *
     * @param adapterPosition*/
    public void startAnim(int adapterPosition);

    /**
     * 完成动画
     *
     * @param adapterPosition*/
    public void endAnim(int adapterPosition);
}
