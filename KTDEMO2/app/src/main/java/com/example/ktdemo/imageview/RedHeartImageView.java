package com.example.ktdemo.imageview;

import android.content.Context;
import android.util.AttributeSet;

/**
 * 红心点赞ImageView
 * 宽度和高度必须一样
 **/
public class RedHeartImageView extends androidx.appcompat.widget.AppCompatImageView {
    //缩小的时间
    private int TIME_REDUCE = 200;
    //放大的时间
    private int TIME_ADD = 200;


    public RedHeartImageView(Context context) {
        this(context, null);
    }

    public RedHeartImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RedHeartImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initParams();
    }

    private void initParams() {

    }

    @Override
    protected void onDetachedFromWindow() {
        clearAnimation();
        super.onDetachedFromWindow();
    }

    /**
     * 设置动画资源
     */
    public void setResWithAnim(int resId) {
        setImageResource(resId);
        animate().scaleX(1.2f).scaleY(1.2f).setDuration(TIME_ADD).withEndAction(new Runnable() {
            @Override
            public void run() {
                animate().scaleX(0.9f).scaleY(0.9f).setDuration(TIME_REDUCE).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        animate().scaleX(1.2f).scaleY(1.2f).setDuration(TIME_ADD).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                animate().scaleX(1f).scaleY(1f).setDuration(TIME_REDUCE).start();
                            }
                        }).start();
                    }
                }).start();
            }
        }).start();
    }

    /**
     * 不带动画地设置资源
     * */
    public void setRes(int resId){
        setImageResource(resId);
    }

}
