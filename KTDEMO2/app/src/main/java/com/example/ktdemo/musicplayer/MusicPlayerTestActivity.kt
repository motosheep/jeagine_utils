package com.example.ktdemo.musicplayer

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.ktdemo.R
import com.google.gson.Gson
import com.north.light.libmusiccontroller.MusicController
import com.north.light.libmusiccontroller.bean.MusicPlayInfo
import com.north.light.libmusiccontroller.bean.MusicPlayObj
import com.north.light.libmusiccontroller.listener.MusicEventListener
import com.north.light.libmusiccontroller.listener.MusicProgressListener

/**
 * 音乐播放act
 */
class MusicPlayerTestActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_music_player_test)

        MusicController.getMusicController().init(this.application)
        MusicController.getMusicController().setMusicStatusListener(mStatusListener)
        MusicController.getMusicController().setMusicProgressListener(mProgressListener)

        findViewById<Button>(R.id.play).setOnClickListener {
            MusicController.getMusicController().play(MusicPlayObj().apply {
                this.playUrl = "http://music.163.com/song/media/outer/url?id=317151.mp3"
                this.author = "呵呵"
                this.cover="http://www.baidu.com"
                this.title = "标题"
            })
        }


        findViewById<Button>(R.id.autoPlay).setOnClickListener {
            MusicController.getMusicController().autoPlay(MusicPlayObj().apply {
                this.playUrl = "http://music.163.com/song/media/outer/url?id=317151.mp3"
                this.author = "呵呵"
                this.cover="http://www.baidu.com"
                this.title = "标题"
            })
        }

        findViewById<Button>(R.id.autoPlay2).setOnClickListener {
            MusicController.getMusicController().autoPlay(MusicPlayObj().apply {
                this.playUrl = "http://ting6.yymp3.net:82/new14/zhangyj/5.mp3"
                this.author = "呵呵2"
                this.cover="http://www.baidu.com2"
                this.title = "标题2"
            })
        }



        findViewById<Button>(R.id.pause).setOnClickListener {
            MusicController.getMusicController().pause()

        }


        findViewById<Button>(R.id.resume).setOnClickListener {
            MusicController.getMusicController().resume()

        }

        findViewById<Button>(R.id.stop).setOnClickListener {
            MusicController.getMusicController().stop()

        }

        findViewById<Button>(R.id.release).setOnClickListener {
            MusicController.getMusicController().release()

        }

        findViewById<Button>(R.id.seek).setOnClickListener {
            MusicController.getMusicController().seekTo(65488, true)

        }
    }

    override fun onDestroy() {
        MusicController.getMusicController().removeMusicStatusListener(mStatusListener)
        MusicController.getMusicController().removeMusicProgressListener(mProgressListener)
        super.onDestroy()
    }

    private val mStatusListener = object : MusicEventListener {
        override fun status(musicInfo: MusicPlayInfo?) {
            Log.d("MusicPlayerTestActivity", "play status " + Gson().toJson(musicInfo ?: ""))
        }
    }

    private val mProgressListener = object : MusicProgressListener {
        override fun progressInfo(url: String?, progress: Long, buffered: Long, duration: Long) {
            Log.d("MusicPlayerTestActivity", "progressInfo $url $progress $buffered $duration")

        }

    }
}