package com.example.ktdemo

import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.widget.Button
import android.widget.FrameLayout
import android.widget.ImageView
import android.widget.SeekBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager.widget.ViewPager
import com.airbnb.lottie.LottieAnimationView
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import com.example.ktdemo.anim.AnimTestActivity
import com.example.ktdemo.animlogo.AnimLogoActivity
import com.example.ktdemo.bilidanmu.*
import com.example.ktdemo.bitmap.PicCropMixActivity
import com.example.ktdemo.blur.CusBlurActivity
import com.example.ktdemo.ctxobserver.CtxObserverManager
import com.example.ktdemo.ctxobserver.bean.ObserverChangeInfo
import com.example.ktdemo.ctxobserver.listener.ObserverChangeListener
import com.example.ktdemo.download.DownLoadActivity
import com.example.ktdemo.draghelper.DragHelperActivity
import com.example.ktdemo.exerciseselection.ui.SelectionProgressActivity
import com.example.ktdemo.floatmenu.FloatMenuActivity
import com.example.ktdemo.glide.GlideApp
import com.example.ktdemo.glide.progress.callback.GlideProgressListener
import com.example.ktdemo.glide.progress.callback.GlideProgressManager
import com.example.ktdemo.guide.GuideParentFragment
import com.example.ktdemo.guide.widget.GuideBottomImageView
import com.example.ktdemo.imageview.RedHeartImageView
import com.example.ktdemo.musicplayer.MusicPlayerTestActivity
import com.example.ktdemo.musicx.MusicXActivity
import com.example.ktdemo.picbanner.PicBannerActivity
import com.example.ktdemo.poptopbar.TopBarPopActivity
import com.example.ktdemo.progressbar.ProgressAnimBar
import com.example.ktdemo.progressbar.ProgressBarInfo
import com.example.ktdemo.progressbar.ProgressHorizontalBar
import com.example.ktdemo.progressbar.ProgressVerticalBar
import com.example.ktdemo.recyclerview.CusRecyclerviewTestActivity
import com.example.ktdemo.scrollview.ScrollETActivity
import com.example.ktdemo.scrollviewtest.ScrollviewTestActivity
import com.example.ktdemo.selection.shake.SelectionShakeImgView
import com.example.ktdemo.selection.shake.SelectionShakeTxtContainerView
import com.example.ktdemo.selection.ui.SelectionActivity
import com.example.ktdemo.sharemotion.ShareMotionActivity
import com.example.ktdemo.sliderecyclerview.SlideRecyclerviewActivity
import com.example.ktdemo.sliper.SliperActivity
import com.example.ktdemo.span.SpanTestActivity
import com.example.ktdemo.staggle.frist.StaggleActivity
import com.example.ktdemo.staggle.second.GridSecondActivity
import com.example.ktdemo.svga.SVGATestActivity
import com.example.ktdemo.template.TemplateActivity
import com.example.ktdemo.template.TemplateDragActivity
import com.example.ktdemo.textview.ExpandTextView
import com.example.ktdemo.vpgallery.VPTestActivity
import com.example.ktdemo.water.ImageViewActivity
import com.google.gson.Gson
import com.north.light.libltobserver.LTObserver
import com.north.light.libltobserver.listener.LTObserverListener
import com.north.light.libltobserver.thread.LTThread
import com.north.light.libmusicplayerx.utils.KtLogUtil
import com.north.light.libullet.BulletInfo
import com.north.light.libullet.BulletRecyclerView
import java.io.Serializable


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //test1
//        val nav = findViewById<NavBottomView>(R.id.nav)
//        val icList: MutableList<NavBottomUIInfo> = ArrayList()
//        for (i in 0..4) {
//            icList.add(NavBottomUIInfo().apply {
//                this.nameSel = "标题${i}"
//                this.nameUnSel = "标题${i}"
//                this.nameSelColor = this@MainActivity.resources.getColor(R.color.black)
//                this.nameUnSelColor = this@MainActivity.resources.getColor(R.color.purple_700)
//                this.identify = i
//                this.imgSel = NavBitmapUtils.getInstance()
//                    .getResBitmap(this@MainActivity, R.mipmap.ic_launcher)
//                this.imgUnSel =
//                    NavBitmapUtils.getInstance().getResBitmap(this@MainActivity, R.mipmap.test)
//            })
//        }
//        nav.initUI(icList)
//        nav.setUnreadWithPosition(0, NavBottomUnreadInfo().apply {
//            this.msgCount = 2
//        })
//        nav.setUnreadWithPosition(1, NavBottomUnreadInfo().apply {
//            this.msgCount = 2
//        })
//
//        Handler().postDelayed(Runnable {
//            nav.readAll()
//        }, 2000)

//        startActivity(Intent(this, TemplateDragActivity::class.java))

        val imgUrl = "https://www.baidu.com/img/PCtm_d9c8750bed0b3c7d089fa7d55720d6cf.png"

        GlideProgressManager.getInstance().setGlideProgressListener(object : GlideProgressListener {
            override fun onProgress(url: String?, progress: Int, isFinish: Boolean) {
                Log.d("glide", "glide url：$url progress:$progress isFinish:$isFinish")
            }
        })

        GlideApp.with(this).asBitmap().load(imgUrl).into(object : SimpleTarget<Bitmap>() {
            override fun onResourceReady(resource: Bitmap, transition: Transition<in Bitmap>?) {
                Log.d("glide", "glide onResourceReady")
            }
        })


        findViewById<TextView>(R.id.tvGallery).setOnClickListener {
//            val intent = Intent(this, VPTestActivity::class.java)
//            startActivity(intent)

//            startActivity(Intent(this, TemplateDragActivity::class.java))
        }
//        findViewById<TextView>(R.id.tvGallery).performClick()

        findViewById<TextView>(R.id.tvStaggle).setOnClickListener {
            val intent = Intent(this, StaggleActivity::class.java)
            startActivity(intent)
        }

        findViewById<TextView>(R.id.tvGSecond).setOnClickListener {
            val intent = Intent(this, GridSecondActivity::class.java)
            startActivity(intent)
        }

//        findViewById<TextView>(R.id.tvGSecond).performClick()

        findViewById<Button>(R.id.sharemotion).setOnClickListener {
            val intent = Intent(this, ShareMotionActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.slidetabrecy).setOnClickListener {
            val intent = Intent(this, SlideRecyclerviewActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.selectionProgress).setOnClickListener {
            val intent = Intent(this, SelectionProgressActivity::class.java)
            startActivity(intent)
        }


//        findViewById<Button>(R.id.selectionProgress).performClick()


        findViewById<Button>(R.id.selection).setOnClickListener {
            val intent = Intent(this, SelectionActivity::class.java)
            startActivity(intent)
        }



        findViewById<Button>(R.id.floatMenu).setOnClickListener {
            //进入菜单页面
            val intent = Intent(this, FloatMenuActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.animLogo).setOnClickListener {
            val intent = Intent(this, AnimLogoActivity::class.java)
            startActivity(intent)
        }
        findViewById<Button>(R.id.sliper).setOnClickListener {
            val intent = Intent(this, SliperActivity::class.java)
            startActivity(intent)
        }
//        findViewById<Button>(R.id.animLogo).performClick()


//        Log.d("dd", "dd")
//
//        startActivity(Intent(this, DialogFragmentActivity::class.java))


        val bulletRec = findViewById<BulletRecyclerView>(R.id.bulletRecy)
        val list: MutableList<BulletInfo> = ArrayList();
        list.add(BulletInfo().apply {
            type = 1
            nickName = "小红"
            content = "吸大麻"
        })
        list.add(BulletInfo().apply {
            type = 1
            nickName = "小红2"
            content = "吸大麻"
        })
        list.add(BulletInfo().apply {
            type = 1
            nickName = "小红3"
            content = "吸大麻"
        })
        list.add(BulletInfo().apply {
            type = 1
            nickName = "小红4"
            content = "吸大麻"
        })
        list.add(BulletInfo().apply {
            type = 1
            nickName = "小红5"
            content = "吸大麻"
        })
        list.add(BulletInfo().apply {
            type = 1
            nickName = "小红6"
            content = "吸大麻"
        })
        bulletRec.setData(list)

        val bt = findViewById<Button>(R.id.tt)
        bt.setOnClickListener {
            val list2: MutableList<BulletInfo> = ArrayList();
            list2.add(BulletInfo().apply {
                type = 1
                nickName = "小红"
                content = "吸大麻"
            })
            list2.add(BulletInfo().apply {
                type = 1
                nickName = "小红"
                content = "吸大麻"
            })
            bulletRec.setData(list2)
        }

        val pb = findViewById<ProgressAnimBar>(R.id.pb)
        pb.progress = 80
        Handler().postDelayed(Runnable {
            pb.progress = 20
        }, 2000)


        val ivHeart = findViewById<RedHeartImageView>(R.id.ivHeart)
        ivHeart.setRes(R.drawable.video_icon_like1_nor)
        Handler().postDelayed(Runnable {
            ivHeart.setResWithAnim(R.drawable.video_icon_like1_pre)

            Handler().postDelayed(Runnable {
                ivHeart.setRes(R.drawable.video_icon_like1_nor)
            }, 2000)

        }, 2000)


        val ivHeart2 = findViewById<SelectionShakeImgView>(R.id.ivHeart2)
//        ivHeart2.setRes(R.drawable.video_icon_like1_nor)
        Handler().postDelayed(Runnable {
            ivHeart2.setResWithAnim(R.drawable.video_icon_like1_pre)

            Handler().postDelayed(Runnable {
                ivHeart2.setRes(R.drawable.video_icon_like1_nor)
            }, 2000)

        }, 2000)


        val shakeTxtContainer = findViewById<SelectionShakeTxtContainerView>(R.id.shakeTx)
        shakeTxtContainer.show(false)


        findViewById<Button>(R.id.svTest).setOnClickListener {
            val intent = Intent(this, ScrollviewTestActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.picbanner).setOnClickListener {
            val intent = Intent(this, PicBannerActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.musicplayer).setOnClickListener {
            val intent = Intent(this, MusicPlayerTestActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.rotate).setOnClickListener {
            val intent = Intent(this, ImageViewActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.spanString).setOnClickListener {
            val intent = Intent(this, SpanTestActivity::class.java)
            startActivity(intent)
        }
        findViewById<Button>(R.id.topbarpop).setOnClickListener {
            val intent = Intent(this, TopBarPopActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.picCropMix).setOnClickListener {
            val intent = Intent(this, PicCropMixActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.cusBlur).setOnClickListener {
            val intent = Intent(this, CusBlurActivity::class.java)
            startActivity(intent)
        }
//        findViewById<Button>(R.id.cusBlur).performClick()

        supportFragmentManager.beginTransaction().replace(
            R.id.fragmentlayout,
            MainFragment()
        ).commitAllowingStateLoss()

        val vp: ViewPager = findViewById(R.id.fragmentVp)
        vp.adapter = object : FragmentStatePagerAdapter(supportFragmentManager) {
            override fun getCount(): Int {
                return 4
            }

            override fun getItem(position: Int): Fragment {
                return MainFragment()
            }
        }

        LTObserver.with(this)
            .setIdentify("dd")
            .setCallBackListener(object : LTObserverListener<String>() {
                override fun callback(t: String) {
                    val a = (Thread.currentThread() == Looper.getMainLooper().getThread())
                    LogUtil.d("LTObserver: str $t thread: $a")
                }
            }).setCallBackListener(object : LTObserverListener<TestObj>() {
                override fun callback(t: TestObj) {
                    val a = (Thread.currentThread() == Looper.getMainLooper().getThread())
                    LogUtil.d("LTObserver: TestObj " + t.a + " thread: " + a)
                }
            })
            .build()
        Thread(Runnable {
            Thread.sleep(2000)
            LTObserver.sender().send("d str io")
            LTObserver.sender().thread(LTThread.MAIN).send("d str main")
            LTObserver.sender().send(TestObj("d io"))
            LTObserver.sender().thread(LTThread.MAIN).send(TestObj("d main"))
        }).start()



        findViewById<Button>(R.id.bilidanmu).setOnClickListener {
            val intent = Intent(this, BiliDanmuActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.bilidanmu2).setOnClickListener {
            val intent = Intent(this, BiliDanMu2Activity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.bilidanmu3).setOnClickListener {
            val intent = Intent(this, BiliDanMuCusActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.bilidanmu4).setOnClickListener {
            val intent = Intent(this, BiliDaMuRecyActivity::class.java)
            startActivity(intent)
        }


        val ex: ExpandTextView = findViewById(R.id.textEx);
        ex.setText("789")

        Handler().postDelayed(Runnable {
            ex.setText("findViewByIdfindViewByIdfindViewByIdfindViexdfindViewByIdfindViewByIdfindViewByIdfindViewByIdfindViexdfindViewByIdfindViexdfindViewById")
        }, 2000);

        Handler().postDelayed(Runnable {
            ex.setCanExpand(true)
            ex.setExpandTxtColor(R.color.teal_200)
            ex.setText("findViewByIdfindViewByIdfindViewByIdfindViexdfindViewByIdfindViewByIdfindViewByIdfindViewByIdfindViexdfindViewByIdfindViexdfindViewById")
        }, 4000);


        Handler().postDelayed(Runnable {
            ex.setCanExpand(false)
            ex.setText("findViewByIdfindViewByIdfindViewByIdfindViexdfindViewByIdfindViewByIdfindViewByIdfindViewByIdfindViexdfindViewByIdfindViexdfindViewById")
        }, 6000);


        findViewById<Button>(R.id.cusRecy).setOnClickListener {
            val intent = Intent(this, CusRecyclerviewTestActivity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.svga).setOnClickListener {
            val intent = Intent(this, SVGATestActivity::class.java)
            startActivity(intent)
        }


//        findViewById<Button>(R.id.svga).performClick()


//        findViewById<Button>(R.id.spanString).performClick()

        val lottie = findViewById<LottieAnimationView>(R.id.ltAnim)
//        //        mGifDrawable = (GifDrawable) mBinding.gifLoading.getDrawable();
//     lottie.setScaleType(ImageView.ScaleType.CENTER_INSIDE)
//     lottie.setImageAssetsFolder("images")
//     lottie.setAnimation("selectyes.json")
//     lottie.setRepeatCount(-1)
        lottie.playAnimation()

        val seekBar = findViewById<SeekBar>(R.id.skVideoSeekBar)

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {

            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
                seekBar?.thumb = ContextCompat.getDrawable(
                    this@MainActivity,
                    R.drawable.shape_bg_seek_bar_thumb_white
                )

            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
                seekBar?.thumb = ContextCompat.getDrawable(
                    this@MainActivity,
                    R.drawable.shape_bg_seek_bar_thumb_white2
                )

            }

        })

        val limitText = findViewById<LimitTextView>(R.id.limitText)

        limitText.setTextWithLimit("aabbcc", 7)



        findViewById<Button>(R.id.download).setOnClickListener {
            val intent = Intent(this, DownLoadActivity::class.java)
            startActivity(intent)
        }



        findViewById<Button>(R.id.danmu2).setOnClickListener {
            val intent = Intent(this, DanMu2Activity::class.java)
            startActivity(intent)
        }


        findViewById<Button>(R.id.musicx).setOnClickListener {
            val intent = Intent(this, MusicXActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.scrollet).setOnClickListener {
            val intent = Intent(this, ScrollETActivity::class.java)
            startActivity(intent)
        }

        findViewById<Button>(R.id.animtest).setOnClickListener {
            val intent = Intent(this, AnimTestActivity::class.java)
            startActivity(intent)
        }
//        findViewById<Button>(R.id.scrollet).performClick()
//
//        val parentView = findViewById<LinearLayout>(R.id.llRoot)
//        val childView = findViewById<Button>(R.id.musicx)
//        childView.post {
//            LogUtil.d("pos run child")
//        }
//        parentView.post {
//            LogUtil.d("pos run parent")
//        }
//        childView.height

        val pbBar = findViewById<ProgressHorizontalBar>(R.id.pbBar)
        pbBar.setProgress(ProgressBarInfo().apply {
            this.progress = 50
            this.progressColor = resources.getColor(R.color.bluetest)
        })

        val pbVBar = findViewById<ProgressVerticalBar>(R.id.pbVBar)
        pbVBar.setProgress(ProgressBarInfo().apply {
            this.progress = 50
            this.progressColor = resources.getColor(R.color.bluetest)
        })

        val draghelper: Button = findViewById<Button>(R.id.draghelper)
        draghelper.setOnClickListener {
            val intent = Intent(this, DragHelperActivity::class.java)
            startActivity(intent)
        }
//        draghelper.performClick()

//        TCBuglyManager.getInstance().init(this, true, TCBuglyManager.TCBuglyInfo().apply {
//            this.appId = "470f7724c9"
//            this.channelCode = "channel"
//            this.deviceId = "deviceId"
//        })
//
//        Thread(Runnable {
//            Thread.sleep(2000)
//            throw Exception("11 fix 1.0.1++")
//        }).start()

        val gson = Gson()
        val map = HashMap<String, Any>()
        map.put("1", "one")
        map.put("11", 1)
        map.put("111", Test())
        val toJson = gson.toJson(map)
        KtLogUtil.d("转换结果：${toJson}")


        val tvObser = findViewById<TextView>(R.id.tvObser)
        val picApi = CtxObserverManager.getPicObserver(this, null, object : ObserverChangeListener {
            override fun change(info: ObserverChangeInfo) {
                runOnUiThread {
                    tvObser.text = "图片变化： " + info.toString()
                }
            }
        })
        val videoApi =
            CtxObserverManager.getVideoObserver(this, null, object : ObserverChangeListener {
                override fun change(info: ObserverChangeInfo) {
                    runOnUiThread {
                        tvObser.text = "视频变化： " + info.toString()
                    }
                }
            })
    }

}

class Test : Serializable {
    var first = 0
}
