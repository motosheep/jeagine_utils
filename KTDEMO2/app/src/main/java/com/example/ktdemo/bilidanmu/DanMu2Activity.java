package com.example.ktdemo.bilidanmu;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;
import com.north.light.libnorthlightbullet.BulletDrawBgListener;
import com.north.light.libnorthlightbullet.BulletSurfaceView;
import com.north.light.libnorthlightbullet.data.BulletInfo;

public class DanMu2Activity extends AppCompatActivity {
//    private BulletSurfaceView sfContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dan_mu2);
        Button tvClearData = findViewById(R.id.tvClearData);
        Button tvClearScreen = findViewById(R.id.tvClearScreen);
        Button tvPause = findViewById(R.id.tvPause);
        Button tvResume = findViewById(R.id.tvResume);
        Button tvShow = findViewById(R.id.tvShow);
        Button tvHide = findViewById(R.id.tvHide);


        BiliStringRecyclerView2 danmu2rv = findViewById(R.id.danmu2rv);
        danmu2rv.initData();


//        sfContent = findViewById(R.id.sfContent);
//        sfContent.setBulletDrawBgListener(new BulletDrawBgListener() {
//            @Override
//            public void drawBg(BulletInfo bulletInfo, Canvas canvas, float left, float top, float right, float bottom) {
//                Paint mDrawPaint = new Paint();
//                mDrawPaint.setColor(Color.WHITE);
//                mDrawPaint.setStyle(Paint.Style.STROKE);
//                mDrawPaint.setStrokeWidth(2f);
//                canvas.drawRoundRect(left - 10, top, right + 10, bottom, 8, 8, mDrawPaint);
//            }
//        });
//
//
//        for (int i = 0; i < 100; i++) {
//            BulletInfo test = new BulletInfo();
//            test.setId(String.valueOf(i));
//            test.setText("我是姆巴佩" + (i));
//            test.setTextSize(16);
//            test.setShowTime(System.currentTimeMillis() + i * 500);
//            sfContent.addBulletSingle(test);
//        }
//
//        tvClearScreen.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sfContent.clearScreen();
//            }
//        });
//        tvClearData.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sfContent.clear();
//            }
//        });
//        tvPause.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sfContent.pause();
//            }
//        });
//        tvResume.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sfContent.resume();
//            }
//        });
//        tvHide.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sfContent.hide();
//            }
//        });
//        tvShow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                sfContent.show();
//            }
//        });

    }

    @Override
    protected void onDestroy() {
//        sfContent.release();
        super.onDestroy();
    }
}