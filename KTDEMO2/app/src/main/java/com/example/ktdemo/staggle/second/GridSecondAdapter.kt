package com.example.ktdemo.staggle.second

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.example.ktdemo.R

/**
 * FileName: GridSecondAdapter
 * Author: lzt
 * Date: 2023/5/10 09:34
 */
class GridSecondAdapter : RecyclerView.Adapter<GridSecondAdapter.GridNormalHolder>() {

    private var mData: ArrayList<String> = ArrayList()

    fun setData(data: ArrayList<String>) {
        this.mData = data
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GridNormalHolder {
        val rootView: View = LayoutInflater.from(parent.context).inflate(
            R.layout.item_grid_normal_child,
            parent, false
        )
        return GridNormalHolder(rootView)
    }

    override fun onBindViewHolder(holder: GridNormalHolder, position: Int) {
        val vParams = holder.clRoot.layoutParams
        if (position == 0) {
            vParams.height = 200
        } else {
            vParams.height = 100
        }
        holder.clRoot.layoutParams = vParams
    }

    override fun getItemCount(): Int {
        return mData.size
    }

    class GridNormalHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var clRoot: ConstraintLayout = itemView.findViewById(R.id.clRoot)
    }
}