package com.example.ktdemo.selection.shake;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

/**
 * FileName: SelectionShakeTxtContainerView
 * Author: lzt
 * Date: 2022/10/10 09:11
 * 文字抖动view
 */
public class SelectionShakeTxtContainerView extends LinearLayout {
    private static final int TIME_DURATION = 1000;
    private static final int TIME_INTERVAL = 4000;
    private Handler mUIHandler = new Handler(Looper.getMainLooper());

    public SelectionShakeTxtContainerView(Context context) {
        super(context);
        init();
    }

    public SelectionShakeTxtContainerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SelectionShakeTxtContainerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    protected void onDetachedFromWindow() {
        clearAnimation();
        mUIHandler.removeCallbacksAndMessages(null);
        super.onDetachedFromWindow();
    }

    private void init() {

    }

    //外部调用----------------------------------------------


    /**
     * @param needShake 是否需要抖动
     */
    public void show(boolean needShake) {
        clearAnimation();
        setVisibility(View.GONE);
        //透明度
        animate().alpha(0).setDuration(1).withEndAction(new Runnable() {
            @Override
            public void run() {
                setVisibility(View.VISIBLE);
                animate().alpha(1).setDuration(TIME_DURATION).start();
            }
        }).start();
        if (needShake) {
            //抖动
            animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                @Override
                public void run() {
                    animate().rotation(16f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                        @Override
                        public void run() {
                            animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                @Override
                                public void run() {
                                    animate().rotation(0f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                                        @Override
                                        public void run() {
                                            //后续会一直抖动
                                            shakeAllTime();
                                        }
                                    }).start();
                                }
                            }).start();
                        }
                    }).start();
                }
            }).start();
        }
    }

    /**
     * 一直抖动
     */
    private void shakeAllTime() {
        clearAnimation();
        animate().rotation(-8f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
            @Override
            public void run() {
                animate().rotation(16f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                    @Override
                    public void run() {
                        animate().rotation(0f).setDuration(TIME_DURATION / 4).withEndAction(new Runnable() {
                            @Override
                            public void run() {
                                mUIHandler.postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        shakeAllTime();
                                    }
                                }, TIME_INTERVAL);
                            }
                        }).start();
                    }
                }).start();
            }
        }).start();
    }


    public void hide() {
        clearAnimation();
        setVisibility(View.GONE);
    }
}
