package com.example.ktdemo.span.view;

import android.graphics.Color;
import android.text.style.ImageSpan;

import java.io.Serializable;

/**
 * author:li
 * date:2022/10/22
 * desc:span text view 传入info
 * change by lzt 20221219 增加文字是否加粗
 */
public class SpanViewInfo implements Serializable {
    //类型：0普通文字 1普通图片
    protected int type;
    //文字
    protected TxtInfo text;
    //图片
    protected PicInfo pic;

    public static class TxtInfo implements Serializable {
        protected String text;
        protected int size = 12;
        protected int color = Color.BLACK;
        protected boolean bold = false;

        public boolean isBold() {
            return bold;
        }

        public void setBold(boolean bold) {
            this.bold = bold;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public int getSize() {
            return size;
        }

        public void setSize(int size) {
            this.size = size;
        }

        public int getColor() {
            return color;
        }

        public void setColor(int color) {
            this.color = color;
        }
    }

    public static class PicInfo implements Serializable {
        protected int picId = -1;
        protected int width = 20;
        protected int height = 20;
        protected int align = ImageSpan.ALIGN_CENTER;


        public int getAlign() {
            return align;
        }

        public void setAlign(int align) {
            this.align = align;
        }

        public int getPicId() {
            return picId;
        }

        public void setPicId(int picId) {
            this.picId = picId;
        }

        public int getWidth() {
            return width;
        }

        public void setWidth(int width) {
            this.width = width;
        }

        public int getHeight() {
            return height;
        }

        public void setHeight(int height) {
            this.height = height;
        }
    }


    public TxtInfo getText() {
        return text;
    }

    public void setText(TxtInfo text) {
        this.text = text;
    }

    public PicInfo getPic() {
        return pic;
    }

    public void setPic(PicInfo pic) {
        this.pic = pic;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
