package com.example.ktdemo.staggle.frist

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ktdemo.R

/**
 * 瀑布流
 * */
class StaggleActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_staggle)

        //recyclerview
        val rvContent: RecyclerView = findViewById(R.id.rvContent)
        val staggleAdapter = StaggleAdapter()
        val stagLayoutManager = GridLayoutManager(this, 6)
        stagLayoutManager.spanSizeLookup = SpanLoopUp()
        rvContent.layoutManager = stagLayoutManager
        rvContent.adapter = staggleAdapter

        val dataList: ArrayList<StaggleBean> = ArrayList()

        dataList.add(StaggleBean().apply {
            this.type = StaggleAdapter.TYPE_FIRST_ITEM
            this.title = "第一"
        })

        dataList.add(StaggleBean().apply {
            this.type = StaggleAdapter.TYPE_SECOND_ITEM
            this.title = "第二"
        })

        for (pos in 0..10) {
            dataList.add(StaggleBean().apply {
                this.type = StaggleAdapter.TYPE_OTHER_ITEM
                this.title = pos.toString()
            })
        }

        staggleAdapter.setmData(dataList)
    }

    inner class SpanLoopUp : GridLayoutManager.SpanSizeLookup() {
        override fun getSpanSize(position: Int): Int {
            return when (position) {
                0 -> {
                    3
                }
                1 -> {
                    3
                }
                else -> {
                    2
                }
            }
        }
    }
}