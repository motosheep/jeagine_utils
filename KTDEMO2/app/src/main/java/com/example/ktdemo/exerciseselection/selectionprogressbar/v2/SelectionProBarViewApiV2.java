package com.example.ktdemo.exerciseselection.selectionprogressbar.v2;

/**
 * FileName: SelectionProgressBarViewApi
 * Author: lzt
 * Date: 2022/7/7 9:07
 * progress bar view实现api
 */
public interface SelectionProBarViewApiV2 {

    /**
     * 设置参数--包含所有信息
     */
    public void setProgress(SelectionProBarInfoV2 info);

    public void setProgress(SelectionProBarInfoV2 info, boolean anim);

    public void setProgress(SelectionProBarInfoV2 info, boolean anim, long delay);


}
