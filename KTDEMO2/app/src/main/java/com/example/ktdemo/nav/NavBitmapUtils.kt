package com.example.ktdemo.nav

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Canvas
import android.graphics.drawable.Drawable
import android.os.Build

/**
 * FileName: NavBitmapUtils
 * Author: lzt
 * Date: 2022/5/11 8:23
 * bitmap工具
 */
class NavBitmapUtils {

    companion object {
        fun getInstance(): NavBitmapUtils {
            return SingleHolder.mInstance
        }
    }

    object SingleHolder {
        val mInstance = NavBitmapUtils()
    }

    fun getResBitmap(context: Context, vectorDrawableId: Int): Bitmap? {
        var bitmap: Bitmap? = null
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP) {
            val vectorDrawable: Drawable = context.getDrawable(vectorDrawableId)!!
            bitmap = Bitmap.createBitmap(
                vectorDrawable.intrinsicWidth,
                vectorDrawable.intrinsicHeight, Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(bitmap)
            vectorDrawable.setBounds(0, 0, canvas.width, canvas.height)
            vectorDrawable.draw(canvas)
        } else {
            bitmap = BitmapFactory.decodeResource(context.resources, vectorDrawableId)
        }
        return bitmap
    }
}