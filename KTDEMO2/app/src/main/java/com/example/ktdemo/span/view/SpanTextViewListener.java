package com.example.ktdemo.span.view;

import android.view.View;

/**
 * author:li
 * date:2022/10/22
 * desc:
 */
public interface SpanTextViewListener {

    void LongClick(View v);

    <T extends SpanViewInfo> void click(View v, T info);
}
