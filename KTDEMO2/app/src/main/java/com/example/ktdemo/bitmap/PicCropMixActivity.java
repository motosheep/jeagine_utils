package com.example.ktdemo.bitmap;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;
import com.example.ktdemo.selection.SelectionViewShotUtils;

public class PicCropMixActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_crop_mix);

        ImageView ivResult = findViewById(R.id.ivResult);
        LinearLayout llSnap = findViewById(R.id.llSnap);

//        //缩放
//        Bitmap srcBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.classes_bg_img2);
////        Bitmap targetBitmap = PicCropUtils.squareCrop(srcBitmap, 2100);
//        try {
//            ImgWaterSize size = new ImgWaterSize();
//            size.setCompressRate(80);
//            size.setHeightPercent(1f);
//            size.setWidthPercent(1f);
//            List<String> waterResult = new ImgWaterUtils
//                    .Builder()
//                    .with(this)
//                    .source(srcBitmap, R.mipmap.about_icon, size,
//                            ImgWaterOrg.CENTER_VERTICAL_HORIZONTAL)
//                    .launch();
//            ivResult.setImageBitmap(BitmapFactory.decodeFile(waterResult.get(0)));
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap srcbitmap = BitmapFactory.decodeResource(getResources(), R.mipmap.ddddddd);
                Bitmap logo = BitmapFactory.decodeResource(getResources(), R.mipmap.about_icon);
                SelectionViewShotUtils.viewSnapshot(llSnap, new SelectionViewShotUtils.ViewSnapListener() {
                    @Override
                    public void success(Bitmap bitmap) {
                        try {
                            Bitmap resultBitmap = new ImgDownloadWaterUtils().addWater(srcbitmap, "向上号：0001", "向上", logo);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    ivResult.setImageBitmap(resultBitmap);
                                }
                            });
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void failed(String message) {

                    }
                });

            }
        }, 2000);


    }
}