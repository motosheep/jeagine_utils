package com.example.ktdemo.selection;

import android.content.Context;
import android.util.AttributeSet;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;

/**
 * FileName: SelectionRelativelayout
 * Author: lzt
 * Date: 2022/10/9 17:09
 */
public class SelectionRelativelayout extends ConstraintLayout implements SelectionRelativelayoutApi {

    private SelectionRecyclerview mRecyclerview;
    private SelectionAnimView mAnimView;

    //监听事件
    private SelectionRelativelayoutListener mListener;

    //传入的adapter position
    private int mAdapterPosition;


    public SelectionRelativelayout(Context context) {
        super(context);
        init();
    }

    public SelectionRelativelayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SelectionRelativelayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    /**
     * 初始化控件
     */
    private void init() {
        mRecyclerview = new SelectionRecyclerview(getContext());
        mAnimView = new SelectionAnimView(getContext());

        addView(mRecyclerview);
        LayoutParams recyclerviewParams = (LayoutParams) mRecyclerview.getLayoutParams();
        recyclerviewParams.topToTop = LayoutParams.PARENT_ID;
        recyclerviewParams.bottomToBottom = LayoutParams.PARENT_ID;
        recyclerviewParams.startToStart = LayoutParams.PARENT_ID;
        recyclerviewParams.endToEnd = LayoutParams.PARENT_ID;
        recyclerviewParams.width = 0;
        recyclerviewParams.height = LayoutParams.WRAP_CONTENT;
        mRecyclerview.setLayoutParams(recyclerviewParams);
        mRecyclerview.setOverScrollMode(RecyclerView.OVER_SCROLL_NEVER);

        addView(mAnimView);
        LayoutParams animParams = (LayoutParams) mAnimView.getLayoutParams();
        animParams.topToTop = LayoutParams.PARENT_ID;
        animParams.bottomToBottom = LayoutParams.PARENT_ID;
        animParams.startToStart = LayoutParams.PARENT_ID;
        animParams.endToEnd = LayoutParams.PARENT_ID;
        animParams.width = 0;
        animParams.height = 0;
        mAnimView.setLayoutParams(animParams);


        //监听事件
        mAnimView.setAnimEventListener(null);
        mAnimView.setAnimEventListener(new SelectionAnimView.AnimEventListener() {
            @Override
            public void start() {
                notifyAnimStart(mAdapterPosition);
            }

            @Override
            public void finish() {
                notifyAnimEnd(mAdapterPosition);
            }
        });

    }


    @Override
    public SelectionRecyclerview getRecyclerView() {
        return mRecyclerview;
    }

    @Override
    public SelectionAnimView getAnimView() {
        return mAnimView;
    }


    @Override
    public void startAnim(SelectionAnimInfo info, int adapterPosition) {
        if (info == null || mAnimView == null) {
            return;
        }
        this.mAdapterPosition = adapterPosition;
        this.mAnimView.startAnim(info);
    }

    //监听相关-----------------------------------------------------------------------------


    @Override
    public void setSelectionRelativelayoutListener(SelectionRelativelayoutListener listener) {
        this.mListener = listener;
    }

    @Override
    public void removeSelectionRelativelayoutListener() {
        this.mListener = null;
    }

    private void notifyAnimStart(int adapterPosition) {
        if (this.mListener != null) {
            this.mListener.startAnim(adapterPosition);
        }
    }

    private void notifyAnimEnd(int adapterPosition) {
        if (this.mListener != null) {
            this.mListener.endAnim(adapterPosition);
        }
    }
}
