package com.example.ktdemo.nav

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView

/**
 * FileName: NavAnimImageVIew
 * Author: lzt
 * Date: 2022/5/11 9:40
 */
class NavBottomAnimImageVIew : AppCompatImageView {
    constructor(context: Context) : super(context) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init()
    }

    fun init() {
        //初始化相关属性
    }

    override fun onDetachedFromWindow() {
        animate().cancel()
        super.onDetachedFromWindow()
    }

    /**
     * 增加选中时的动画
     * */
    override fun setSelected(selected: Boolean) {
        super.setSelected(selected)
    }

    public fun showAnim(show:Boolean){
        if (show) {
            animate().scaleX(0.8f).scaleY(0.8f).setDuration(100).withEndAction(Runnable {
                animate().scaleX(1f).scaleY(1f).setDuration(100).start()
            }).start()
        }
    }
}