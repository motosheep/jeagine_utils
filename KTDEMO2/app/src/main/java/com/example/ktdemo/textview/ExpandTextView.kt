package com.example.ktdemo.textview

import android.content.Context
import android.graphics.Color
import android.text.Spannable
import android.text.SpannableStringBuilder
import android.text.TextPaint
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.ClickableSpan
import android.util.AttributeSet
import android.view.View
import androidx.appcompat.widget.AppCompatTextView

/**
 * 文字折叠控件
 * 默认不使用折叠
 * */
class ExpandTextView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0
) : AppCompatTextView(context, attrs, defStyleAttr) {

    var collapsed = true
        set(value) {
            if (field != value) {
                field = value
                requestLayout()
            }
        }

    private var collapsedMaxLine = 3

    private var content: String = ""

    private var hasMore = false

    private var collapsedColor = Color.BLACK

    private var collapsedUnderLine = false

    init {
        movementMethod = LinkMovementMethod.getInstance()
    }

    //能否折叠标识
    private var canExpand: Boolean = false

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        if (!TextUtils.isEmpty(content) && canExpand) {
            if (layout != null) {
                if (layout.lineCount > collapsedMaxLine) {
                    hasMore = true
                }

                if (collapsed) {
                    if (layout.lineCount > collapsedMaxLine) {
                        maxLines = collapsedMaxLine
                        val endIndex = layout.getLineEnd(collapsedMaxLine - 1)
                        super.setText(getCollapsedContent(endIndex))
                    }
                } else {
                    if (hasMore) {
                        maxLines = Int.MAX_VALUE
                        super.setText(getAllContent())
                        hasMore = false
                    }
                }
            }
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    private fun getAllContent(): SpannableStringBuilder {
        return SpannableStringBuilder(content)
            .apply {
                append("  收起")
                setSpan(ClickableColorSpan(color = collapsedColor, underLine = collapsedUnderLine) {
                    collapsed = !collapsed
                }, length - 4, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
    }


    private fun getCollapsedContent(endIndex: Int): SpannableStringBuilder {
        return SpannableStringBuilder(content.substring(0, endIndex - 7))
            .apply {
                append("...")
                append("全文")
                setSpan(ClickableColorSpan(color = collapsedColor, underLine = collapsedUnderLine) {
                    collapsed = !collapsed
                }, length - 2, length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            }
    }


    class ClickableColorSpan(
        private val color: Int,
        private val underLine: Boolean,
        private val click: () -> Unit
    ) : ClickableSpan() {

        override fun updateDrawState(ds: TextPaint) {
            ds.color = color
            ds.isUnderlineText = underLine
        }

        override fun onClick(widget: View) {
            click.invoke()
        }
    }

    //外部调用方法-------------------------------------------------------------------------------

    /**
     * 设置文字--所有配置参数都应该放到设置文字之前，才会生效
     * */
    fun setText(content: String) {
        this.content = content
        super.setText(content)
        maxLines = Int.MAX_VALUE
        hasMore = false
    }

    /**
     * 设置能否折叠
     * */
    fun setCanExpand(canExpand: Boolean) {
        this.canExpand = canExpand
    }

    /**
     * 设置最大行数
     * */
    fun setLimitLine(maxLine: Int) {
        this.collapsedMaxLine = maxLine
    }

    /**
     * 设置收起文字的颜色
     * */
    fun setExpandTxtColor(resColor: Int) {
        this.collapsedColor = resources.getColor(resColor)
    }

    fun toggleCollapsed() {
        collapsed = !collapsed
        requestLayout()
    }
}
