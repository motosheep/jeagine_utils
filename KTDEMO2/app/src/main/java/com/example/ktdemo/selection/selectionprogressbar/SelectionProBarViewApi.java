package com.example.ktdemo.selection.selectionprogressbar;

/**
 * FileName: SelectionProgressBarViewApi
 * Author: lzt
 * Date: 2022/7/7 9:07
 * progress bar view实现api
 */
public interface SelectionProBarViewApi {

    /**
     * 设置参数--包含所有信息
     */
    public void setProgress(SelectionProBarInfo info);

    public void setProgress(SelectionProBarInfo info, boolean anim);


}
