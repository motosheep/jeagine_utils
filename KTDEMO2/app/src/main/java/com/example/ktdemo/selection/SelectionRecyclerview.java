package com.example.ktdemo.selection;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;


/**
 * FileName: SelectionRecyclerview
 * Author: lzt
 * Date: 2022/10/9 17:09
 */
public class SelectionRecyclerview extends RecyclerView {


    public SelectionRecyclerview(@NonNull Context context) {
        super(context);
    }

    public SelectionRecyclerview(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public SelectionRecyclerview(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onTouchEvent(MotionEvent e) {
        return super.onTouchEvent(e);
    }
}
