package com.example.ktdemo.svga;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.R;
import com.north.light.libsvga.SVGAAnimManager;
import com.north.light.libsvga.SVGAStatusListener;
import com.opensource.svgaplayer.SVGAImageView;

public class SVGATestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_svgatest);

        SVGAStringRecyclerView svgaStringRecyclerView = findViewById(R.id.activity_svga_test);
//
        SVGAAnimManager.getInstance().init(this);
//
        svgaStringRecyclerView.initData();
//        SVGAImageView svgaImageView = findViewById(R.id.ivSvga);
//        svgaImageView.setLoops(1);
//        SVGAAnimManager.getInstance().loadAssets(SVGATestActivity.this, "newyesyes.svga",
//                svgaImageView, null, new SVGAStatusListener() {
//                    @Override
//                    public void start() {
//                        LogUtil.d("start");
//                    }
//
//                    @Override
//                    public void pause() {
//                        LogUtil.d("onPause");
//                    }
//
//                    @Override
//                    public void finish() {
//                        LogUtil.d("onFinished");
////                        SVGAAnimManager.getInstance().loadAssets(SVGATestActivity.this, "tt2.svga",
////                                svgaImageView, null, null);
//                    }
//
//                    @Override
//                    public void repeat() {
//                        LogUtil.d("onRepeat");
//
//                    }
//                });
    }
}