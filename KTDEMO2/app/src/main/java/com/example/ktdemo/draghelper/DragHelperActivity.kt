package com.example.ktdemo.draghelper

import android.os.Bundle
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.ktdemo.R
import com.example.ktdemo.draghelper.widget.DragLayout
import com.example.ktdemo.draghelper.widget.DragLayoutEventListener
import com.north.light.libmusicplayerx.utils.KtLogUtil

class DragHelperActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_drag_helper)

        val dlLayout: DragLayout = findViewById(R.id.dlLayout)
        dlLayout.setDragLayoutEventListener(object : DragLayoutEventListener {
            override fun toTop() {
                KtLogUtil.d("DragLayout toTop")
            }

            override fun toMiddle() {
                KtLogUtil.d("DragLayout toMiddle")
            }

            override fun toBottom() {
                KtLogUtil.d("DragLayout toBottom")
            }

            override fun inflateView(
                topContainer: RelativeLayout,
                middleContainer: RelativeLayout,
                bottomContainer: RelativeLayout
            ) {
                KtLogUtil.d("DragLayout inflateView")
                val recy: DragHelperRecyclerView = DragHelperRecyclerView(
                    this@DragHelperActivity, null
                )
                bottomContainer.addView(recy)
                val params = recy.layoutParams
                params.width = ViewGroup.LayoutParams.MATCH_PARENT
                params.height = ViewGroup.LayoutParams.MATCH_PARENT
                recy.layoutParams = params
                recy.initData()
                dlLayout.initDragRecyclerView(recy)

                topContainer.addView(TextView(this@DragHelperActivity).apply {
                    text = getString(R.string.novel)
                })
            }

        })
    }
}