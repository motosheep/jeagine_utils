package com.example.ktdemo.bitmap.crop;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * FileName: PicCropUtils
 * Author: lzt
 * Date: 2022/10/25 09:39
 */
public class PicCropUtils implements Serializable {

    /**
     * 按照指定的4:3比例,对源Bitmap进行裁剪
     *
     * @param srcBitmap 源图片对应的Bitmap
     * @return Bitmap
     */
    public static Bitmap centerCrop4To3(Bitmap srcBitmap) {
        int desWidth = srcBitmap.getWidth();
        int desHeight = srcBitmap.getHeight();
        float desRate = (float) desWidth / desHeight;
        float rate = 4f / 3f;
        if (desRate > rate) {//宽有多余
            desWidth = (int) (desHeight * 4 / 3);
        } else {//宽有不够，裁剪高度
            desHeight = (int) (desWidth * 3 / 4);
        }
        return centerCrop(srcBitmap, desWidth, desHeight);
    }

    /**
     * 按照指定的6:9比例,对源Bitmap进行裁剪
     *
     * @param srcBitmap 源图片对应的Bitmap
     * @return Bitmap
     */
    public static Bitmap centerCrop16To9(Bitmap srcBitmap) {
        int desWidth = srcBitmap.getWidth();
        int desHeight = srcBitmap.getHeight();
        float desRate = (float) desWidth / desHeight;
        float rate = 16f / 9f;
        if (desRate > rate) {//宽有多余
            desWidth = (int) (desHeight * 16 / 9);
        } else {//宽有不够，裁剪高度
            desHeight = (int) (desWidth * 9 / 16);
        }
        return centerCrop(srcBitmap, desWidth, desHeight);
    }

    /**
     * 按照指定的宽高比例,对源Bitmap进行裁剪
     * 注意，输出的Bitmap只是宽高比与指定宽高比相同，大小未必相同
     *
     * @param srcBitmap 源图片对应的Bitmap
     * @param desWidth  目标图片宽度
     * @param desHeight 目标图片高度
     * @return Bitmap
     */
    public static Bitmap centerCrop(Bitmap srcBitmap, int desWidth, int desHeight) {
        int srcWidth = srcBitmap.getWidth();
        int srcHeight = srcBitmap.getHeight();
        int newWidth = srcWidth;
        int newHeight = srcHeight;
        float srcRate = (float) srcWidth / srcHeight;
        float desRate = (float) desWidth / desHeight;
        int dx = 0, dy = 0;
        if (srcRate == desRate) {
            return srcBitmap;
        } else if (srcRate > desRate) {
            newWidth = (int) (srcHeight * desRate);
            dx = (srcWidth - newWidth) / 2;
        } else {
            newHeight = (int) (srcWidth / desRate);
            dy = (srcHeight - newHeight) / 2;
        }
        //创建目标Bitmap，并用选取的区域来绘制
        Bitmap desBitmap = Bitmap.createBitmap(srcBitmap, dx, dy, newWidth, newHeight);
        return desBitmap;
    }

    /**
     * 剪裁一个居中正方形的bitmap
     */
    public static Bitmap squareCrop(Bitmap srcBitmap, int length) {
        try {
            int srcWidth = srcBitmap.getWidth();
            int srcHeight = srcBitmap.getHeight();
            int targetLength = length;
            //判断横竖屏
            boolean isLandScape = (srcWidth >= srcHeight);
            Bitmap scaleBitmap;
            if (isLandScape) {
                //横屏图片--高度作为基准
                float startDx = (srcWidth - srcHeight) * 1.0f / 2.0f;
                scaleBitmap = Bitmap.createBitmap(srcBitmap, (int) startDx, 0, srcHeight, srcHeight);
            } else {
                //竖屏图片--宽度作为基准
                float startDy = (srcHeight - srcWidth) * 1.0f / 2.0f;
                scaleBitmap = Bitmap.createBitmap(srcBitmap, 0, (int) startDy, srcWidth, srcWidth);
            }
            //再根据length缩放
            int srcLength = scaleBitmap.getWidth();
            if (srcLength <= targetLength) {
                //比他小，不用缩放
                return scaleBitmap;
            }
            //计算缩放位置
            int scaleStartPos = (int) ((srcLength - targetLength) * 1.0f / 2.0f);
            Bitmap trainBitmap = Bitmap.createBitmap(scaleBitmap, scaleStartPos, scaleStartPos, targetLength, targetLength);
            if (!scaleBitmap.isRecycled()) {
                scaleBitmap.recycle();
            }
            return trainBitmap;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return srcBitmap;
    }
}
