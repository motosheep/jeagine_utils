package com.example.ktdemo.selection;

import android.graphics.Bitmap;

import java.io.Serializable;

/**
 * FileName: SelectionAnimInfo
 * Author: lzt
 * Date: 2022/10/9 17:36
 * 动画属性
 */
public class SelectionAnimInfo implements Serializable {

    private Bitmap bitmap;
    private float startX;
    private float startY;
    private float viewWidth;
    private float viewHeight;

    public SelectionAnimInfo(Bitmap bitmap, float startX, float startY, float viewWidth, float viewHeight) {
        this.bitmap = bitmap;
        this.startX = startX;
        this.startY = startY;
        this.viewWidth = viewWidth;
        this.viewHeight = viewHeight;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public float getStartX() {
        return startX;
    }

    public void setStartX(float startX) {
        this.startX = startX;
    }

    public float getStartY() {
        return startY;
    }

    public void setStartY(float startY) {
        this.startY = startY;
    }

    public float getViewWidth() {
        return viewWidth;
    }

    public void setViewWidth(float viewWidth) {
        this.viewWidth = viewWidth;
    }

    public float getViewHeight() {
        return viewHeight;
    }

    public void setViewHeight(float viewHeight) {
        this.viewHeight = viewHeight;
    }
}
