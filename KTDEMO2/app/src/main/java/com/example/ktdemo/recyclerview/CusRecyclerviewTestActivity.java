package com.example.ktdemo.recyclerview;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;

import org.jetbrains.annotations.NotNull;

public class CusRecyclerviewTestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cus_recyclerview_test);
        CusRecyStringRecyclerView cusRecyStringRecyclerView = findViewById(R.id.cusRecyView);
        cusRecyStringRecyclerView.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull @NotNull Rect outRect, @NonNull @NotNull View view, @NonNull @NotNull RecyclerView parent, @NonNull @NotNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                int position = parent.getChildLayoutPosition(view);
                int totalCount = 0;
                if (parent.getAdapter() != null) {
                    totalCount = parent.getAdapter().getItemCount();
                }
                if (position ==0) {
                    outRect.right = 200;
                }
            }
        });
        cusRecyStringRecyclerView.initData();
    }
}