package com.example.ktdemo.ctxobserver.api;

import java.io.Serializable;

/**
 * FileName: ContentObserverApi
 * Author: lzt
 * Date: 2023/4/28 10:46
 */
public interface ContentObserverApi extends Serializable {

    void release();

}
