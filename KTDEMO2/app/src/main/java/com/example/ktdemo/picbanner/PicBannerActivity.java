package com.example.ktdemo.picbanner;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.CustomTarget;
import com.bumptech.glide.request.transition.Transition;
import com.example.ktdemo.R;
import com.example.ktdemo.picbanner.banner.BannerPicInfo;
import com.example.ktdemo.picbanner.banner.BannerPicLoadListener;
import com.example.ktdemo.picbanner.banner.BannerPicSizeCallback;
import com.example.ktdemo.picbanner.banner.BannerPicView;
import com.example.ktdemo.picbanner.indicate.IndicateInfo;
import com.example.ktdemo.picbanner.indicate.IndicateView;

import java.util.ArrayList;
import java.util.List;

public class PicBannerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pic_banner);
        BannerPicView bannerPicView = findViewById(R.id.bannerView);
        IndicateView indicateView = findViewById(R.id.indicateView);

        List<BannerPicInfo> bannerPicInfo = new ArrayList<>();
        String netPath = "https://img-blog.csdnimg.cn/4a1c8119a86942598cafbe7d93c08647.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAREzkuLblk4jmoLnovr7mlq8=,size_20,color_FFFFFF,t_70,g_se,x_16";
        String netPath2 = "https://img-blog.csdnimg.cn/07fa99fd7ad945689f470aa4cf21b2ec.png?x-oss-process=image/watermark,type_ZHJvaWRzYW5zZmFsbGJhY2s,shadow_50,text_Q1NETiBAREzkuLblk4jmoLnovr7mlq8=,size_12,color_FFFFFF,t_70,g_se,x_16";
        for (int i = 0; i < 10; i++) {
            BannerPicInfo cache = new BannerPicInfo();
            cache.setPicUrl(netPath);
            if (i % 2 == 1) {
                cache.setPicUrl(netPath);
            } else {
                cache.setPicUrl(netPath2);
            }
            bannerPicInfo.add(cache);
        }
        IndicateInfo indicateInfo = new IndicateInfo();
        indicateInfo.setTotal(bannerPicInfo.size());
        indicateView.initParams(indicateInfo);
        bannerPicView.setDataPositionListener(new BannerPicView.DataPositionListener() {
            @Override
            public void pos(int curPos, int total) {
                Log.d("PicBannerActivity", "---pos---: " + curPos + " total: " + total);
                indicateView.update(curPos, total);
            }

            @Override
            public void empty() {
                Log.d("PicBannerActivity", "---empty---");
            }
        });
        bannerPicView.setData(bannerPicInfo, new BannerPicLoadListener() {
            @Override
            public void load(ImageView img, String url, BannerPicSizeCallback picSizeCallback) {
                Glide.with(PicBannerActivity.this)
                        .asBitmap()
                        .load(url)
                        .into(new CustomTarget<Bitmap>() {
                            @Override
                            public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {
                                if (picSizeCallback != null) {
                                    picSizeCallback.size(url, resource.getWidth(), resource.getHeight(), resource);
                                }
                            }

                            @Override
                            public void onLoadCleared(@Nullable Drawable placeholder) {

                            }
                        });
            }
        });
        bannerPicView.canAutoScroll(true);
    }
}