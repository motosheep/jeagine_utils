package com.example.ktdemo;

/**
 * FileName: TestObj
 * Author: lzt
 * Date: 2022/11/17 09:01
 */
public class TestObj {
    private String a;

    public TestObj(String a) {
        this.a = a;
    }

    public String getA() {
        return a;
    }

    public void setA(String a) {
        this.a = a;
    }
}
