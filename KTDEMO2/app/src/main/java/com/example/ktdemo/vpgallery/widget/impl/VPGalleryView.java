package com.example.ktdemo.vpgallery.widget.impl;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.example.ktdemo.LogUtil;
import com.example.ktdemo.vpgallery.widget.VPOverlayPageAdapter;

/**
 * author:li
 * date:2023/6/2
 * desc:
 */
public class VPGalleryView extends ViewPager {
    private VPOverlayPageAdapter mInfoAdapter;

    public VPGalleryView(@NonNull Context context) {
        super(context);
        init();
    }

    public VPGalleryView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    private void init() {

    }

    @Override
    public void setAdapter(@Nullable PagerAdapter adapter) {
        super.setAdapter(adapter);
        if (adapter instanceof VPOverlayPageAdapter) {
            this.mInfoAdapter = (VPOverlayPageAdapter) adapter;
        }
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        addOnPageChangeListener(onPageChangeListener);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        removeOnPageChangeListener(onPageChangeListener);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return super.onInterceptTouchEvent(ev);
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        int action = ev.getAction();
        switch (action) {
            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                LogUtil.d("vp event up or cancel");
                break;
            case MotionEvent.ACTION_DOWN:
            case MotionEvent.ACTION_MOVE:
                LogUtil.d("vp event down or move");
                break;
        }
        return super.onTouchEvent(ev);
    }

    private final ViewPager.OnPageChangeListener onPageChangeListener = new OnPageChangeListener() {
        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {
            LogUtil.d("onPageSelected pos: " + position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if (state == ViewPager.SCROLL_STATE_IDLE) {
                LogUtil.d("vp event idle");
            }
        }
    };
}
