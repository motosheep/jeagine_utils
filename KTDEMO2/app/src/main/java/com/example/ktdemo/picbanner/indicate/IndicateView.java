package com.example.ktdemo.picbanner.indicate;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.List;

/**
 * FileName: IndicateView
 * Author: lzt
 * Date: 2022/10/18 13:57
 * 指示器控件
 */
public class IndicateView extends LinearLayout {

    private IndicateInfo mIndicateInfo;

    //是否已经初始化
    private boolean isInit = false;
    //控件集合
    private List<ImageView> mImgList = new ArrayList<>();

    public IndicateView(Context context) {
        super(context);
        init();
    }

    public IndicateView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public IndicateView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }


    private void init() {
        setGravity(Gravity.CENTER_VERTICAL);
        setOrientation(LinearLayout.HORIZONTAL);
    }

    //外部调用----------------------------------------------------------------------------------


    /**
     * 初始化样式--必须第一个调用
     */
    public void initParams(IndicateInfo info) {
        try {
            postDelayed(new Runnable() {
                @Override
                public void run() {
                    mImgList.clear();
                    removeAllViews();

                    if (info == null) {
                        mIndicateInfo = null;
                        isInit = true;
                        return;
                    }
                    mIndicateInfo = info;
                    int total = mIndicateInfo.getTotal();
                    int size = mIndicateInfo.getSize();
                    int interval = mIndicateInfo.getInterval();
                    int defBg = mIndicateInfo.getUnSelBgRes();
                    int selBg = mIndicateInfo.getSelBgRes();
                    for (int i = 0; i < total; i++) {
                        ImageView indicateView = new ImageView(getContext());
                        addView(indicateView);
                        LinearLayout.LayoutParams indicateParams = (LayoutParams) indicateView.getLayoutParams();
                        indicateParams.width = size;
                        indicateParams.height = size;
                        if (i != 0) {
                            indicateParams.leftMargin = interval;
                            indicateView.setImageResource(defBg);
                        } else {
                            indicateView.setImageResource(selBg);
                        }
                        indicateView.setLayoutParams(indicateParams);

                        mImgList.add(indicateView);
                    }
                    isInit = true;
                }
            }, 20);
        } catch (Exception e) {
            e.printStackTrace();
            isInit = false;
        }
    }

    /**
     * 更新当前进度
     *
     * @param pos 从1开始
     */
    public void update(int pos, int total) {
        if (!isInit) {
            return;
        }
        if (mIndicateInfo == null || mIndicateInfo.getTotal() < total || pos > mIndicateInfo.getTotal()) {
            return;
        }
        try {
            post(new Runnable() {
                @Override
                public void run() {
                    int defBg = mIndicateInfo.getUnSelBgRes();
                    int selBg = mIndicateInfo.getSelBgRes();
                    for (int i = 0; i < mImgList.size(); i++) {
                        ImageView img = mImgList.get(i);
                        if (img != null) {
                            if (pos != i) {
                                img.setImageResource(defBg);
                            } else {
                                img.setImageResource(selBg);
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
