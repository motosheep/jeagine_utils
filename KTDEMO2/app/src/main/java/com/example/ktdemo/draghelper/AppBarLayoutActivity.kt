package com.example.ktdemo.draghelper

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.ktdemo.R

class AppBarLayoutActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_app_bar_layout)
        val rvContent:DragHelperRecyclerView = findViewById(R.id.rvContent)
        rvContent.initData()
    }
}