package com.example.ktdemo.guide.impl;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.ktdemo.R;
import com.example.ktdemo.guide.base.GuideBaseFragment;
import com.example.ktdemo.guide.widget.GuideBottomImageView;

/**
 * FileName: GuideBFragment
 * Author: lzt
 * Date: 2023/6/30 14:32
 * Description:
 */
public class GuideBFragment extends GuideBaseFragment {

    public static GuideBFragment newInstance(int resId) {
        GuideBFragment fragment = new GuideBFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(BUILDER_STR_RES_ID, resId);
        return fragment;
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_image_guide_b;
    }

    @Override
    protected void initViews(View root) {
        Bundle bundle = getArguments();
        int resId = R.mipmap.ic_guilde_2;
        if (bundle != null) {
            resId = getArguments().getInt(BUILDER_STR_RES_ID, R.mipmap.ic_guilde_2);
        }
        TextView tvOver = root.findViewById(R.id.tvOver);
        TextView tvNext = root.findViewById(R.id.tvNext);
        GuideBottomImageView vImage = root.findViewById(R.id.vImage);
        vImage.setImageResource(resId);
        tvOver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
        tvNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                next();
            }
        });
    }
}
