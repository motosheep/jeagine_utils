package com.example.ktdemo.bilidanmu;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.PagerSnapHelper;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;
import com.north.light.libnorthlightbullet.BulletDrawBgListener;
import com.north.light.libnorthlightbullet.BulletSurfaceView;
import com.north.light.libnorthlightbullet.data.BulletInfo;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;


/**
 * @Author: lzt
 * @CreateDate: 2021/7/30 10:30
 * @Version: 1.0
 */
public class BiliStringRecyclerView2 extends RecyclerView {


    public BiliStringRecyclerView2(@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
        new PagerSnapHelper().attachToRecyclerView(this);
    }

    /**
     * 设置数据
     */
    public void initData() {
        CoorAdapter adapter = new CoorAdapter();
        List<String> da = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            da.add(i + "");
        }
        adapter.mData = da;
        this.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public static class CoorAdapter extends Adapter<CoorAdapter.CoorVH> {
        private List<String> mData = new ArrayList<>();

        @NotNull
        @Override
        public CoorVH onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            return new CoorVH(LayoutInflater.from(parent.getContext()).inflate(
                    R.layout.activity_bilidanmu_item_2,
                    parent, false));
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull CoorVH holder, int position) {
            String data = mData.get(position);
            holder.mContent.setText(data);
            BulletSurfaceView sfContent = holder.sfBullet;
            sfContent.setBulletDrawBgListener(new BulletDrawBgListener() {
                @Override
                public void drawBg(BulletInfo bulletInfo, Canvas canvas, float left, float top, float right, float bottom) {
                    Paint mDrawPaint = new Paint();
                    mDrawPaint.setColor(Color.WHITE);
                    mDrawPaint.setStyle(Paint.Style.STROKE);
                    mDrawPaint.setStrokeWidth(2f);
                    canvas.drawRoundRect(left - 10, top, right + 10, bottom, 8, 8, mDrawPaint);
                }
            });
            sfContent.clear();
            for (int i = 0; i < 100; i++) {
                BulletInfo test = new BulletInfo();
                test.setId(String.valueOf(i));
                test.setText("我是姆巴佩" + (i));
                test.setTextSize(16);
                test.setShowTime(System.currentTimeMillis() + i * 500);
                sfContent.addBulletSingle(test);
            }
            sfContent.setIdentify(String.valueOf(position));
        }

        @Override
        public int getItemCount() {
            return mData.size();
        }

        public static class CoorVH extends ViewHolder {
            private TextView mContent;
            private BulletSurfaceView sfBullet;

            public CoorVH(@NonNull @NotNull View itemView) {
                super(itemView);
                mContent = itemView.findViewById(R.id.activity_coor_adapter_item_txt);
                sfBullet = itemView.findViewById(R.id.sfBullet);
            }
        }
    }
}
