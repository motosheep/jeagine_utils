package com.example.ktdemo.sharemotion;

import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import com.example.ktdemo.R;

/**
 * 详情
 */
public class ShareMotionDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);
        setContentView(R.layout.activity_share_motion_detail);
        ImageView mHeaderImageView = findViewById(R.id.ivImg);
        ViewCompat.setTransitionName(mHeaderImageView, "ddd");
    }
}