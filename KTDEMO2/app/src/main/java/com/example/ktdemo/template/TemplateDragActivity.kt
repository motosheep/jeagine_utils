package com.example.ktdemo.template

import android.graphics.Color
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.ktdemo.LogUtil
import com.example.ktdemo.R
import com.north.light.libmediatemplate.widget.base.TemplateNormalTextView
import com.north.light.libmediatemplate.widget.touch.TemplateTouchImageView
import com.north.light.libmediatemplate.widget.touch.TemplateTouchRootView

class TemplateDragActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_template_drag)

        val tvTest: TemplateNormalTextView = findViewById(R.id.tvTest)
        val cl2: View = findViewById(R.id.cl2)
        tvTest.setText("测试文字描边\n微信公众号：恋恋电影")
//        cl2.post(Runnable {
//            cl2.rotation = -6f
//        })

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tvTest.switchStroke(true)
        }, 2000)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tvTest.strokeWidth(30f)
        }, 4000)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tvTest.strokeColor(Color.BLUE)
        }, 6000)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            tvTest.setText("夏天到了")
        }, 8000)


        val clRoot: TemplateTouchRootView = findViewById(R.id.clRoot)
        val touchImgView = TemplateTouchImageView(clRoot.context)
        touchImgView.id = R.id.temp_drag_img_1
        touchImgView.setBackgroundColor(Color.BLUE)

        val touchImgView2 = TemplateTouchImageView(clRoot.context)
        touchImgView2.id = R.id.temp_drag_img_2
        touchImgView2.setBackgroundColor(Color.RED)

        val touchImgView3 = TemplateTouchImageView(clRoot.context)
        touchImgView3.id = R.id.temp_drag_img_3
        touchImgView3.setBackgroundColor(Color.YELLOW)

        clRoot.addChildView(touchImgView)
        touchImgView.setViewClickListener(object : TemplateTouchImageView.ViewClickListener {
            override fun click(v: View) {
                LogUtil.d("点击事件")
            }
        })

        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            clRoot.addChildView(touchImgView2)
            LogUtil.d("事件----禁止分发")
        }, 2000)
        Handler(Looper.getMainLooper()).postDelayed(Runnable {
            clRoot.addChildView(touchImgView3)
            LogUtil.d("事件----启动发")
        }, 6000)


        val recy = findViewById<TemplateRecyView>(R.id.rvContent)
        recy.layoutManager = LinearLayoutManager(this)
        val adapter = object : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
            val data = arrayListOf<String>()
            override fun onCreateViewHolder(
                parent: ViewGroup,
                viewType: Int
            ): RecyclerView.ViewHolder {
                val hol = LayoutInflater.from(this@TemplateDragActivity)
                    .inflate(R.layout.item_template_drag_ho, parent, false)
                return Holer(hol)
            }

            override fun getItemCount(): Int {
                return data.size
            }

            override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

            }

            fun add(add: ArrayList<String>) {
                data.addAll(add)
                notifyDataSetChanged()
            }

            fun setdata(add: ArrayList<String>) {
                data.clear()
                data.addAll(add)
                notifyDataSetChanged()
            }
        }
        recy.adapter = adapter
        val testLst = arrayListOf<String>()
        for (i in 0 until 10) {
            testLst.add(i.toString())
        }
        adapter.setdata(testLst)

        val test2Lst = arrayListOf<String>()
        for (i in 0 until 2) {
            test2Lst.add(i.toString())
        }
        recy.postDelayed(Runnable {
            adapter.add(test2Lst)
        }, 4000)
    }


    class Holer(itemView: View) : RecyclerView.ViewHolder(itemView) {

    }
}