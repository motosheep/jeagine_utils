package com.example.ktdemo.bilidanmu;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;

public class BiliDaMuRecyActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bili_da_mu_recy);
        BiliStringRecyclerView recy = findViewById(R.id.recy);
        recy.initData();
    }
}