package com.example.ktdemo.guide.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import androidx.viewpager.widget.ViewPager;

/**
 * Description:可以禁止ViewPager左右滑动
 * Data：2021/9/24
 * Author: lulu
 */
public class GuideNoSwipeViewPager extends ViewPager {
    private boolean canSwipe = false;
    public GuideNoSwipeViewPager(Context context, AttributeSet attributeSet){
        super(context, attributeSet);
    }
    public void setCanSwipe(boolean canSwipe)
    {
        this.canSwipe = canSwipe;
    }
    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        return canSwipe && super.onTouchEvent(ev);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return canSwipe && super.onInterceptTouchEvent(ev);
    }

}
