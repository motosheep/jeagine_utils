package com.example.ktdemo.sliper;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.example.ktdemo.R;
import com.north.light.libsliderecyclerview.SlideBaseAdapter;
import com.north.light.libsliderecyclerview.SlideRootView;

import java.util.ArrayList;
import java.util.List;

public class SliperActivity extends AppCompatActivity {

    private int counter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sliper);

        SlideRootView rv = findViewById(R.id.rvContent);
        SlideAdapter myAdapter = new SlideAdapter();
        rv.setAdapter(myAdapter);
        myAdapter.setOnLoadEventListener(new SlideBaseAdapter.OnLoadEventListener<SlideObj>() {
            @Override
            public void load() {
                //加载更多数据
//                if (counter <= 1) {
//                    LogUtil.d("加载更多数据");
//                    List<String> addDataList = new ArrayList<>();
//                    for (int i = 1; i < 5; i++) {
//                        addDataList.add("加载更多数据" + i);
//                    }
//                    myAdapter.addData(addDataList);
//                    counter++;
//                }
                myAdapter.setLoadFinish();
            }

            @Override
            public void noData() {
                Log.d("Sliper", "sliper noData");
                List<SlideObj> addDataList = new ArrayList<>();
                for (int i = 1; i < 6; i++) {
                    addDataList.add(new SlideObj("加载的" + i));
                }
                myAdapter.addData(addDataList);
                myAdapter.setLoadFinish();
            }

            @Override
            public void swipe(int currentPosition, SlideObj obj) {
                Log.d("Sliper", "sliper swipe " + "\ncurrentPosition: " + currentPosition + "\nobj: " + obj.getTitle());
            }

            @Override
            public void swipe(SlideObj obj) {
                Log.d("Sliper", "sliper swipe 2" + "\nobj: " + obj.getTitle());
            }
        });


        List<SlideObj> mStrings = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            mStrings.add(new SlideObj(String.valueOf(i)));
        }
        myAdapter.setData(mStrings);
    }
}