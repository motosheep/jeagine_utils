package com.example.ktdemo.vpgallery.cache;

import android.graphics.Bitmap;

import java.util.Map;

public interface VPGalleryCacheListener {

    void finish(Map<String, Bitmap> source, Map<String, Bitmap> bulletBit);

    void empty();

    Bitmap getBitmap(String url) throws Exception;

}
