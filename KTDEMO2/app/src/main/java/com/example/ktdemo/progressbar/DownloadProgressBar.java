package com.example.ktdemo.progressbar;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.Nullable;

/**
 * FileName: DownloadProgressBar
 * Author: lzt
 * Date: 2023/3/1 18:08
 * 下载进度条
 */
public class DownloadProgressBar extends View {

    //画笔
    private Paint mPaint = new Paint();

    //边距间距
    private int INTERVAL = dp2px(1);

    private String COLOR_BLUE = "#4080FF";
    private String COLOR_WHITE = "#FFFFFF";

    //当前进度
    private long progress = 0;


    public DownloadProgressBar(Context context) {
        super(context);
        init();
    }

    public DownloadProgressBar(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DownloadProgressBar(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    //能否绘制
    private boolean canDraw() {
        if (getVisibility() == View.VISIBLE && getMeasuredHeight() != 0 &&
                getMeasuredWidth() != 0) {
            return true;
        }
        return false;
    }

    private void init() {
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeWidth(1);
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setTextAlign(Paint.Align.LEFT);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (canDraw()) {
            int centerX = getMeasuredWidth() / 2;
            int centerY = getMeasuredHeight() / 2;
            canvas.save();


            //底圆边
            mPaint.setStyle(Paint.Style.FILL);
            int radius2 = centerX - INTERVAL * 2;
            mPaint.setColor(Color.parseColor(COLOR_WHITE));
            canvas.drawCircle(centerX, centerY, radius2, mPaint);

            //底圆
            mPaint.setStyle(Paint.Style.STROKE);
            int radius = centerX - INTERVAL * 2;
            mPaint.setColor(Color.parseColor(COLOR_BLUE));
            mPaint.setStrokeWidth(dp2px(1));
            canvas.drawCircle(centerX, centerY, radius, mPaint);

            //进度
            mPaint.setColor(Color.parseColor(COLOR_BLUE));
            mPaint.setStyle(Paint.Style.FILL);
            int left = INTERVAL;
            int top = INTERVAL;
            int right = getMeasuredWidth() - INTERVAL;
            int bottom = getMeasuredHeight() - INTERVAL;
            float percent = this.progress / 100f;
            int angle = (int) (360 * percent);
            canvas.drawArc(left, top, right, bottom, -90, angle, true, mPaint);

            //覆盖
            mPaint.setStyle(Paint.Style.FILL);
            int radius3 = centerX - INTERVAL * 3;
            mPaint.setColor(Color.parseColor(COLOR_WHITE));
            mPaint.setStrokeWidth(dp2px(1));
            canvas.drawCircle(centerX, centerY, radius3, mPaint);

            canvas.restore();
        }
    }

    private int dp2px(int dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


    //外部调用-----------------------------------------------------------------------------


    /**
     * 设置进度条
     */
    private void setProgress(long progress) {
        this.progress = progress % 100;
        postInvalidate();
    }

}
