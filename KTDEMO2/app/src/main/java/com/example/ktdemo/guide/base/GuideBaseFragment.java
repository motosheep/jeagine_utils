package com.example.ktdemo.guide.base;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ktdemo.guide.GuideParentFragment;

/**
 * FileName: GuideBaseFragment
 * Author: lzt
 * Date: 2023/6/30 14:27
 * Description:
 */
public abstract class GuideBaseFragment extends Fragment {
    protected static String BUILDER_STR_RES_ID = "BUILDER_STR_RES_ID";

    private View mRootView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        mRootView = LayoutInflater.from(container.getContext()).inflate(
                getLayoutId(), container, false);
        return mRootView;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViews(mRootView);
    }

    protected void dismiss() {
        if (getParentFragment() instanceof GuideParentFragment) {
            ((GuideParentFragment) getParentFragment()).dismiss();
        }
    }

    protected void next() {
        if (getParentFragment() instanceof GuideParentFragment) {
            ((GuideParentFragment) getParentFragment()).next();
        }
    }

    protected void finish() {
        if (getParentFragment() instanceof GuideParentFragment) {
            ((GuideParentFragment) getParentFragment()).finish();
        }
    }

    protected abstract int getLayoutId();

    protected abstract void initViews(View root);

}
