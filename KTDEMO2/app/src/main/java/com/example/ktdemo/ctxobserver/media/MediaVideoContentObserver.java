package com.example.ktdemo.ctxobserver.media;

import android.content.Context;
import android.net.Uri;
import android.os.Handler;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.example.ktdemo.ctxobserver.base.ContentBaseObserver;
import com.example.ktdemo.ctxobserver.listener.ObserverChangeListener;
import com.example.ktdemo.ctxobserver.type.UriType;
import com.north.light.libmusicplayerx.utils.KtLogUtil;

import java.util.Collection;

/**
 * FileName: MediaPicContentObserver
 * Author: lzt
 * Date: 2023/4/28 10:21
 * 多媒体视频监控
 * MediaStore.Video.Media.EXTERNAL_CONTENT_URI
 */
public class MediaVideoContentObserver extends ContentBaseObserver {

    public MediaVideoContentObserver(Context context, Handler handler, ObserverChangeListener listener) {
        super(context, handler, listener);
    }

    @Override
    public boolean deliverSelfNotifications() {
        return super.deliverSelfNotifications();
    }

    @Override
    public void onChange(boolean selfChange) {
        super.onChange(selfChange);
        KtLogUtil.d("change MediaVideoContentObserver onChange1");
    }

    @Override
    public void onChange(boolean selfChange, @Nullable Uri uri) {
        super.onChange(selfChange, uri);
        KtLogUtil.d("change MediaVideoContentObserver onChange2");
        notifyChange(UriType.TYPE_VIDEO,selfChange,uri);
    }


    @Override
    public void init() {
        if (getBaseContext() != null) {
            getBaseContext().getContentResolver().registerContentObserver(
                    MediaStore.Video.Media.INTERNAL_CONTENT_URI, true, this);
            getBaseContext().getContentResolver().registerContentObserver(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, true, this);
        }
    }

    @Override
    public void release() {
        if (getBaseContext() != null) {
            getBaseContext().getContentResolver().unregisterContentObserver(this);
        }
    }
}
