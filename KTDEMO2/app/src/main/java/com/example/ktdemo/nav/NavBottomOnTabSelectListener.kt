package com.example.ktdemo.nav

/**
 * FileName: OnTabSelectListener
 * Author: lzt
 * Date: 2022/5/11 9:45
 */
interface NavBottomOnTabSelectListener {
    fun select(position: Int)
    fun unSelect(position: Int)
    fun reSelect(position: Int)
}