package com.example.ktdemo.sliderecyclerview;

import android.graphics.Rect;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ktdemo.R;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

/**
 * 滑动tab
 */
public class SlideRecyclerviewActivity extends AppCompatActivity {
    private SlideTabRecyclerview recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slide_recyclerview);


        recyclerview = findViewById(R.id.rvContent);
        recyclerview.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.HORIZONTAL, false));
        recyclerview.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(@NonNull Rect outRect, @NotNull View view, @NotNull RecyclerView parent, @NotNull RecyclerView.State state) {
                super.getItemOffsets(outRect, view, parent, state);
                outRect.top = 30;
                outRect.left = 30;
            }
        });
        List<String> list = new ArrayList<>();
        for (int i = 0; i < 200; i++) {
            list.add(String.valueOf(i));
        }
        recyclerview.setAdapter(new SlideAdapter(list));
    }

    private static class SlideAdapter extends RecyclerView.Adapter<SlideViewHolder> {
        private List<String> mData = new ArrayList<>();

        public SlideAdapter(List<String> mData) {
            this.mData = mData;
        }

        public void setData(List<String> mData) {
            this.mData = mData;
        }

        @NotNull
        @Override
        public SlideViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_slide_info_txt,
                    parent, false);
            return new SlideViewHolder(view);
        }

        @Override
        public void onBindViewHolder(@NonNull @NotNull SlideViewHolder holder, int position) {

        }

        @Override
        public int getItemCount() {
            return mData.size();
        }
    }

    public static class SlideViewHolder extends RecyclerView.ViewHolder {
        public SlideViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }
    }
}