package com.example.ktdemo.nav

import androidx.viewpager.widget.ViewPager

/**
 * FileName: NavigationBottomApi
 * Author: lzt
 * Date: 2022/5/10 15:22
 */
interface NavBottomApi {

    //normal----------------------------------------------------------------

    /**
     * 设置显示的数据
     */
    fun initUI(uiInfoList: MutableList<NavBottomUIInfo>?)

    /**
     * 设置选中位置：0开始
     * */
    fun setSelWithPosition(position: Int)

    /**
     * 设置选中位置：传入对象的identify
     * */
    fun setSelWithIdentify(identify: Int)

    /**
     * 设置与viewpager关联
     * */
    fun setupWithViewPager(vp: ViewPager?)

    /**
     * tab数量
     * */
    fun getTabCount(): Int

    /**
     * 清空所有tab
     * */
    fun removeAllTab()

    /**
     * 获取当前选中位置
     * */
    fun getCurrentPosition(): Int

    /**
     * 是否显示图片点击动画
     * */
    fun clickImgAnim(show: Boolean)

    /**
     * 是否显示文字点击动画
     * */
    fun clickTxtAnim(show: Boolean)

    //未读--------------------------------------------------------------------

    /**
     * 设置某个位置下的未读数
     * */
    fun setUnreadWithPosition(position: Int, info: NavBottomUnreadInfo)

    /**
     * 设置某个identify的未读数
     * */
    fun setUnReadWithIdentify(identify: Int, info: NavBottomUnreadInfo)

    /**
     * 清空某个位置下的未读数
     * */
    fun clearUnreadWithPosition(position: Int)

    /**
     * 清空某个identify下的未读数
     * */
    fun clearUnreadWithIdentify(identify: Int)

    /**
     * 设置全部为已读
     * */
    fun readAll()


    //监听--------------------------------------------------------------------

    /**
     * 点击监听
     * */
    fun setOnTabSelectListener(listener: NavBottomOnTabSelectListener)


    /**
     * 移除监听
     * */
    fun removeOnTabSelectListener()
}