package com.example.ktdemo.exerciseselection.text;



import com.example.ktdemo.R;

import java.io.Serializable;

/**
 * FileName: SelectionAnimHotInfo
 * Author: lzt
 * Date: 2022/10/27 15:37
 * 热度动画信息类
 */
public class SelectionAnimHotInfo implements Serializable {

    private int identify = -1;

    private int textRes = R.drawable.ic_do_exam_hot_add;

    //宽度 dp
    private int showWidth = 40;

    public int getTextRes() {
        return textRes;
    }

    public void setTextRes(int textRes) {
        this.textRes = textRes;
    }

    public int getShowWidth() {
        return showWidth;
    }

    public void setShowWidth(int showWidth) {
        this.showWidth = showWidth;
    }

    public int getIdentify() {
        return identify;
    }

    public void setIdentify(int identify) {
        this.identify = identify;
    }
}
