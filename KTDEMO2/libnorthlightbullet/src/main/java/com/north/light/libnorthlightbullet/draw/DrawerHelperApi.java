package com.north.light.libnorthlightbullet.draw;

import android.graphics.Canvas;

import com.north.light.libnorthlightbullet.data.BulletInfo;
import com.north.light.libnorthlightbullet.data.BulletInnerInfo;

import java.io.Serializable;
import java.util.List;

/**
 * FileName: DrawerHelperApi
 * Author: lzt
 * Date: 2023/1/14 14:23
 */
public interface DrawerHelperApi extends Serializable {

    //数据处理---------------------------------------------------------------

    public void checkDataNormal();

    //弹幕数据相关------------------------------------------------------------

    /**
     * 添加一条弹幕
     */
    public void addBulletSingle(BulletInfo data);

    /**
     * 添加一个队列的弹幕
     */
    public void addBulletList(List<BulletInfo> data);

    /**
     * 添加一个队列的弹幕
     */
    public void setBulletList(List<BulletInfo> data);

    /**
     * 获取弹幕所有消息
     */
    public List<BulletInnerInfo> getBulletList();

    //操作相关------------------------------------------------------------

    /**
     * 清空弹幕所有数据信息
     */
    public void clearAllData();

    /**
     * 清空弹幕信息
     * */
    public void clearShowData();


    /**
     * 绘制--必须外部调用，否则不会触发
     */
    public void draw(Canvas canvas, int parentWidth, int parentHeight,
                     DrawerStatus drawerStatus,
                     DrawerHelperDrawingEvent listener);

    /**
     * 定时调用
     */
    public void timer(long interval);

    /**
     * 重置计时
     */
    public void resetTime();

    /**
     * 获取当前计时
     */
    public long getCurrentTime();

    /**
     * 清屏
     * */
    public void clearScreen();

    /**
     * 拖动进度
     * */
    public void seekTo(long progress);


}
