package com.north.light.libnorthlightbullet.draw;

import java.io.Serializable;

/**
 * FileName: DrawTimeHelperApi
 * Author: lzt
 * Date: 2023/1/14 14:41
 * 绘制时间api
 */
public interface DrawTimeHelperApi extends Serializable {

    /**
     * 开始
     */
    public void start();

    /**
     * 更新
     */
    public void update(long progress);

    /**
     * 设置进度条
     */
    public void setProgress(long progress);

    /**
     * 获取
     */
    public long get();

    /**
     * 重置
     */
    public void reset();
}
