package com.north.light.libnorthlightbullet.draw;

import android.graphics.Canvas;

import com.north.light.libnorthlightbullet.data.BulletInfo;

import java.io.Serializable;

/**
 * FileName: DrawerHelperDrawingEvent
 * Author: lzt
 * Date: 2023/1/14 14:59
 */
public interface DrawerHelperDrawingEvent extends Serializable {
    void drawBg(BulletInfo bulletInfo, Canvas canvas, float left, float top, float right, float bottom);

}
