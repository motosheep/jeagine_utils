package com.north.light.libnorthlightbullet.utils;

import android.content.res.Resources;

import java.io.Serializable;

/**
 * FileName: BulletScreenUtils
 * Author: lzt
 * Date: 2023/1/10 15:42
 */
public class BulletScreenUtils implements Serializable {

    public static int dp2px(int dpValue) {
        final float scale = Resources.getSystem().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }
}
