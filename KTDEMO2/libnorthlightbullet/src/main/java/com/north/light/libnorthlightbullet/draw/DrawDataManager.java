package com.north.light.libnorthlightbullet.draw;

import com.north.light.libnorthlightbullet.data.BulletInfo;
import com.north.light.libnorthlightbullet.data.BulletInnerInfo;
import com.north.light.libnorthlightbullet.utils.BulletInfoTrain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * FileName: DrawDataManager
 * Author: lzt
 * Date: 2023/1/14 14:22
 * 绘制数据管理类
 */
public class DrawDataManager implements Serializable {

    //总数据集合
    private List<BulletInnerInfo> dataList = new ArrayList<>();

    private final Object SYN = new Object();


    public void setDataList(List<BulletInnerInfo> dataList) {
        this.dataList = dataList;
    }

    public void addBulletSingle(BulletInfo data) {
        synchronized (SYN) {
            dataList.add(BulletInfoTrain.getInstance().trainInner(data));
            Collections.sort(dataList, new Comparator<BulletInnerInfo>() {
                @Override
                public int compare(BulletInnerInfo o1, BulletInnerInfo o2) {
                    return (int) (o1.getShowTime() - o2.getShowTime());
                }
            });
        }
    }

    public void addBulletList(List<BulletInfo> data) {
        synchronized (SYN) {
            dataList.addAll(BulletInfoTrain.getInstance().trainInner(data));
            Collections.sort(dataList, new Comparator<BulletInnerInfo>() {
                @Override
                public int compare(BulletInnerInfo o1, BulletInnerInfo o2) {
                    return (int) (o1.getShowTime() - o2.getShowTime());
                }
            });
        }
    }


    public void setBulletList(List<BulletInfo> data) {
        synchronized (SYN) {
            dataList.clear();
            dataList.addAll(BulletInfoTrain.getInstance().trainInner(data));
            Collections.sort(dataList, new Comparator<BulletInnerInfo>() {
                @Override
                public int compare(BulletInnerInfo o1, BulletInnerInfo o2) {
                    return (int) (o1.getShowTime() - o2.getShowTime());
                }
            });
        }
    }

    public List<BulletInnerInfo> getBulletList() {
        synchronized (SYN) {
            return dataList;
        }
    }

    public void clear() {
        synchronized (SYN) {
            dataList.clear();
        }
    }

    public boolean isEmpty() {
        synchronized (SYN) {
            return dataList == null || dataList.size() == 0;
        }
    }

    public int size() {
        synchronized (SYN) {
            if (isEmpty()) {
                return 0;
            }
            return getBulletList().size();
        }
    }

    public BulletInnerInfo get(int pos) {
        synchronized (SYN) {
            if (isEmpty() || pos >= getBulletList().size() || pos < 0) {
                return null;
            }
            return getBulletList().get(pos);
        }
    }

    public void add(BulletInnerInfo innerInfo) {
        if (getBulletList() == null) {
            dataList = new ArrayList<>();
        }
        add(dataList.size(), innerInfo);
    }

    public void add(int pos, BulletInnerInfo innerInfo) {
        synchronized (SYN) {
            if (getBulletList() == null) {
                dataList = new ArrayList<>();
            }
            dataList.add(pos, innerInfo);
        }
    }

    public void set(int pos, BulletInnerInfo innerInfo) {
        synchronized (SYN) {
            if (getBulletList() == null) {
                dataList = new ArrayList<>();
            }
            dataList.set(pos, innerInfo);
        }
    }

}
