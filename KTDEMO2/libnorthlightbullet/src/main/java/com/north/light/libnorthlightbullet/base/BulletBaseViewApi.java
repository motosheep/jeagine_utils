package com.north.light.libnorthlightbullet.base;

/**
 * FileName: BulletBaseViewApi
 * Author: lzt
 * Date: 2023/1/13 13:47
 * view 基础类
 */
public interface BulletBaseViewApi {

    public void drawBullet();

    public void timer(long interval);
}
