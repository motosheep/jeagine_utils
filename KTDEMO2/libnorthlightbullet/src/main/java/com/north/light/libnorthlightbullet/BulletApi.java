package com.north.light.libnorthlightbullet;

import com.north.light.libnorthlightbullet.base.BulletBaseViewApi;
import com.north.light.libnorthlightbullet.data.BulletInfo;

import java.util.List;

/**
 * FileName: BulletApi
 * Author: lzt
 * Date: 2023/1/10 14:45
 */
public interface BulletApi extends BulletBaseViewApi {

    /**
     * 添加一条弹幕
     */
    public void addBulletSingle(BulletInfo data);

    /**
     * 添加一个队列的弹幕
     */
    public void addBulletList(List<BulletInfo> data);

    /**
     * 清空弹幕数据源
     */
    public void clear();

    /**
     * 清屏
     */
    public void clearScreen();

    /**
     * 释放
     */
    public void release();

    /**
     * 暂停
     */
    public void pause();

    /**
     * 恢复
     */
    public void resume();


    /**
     * 是否绘制中
     */
    public boolean isPlaying();

    /**
     * 显示布局
     */
    public void show();

    /**
     * 隐藏布局
     */
    public void hide();

    /**
     * 拖动进度
     */
    public void seekTo(long progress);

    /**
     * 设置监听事件
     */
    public void setBulletDrawBgListener(BulletDrawBgListener listener);

    public void removeBulletDrawBgListener();

    public void setBulletEventListener(BulletEventListener listener);

    public void removeBulletEventListener();

    /**
     * 当前播放时间
     */
    public long getCurrentTime();


}
