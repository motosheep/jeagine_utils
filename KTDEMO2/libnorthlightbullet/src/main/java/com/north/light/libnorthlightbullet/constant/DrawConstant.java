package com.north.light.libnorthlightbullet.constant;

import java.io.Serializable;

/**
 * FileName: DrawConstant
 * Author: lzt
 * Date: 2023/1/30 14:01
 * 绘制常量
 */
public class DrawConstant implements Serializable {

    public static long CON_TASK_INTERVAL = 20;

    public static int CON_DRAW_COL_MOVE_PX = 3;
    public static int CON_DRAW_COL_MOVE_PY = 6;
}
