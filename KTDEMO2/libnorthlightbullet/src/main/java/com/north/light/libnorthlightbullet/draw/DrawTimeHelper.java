package com.north.light.libnorthlightbullet.draw;

/**
 * FileName: DrawTimeHelper
 * Author: lzt
 * Date: 2023/1/14 14:40
 * 绘制时间管理
 */
public class DrawTimeHelper implements DrawTimeHelperApi {
    private long mStartTime = -1;
    private long mProgress = 0;

    @Override
    public void start() {
        if (mStartTime == -1) {
            mStartTime = System.currentTimeMillis();
        }
    }

    @Override
    public void update(long pro) {
        mProgress = mProgress + pro;
    }

    @Override
    public void setProgress(long progress) {
        mProgress = progress;
    }

    @Override
    public long get() {
        return mStartTime + mProgress;
    }

    @Override
    public void reset() {
        mStartTime = -1;
        mProgress = 0;
    }
}
