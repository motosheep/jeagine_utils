package com.north.light.libnorthlightbullet.utils;

import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.util.Log;

import com.north.light.libnorthlightbullet.data.BulletInnerInfo;

import java.io.Serializable;

/**
 * FileName: BulletPaintUtils
 * Author: lzt
 * Date: 2023/1/11 09:12
 * 画笔工具类
 */
public class BulletPaintUtils implements Serializable {

    /**
     * 获取字体高度
     */
    public static float getTextHeight(BulletInnerInfo bulletInfo) {
        if (bulletInfo == null) {
            return 0;
        }
        try {
            Paint paint = new Paint();
            paint.setTypeface((bulletInfo.isBold()) ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            paint.setTextSize(BulletScreenUtils.dp2px(bulletInfo.getTextSize()));
            Paint.FontMetrics fm = paint.getFontMetrics();
            float height1 = fm.descent - fm.ascent;
            float height2 = fm.bottom - fm.top + fm.leading;
            float height3 = fm.bottom - fm.top;
            Log.d("BulletPaintUtils", "getTextHeight: height1 " + (height1) + " height2: " + height2 + " height3: " + height3);
            return height2;
        } catch (Exception e) {
            return 0;
        }
    }
    /**
     * 获取字体高度
     */
    public static float getTextDescent(BulletInnerInfo bulletInfo) {
        if (bulletInfo == null) {
            return 0;
        }
        try {
            Paint paint = new Paint();
            paint.setTypeface((bulletInfo.isBold()) ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            paint.setTextSize(BulletScreenUtils.dp2px(bulletInfo.getTextSize()));
            Paint.FontMetrics fm = paint.getFontMetrics();
            float height1 = fm.descent;
            return height1;
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * 获取控件中字体绘制的y轴
     */
    public static float getTextDrawY(BulletInnerInfo bulletInfo, float parentHeight) {
        try {
            Paint paint = new Paint();
            paint.setTypeface((bulletInfo.isBold()) ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            paint.setTextSize(BulletScreenUtils.dp2px(bulletInfo.getTextSize()));
            Paint.FontMetrics fontMetrics = paint.getFontMetrics();
            float y = parentHeight / 2 + (Math.abs(fontMetrics.ascent) - fontMetrics.descent) / 2;
            return y;
        } catch (Exception e) {
            e.printStackTrace();
            return 0f;
        }

    }

    /**
     * 获取字体宽度
     */
    public static float getTextWidth(BulletInnerInfo bulletInfo) {
        if (bulletInfo == null) {
            return 0;
        }
        try {
            Paint paint = new Paint();
            Rect rect = new Rect();
            paint.setTypeface((bulletInfo.isBold()) ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
            paint.setTextSize(BulletScreenUtils.dp2px(bulletInfo.getTextSize()));
            paint.getTextBounds(bulletInfo.getText(), 0, bulletInfo.getText().length(), rect);
            return rect.width();
        } catch (Exception e) {
            return 0;
        }
    }
}
