package com.north.light.libnorthlightbullet;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;

import com.north.light.libnorthlightbullet.base.BulletBaseSurfaceView;
import com.north.light.libnorthlightbullet.data.BulletInfo;
import com.north.light.libnorthlightbullet.draw.DrawerHelper;
import com.north.light.libnorthlightbullet.draw.DrawerHelperApi;
import com.north.light.libnorthlightbullet.draw.DrawerHelperDrawingEvent;
import com.north.light.libnorthlightbullet.draw.DrawerStatus;
import com.north.light.libnorthlightbullet.task.DrawTask;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 * FileName: BulletSurfaceView
 * Author: lzt
 * Date: 2023/1/10 14:55
 */
public class BulletSurfaceView extends BulletBaseSurfaceView implements BulletApi {
    private static final String TAG = BulletSurfaceView.class.getSimpleName();

    //是否暂停
    private boolean pausing = false;

    //线程安全对象
    private final Object OJB = new Object();

    //监听
    private BulletDrawBgListener mListener;
    private BulletEventListener mEventListener;

    //显隐
    private boolean show = true;

    private boolean windowFocus = true;

    //是否拖动中
    private AtomicBoolean mSeeking = new AtomicBoolean(false);

    //身份
    private String identify = "";

    private DrawTask drawTask;
    private DrawerHelperApi drawerHelperApi;

    public BulletSurfaceView(Context context) {
        super(context);
        initView();
    }

    public BulletSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView();
    }

    public BulletSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    public void setIdentify(String identify) {
        this.identify = identify;
    }

    public boolean isWindowFocus() {
        return windowFocus;
    }

    public void setWindowFocus(boolean windowFocus) {
        this.windowFocus = windowFocus;
    }

    public boolean isPausing() {
        return pausing;
    }

    public void setPausing(boolean pausing) {
        this.pausing = pausing;
    }

    /**
     * 初始化方法
     */
    private void initView() {
        drawerHelperApi = new DrawerHelper();
        drawTask = new DrawTask(this);
    }


    @Override
    public void timer(long interval) {
        //时间检查
        if (!isPausing() && isWindowFocus()) {
            drawerHelperApi.timer(interval);
        }
        //检查数据--加入显示集合
        if (isWindowFocus() && !mSeeking.get()) {
            drawerHelperApi.checkDataNormal();
        }
        drawBullet();
    }

    /**
     * 能否绘制
     */
    private boolean canDraw() {
        int mWidth = getMeasuredWidth();
        int mHeight = getMeasuredHeight();
        if (mWidth == 0 || mHeight == 0) {
            return false;
        }
        return true;
    }

    /**
     * 绘制方法
     */
    @Override
    protected void drawCanvas(Canvas canvas) {
        Log.d(TAG, "drawCanvas" + identify);
        //线程安全
        synchronized (OJB) {
            //绘制
            if (!canDraw()) {
                return;
            }
            int measureWidth = getMeasuredWidth();
            int measureHeight = getMeasuredHeight();
            if (drawerHelperApi != null) {
                DrawerStatus drawerStatus = new DrawerStatus(show, isWindowFocus(), isPausing());
                drawerHelperApi.draw(canvas, measureWidth, measureHeight, drawerStatus, new DrawerHelperDrawingEvent() {
                    @Override
                    public void drawBg(BulletInfo bulletInfo, Canvas canvas, float left, float top, float right, float bottom) {
                        if (mListener != null) {
                            mListener.drawBg(bulletInfo, canvas, left, top, right, bottom);
                        }
                    }
                });
            }
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        Log.d(TAG, "onWindowFocusChanged hasWindowFocus: " + hasWindowFocus);
        setWindowFocus(hasWindowFocus);
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (drawerHelperApi != null) {
            drawerHelperApi.resetTime();
            drawerHelperApi.clearShowData();
        }
        if (drawTask != null) {
            drawTask.start();
        }
        Log.d(TAG, "onAttachedToWindow: " + identify);
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (drawTask != null) {
            drawTask.release();
        }
        Log.d(TAG, "onDetachedFromWindow: " + identify);
    }

    //外部调用方法-----------------------------------------------------------------------------------

    @Override
    public void addBulletSingle(BulletInfo data) {
        synchronized (OJB) {
            if (drawerHelperApi != null) {
                drawerHelperApi.addBulletSingle(data);
            }
        }
    }

    @Override
    public void addBulletList(List<BulletInfo> data) {
        synchronized (OJB) {
            if (drawerHelperApi != null) {
                drawerHelperApi.addBulletList(data);
            }
        }
    }

    @Override
    public void clear() {
        synchronized (OJB) {
            if (drawerHelperApi != null) {
                drawerHelperApi.clearAllData();
            }
        }
    }

    @Override
    public void clearScreen() {
        synchronized (OJB) {
            if (drawerHelperApi != null) {
                drawerHelperApi.clearScreen();
            }
        }
    }

    @Override
    public void release() {
        setPausing(true);
        super.release();
    }

    @Override
    public void pause() {
        setPausing(true);
    }

    @Override
    public void resume() {
        setPausing(false);
    }

    @Override
    public boolean isPlaying() {
        return !isPausing();
    }

    @Override
    public void show() {
        show = true;
    }

    @Override
    public void hide() {
        show = false;
    }

    @Override
    public void seekTo(long progress) {
        synchronized (OJB) {
            mSeeking.set(true);
            if (drawerHelperApi != null) {
                drawerHelperApi.seekTo(progress);
            }
            mSeeking.set(false);
        }
    }

    @Override
    public void setBulletDrawBgListener(BulletDrawBgListener listener) {
        this.mListener = listener;
    }

    @Override
    public long getCurrentTime() {
        if (drawerHelperApi != null) {
            return drawerHelperApi.getCurrentTime();
        }
        return 0;
    }

    @Override
    public void removeBulletDrawBgListener() {
        this.mListener = null;
    }

    @Override
    public void setBulletEventListener(BulletEventListener listener) {
        this.mEventListener = listener;
    }

    @Override
    public void removeBulletEventListener() {
        this.mEventListener = null;
    }


}
