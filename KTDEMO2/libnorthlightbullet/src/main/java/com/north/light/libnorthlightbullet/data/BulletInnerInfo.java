package com.north.light.libnorthlightbullet.data;

import android.text.TextUtils;

import java.util.Random;

/**
 * FileName: BulletInnerInfo
 * Author: lzt
 * Date: 2023/1/10 20:14
 * 弹幕内部信息类
 */
public class BulletInnerInfo extends BulletInfo {
    //是否已经show
    private boolean shown = false;
    //是否显示后，再消失
    private boolean goneAfterShow = false;
    //当前位置
    private float positionX, positionY;
    //绘制内容宽度,高度
    private float contentWidth, contentHeight;
    //是否已经计算了绘制位置
    private boolean hadCulPos = false;

    //是否已经添加进展示列表
    private boolean isAddToShowList = false;


    public boolean isAddToShowList() {
        return isAddToShowList;
    }

    public void setAddToShowList(boolean addToShowList) {
        isAddToShowList = addToShowList;
    }

    public float getContentHeight() {
        return contentHeight;
    }

    public void setContentHeight(float contentHeight) {
        this.contentHeight = contentHeight;
    }

    public float getContentWidth() {
        return contentWidth;
    }

    public void setContentWidth(float contentWidth) {
        this.contentWidth = contentWidth;
    }

    public boolean isHadCulPos() {
        return hadCulPos;
    }

    public void setHadCulPos(boolean hadCulPos) {
        this.hadCulPos = hadCulPos;
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }

    public boolean isShown() {
        return shown;
    }

    public void setShown(boolean shown) {
        this.shown = shown;
    }

    public boolean isGoneAfterShow() {
        return goneAfterShow;
    }

    public void setGoneAfterShow(boolean goneAfterShow) {
        this.goneAfterShow = goneAfterShow;
    }

    @Override
    public String getId() {
        if (TextUtils.isEmpty(super.getId())) {
            String randomId = String.valueOf(new Random().nextInt(1000));
            return randomId;
        }
        return super.getId();
    }
}
