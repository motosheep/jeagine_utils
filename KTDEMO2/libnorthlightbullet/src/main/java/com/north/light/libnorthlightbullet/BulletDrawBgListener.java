package com.north.light.libnorthlightbullet;

import android.graphics.Canvas;

import com.north.light.libnorthlightbullet.data.BulletInfo;

import java.io.Serializable;

/**
 * FileName: BulletDrawBgListener
 * Author: lzt
 * Date: 2023/1/11 15:07
 * 弹幕绘制背景listener
 */
public interface BulletDrawBgListener extends Serializable {

    void drawBg(BulletInfo bulletInfo, Canvas canvas, float left, float top, float right, float bottom);
}
