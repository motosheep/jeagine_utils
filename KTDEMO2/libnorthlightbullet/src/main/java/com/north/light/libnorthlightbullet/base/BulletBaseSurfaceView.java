package com.north.light.libnorthlightbullet.base;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.PorterDuff;
import android.os.Looper;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import androidx.annotation.NonNull;

/**
 * FileName: BulletSurfaceView
 * Author: lzt
 * Date: 2023/1/10 14:55
 */
public abstract class BulletBaseSurfaceView extends SurfaceView implements SurfaceHolder.Callback, BulletBaseViewApi {
    private static final String TAG = BulletBaseSurfaceView.class.getSimpleName();

    private SurfaceHolder mSurfaceHolder;
    //用于绘图的Canvas
    private Canvas mCanvas;
    //是否准备完成
    private boolean isPrepare = false;

    public BulletBaseSurfaceView(Context context) {
        super(context);
        init();
    }

    public BulletBaseSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public BulletBaseSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        try {
            setZOrderMediaOverlay(true);
            setWillNotCacheDrawing(true);
            setDrawingCacheEnabled(false);
            setWillNotDraw(true);
            mSurfaceHolder = getHolder();
            mSurfaceHolder.addCallback(this);
            mSurfaceHolder.setFormat(PixelFormat.TRANSLUCENT);
            setZOrderOnTop(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceCreated(@NonNull SurfaceHolder holder) {
        Log.d(TAG, "surface life surfaceCreated");
        try {
            mSurfaceHolder = holder;
            Canvas canvas = mSurfaceHolder.lockCanvas();
            if (canvas != null) {
                clearCanvas(canvas);
                mSurfaceHolder.unlockCanvasAndPost(canvas);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        isPrepare = true;
    }

    @Override
    public void surfaceChanged(@NonNull SurfaceHolder holder, int format, int width, int height) {
        Log.d(TAG, "surface life surfaceChanged" );
    }

    @Override
    public void surfaceDestroyed(@NonNull SurfaceHolder holder) {
        Log.d(TAG, "surface life surfaceDestroyed");
        isPrepare = false;
    }

    @Override
    public void drawBullet() {
        try {
            synchronized (mSurfaceHolder) {
                mCanvas = mSurfaceHolder.lockCanvas();
                if (mCanvas != null) {
                    clearCanvas(mCanvas);
                    drawCanvas(mCanvas);
                }
                if (mCanvas != null) {
                    mSurfaceHolder.unlockCanvasAndPost(mCanvas);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 释放
     */
    protected void release() {

    }


    protected Canvas getCanvas() {
        return mCanvas;
    }

    public boolean isPrepare() {
        return isPrepare;
    }

    public void setPrepare(boolean prepare) {
        isPrepare = prepare;
    }

    public static boolean isInMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }

    //子类调用--------------------------------------------------

    /**
     * 清屏
     */
    protected void clearCanvas(Canvas canvas) {
        if (canvas != null) {
            canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        }
    }


    //子类继承--------------------------------------------------
    protected abstract void drawCanvas(Canvas canvas);

}
