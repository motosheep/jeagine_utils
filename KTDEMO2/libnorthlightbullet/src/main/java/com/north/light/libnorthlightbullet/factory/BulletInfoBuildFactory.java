package com.north.light.libnorthlightbullet.factory;

import com.north.light.libnorthlightbullet.data.BulletInfo;

import java.io.Serializable;

/**
 * FileName: BulletInfoBuildFactory
 * Author: lzt
 * Date: 2023/1/12 10:07
 * 外部调用生成弹幕对象类
 */
public class BulletInfoBuildFactory implements Serializable {

    public static BulletInfo create() {
        return new BulletInfo();
    }

}
