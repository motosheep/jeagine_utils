package com.north.light.libnorthlightbullet.data;

import android.graphics.Color;
import android.util.SparseArray;

import java.io.Serializable;

/**
 * FileName: BulletInfo
 * Author: lzt
 * Date: 2023/1/10 14:48
 * 弹幕库
 */
public class BulletInfo implements Serializable {

    /**
     * 主键
     */
    private String id;

    /**
     * 显示文字
     */
    private String text;

    /**
     * 显示时间
     */
    private long showTime;

    /**
     * 文字颜色
     */
    private int textColor = Color.BLACK;

    /**
     * 字体大小
     * */
    private int textSize = 16;

    /**
     * 是否加粗
     * */
    private boolean bold = false;

    /**
     * 外部定义字段
     * */
    private SparseArray<Object> tags = new SparseArray<>();

    public void setTag(int key, Object tag) {
        this.tags.put(key, tag);
    }

    public Object getTag(int key) {
        if (tags == null) {
            return null;
        }
        return tags.get(key);
    }

    public SparseArray<Object> getTags() {
        return tags;
    }

    public void setTags(SparseArray<Object> tags) {
        this.tags = tags;
    }

    public boolean isBold() {
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public int getTextSize() {
        return textSize;
    }

    public void setTextSize(int textSize) {
        this.textSize = textSize;
    }

    public int getTextColor() {
        return textColor;
    }

    public void setTextColor(int textColor) {
        this.textColor = textColor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getShowTime() {
        return showTime;
    }

    public void setShowTime(long showTime) {
        this.showTime = showTime;
    }
}
