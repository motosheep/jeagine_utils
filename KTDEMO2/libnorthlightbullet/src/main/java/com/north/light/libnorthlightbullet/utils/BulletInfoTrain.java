package com.north.light.libnorthlightbullet.utils;

import android.text.TextUtils;

import com.north.light.libnorthlightbullet.data.BulletInfo;
import com.north.light.libnorthlightbullet.data.BulletInnerInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * FileName: BulletInfoTrain
 * Author: lzt
 * Date: 2023/1/10 20:20
 * 弹幕信息转换
 */
public class BulletInfoTrain implements Serializable {

    private static class SingleHolder implements Serializable {
        static BulletInfoTrain mInstance = new BulletInfoTrain();
    }

    public static BulletInfoTrain getInstance() {
        return SingleHolder.mInstance;
    }


    /**
     * 数据转换
     */
    public BulletInnerInfo trainInner(BulletInfo bulletInfo) {
        if (bulletInfo == null) {
            return new BulletInnerInfo();
        }
        BulletInnerInfo cache = new BulletInnerInfo();
        if (!TextUtils.isEmpty(bulletInfo.getId())) {
            cache.setId(bulletInfo.getId());
        } else {
            cache.setId(String.valueOf(System.currentTimeMillis()));
        }
        cache.setShowTime(bulletInfo.getShowTime());
        cache.setText(bulletInfo.getText());
        cache.setTextColor(bulletInfo.getTextColor());
        cache.setTextSize(bulletInfo.getTextSize());
        cache.setBold(bulletInfo.isBold());
        cache.setTags(bulletInfo.getTags());
        return cache;
    }

    /**
     * 列表转换
     */
    public List<BulletInnerInfo> trainInner(List<BulletInfo> list) {
        if (list == null || list.size() == 0) {
            return new ArrayList<>();
        }
        List<BulletInnerInfo> result = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            BulletInfo cache = list.get(i);
            if (TextUtils.isEmpty(cache.getId())) {
                cache.setId(String.valueOf(System.currentTimeMillis()) + i);
            }
            result.add(trainInner(cache));
        }
        return result;
    }

    public BulletInfo trainOuter(BulletInnerInfo bulletInfo) {
        if (bulletInfo == null) {
            return new BulletInfo();
        }
        BulletInfo cache = new BulletInfo();
        cache.setShowTime(bulletInfo.getShowTime());
        cache.setText(bulletInfo.getText());
        cache.setTextColor(bulletInfo.getTextColor());
        cache.setTextSize(bulletInfo.getTextSize());
        cache.setBold(bulletInfo.isBold());
        cache.setId(bulletInfo.getId());
        cache.setTags(bulletInfo.getTags());
        return cache;
    }

}
