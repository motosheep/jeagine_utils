package com.north.light.libnorthlightbullet.draw;

import java.io.Serializable;

/**
 * FileName: DrawerStatus
 * Author: lzt
 * Date: 2023/1/14 15:09
 * 绘制状态
 */
public class DrawerStatus implements Serializable {

    private boolean show;
    private boolean windowFocus;
    private boolean pausing;

    public DrawerStatus(boolean show, boolean windowFocus, boolean pausing) {
        this.show = show;
        this.windowFocus = windowFocus;
        this.pausing = pausing;
    }

    public boolean isShow() {
        return show;
    }

    public void setShow(boolean show) {
        this.show = show;
    }

    public boolean isWindowFocus() {
        return windowFocus;
    }

    public void setWindowFocus(boolean windowFocus) {
        this.windowFocus = windowFocus;
    }

    public boolean isPausing() {
        return pausing;
    }

    public void setPausing(boolean pausing) {
        this.pausing = pausing;
    }
}
