// LibPXMusicListener.aidl
package com.north.light.libmusicplayerx;

// Declare any non-default types here with import statements

interface LibPXMusicListener{
    void init();

    void play(String url);

    void pause(String url);

    void resume(String url);

    void stop(String url);

    void finish(String url);

    void needToInit();
}