
package com.north.light.libmusicplayerx;
import com.north.light.libmusicplayerx.LibPXMusicListener;
import com.north.light.libmusicplayerx.LibPXMusicProgressListener;


interface LibPXMusicBinder {

    void init();

    void log(String message);

    void setLibPXMusicListener(LibPXMusicListener listener);

    void removeLibPXMusicListener(LibPXMusicListener listener);

    void setLibPXMusicProgressListener(LibPXMusicProgressListener listener);

    void removeLibPXMusicProgressListener(LibPXMusicProgressListener listener);

    
        /**
         * 播放音频
         * */
    void play(String url);
    void playAsset(String resId);
    
        /**
         * 停止
         * */
        void stop();
    
        /**
         * 恢复
         * */
        void resume();
    
        /**
         * 暂停
         * */
        void pause();
    
        /**
         * 释放--调用以后，需要重新init
         * */
        void release();
    
        //状态相关-----------------------------------------------------------------------------
    
    
        /**
         * 是否播放中
         * */
        boolean isPlaying();
    
        //进度相关-----------------------------------------------------------------------------
    
        /**
         * 拖动进度
         * */
        void seekTo(int progress);
    
        /**
         * 获取进度
         * */
        int getProgress();
    
        /**
         * 获取总播放时长
         * */
        int getDuration();
}