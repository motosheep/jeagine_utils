package com.north.light.libmusicplayerx;


interface LibPXMusicProgressListener {
       /**
        * 播放进度
        * */
       void progress(String url,int current,int total);

       /**
        * 缓存进度
        * */
       void buffering(String url,int current,int total);
}