package com.north.light.libmusicplayerx.listener

import java.io.Serializable

/**
 * FileName: LibPXMusicEventListener
 * Author: lzt
 * Date: 2023/2/2 10:52
 */
interface LibPXMusicEventListener : Serializable {

    fun play(url: String?)

    fun pause(url: String?)

    fun resume(url: String?)

    fun stop(url: String?)

    fun finish(url: String?)

    fun needToInit()

}