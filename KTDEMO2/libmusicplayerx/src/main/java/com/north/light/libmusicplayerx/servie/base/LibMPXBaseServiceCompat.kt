package com.north.light.libmusicplayerx.servie.base

/**
 * FileName: BaseServiceCompat
 * Author: lzt
 * Date: 2023/3/9 10:00
 */

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build
import android.text.TextUtils
import androidx.core.app.NotificationCompat
import com.north.light.libmusicplayerx.R

abstract class LibMPXBaseServiceCompat : Service() {

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            try {
                //适配安卓8.0
                val channelId = getChannelId
                val channelName = channelName
                val channel = NotificationChannel(
                    channelId.toString(),
                    channelName,
                    NotificationManager.IMPORTANCE_NONE
                )
                val manager = applicationContext.getSystemService(NOTIFICATION_SERVICE)
                        as NotificationManager
                manager.createNotificationChannel(channel)
                startForeground(channelId, notification)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    /**
     * Default content for notification , subclasses can be overwritten and returned
     */
    private val notificationContent: String
        get() = ""

    /**
     * Displayed notifications, subclasses can be overwritten and returned
     */
    private val notification: Notification
        get() = createNormalNotification(notificationContent)

    private fun createNormalNotification(content: String?): Notification {
        val builder = NotificationCompat
            .Builder(this, getChannelId.toString() + "")
        if (TextUtils.isEmpty(content)) {
            return builder.build()
        }
        builder.setContentTitle(notifyContentTitle)
            .setContentText(content)
            .setWhen(System.currentTimeMillis())
            .setSmallIcon(smallIcon)
            .setLargeIcon(largeIcon)
            .setPriority(NotificationCompat.PRIORITY_MIN)
            .build()
        return builder.build()
    }

    /**
     * Large icon for notification , subclasses can be overwritten and returned
     */
    private val largeIcon: Bitmap
        get() = BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher)

    /**
     * Small icon for notification , subclasses can be overwritten and returned
     */
    private val smallIcon: Int
        get() = R.mipmap.ic_launcher

    companion object {
        fun startService(context: Context, intent: Intent?) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                //适配安卓8.0
                context.startForegroundService(intent)
            } else {
                context.startService(intent)
            }
        }
    }

    //子类继承--------------------------------------------------------------
    /**
     * Notification channelName
     */
    protected abstract val channelName: String

    /**
     * Notification channelId,must not be 0
     */
    protected abstract val getChannelId: Int

    protected abstract val notifyContentTitle: String

}