package com.north.light.libmusicplayerx.api

import android.content.Context
import com.north.light.libmusicplayerx.listener.LibPXMusicEventListener
import com.north.light.libmusicplayerx.listener.LibPXMusicProgressListener
import java.io.Serializable

/**
 * FileName: LibPXMusicApi
 * Author: lzt
 * Date: 2023/2/2 10:36
 */
interface LibPXMusicApi : Serializable {

    //控制相关-----------------------------------------------------------------------------

    /**
     * 初始化
     * */
    fun init(context: Context)

    /**
     * 播放音频
     * */
    fun play(url: String)
    fun playAsset(resId: String)

    /**
     * 停止
     * */
    fun stop()

    /**
     * 恢复
     * */
    fun resume()

    /**
     * 暂停
     * */
    fun pause()

    /**
     * 释放--调用以后，需要重新init
     * */
    fun release()

    //状态相关-----------------------------------------------------------------------------


    /**
     * 是否播放中
     * */
    fun isPlaying(): Boolean

    //进度相关-----------------------------------------------------------------------------

    /**
     * 拖动进度
     * */
    fun seekTo(progress: Int)

    /**
     * 获取进度
     * */
    fun getProgress(): Int

    /**
     * 获取总播放时长
     * */
    fun getDuration(): Int

    //监听相关-----------------------------------------------------------------------------


    /**
     * 设置进度监听
     * */
    fun setProgressListener(listener: LibPXMusicProgressListener)

    fun removeProgressListener(listener: LibPXMusicProgressListener)

    fun setEventListener(listener: LibPXMusicEventListener)


    fun removeEventListener(listener: LibPXMusicEventListener)


}