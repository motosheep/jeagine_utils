package com.north.light.libmusicplayerx

import com.north.light.libmusicplayerx.api.LibPXMusicApi
import com.north.light.libmusicplayerx.impl.LibPXMusicMediaOncePlayerImpl
import com.north.light.libmusicplayerx.impl.LibPXMusicMediaPlayerImpl
import java.io.Serializable

/**
 * FileName: LibPXMusic
 * Author: lzt
 * Date: 2023/2/2 10:35
 */
class PXMusic : Serializable {
    private var musicImpl: LibPXMusicApi? = null
    private var musicOnceImpl: LibPXMusicApi? = null

    companion object {
        @JvmStatic
        fun getPlayer(): LibPXMusicApi {
            return SingleHolder.player
        }

        @JvmStatic
        fun getOncePlayer(): LibPXMusicApi {
            return SingleHolder.oncePlayer
        }
    }

    object SingleHolder : Serializable {
        val player = PXMusic().musicImpl ?: throw Exception("don't fuck me")
        val oncePlayer = PXMusic().musicOnceImpl ?: throw Exception("don't fuck me")
    }

    init {
        musicImpl = LibPXMusicMediaPlayerImpl()
        musicOnceImpl = LibPXMusicMediaOncePlayerImpl()
    }


}