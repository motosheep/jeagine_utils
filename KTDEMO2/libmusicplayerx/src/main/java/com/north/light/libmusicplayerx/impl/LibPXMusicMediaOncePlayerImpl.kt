package com.north.light.libmusicplayerx.impl

import android.content.Context
import android.media.MediaPlayer
import com.north.light.libmusicplayerx.api.LibPXMusicApi
import com.north.light.libmusicplayerx.listener.LibPXMusicEventListener
import com.north.light.libmusicplayerx.listener.LibPXMusicProgressListener

/**
 * FileName: LibPXMusicMediaOncePlayerImpl
 * Author: lzt
 * Date: 2023/2/22 14:20
 * 只播放一次，没有其他任何回调，每次播放都会new一个对象
 */
class LibPXMusicMediaOncePlayerImpl : LibPXMusicApi {
    //外部传入context
    private lateinit var mContext: Context

    override fun init(context: Context) {
        mContext = context.applicationContext
    }

    override fun play(url: String) {

    }

    override fun playAsset(resId: String) {
        val assets = mContext.assets.openFd(resId)
        val mPlayer = MediaPlayer()
        mPlayer.reset()
        mPlayer.setDataSource(assets)
        mPlayer.prepareAsync()
        mPlayer.setOnPreparedListener {
            mPlayer.start()
        }
    }

    override fun stop() {

    }

    override fun resume() {

    }

    override fun pause() {

    }

    override fun release() {

    }

    override fun isPlaying(): Boolean {
        return false
    }

    override fun seekTo(progress: Int) {

    }

    override fun getProgress(): Int {
        return 0
    }

    override fun getDuration(): Int {
        return 0
    }

    override fun setProgressListener(listener: LibPXMusicProgressListener) {

    }

    override fun removeProgressListener(listener: LibPXMusicProgressListener) {

    }

    override fun setEventListener(listener: LibPXMusicEventListener) {

    }

    override fun removeEventListener(listener: LibPXMusicEventListener) {

    }
}