package com.north.light.libmusicplayerx.listener

import java.io.Serializable

/**
 * FileName: LibPXMusicProgressListener
 * Author: lzt
 * Date: 2023/2/2 10:41
 */
interface LibPXMusicProgressListener : Serializable {

    /**
     * 播放进度
     * */
    fun progress(url: String?, current: Int, total: Int)

    /**
     * 缓存进度
     * */
    fun buffering(url: String?, current: Int, total: Int)
}