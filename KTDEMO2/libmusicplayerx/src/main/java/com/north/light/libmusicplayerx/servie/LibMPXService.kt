package com.north.light.libmusicplayerx.servie

import android.content.Intent
import android.os.Handler
import android.os.IBinder
import android.os.Looper
import com.north.light.libmusicplayerx.LibPXMusicBinder
import com.north.light.libmusicplayerx.LibPXMusicListener
import com.north.light.libmusicplayerx.LibPXMusicProgressListener
import com.north.light.libmusicplayerx.PXMusic
import com.north.light.libmusicplayerx.listener.LibPXMusicEventListener
import com.north.light.libmusicplayerx.servie.base.LibMPXBaseServiceCompat
import com.north.light.libmusicplayerx.utils.KtLogUtil
import java.util.concurrent.CopyOnWriteArrayList

class LibMPXService : LibMPXBaseServiceCompat() {

    override val channelName: String
        get() = javaClass.simpleName
    override val getChannelId: Int
        get() = 10011
    override val notifyContentTitle: String
        get() = "music player"

    private var mHandler: Handler? = null

    private val mEventListener: CopyOnWriteArrayList<LibPXMusicListener> = CopyOnWriteArrayList()
    private val mProgressListener: CopyOnWriteArrayList<LibPXMusicProgressListener> = CopyOnWriteArrayList()

    override fun onCreate() {
        mHandler?.removeCallbacksAndMessages(null)
        mHandler = Handler(Looper.getMainLooper())
        PXMusic.getPlayer().init(this)
        PXMusic.getPlayer().setEventListener(musicEvent)
        PXMusic.getPlayer().setProgressListener(musicProgressListener)
        KtLogUtil.d("LibMPXService onCreate")
        super.onCreate()
    }

    override fun onDestroy() {
        mHandler?.removeCallbacksAndMessages(null)
        PXMusic.getPlayer().removeEventListener(musicEvent)
        PXMusic.getPlayer().removeProgressListener(musicProgressListener)
        KtLogUtil.d("LibMPXService onDestroy")
        super.onDestroy()
    }

    private val musicEvent = object : LibPXMusicEventListener {
        override fun play(url: String?) {
            mEventListener.forEach {
                it.play(url)
            }
        }

        override fun pause(url: String?) {
            mEventListener.forEach {
                it.pause(url)
            }
        }

        override fun resume(url: String?) {
            mEventListener.forEach {
                it.resume(url)
            }
        }

        override fun stop(url: String?) {
            mEventListener.forEach {
                it.stop(url)
            }
        }

        override fun finish(url: String?) {
            mEventListener.forEach {
                it.finish(url)
            }
        }

        override fun needToInit() {
            mEventListener.forEach {
                it.needToInit()
            }
        }

    }

    private val musicProgressListener = object : com.north.light.libmusicplayerx.listener.LibPXMusicProgressListener {
        override fun progress(url: String?, current: Int, total: Int) {
            mProgressListener.forEach {
                it.progress(url, current, total)
            }
        }

        override fun buffering(url: String?, current: Int, total: Int) {
            mProgressListener.forEach {
                it.buffering(url, current, total)
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder {
        return mBinder
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        return super.onStartCommand(intent, flags, startId)
    }


    private var mBinder = object : LibPXMusicBinder.Stub() {
        override fun init() {
            KtLogUtil.d("LibMPXService init")
            PXMusic.getPlayer()
            PXMusic.getOncePlayer()
            mEventListener.forEach {
                it.init()
            }
        }

        override fun log(message: String?) {
            KtLogUtil.d("LibMPXService log${message}")
        }

        override fun setLibPXMusicListener(listener: LibPXMusicListener?) {
            if (listener == null) {
                return
            }
            mEventListener.add(listener)
        }

        override fun removeLibPXMusicListener(listener: LibPXMusicListener?) {
            if (listener == null) {
                return
            }
            mEventListener.remove(listener)
        }

        override fun setLibPXMusicProgressListener(listener: LibPXMusicProgressListener?) {
            if (listener == null) {
                return
            }
            mProgressListener.add(listener)
        }

        override fun removeLibPXMusicProgressListener(listener: LibPXMusicProgressListener?) {
            if (listener == null) {
                return
            }
            mProgressListener.remove(listener)
        }

        override fun play(url: String?) {
            KtLogUtil.d("LibMPXService play${url}")
            PXMusic.getPlayer().play(url ?: return)
        }

        override fun playAsset(resId: String?) {
            PXMusic.getPlayer().playAsset(resId ?: return)
        }

        override fun stop() {
            PXMusic.getPlayer().stop()
        }

        override fun resume() {
            PXMusic.getPlayer().resume()
        }

        override fun pause() {
            PXMusic.getPlayer().pause()
        }

        override fun release() {
            PXMusic.getPlayer().release()
        }

        override fun isPlaying(): Boolean {
            return PXMusic.getPlayer().isPlaying()
        }

        override fun seekTo(progress: Int) {
            PXMusic.getPlayer().seekTo(progress)
        }

        override fun getProgress(): Int {
            return PXMusic.getPlayer().getProgress()
        }

        override fun getDuration(): Int {
            return PXMusic.getPlayer().getDuration()
        }
    }
}