package com.north.light.libmusicplayerx.impl

/**
 * FileName: Controller
 * Author: lzt
 * Date: 2023/2/2 13:55
 */
object LibPXMusicHandlerParams {
    const val HANDLER_COMPLETE = -2
    const val HANDLER_NOT_INIT = -1
    const val HANDLER_PLAY = 0
    const val HANDLER_PAUSE = 1
    const val HANDLER_RESUME = 2
    const val HANDLER_STOP = 3
}