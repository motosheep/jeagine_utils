package com.north.light.libbilibilidfm.base;

import android.content.Context;
import android.util.AttributeSet;

import master.flame.danmaku.ui.widget.DanmakuView;

/**
 * FileName: BiliDanmakuView
 * Author: lzt
 * Date: 2022/12/12 11:28
 */
public class BiliDanmakuView extends DanmakuView {

    
    public BiliDanmakuView(Context context) {
        super(context);
    }

    public BiliDanmakuView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BiliDanmakuView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
