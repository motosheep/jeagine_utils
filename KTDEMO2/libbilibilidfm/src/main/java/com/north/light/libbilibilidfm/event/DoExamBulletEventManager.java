package com.north.light.libbilibilidfm.event;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * FileName: DoExamBulletEventManager
 * Author: lzt
 * Date: 2022/12/15 08:59
 * 做题弹幕管理类
 */
public class DoExamBulletEventManager implements Serializable {
    private ConcurrentHashMap<String, DoExamBulletEventListener> mListenerMap = new ConcurrentHashMap<>();

    private static class SingleHolder implements Serializable {
        static DoExamBulletEventManager mInstance = new DoExamBulletEventManager();
    }

    public static DoExamBulletEventManager getInstance() {
        return SingleHolder.mInstance;
    }


    public void setBulletEventListener(String key, DoExamBulletEventListener listener) {
        if (listener == null || TextUtils.isEmpty(key)) {
            return;
        }
        mListenerMap.put(key, listener);
    }


    public void removeBulletEventListener(String key) {
        if (TextUtils.isEmpty(key)) {
            return;
        }
        mListenerMap.remove(key);
    }


    /**
     * 通知播放or暂停
     *
     * @param pageType 1首页推荐 2首页关注 3投一投详情
     */
    public void notifyPlaying(int adapterPos, int pageType, boolean play, String url) {
        if (mListenerMap == null || mListenerMap.size() == 0) {
            return;
        }
        for (Map.Entry<String, DoExamBulletEventListener> cache : mListenerMap.entrySet()) {
            if (cache.getValue() != null) {
                if (play) {
                    cache.getValue().play(adapterPos, pageType, url);
                } else {
                    cache.getValue().pause(adapterPos, pageType, url);
                }
            }
        }
    }

    /**
     * 通知进度条
     *
     * @param pageType 1首页推荐 2首页关注 3投一投详情
     */
    public void notifyProgress(int adapterPos, int pageType, String url, long progress, long duration) {
        if (mListenerMap == null || mListenerMap.size() == 0) {
            return;
        }
        for (Map.Entry<String, DoExamBulletEventListener> cache : mListenerMap.entrySet()) {
            if (cache.getValue() != null) {
                cache.getValue().progress(adapterPos, pageType, url, progress, duration);
            }
        }
    }


}
