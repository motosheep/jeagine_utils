package com.north.light.libbilibilidfm.base;

import android.content.Context;
import android.util.AttributeSet;

import master.flame.danmaku.ui.widget.DanmakuSurfaceView;

/**
 * FileName: BiliDanmakuView
 * Author: lzt
 * Date: 2022/12/12 11:28
 */
public class BiliDanmakuSurfaceView extends DanmakuSurfaceView {


    public BiliDanmakuSurfaceView(Context context) {
        super(context);
    }

    public BiliDanmakuSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public BiliDanmakuSurfaceView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
