package com.north.light.libbilibilidfm;

import java.io.Serializable;

/**
 * FileName: DoExamCusBulletInfo
 * Author: lzt
 * Date: 2022/12/12 15:09
 * 弹幕信息类
 */
public class DoExamCusBulletInfo implements Serializable {
    //显示标题
    private String content;
    //出现的时间--相对于视频
    private long showTime;
    //是否用户自身
    private boolean owner = false;
    //字体大小--默认16
    private float textSize = 14f;


    //本地数据--与前一个弹幕的间隔
    private long beforeInterval = 0L;

    @Deprecated
    public DoExamCusBulletInfo(String content, long showTime) {
        this.content = content;
        this.showTime = showTime;
    }

    public DoExamCusBulletInfo(String content, long showTime, boolean owner) {
        this.content = content;
        this.showTime = showTime;
        this.owner = owner;
    }

    public long getBeforeInterval() {
        return beforeInterval;
    }

    public void setBeforeInterval(long beforeInterval) {
        this.beforeInterval = beforeInterval;
    }

    public float getTextSize() {
        return textSize;
    }

    public void setTextSize(float textSize) {
        this.textSize = textSize;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public long getShowTime() {
        return showTime;
    }

    public void setShowTime(long showTime) {
        this.showTime = showTime;
    }
}
