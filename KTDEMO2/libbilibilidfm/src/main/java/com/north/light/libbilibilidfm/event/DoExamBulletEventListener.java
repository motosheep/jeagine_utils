package com.north.light.libbilibilidfm.event;

/**
 * FileName: DoExamBulletEventListener
 * Author: lzt
 * Date: 2022/12/15 09:00
 */
public interface DoExamBulletEventListener {

    void play(int adapterPos, int pageType, String url);

    void pause(int adapterPos, int pageType, String url);

    void progress(int adapterPos, int pageType, String url, long progress, long duration);
}
