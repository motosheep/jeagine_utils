package com.north.light.libbilibilidfm.utils;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * FileName: CollectionsUtils
 * Author: lzt
 * Date: 2022/4/2 10:13
 * change by lzt 20220428 增加克隆对象func
 */
public class DoExamCusBulletCollectionsUtils implements Serializable {

    /**
     * 判断集合是否为空
     */
    public static <T> boolean isEmpty(Collection<T> source) {
        return source == null || source.size() == 0;
    }


    //克隆相关-start----------------------------------------------------------------------------------


    /**
     * 克隆ser序列list
     */
    public static <T> ArrayList<T> cloneListSer(ArrayList<T> source) {
        if (isEmpty(source)) {
            return new ArrayList<>();
        }
        ArrayList<T> result = new ArrayList<>();
        for (T cacheT : source) {
            T target = cloneObjectSer(cacheT);
            result.add(target);
        }
        return result;
    }

    /**
     * 克隆ser序列list
     */
    public static <T> List<T> cloneListSer(List<T> source) {
        if (isEmpty(source)) {
            return new ArrayList<>();
        }
        List<T> result = new ArrayList<>();
        for (T cacheT : source) {
            T target = cloneObjectSer(cacheT);
            result.add(target);
        }
        return result;
    }

    /**
     * 传入对象需要实现Serializable
     */
    public static <T> T cloneObjectSer(T obj) {
        T result = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        ByteArrayInputStream byteArrayInputStream = null;
        ObjectOutputStream outputStream = null;
        ObjectInputStream inputStream = null;
        try {
            //对象写到内存中
            byteArrayOutputStream = new ByteArrayOutputStream();
            outputStream = new ObjectOutputStream(byteArrayOutputStream);
            outputStream.writeObject(obj);

            //从内存中再读出来
            byteArrayInputStream = new ByteArrayInputStream(byteArrayOutputStream.toByteArray());
            inputStream = new ObjectInputStream(byteArrayInputStream);
            result = (T) inputStream.readObject();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                if (outputStream != null)
                    outputStream.close();
                if (inputStream != null)
                    inputStream.close();
                if (byteArrayOutputStream != null)
                    byteArrayOutputStream.close();
                if (byteArrayInputStream != null)
                    byteArrayInputStream.close();

            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return result;
    }

    /**
     * 传入对象需要实现Parcelable
     */
    public static <T> T cloneObjectPar(Parcelable input) {
        Parcel parcel = null;
        try {
            parcel = Parcel.obtain();
            parcel.writeParcelable(input, 0);
            parcel.setDataPosition(0);
            return parcel.readParcelable(input.getClass().getClassLoader());
        } finally {
            parcel.recycle();
        }
    }

    //克隆相关-end----------------------------------------------------------------------------------

    /**
     * 取某个数据集合的前N条
     */
    public static <T> List<T> getListWithCount(List<T> source, int count) {
        if (isEmpty(source)) {
            return new ArrayList<>();
        }
        if (source.size() <= count) {
            return source;
        }
        List<T> result = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            result.add(source.get(i));
        }
        return result;
    }

}
