package com.north.light.libbilibilidfm;

import java.util.List;

/**
 * FileName: DoExamCusBulletViewApi
 * Author: lzt
 * Date: 2022/12/12 15:12
 */
public interface DoExamCusBulletViewApi {

    /**
     * 设置数据--设置后会自动播放
     */
    public void setData(List<DoExamCusBulletInfo> doExamList);

    /**
     * 插入数据--用户发送了一个弹幕
     */
    public void addBullet(DoExamCusBulletInfo info);

    /**
     * 释放
     */
    public void clean();

    /**
     * 滚动到指定位置
     */
    public void seek(long time);

    /**
     * 重新播放
     */
    public void replay();

    /**
     * 设置标识
     */
    public void setIdentify(String key);

    /**
     * 显示隐藏
     */
    public void setShow(boolean show,boolean autoResume);
}
