package com.north.light.libltobserver.contentprovider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.north.light.libltobserver.thread.LTThreadUtils;

/**
 * FileName: LTContentProvider
 * Author: lzt
 * Date: 2022/11/17 16:09
 */
public class LTContentProvider extends ContentProvider {

    /**
     * 全局content
     */
    private static volatile Context mAppContent;


    public static Context getAppContent() {
        return mAppContent;
    }

    @Override
    public boolean onCreate() {
        mAppContent = getContext().getApplicationContext();
        LTThreadUtils.getInstance().init();
        return false;
    }

    @Nullable
    @Override
    public Cursor query(@NonNull Uri uri, @Nullable String[] projection, @Nullable String selection, @Nullable String[] selectionArgs, @Nullable String sortOrder) {
        return null;
    }

    @Nullable
    @Override
    public String getType(@NonNull Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(@NonNull Uri uri, @Nullable ContentValues values) {
        return null;
    }

    @Override
    public int delete(@NonNull Uri uri, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }

    @Override
    public int update(@NonNull Uri uri, @Nullable ContentValues values, @Nullable String selection, @Nullable String[] selectionArgs) {
        return 0;
    }
}
