package com.north.light.libltobserver;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.north.light.libltobserver.impl.LTObserverReceiver;
import com.north.light.libltobserver.impl.LTObserverSender;

import java.io.Serializable;

/**
 * FileName: LTObserver
 * Author: lzt
 * Date: 2022/11/15 14:33
 * 自定义观察者
 */
public class LTObserver implements Serializable {


    //接收者构造
    public static LTObserverReceiver with(AppCompatActivity activity) {
        return new LTObserverReceiver(activity);
    }

//    public static LTObserverReceiver with(Fragment fragment) {
//        return new LTObserverReceiver(fragment);
//    }

    //发送事件
    public static LTObserverSender sender() {
        return new LTObserverSender();
    }

}
