package com.north.light.libltobserver.context;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import java.io.Serializable;

/**
 * FileName: LTContextInfo
 * Author: lzt
 * Date: 2022/11/15 14:36
 * context builder
 */
public class LTContextInfo implements Serializable {

    private AppCompatActivity activity;

//    private Fragment fragment;

    private LTContextType contextType;

    /**
     * 设置type
     */
    public void setActivity(AppCompatActivity activity) {
        this.contextType = LTContextType.TYPE_X_ACTIVITY;
        this.activity = activity;
    }

//    public void setFragment(Fragment fragment) {
//        this.contextType = LTContextType.TYPE_X_FRAGMENT;
//        this.fragment = fragment;
//    }

    public AppCompatActivity getActivity() {
        return activity;
    }

//    public Fragment getFragment() {
//        return fragment;
//    }

    public LTContextType getContextType() {
        return contextType;
    }

    public void setContextType(LTContextType contextType) {
        this.contextType = contextType;
    }
}
