package com.north.light.libltobserver.thread;

import android.os.Handler;
import android.os.Looper;

import java.io.Serializable;

/**
 * FileName: LTThreadUtils
 * Author: lzt
 * Date: 2022/11/17 16:10
 */
public class LTThreadUtils implements Serializable {
    private Handler mUIHandler;

    private static class SingleHolder implements Serializable {
        static LTThreadUtils mInstance = new LTThreadUtils();
    }

    public static LTThreadUtils getInstance() {
        return SingleHolder.mInstance;
    }

    private Handler getUIHandler() {
        return mUIHandler;
    }

    public void init() {
        if (mUIHandler != null) {
            mUIHandler.removeCallbacksAndMessages(null);
        }
        mUIHandler = null;
        mUIHandler = new Handler(Looper.getMainLooper());
    }

    public void runOnUIThread(Runnable runnable) {
        if (getUIHandler() == null) {
            return;
        }
        getUIHandler().post(runnable);
    }


}
