package com.north.light.libltobserver.context;

import java.io.Serializable;

/**
 * FileName: LTContextType
 * Author: lzt
 * Date: 2022/11/15 14:37
 */
public enum LTContextType implements Serializable {
    TYPE_X_ACTIVITY(1, "activity"),
//    TYPE_X_FRAGMENT(2, "fragment"),
    ;


    private int type;
    private String desc;

    LTContextType(int type, String desc) {
        this.type = type;
        this.desc = desc;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
