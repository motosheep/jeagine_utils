package com.north.light.libltobserver.listener;

import androidx.annotation.NonNull;

/**
 * FileName: LTObserverListener
 * Author: lzt
 * Date: 2022/11/16 16:57
 * 监听回调
 */
public abstract class LTObserverListener<T> {

    public abstract void callback(@NonNull T t);
}
