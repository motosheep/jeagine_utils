package com.north.light.libltobserver.bean;

import java.io.Serializable;

/**
 * FileName: LTTrainBean
 * Author: lzt
 * Date: 2022/11/17 09:51
 */
public class LTTrainBean<T> implements Serializable {
    private int pos;
    private T t;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
