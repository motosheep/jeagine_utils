package com.north.light.libltobserver.pool;

import android.text.TextUtils;

import com.north.light.libltobserver.impl.LTObserverReceiver;

import java.io.Serializable;
import java.util.concurrent.ConcurrentHashMap;

/**
 * FileName: LTObserverPool
 * Author: lzt
 * Date: 2022/11/16 16:15
 * 观察者对象池
 */
public class LTObserverPool implements Serializable {

    private ConcurrentHashMap<String, LTObserverReceiver> mObserverBuilderMap = new ConcurrentHashMap<>();

    private static class SingleHolder implements Serializable {
        static LTObserverPool mInstance = new LTObserverPool();
    }

    public static LTObserverPool getInstance() {
        return SingleHolder.mInstance;
    }

    /**
     * 添加复用对象
     */
    public void add(String tag, LTObserverReceiver builder) {
        if (TextUtils.isEmpty(tag)) {
            return;
        }
        mObserverBuilderMap.put(tag, builder);
    }

    /**
     * 移除复用对象
     */
    public void remove(String tag) {
        if (TextUtils.isEmpty(tag)) {
            return;
        }
        mObserverBuilderMap.remove(tag);
    }

    /**
     * 判断复用对象是否存在
     */
    public boolean checkExist(String tag) {
        return mObserverBuilderMap.get(tag) != null;
    }


    public ConcurrentHashMap<String, LTObserverReceiver> getObserverBuilderMap() {
        return mObserverBuilderMap;
    }

    /**
     * 获取指定tag的builder
     */
    public LTObserverReceiver getBuilder(String tag) {
        if (TextUtils.isEmpty(tag)) {
            return null;
        }
        return mObserverBuilderMap.get(tag);
    }
}
