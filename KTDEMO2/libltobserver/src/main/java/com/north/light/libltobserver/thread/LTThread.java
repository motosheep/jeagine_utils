package com.north.light.libltobserver.thread;

/**
 * FileName: LTThread
 * Author: lzt
 * Date: 2022/11/17 16:15
 */
public enum LTThread {
    MAIN(1);

    private int type;

    LTThread(int type) {
        this.type = type;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }
}
