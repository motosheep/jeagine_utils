package com.north.light.libtencentbugly

import android.content.Context
import com.tencent.bugly.crashreport.CrashReport
import com.tencent.bugly.crashreport.CrashReport.UserStrategy
import java.io.Serializable

/**
 * FileName: TCBuglyManager
 * Author: lzt
 * Date: 2023/4/26 10:36
 * 腾讯bugly管理类
 */
class TCBuglyManager : Serializable {

    private object SingleHolder : Serializable {
        val mInstance = TCBuglyManager()
    }

    companion object {

        @JvmStatic
        fun getInstance(): TCBuglyManager {
            return SingleHolder.mInstance
        }
    }


    /**
     * 初始化--必须同意隐私后才能调用
     * */
    fun init(context: Context, isDebug: Boolean, info: TCBuglyInfo) {
        try {
            val packageName = context.packageName
            val processName: String = LibBuglyProcessUtil.getInstance()
                .getCurrentProcessName(context.applicationContext) ?: return
            val strategy = UserStrategy(context)
            strategy.isUploadProcess = (processName == packageName)
            if (!info.channelCode.isNullOrBlank()) {
                strategy.appChannel = info.channelCode
            }
            if (!info.deviceId.isNullOrBlank()) {
                strategy.deviceID = info.deviceId
            }
            if (!info.deviceModel.isNullOrBlank()) {
                strategy.deviceModel = info.deviceModel
            } else {
                strategy.deviceModel = LibBuglyBrandUtils.getInstance().getBrand()
            }

            if (!info.appId.isNullOrBlank()) {
                // Bugly 会在启动20s后联网同步数据
                strategy.appReportDelay = 20000
                CrashReport.setIsDevelopmentDevice(context, isDebug)
                CrashReport.initCrashReport(context, info.appId, isDebug, strategy)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }


    class TCBuglyInfo : Serializable {
        var appId: String? = null
        var channelCode: String? = null
        var deviceId: String? = null
        var deviceModel: String? = null
    }
}