package com.north.light.libtencentbugly

import android.os.Build
import android.text.TextUtils
import java.io.Serializable

/**
 * FileName: BrandUtils
 * Author: lzt
 * Date: 2023/4/26 13:55
 */
class LibBuglyBrandUtils : Serializable {

    private val XIAOMI = "xiaomi"
    private val HUAWEI = "huawei"
    private val OPPO = "oppo"
    private val VIVO = "vivo"
    private val MEIZU = "meizu"
    private val MEIZU2 = "22c4185e"

    private object SingleHolder {
        var mInstance = LibBuglyBrandUtils()
    }

    companion object {
        @JvmStatic
        fun getInstance(): LibBuglyBrandUtils {
            return SingleHolder.mInstance
        }
    }


    /**
     * 获取手机的品牌
     */
    fun getBrand(): String {
        val model = Build.MODEL
        if (!TextUtils.isEmpty(Build.BRAND)) {
            return Build.BRAND + "/" + model
        } else if (!TextUtils.isEmpty(Build.MANUFACTURER)) {
            return Build.MANUFACTURER + "/" + model
        }
        return model
    }

    fun isBrandXiaoMi(): Boolean {
        return getBrand().lowercase() == XIAOMI
    }

    fun isBrandHuawei(): Boolean {
        return getBrand().lowercase() == HUAWEI
    }

    fun isBrandMeizu(): Boolean {
        return getBrand().lowercase() == MEIZU || getBrand().lowercase() == MEIZU2
    }

    fun isBrandOppo(): Boolean {
        return getBrand().lowercase() == OPPO
    }

    fun isBrandVivo(): Boolean {
        return getBrand().lowercase() == VIVO
    }
}