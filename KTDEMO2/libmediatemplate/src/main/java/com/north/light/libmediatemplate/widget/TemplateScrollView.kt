package com.north.light.libmediatemplate.widget

import android.content.Context
import android.util.AttributeSet
import androidx.core.widget.NestedScrollView

/**
 * FileName: TemplateScrollView
 * Author: lzt
 * Date: 2023/7/5 10:08
 * Description:
 */
class TemplateScrollView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null
) : NestedScrollView(context, attrs) {

    init {
        overScrollMode = NestedScrollView.OVER_SCROLL_NEVER
    }
}