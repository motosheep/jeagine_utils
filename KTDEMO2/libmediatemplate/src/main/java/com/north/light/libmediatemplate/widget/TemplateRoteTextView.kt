package com.north.light.libmediatemplate.widget

import android.content.Context
import android.util.AttributeSet
import com.north.light.libmediatemplate.textview.AndroidRotateImplTextView
import com.north.light.libmediatemplate.textview.AndroidRotateTextView
import com.north.light.libmediatemplate.widget.base.TemplateNormalTextView

/**
 * FileName: TemplateVerticalTextView
 * Author: lizhengting
 * Date: 2023/6/30 23:31
 * Description:
 */
class TemplateRoteTextView(context: Context, attrs: AttributeSet? = null) :
    TemplateNormalTextView(context, attrs) {


    override fun getImpTextView(attrs: AttributeSet?): AndroidRotateImplTextView {
        return AndroidRotateTextView(context, attrs)
    }
}