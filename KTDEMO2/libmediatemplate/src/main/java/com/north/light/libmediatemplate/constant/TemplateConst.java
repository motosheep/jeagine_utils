package com.north.light.libmediatemplate.constant;

import java.io.Serializable;

/**
 * FileName: TemplateConst
 * Author: lzt
 * Date: 2023/7/7 13:44
 * Description:
 */
public class TemplateConst implements Serializable {

    public static final int NO_RESOURCE = -100;
}
