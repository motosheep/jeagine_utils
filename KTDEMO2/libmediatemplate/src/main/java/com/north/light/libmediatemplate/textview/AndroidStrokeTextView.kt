package com.north.light.libmediatemplate.textview

import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.TextView

/**
 * FileName: StrokeTextView
 * Author: lzt
 * Date: 2023/7/19 08:38
 * Description:继承了基类，扩展了描边的textview，描边默认关闭
 */
open class AndroidStrokeTextView : AndroidBaseTextView {

    private var outlineTextView: TextView

    //描边相关变量
    private var mStrokeSwitch = false
    private var mStrokeColor = Color.BLACK
    private var mStrokeWidth = 3f

    constructor(context: Context) : super(context) {
        outlineTextView = TextView(context)
        init()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        outlineTextView = TextView(context, attrs)
        init()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyle: Int) : super(
            context,
            attrs,
            defStyle
    ) {
        outlineTextView = TextView(context, attrs, defStyle)
        init()
    }

    private fun init() {
        outlineTextView.includeFontPadding = false
        initStrokePaint(mStrokeWidth, mStrokeColor)
    }

    /**
     * 初始化描边颜色
     * */
    private fun initStrokePaint(strokeWidth: Float, strokeColor: Int) {
        val paint = outlineTextView.paint
        paint.strokeWidth = strokeWidth // 描边宽度
        paint.style = Paint.Style.STROKE
        outlineTextView.setTextColor(strokeColor) // 描边颜色
        outlineTextView.gravity = gravity
    }

    override fun setLayoutParams(params: ViewGroup.LayoutParams) {
        super.setLayoutParams(params)
        outlineTextView.layoutParams = params
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        //设置轮廓文字
        val outlineText = outlineTextView.text
        if (outlineText == null || outlineText != this.text) {
            outlineTextView.text = text
            postInvalidate()
        }
        outlineTextView.measure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onLayout(changed: Boolean, left: Int, top: Int, right: Int, bottom: Int) {
        super.onLayout(changed, left, top, right, bottom)
        outlineTextView.layout(left, top, right, bottom)
    }

    override fun onDraw(canvas: Canvas) {
        if (mStrokeSwitch) {
            outlineTextView.draw(canvas)
        }
        super.onDraw(canvas)
    }

    override fun setTypeface(tf: Typeface?) {
        super.setTypeface(tf)
        if (outlineTextView != null && tf != null) {
            outlineTextView.typeface = tf
        }
    }

    override fun setTextSize(size: Float) {
        super.setTextSize(size)
        if (outlineTextView != null) {
            outlineTextView.textSize = size
        }
    }


    //外部调用方法-----------------------------------------------------------------------------------


    /**
     * 开关描边
     * */
    fun switchStroke(open: Boolean) {
        this.mStrokeSwitch = open
        postInvalidate()
    }

    /**
     * 设置描边大小
     * */
    fun strokeWidth(size: Float) {
        this.mStrokeWidth = size
        initStrokePaint(mStrokeWidth, mStrokeColor)
        postInvalidate()
    }

    /**
     * 设置描边颜色
     * */
    fun strokeColor(color: Int) {
        this.mStrokeColor = color
        initStrokePaint(mStrokeWidth, mStrokeColor)
        postInvalidate()
    }

}