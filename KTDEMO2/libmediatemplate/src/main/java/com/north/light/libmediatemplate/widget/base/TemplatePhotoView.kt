package com.north.light.libmediatemplate.widget.base

import android.content.Context
import android.graphics.Canvas
import android.graphics.Path
import android.graphics.RectF
import android.util.AttributeSet
import com.north.light.libmediatemplate.third.photoview.PhotoView

/**
 * FileName: TemplatePhotoView
 * Author: lizhengting
 * Date: 2023/6/29 23:09
 * Description:
 */
class TemplatePhotoView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : PhotoView(context, attrs) {

    init {

    }


}