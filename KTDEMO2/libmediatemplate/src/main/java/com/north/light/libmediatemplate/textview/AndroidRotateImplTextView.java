package com.north.light.libmediatemplate.textview;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * FileName: AndroidRotateImplTextView
 * Author: lzt
 * Date: 2023/7/19 09:01
 * Description:基类基础impl
 */
public class AndroidRotateImplTextView extends AndroidStrokeTextView {


    public AndroidRotateImplTextView(@NonNull Context context) {
        super(context);
    }

    public AndroidRotateImplTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AndroidRotateImplTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }
}
