package com.north.light.libmediatemplate.widget

import android.content.Context
import android.util.AttributeSet
import androidx.constraintlayout.widget.ConstraintLayout

/**
 * FileName: TemplateRootView
 * Author: lzt
 * Date: 2023/6/29 11:12
 * Description:
 */
class TemplateRootView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null
) : ConstraintLayout(context, attrs) {

    init {

    }
}