package com.north.light.libmediatemplate.widget.base

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Canvas
import android.util.AttributeSet
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import androidx.cardview.widget.CardView
import com.north.light.libmediatemplate.R
import com.north.light.libmediatemplate.constant.TemplateConst
import com.north.light.libmediatemplate.cusview.RadiusCardView
import com.north.light.libmediatemplate.utils.KtLogUtil

/**
 * FileName: TemplateImageView
 * Author: lzt
 * Date: 2023/6/29 11:13
 * Description:模板编辑--图片控件
 */
open class TemplateImageView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null
) : RelativeLayout(context, attrs) {

    private var ivContent: ImageView = getImageView()
    private var vCard: RadiusCardView = RadiusCardView(context, attrs)

    private var selBg = TemplateConst.NO_RESOURCE
    private var unSelBg = TemplateConst.NO_RESOURCE

    //中间图标变量
    private var middleSrc = TemplateConst.NO_RESOURCE
    private var middleSrcWidth = 20
    private var middleSrcHeight = 20

    protected open fun getImageView(): ImageView {
        return ImageView(context)
    }

    init {
        addView(vCard)
        vCard.addView(ivContent)
        vCard.layoutParams = (vCard.layoutParams as LayoutParams).apply {
            this.width = ViewGroup.LayoutParams.MATCH_PARENT
            this.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        ivContent.layoutParams = (ivContent.layoutParams as ViewGroup.LayoutParams).apply {
            this.width = ViewGroup.LayoutParams.MATCH_PARENT
            this.height = ViewGroup.LayoutParams.MATCH_PARENT
        }
        initAttr(attrs)
    }

    protected fun getVCard(): CardView {
        return vCard
    }

    private fun initAttr(attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val ta: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.TemplateImageView)

        selBg = ta.getResourceId(R.styleable.TemplateImageView_temp_iv_sel_shape_bg, TemplateConst.NO_RESOURCE)
        unSelBg = ta.getResourceId(R.styleable.TemplateImageView_temp_iv_un_sel_shape_bg, TemplateConst.NO_RESOURCE)

        val contentMarginStart =
                ta.getDimension(R.styleable.TemplateImageView_temp_iv_content_margin_start, 0f)
        val contentMarginTop =
                ta.getDimension(R.styleable.TemplateImageView_temp_iv_content_margin_top, 0f)
        val contentMarginEnd =
                ta.getDimension(R.styleable.TemplateImageView_temp_iv_content_margin_end, 0f)
        val contentMarginBottom =
                ta.getDimension(R.styleable.TemplateImageView_temp_iv_content_margin_bottom, 0f)

        val contentSrc =
                ta.getResourceId(R.styleable.TemplateImageView_temp_iv_content_src, TemplateConst.NO_RESOURCE)

        middleSrc =
                ta.getResourceId(R.styleable.TemplateImageView_temp_iv_content_middle_src, TemplateConst.NO_RESOURCE)

        middleSrcWidth =
                ta.getInteger(R.styleable.TemplateImageView_temp_iv_content_middle_src_width, 20)

        middleSrcHeight =
                ta.getInteger(R.styleable.TemplateImageView_temp_iv_content_middle_src_height, 20)

        vCard.layoutParams = (vCard.layoutParams as LayoutParams).apply {
            setMargins(
                    contentMarginStart.toInt(),
                    contentMarginTop.toInt(),
                    contentMarginEnd.toInt(),
                    contentMarginBottom.toInt()
            )
        }

        ta.recycle()

        updateSelUI(false)

        if (contentSrc != TemplateConst.NO_RESOURCE) {
            ivContent.setImageResource(contentSrc)
        }
    }

    /**
     * 更新选中状态
     * */
    fun updateSelUI(isSelected: Boolean) {
        if (isSelected) {
            if (selBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(selBg)
            }
        } else {
            if (unSelBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(unSelBg)
            }
        }
    }


    fun getContentView(): ImageView {
        return ivContent
    }


    /**
     * 获取居中的资源文件
     * */
    fun getMiddleRes(): Int {
        return middleSrc
    }

    fun getMidSrcWidth(): Int {
        return middleSrcWidth
    }

    fun getMidSrcHeight(): Int {
        return middleSrcHeight
    }

    protected fun dp2px(dpValue: Int): Int {
        return context.resources.displayMetrics.density.toInt() * dpValue
    }


    override fun onDraw(canvas: Canvas?) {
        super.onDraw(canvas)
        KtLogUtil.d("imageview ondraw")
    }

}