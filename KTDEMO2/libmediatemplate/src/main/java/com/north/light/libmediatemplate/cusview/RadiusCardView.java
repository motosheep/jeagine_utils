package com.north.light.libmediatemplate.cusview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.ColorDrawable;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.cardview.widget.CardView;

import com.north.light.libmediatemplate.R;
import com.north.light.libmediatemplate.utils.KtLogUtil;


/**
 * FileName: RadiusCardView
 * Author: lzt
 * Date: 2023/7/1 09:53
 * Description:
 */
public class RadiusCardView extends CardView {

    private float tlRadius;
    private float trRadius;
    private float brRadius;
    private float blRadius;

    //绘制角度相关-----------------------------------------------
    private boolean drawAngle;
    private int drawAngleMode;
    //角度--百分比------------------------------------------------
    //左上角--百分比
    private float mLeftTopPercent, mTopLeftPercent;
    //左下角--百分比
    private float mLeftBottomPercent, mBottomLeftPercent;
    //右上角--百分比
    private float mRightTopPercent, mTopRightPercent;
    //右下角--百分比
    private float mRightBottomPercent, mBottomRightPercent;

    //角度--dimen------------------------------------------------
    //左上角--百分比
    private float mLeftTopDimen, mTopLeftDimen;
    //左下角--百分比
    private float mLeftBottomDimen, mBottomLeftDimen;
    //右上角--百分比
    private float mRightTopDimen, mTopRightDimen;
    //右下角--百分比
    private float mRightBottomDimen, mBottomRightDimen;

    public RadiusCardView(@NonNull Context context) {
        super(context);
    }

    public RadiusCardView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public RadiusCardView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        if (context == null || attrs == null) {
            return;
        }
        //关闭硬件加速--涉及到绘制时，自定义剪裁路径的影响，关闭后才会显示正常
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.RadiusCardView);
        tlRadius = array.getDimension(R.styleable.RadiusCardView_rcv_topLeftRadiu, 0);
        trRadius = array.getDimension(R.styleable.RadiusCardView_rcv_topRightRadiu, 0);
        brRadius = array.getDimension(R.styleable.RadiusCardView_rcv_bottomRightRadiu, 0);
        blRadius = array.getDimension(R.styleable.RadiusCardView_rcv_bottomLeftRadiu, 0);

        drawAngle = array.getBoolean(R.styleable.RadiusCardView_rcv_draw_angle, false);
        drawAngleMode = array.getInt(R.styleable.RadiusCardView_rcv_draw_angle_mode, 0);

        //百分比参数
        mLeftTopPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_left_top, 0f);
        mTopLeftPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_top_left, 0f);

        mLeftBottomPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_left_bottom, 0f);
        mBottomLeftPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_bottom_left, 0f);

        mRightTopPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_right_top, 0f);
        mTopRightPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_top_right, 0f);

        mRightBottomPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_right_bottom, 0f);
        mBottomRightPercent = array.getFloat(R.styleable.RadiusCardView_rcv_draw_angle_bottom_right, 0f);

        //dp参数
        mLeftTopDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_left_top_dimen, 0f);
        mTopLeftDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_top_left_dimen, 0f);

        mLeftBottomDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_left_bottom_dimen, 0f);
        mBottomLeftDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_bottom_left_dimen, 0f);

        mRightTopDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_right_top_dimen, 0f);
        mTopRightDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_top_right_dimen, 0f);

        mRightBottomDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_right_bottom_dimen, 0f);
        mBottomRightDimen = array.getDimension(R.styleable.RadiusCardView_rcv_draw_angle_bottom_right_dimen, 0f);

        array.recycle();
        setRadius(0);
        setElevation(0f);
        if (checkCus()) {
            setBackground(new ColorDrawable());
            KtLogUtil.d("RadiusCardView 有圆角");
        } else {
            KtLogUtil.d("RadiusCardView 无圆角");
            if (drawAngle) {
                KtLogUtil.d("RadiusCardView 绘制缺角");
                setBackgroundColor(Color.TRANSPARENT);
            }
        }
    }


    private boolean checkCus() {
        if (tlRadius != 0f || trRadius != 0f || brRadius != 0f || blRadius != 0f) {
            return true;
        }
        return false;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        drawCusRadius(canvas);
        drawCusAngle(canvas);
        super.onDraw(canvas);
    }

    private void drawCusAngle(Canvas canvas) {
        if (canvas == null || !drawAngle) {
            return;
        }
        KtLogUtil.d("RadiusCardView drawCusAngle");
        Path path = new Path();
        RectF rectF = getRectF();
        float width = rectF.width();
        float height = rectF.height();

        boolean percentMode = (drawAngleMode == 0);
        boolean dimenMode = (drawAngleMode == 1);

        KtLogUtil.d("RadiusCardView drawCusAngle rotation: " + getRotation());
        if (dimenMode) {
            boolean topLeftUseful = (mTopLeftDimen != 0 && mLeftTopDimen != 0);
            boolean topRightUseful = (mTopRightDimen != 0 && mRightTopDimen != 0);
            boolean bottomLeftUseful = (mBottomLeftDimen != 0 && mLeftBottomDimen != 0);
            boolean bottomRightUseful = (mBottomRightDimen != 0 && mRightBottomDimen != 0);

            //左-》右-》下-》左-》上方向绘制
            path.moveTo(0, 0);
            if (topLeftUseful) {
                path.moveTo(0,  mLeftTopDimen);
                path.lineTo(mTopLeftDimen, 0);
            }

            if (topRightUseful) {
                path.lineTo(width - mTopRightDimen, 0f);
                path.lineTo(width, mRightTopDimen);
            } else {
                path.lineTo(width, 0);
            }

            if (bottomRightUseful) {
                path.lineTo(width, height - mRightBottomDimen);
                path.lineTo(width - mBottomRightDimen, height);
            } else {
                path.lineTo(width, height);
            }

            if (bottomLeftUseful) {
                path.lineTo(mBottomLeftDimen, height);
                path.lineTo(0, height - mLeftBottomDimen);
            } else {
                path.lineTo(0, height);
            }

            path.close();
            canvas.clipPath(path, Region.Op.INTERSECT);
        } else if (percentMode) {
            //百分比绘制模式
            boolean topLeftUseful = (mTopLeftPercent != 0 && mLeftTopPercent != 0);
            boolean topRightUseful = (mTopRightPercent != 0 && mRightTopPercent != 0);
            boolean bottomLeftUseful = (mBottomLeftPercent != 0 && mLeftBottomPercent != 0);
            boolean bottomRightUseful = (mBottomRightPercent != 0 && mRightBottomPercent != 0);

            //左-》右-》下-》左-》上方向绘制
            path.moveTo(0, 0);
            if (topLeftUseful) {
                path.moveTo(0, height * mLeftTopPercent);
                path.lineTo(width * mTopLeftPercent, 0);
            }

            if (topRightUseful) {
                path.lineTo(width * (1 - mTopRightPercent), 0f);
                path.lineTo(width, height * mRightTopPercent);
            } else {
                path.lineTo(width, 0);
            }

            if (bottomRightUseful) {
                path.lineTo(width, height * (1 - mRightBottomPercent));
                path.lineTo(width * (1 - mBottomRightPercent), height);
            } else {
                path.lineTo(width, height);
            }

            if (bottomLeftUseful) {
                path.lineTo(width * mBottomLeftPercent, height);
                path.lineTo(0, height * (1 - mLeftBottomPercent));
            } else {
                path.lineTo(0, height);
            }

            path.close();
            canvas.clipPath(path, Region.Op.INTERSECT);
        }

    }

    private void drawCusRadius(Canvas canvas) {
        if (canvas == null) {
            return;
        }
        if (checkCus()) {
            Path path = new Path();
            RectF rectF = getRectF();
            float[] radius = {tlRadius, tlRadius, trRadius, trRadius, brRadius, brRadius, blRadius, blRadius};
            path.addRoundRect(rectF, radius, Path.Direction.CW);
            canvas.clipPath(path, Region.Op.INTERSECT);
        }
    }

    private RectF getRectF() {
        Rect rect = new Rect();
        getDrawingRect(rect);
        return new RectF(rect);
    }
}
