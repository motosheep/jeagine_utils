package com.north.light.libmediatemplate.widget

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.widget.RelativeLayout
import android.widget.TextView
import com.north.light.libmediatemplate.R
import com.north.light.libmediatemplate.constant.TemplateConst

/**
 * FileName: TemplateNormalTxView
 * Author: lzt
 * Date: 2023/6/29 11:11
 * Description:
 */
open class TemplateSelStatusView @JvmOverloads constructor(
    context: Context, attrs: AttributeSet? = null
) : RelativeLayout(context, attrs) {


    private var selBg = TemplateConst.NO_RESOURCE
    private var unSelBg = TemplateConst.NO_RESOURCE

    init {
        initAttr(attrs)
    }

    protected open fun getTextView(): TextView {
        return TextView(context)
    }

    private fun initAttr(attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val ta: TypedArray =
            context.obtainStyledAttributes(attrs, R.styleable.TemplateSelStatusView)

        selBg = ta.getResourceId(R.styleable.TemplateSelStatusView_temp_sel_status_v_sel_shape_bg, TemplateConst.NO_RESOURCE)
        unSelBg = ta.getResourceId(R.styleable.TemplateSelStatusView_temp_sel_status_v_un_sel_shape_bg, TemplateConst.NO_RESOURCE)

        ta.recycle()

        updateSelUI(false)
    }


    /**
     * 更新选中状态
     * */
    fun updateSelUI(isSelected: Boolean) {
        if (isSelected) {
            if (selBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(selBg)
            }
        } else {
            if (unSelBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(unSelBg)
            }
        }
    }


}