package com.north.light.libmediatemplate.widget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.RelativeLayout
import com.north.light.libmediatemplate.R
import com.north.light.libmediatemplate.textview.AndroidVerticalTextView
import com.north.light.libmediatemplate.constant.TemplateConst
import com.north.light.libmediatemplate.utils.KtLogUtil

/**
 * FileName: TemplateNormalTxView
 * Author: lzt
 * Date: 2023/6/29 11:11
 * Description:
 */
open class TemplateVerticalTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null
) : RelativeLayout(context, attrs) {

    private var tvContent: AndroidVerticalTextView = getTextView(context, attrs)

    private var selBg = TemplateConst.NO_RESOURCE
    private var unSelBg = TemplateConst.NO_RESOURCE

//    private var textHorizontalMargin = dip2px(getContext(), 4f).toFloat()
//    private var textVerticalMargin = dip2px(getContext(), 3f).toFloat()
//    private var lineWidth = dip2px(getContext(), 0.5f).toFloat()

    init {
        addView(tvContent)
        initAttr(attrs)
    }

    protected fun getTextView(context: Context, attrs: AttributeSet?): AndroidVerticalTextView {
        return AndroidVerticalTextView(context, attrs)
    }

    private fun initAttr(attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val ta: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.TemplateVerticalTextView)

        selBg = ta.getResourceId(R.styleable.TemplateVerticalTextView_temp_v_sel_shape_bg, TemplateConst.NO_RESOURCE)
        unSelBg =
                ta.getResourceId(R.styleable.TemplateVerticalTextView_temp_v_un_sel_shape_bg, TemplateConst.NO_RESOURCE)
        val contentMarginStart =
                ta.getDimension(
                        R.styleable.TemplateVerticalTextView_temp_v_content_margin_start,
                        0f
                )
        val contentMarginTop =
                ta.getDimension(
                        R.styleable.TemplateVerticalTextView_temp_v_content_margin_top,
                        0f
                )
        val contentMarginEnd =
                ta.getDimension(
                        R.styleable.TemplateVerticalTextView_temp_v_content_margin_end,
                        0f
                )
        val contentMarginBottom =
                ta.getDimension(
                        R.styleable.TemplateVerticalTextView_temp_v_content_margin_bottom,
                        0f
                )
        val txtBold =
                ta.getBoolean(
                        R.styleable.TemplateVerticalTextView_temp_v_content_bold,
                        false
                )
        tvContent.setTxtBold(txtBold)
        tvContent.layoutParams = (tvContent.layoutParams as LayoutParams).apply {
            setMargins(
                    contentMarginStart.toInt(),
                    contentMarginTop.toInt(),
                    contentMarginEnd.toInt(),
                    contentMarginBottom.toInt()
            )
        }

//        val count: Int = ta.indexCount
//        for (i in 0 until count) {
//            val index: Int = ta.getIndex(i)
//            if (index == R.styleable.TemplateVerticalTextView_temp_v_start) {
//                tvContent.setStartOrientation(ta.getInt(index, AndroidVerticalTextView.START_LEFT))
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_text) {
//                tvContent.text = ta.getString(index)
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_textColor) {
//                tvContent.setTextColor(ta.getColor(index, Color.BLACK))
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_textSize) {
//                tvContent.setTextSize(ta.getDimension(index, 16f))
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_cutChars) {
//                tvContent.setCutChars(ta.getString(index))
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_textVerticalMargin) {
//                textVerticalMargin = ta.getDimension(index, textVerticalMargin)
//                tvContent.setTextVerticalMargin(textVerticalMargin)
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_textHorizontalMargin) {
//                textHorizontalMargin = ta.getDimension(index, textHorizontalMargin)
//                tvContent.setTextHorizontalMargin(textHorizontalMargin)
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_line) {
//                tvContent.setLineOrientation(ta.getInt(index, AndroidVerticalTextView.NONE))
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_lineWidth) {
//                lineWidth = ta.getDimension(index, lineWidth)
//                tvContent.setLineWidth(lineWidth)
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_lineColor) {
//                tvContent.setLineColor(ta.getColor(index, Color.BLACK))
//            } else if (index == R.styleable.TemplateVerticalTextView_temp_v_line2TextMargin) {
//                val line2TextMargin =
//                    textHorizontalMargin / 2 + lineWidth / 2 - ta.getDimension(index, 0f)
//                tvContent.setLine2TextMargin(line2TextMargin)
//            }
//        }

        ta.recycle()

        updateSelUI(false)
    }

    open fun dip2px(context: Context, dpValue: Float): Int {
        val scale = context.resources.displayMetrics.density
        return (dpValue * scale + 0.5f).toInt()
    }


    /**
     * 更新选中状态
     * */
    fun updateSelUI(isSelected: Boolean) {
        if (isSelected) {
            if (selBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(selBg)
            }
        } else {
            if (unSelBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(unSelBg)
            }
        }
    }

    fun getContentView(): AndroidVerticalTextView {
        return tvContent
    }


    fun setTextFontByPath(path: String?) {
        if (path.isNullOrBlank()) {
            return
        }
        try {
            val typeFace = Typeface.createFromFile(path)
            getContentView().setTypeface(typeFace)
        } catch (e: Exception) {
            e.printStackTrace()
            KtLogUtil.d("字体不存在")
        }
    }

}