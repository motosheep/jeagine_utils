package com.north.light.libmediatemplate.widget

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout
import android.widget.ImageView
import com.north.light.libmediatemplate.constant.TemplateConst
import com.north.light.libmediatemplate.widget.base.TemplateImageView
import com.north.light.libmediatemplate.widget.base.TemplatePhotoView

/**
 * FileName: TemplateImageScaleView
 * Author: lizhengting
 * Date: 2023/6/29 22:57
 * Description:支持缩放的ImageView
 */
class TemplateImageScaleView(context: Context, attrs: AttributeSet? = null) :
        TemplateImageView(context, attrs) {

    private var ivPhotoView: TemplatePhotoView? = null

    private var ivMiddle: ImageView

    override fun getImageView(): ImageView {
        if (ivPhotoView == null) {
            ivPhotoView = TemplatePhotoView(context)
            ivPhotoView?.scaleType = ImageView.ScaleType.CENTER_CROP
        }
        return ivPhotoView!!
    }

    init {
        //设置photoview只要抓到了触摸事件，就会不再让父类分发取消，自己全部捕获
//        ivPhotoView?.setAllowParentInterceptOnEdge(false)
        ivMiddle = ImageView(context)
        getVCard().addView(ivMiddle)

        val middleParams = ivMiddle.layoutParams
        if (middleParams is LayoutParams) {
            ivMiddle.layoutParams = (ivMiddle.layoutParams as LayoutParams).apply {
                this.addRule(CENTER_IN_PARENT)
                this.width = dp2px(getMidSrcWidth())
                this.height = dp2px(getMidSrcHeight())
            }
        } else if (middleParams is FrameLayout.LayoutParams) {
            ivMiddle.layoutParams = (ivMiddle.layoutParams as FrameLayout.LayoutParams).apply {
                this.width = dp2px(getMidSrcWidth())
                this.height = dp2px(getMidSrcHeight())
                this.gravity = Gravity.CENTER
            }
        }
        if (getMiddleRes() != TemplateConst.NO_RESOURCE) {
            ivMiddle.setImageResource(getMiddleRes())
        }
        showMiddleIv(false)


    }

    override fun setOnClickListener(l: OnClickListener?) {
        super.setOnClickListener(l)
        ivPhotoView?.setOnClickListener {
            l?.onClick(this)
        }
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        return super.onInterceptTouchEvent(ev)
    }

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        return super.onTouchEvent(ev)
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (parent == null) {
            return super.dispatchTouchEvent(ev)
        }
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_MOVE -> {
                parent.requestDisallowInterceptTouchEvent(true)
            }
            MotionEvent.ACTION_CANCEL,
            MotionEvent.ACTION_UP -> {
                parent.requestDisallowInterceptTouchEvent(false)
            }
        }
        return super.dispatchTouchEvent(ev)
    }


    /**
     * 设置是否可以缩放
     * */
    fun setCanZoom(enable: Boolean) {
        ivPhotoView?.isZoomable = enable
    }

    /**
     * 设置居中控件是否可见
     * */
    fun showMiddleIv(show: Boolean) {
        ivMiddle.visibility = if (show) View.VISIBLE else View.GONE
    }
}