package com.north.light.libmediatemplate.textview;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

/**
 * FileName: AndroidBaseTextView
 * Author: lzt
 * Date: 2023/7/19 09:00
 * Description:textview基类
 */
public class AndroidBaseTextView extends androidx.appcompat.widget.AppCompatTextView {
    public AndroidBaseTextView(@NonNull Context context) {
        super(context);
    }

    public AndroidBaseTextView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
    }

    public AndroidBaseTextView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
