package com.north.light.libmediatemplate.widget.base

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.Gravity
import android.widget.RelativeLayout
import com.north.light.libmediatemplate.R
import com.north.light.libmediatemplate.constant.TemplateConst
import com.north.light.libmediatemplate.textview.AndroidRotateImplTextView
import com.north.light.libmediatemplate.utils.KtLogUtil

/**
 * FileName: TemplateNormalTxView
 * Author: lzt
 * Date: 2023/6/29 11:11
 * Description:
 */
open class TemplateNormalTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null
) : RelativeLayout(context, attrs) {

    private var tvContent: AndroidRotateImplTextView

    private var selBg = TemplateConst.NO_RESOURCE
    private var unSelBg = TemplateConst.NO_RESOURCE

    init {
        tvContent = getImpTextView(attrs)
        //取消字体间距--设置三方字体的时候，这句话尤为重要
        tvContent.includeFontPadding = false
        addView(tvContent)

//        tvContent.layoutParams = (tvContent.layoutParams as ViewGroup.LayoutParams).apply {
//            this.width = ViewGroup.LayoutParams.MATCH_PARENT
//            this.height = ViewGroup.LayoutParams.MATCH_PARENT
//        }

        initAttr(attrs)
    }

    protected open fun getImpTextView(attrs: AttributeSet?): AndroidRotateImplTextView {
        return AndroidRotateImplTextView(context, attrs)
    }

    private fun initAttr(attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val ta: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.TemplateNormalTextView)

        selBg =
                ta.getResourceId(R.styleable.TemplateNormalTextView_temp_normal_tv_sel_shape_bg, TemplateConst.NO_RESOURCE)
        unSelBg = ta.getResourceId(
                R.styleable.TemplateNormalTextView_temp_normal_tv_un_sel_shape_bg,
                TemplateConst.NO_RESOURCE
        )

        val contentMarginStart =
                ta.getDimension(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_margin_start,
                        0f
                )
        val contentMarginTop =
                ta.getDimension(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_margin_top,
                        0f
                )
        val contentMarginEnd =
                ta.getDimension(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_margin_end,
                        0f
                )
        val contentMarginBottom =
                ta.getDimension(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_margin_bottom,
                        0f
                )

        val contentSize =
                ta.getInteger(R.styleable.TemplateNormalTextView_temp_normal_tv_content_size, 10)
        val txtColor =
                ta.getColor(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_color, Color.BLACK
                )

        val txtGravity =
                ta.getInt(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_gravity, 0
                )
        val txtInfo =
                ta.getString(R.styleable.TemplateNormalTextView_temp_normal_tv_content_text)
        val bold =
                ta.getBoolean(R.styleable.TemplateNormalTextView_temp_normal_tv_content_bold, false)

        val strokeSwitch =
                ta.getBoolean(R.styleable.TemplateNormalTextView_temp_normal_tv_content_stroke_switch, false)


        val strokeSize =
                ta.getDimension(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_stroke_size,
                        0f
                )


        val strokeColor =
                ta.getColor(
                        R.styleable.TemplateNormalTextView_temp_normal_tv_content_stroke_color, Color.BLACK
                )

        //文本描边相关
        switchStroke(strokeSwitch)
        if (strokeSwitch) {
            strokeWidth(strokeSize)
            strokeColor(strokeColor)
        }

        setTextColor(txtColor)
        setText(txtInfo)
        setTextSize(contentSize.toFloat())
        setTextBold(bold)
        setTextGravity(txtGravity)

        tvContent.layoutParams = (tvContent.layoutParams as LayoutParams).apply {
            setMargins(
                    contentMarginStart.toInt(),
                    contentMarginTop.toInt(),
                    contentMarginEnd.toInt(),
                    contentMarginBottom.toInt()
            )
        }

        ta.recycle()

        updateSelUI(false)
    }


    open fun setTextColor(resId: Int) {
        tvContent.setTextColor(resId)
    }

    open fun setText(info: String?) {
        tvContent.text = info
    }

    open fun setTextSize(size: Float) {
        tvContent.textSize = size
    }

    open fun setTextBold(bold: Boolean) {
        tvContent.typeface = Typeface.defaultFromStyle(if (bold) Typeface.BOLD else Typeface.NORMAL)
    }

    /**
     * 开关描边
     * */
    fun switchStroke(open: Boolean) {
        tvContent.switchStroke(open)
    }

    /**
     * 设置描边大小
     * */
    fun strokeWidth(size: Float) {
        tvContent.strokeWidth(size)
    }

    /**
     * 设置描边颜色
     * */
    fun strokeColor(color: Int) {
        tvContent.strokeColor(color)
    }


    open fun setTextGravity(txtGravity: Int) {
        when (txtGravity) {
            0 -> {
                tvContent.gravity = Gravity.LEFT
            }
            1 -> {
                tvContent.gravity = Gravity.CENTER
            }
            2 -> {
                tvContent.gravity = Gravity.RIGHT
            }
        }
    }

    /**
     * 更新选中状态
     * */
    fun updateSelUI(isSelected: Boolean) {
        if (isSelected) {
            if (selBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(selBg)
            }
        } else {
            if (unSelBg != TemplateConst.NO_RESOURCE) {
                setBackgroundResource(unSelBg)
            }
        }
    }


    fun getContentView(): AndroidRotateImplTextView {
        return tvContent
    }

    fun setTextFontByPath(path: String?) {
        if (path.isNullOrBlank()) {
            return
        }
        try {
            val typeFace = Typeface.createFromFile(path)
            getContentView().typeface = typeFace
        } catch (e: Exception) {
            e.printStackTrace()
            KtLogUtil.d("字体不存在")
        }
    }


}