package com.north.light.libmediatemplate.textview;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;

import androidx.annotation.IntDef;
import androidx.annotation.NonNull;

import com.north.light.libmediatemplate.R;
import com.north.light.libmediatemplate.utils.KtLogUtil;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

/**
 * FileName: VerticalTextView
 * Author: lzt
 * Date: 2023/6/30 21:07
 * Description:github https://github.com/tenny1225/VerticalTextView
 * change by lzt 20230704 修改高度为自适应
 */
public class AndroidVerticalTextView extends View {

    public static final int START_LEFT = 1;
    public static final int START_RIGHT = 2;
    public static final int NONE = 0;
    public static final int RIGHT_LINE = 2;
    public static final int LEFT_LINE = 1;


    @IntDef({START_LEFT, START_RIGHT})
    @Retention(RetentionPolicy.SOURCE)
    public @interface START_ORIENTATION {
    }

    @IntDef({NONE, RIGHT_LINE, LEFT_LINE})
    @Retention(RetentionPolicy.SOURCE)
    public @interface LINE_ORIENTATION {
    }

    private float textSize = 56;
    private int textColor = Color.BLACK;
    private String text = "";
    private int startOrientation = START_LEFT;
    private int lineOrientation = NONE;
    private float lineWidth = dip2px(getContext(), 0.5f);
    private int lineColor = Color.BLACK;
    private String cutChars;
    private float textHorizontalMargin = dip2px(getContext(), 4);
    private float textVerticalMargin = dip2px(getContext(), 3);
    private float line2TextMargin = -1;

    private Paint paint;
    private int width;
    private int height = -1;

    private boolean txtBold = false;

    public AndroidVerticalTextView(Context context) {
        super(context);
        init();
    }

    public AndroidVerticalTextView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.AndroidVerticalTextView);
        int count = typedArray.getIndexCount();
        for (int i = 0; i < count; i++) {
            int index = typedArray.getIndex(i);
            if (index == R.styleable.AndroidVerticalTextView_v_start) {
                startOrientation = typedArray.getInt(index, START_LEFT);
            } else if (index == R.styleable.AndroidVerticalTextView_v_text) {
                text = typedArray.getString(index);
            } else if (index == R.styleable.AndroidVerticalTextView_v_textColor) {
                textColor = typedArray.getColor(index, Color.BLACK);
            } else if (index == R.styleable.AndroidVerticalTextView_v_textSize) {
                textSize = typedArray.getDimension(index, 16);
            } else if (index == R.styleable.AndroidVerticalTextView_v_cutChars) {
                cutChars = typedArray.getString(index);
            } else if (index == R.styleable.AndroidVerticalTextView_v_textVerticalMargin) {
                textVerticalMargin = typedArray.getDimension(index, textVerticalMargin);
            } else if (index == R.styleable.AndroidVerticalTextView_v_textHorizontalMargin) {
                textHorizontalMargin = typedArray.getDimension(index, textHorizontalMargin);
            } else if (index == R.styleable.AndroidVerticalTextView_v_line) {
                lineOrientation = typedArray.getInt(index, NONE);
            } else if (index == R.styleable.AndroidVerticalTextView_v_lineWidth) {
                lineWidth = typedArray.getDimension(index, lineWidth);
            } else if (index == R.styleable.AndroidVerticalTextView_v_lineColor) {
                lineColor = typedArray.getColor(index, Color.BLACK);
            } else if (index == R.styleable.AndroidVerticalTextView_v_line2TextMargin) {
                line2TextMargin = textHorizontalMargin / 2 + lineWidth / 2 - typedArray.getDimension(index, 0);
            } else if (index == R.styleable.AndroidVerticalTextView_v_textBold) {
                txtBold = typedArray.getBoolean(index, false);
            }
        }
        typedArray.recycle();
        init();
    }

    private void init() {
        paint = new Paint();
        paint.setFakeBoldText(txtBold);
        if (textSize > 0) {
            paint.setTextSize(textSize);
        }
        paint.setColor(textColor);
        paint.setAntiAlias(true);
        paint.setTextAlign(Paint.Align.CENTER);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int h = measureHeight(heightMeasureSpec);
        //此处修复relativelayout下存在的异常
        KtLogUtil.d("AndroidVerticalTextView onMeasure h:" + h);
        //注释by lzt 20230704 取实时绘制高度
        height = h + 20;
//        if (height == -1) {
//            height = h + 20;
//        } else {
//            if (height > h) {
//                height = h;
//            }
//        }
        width = measureWidth(widthMeasureSpec);
        KtLogUtil.d("AndroidVerticalTextView onMeasure width:" + width + " height: " + height);
        setMeasuredDimension(width, height);
    }

    public void setLine2TextMargin(float line2TextMargin) {

        this.line2TextMargin = textHorizontalMargin / 2 + lineWidth / 2 - line2TextMargin;
        invalidate();
    }

    public void setStartOrientation(@LINE_ORIENTATION int startOrientation) {
        this.startOrientation = startOrientation;
        invalidate();
    }

    public void setLineWidth(float lineWidth) {
        this.lineWidth = lineWidth;
        invalidate();
    }

    public void setLineColor(int lineColor) {
        this.lineColor = lineColor;
        invalidate();
    }

    public void setTypeface(Typeface typeface) {
        paint.setTypeface(typeface);
        invalidate();
    }

    public void setTextHorizontalMargin(float textHorizontalMargin) {
        this.textHorizontalMargin = textHorizontalMargin;
        invalidate();
    }

    public void setTextVerticalMargin(float textVerticalMargin) {
        this.textVerticalMargin = textVerticalMargin;
        invalidate();
    }

    public void setLineOrientation(@LINE_ORIENTATION int lineOrientation) {
        this.lineOrientation = lineOrientation;
        invalidate();
    }

    public void setCutChars(String cutChars) {
        this.cutChars = cutChars;
        invalidate();
    }

    public void setTxtBold(boolean txtBold) {
        this.txtBold = txtBold;
        if (paint != null) {
            paint.setFakeBoldText(txtBold);
        }
        invalidate();
    }

    /**
     * 设置文字尺寸
     *
     * @param textSize
     */
    public void setTextSize(float textSize) {
        this.textSize = textSize;
        postInvalidate();
        requestLayout();
    }

    /**
     * 设置文字颜色
     *
     * @param textColor
     */
    public void setTextColor(int textColor) {
        this.textColor = textColor;
        invalidate();
    }

    /**
     * 设置文字
     *
     * @param text
     */
    public void setText(String text) {
        if (TextUtils.isEmpty(text)) {
            this.text = "\t";
        } else {
            this.text = text;
        }
        requestLayout();
        postInvalidate();
    }

    public String getText() {
        return text;
    }

    /**
     * 设置文字起始方向
     *
     * @param startOrientation
     */
    public void setStart(@START_ORIENTATION int startOrientation) {
        this.startOrientation = startOrientation;
        invalidate();
    }


    private int measureWidth(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {

            result = specSize;
        } else {
            return (int) measureTextWidth();
        }
        return result;
    }

    private int measureHeight(int measureSpec) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            //change by lzt 20230704 -- 根据换行符，剪裁字符串，然后获取数组中最大的高度
            int max = -1;
            if (!TextUtils.isEmpty(text)) {
                String[] textArray = text.split("\n");
                for (String cache : textArray) {
                    List<String> trainStrList = dealStrWithEmoji(cache);
                    int textLength = (int) (trainStrList.size() * getOneWordHeight());
                    if (max < textLength) {
                        max = textLength;
                    }
                }
            }
            if (max != -1) {
                result = max;
            } else {
                result = (int) (getOneWordHeight() * dealStrWithEmoji(text).size());
            }
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }
        return result;
    }

    private float measureTextWidth() {
        if (getColNum() == 1) {
            return getOneWordWidth() + getPaddingLeft() + getPaddingRight();
        }

        return getOneWordWidth() * getColNum() + getPaddingLeft() + getPaddingRight();
    }

    private float getTextBaseLine(RectF rect) {
        Paint.FontMetricsInt metricsInt = paint.getFontMetricsInt();
        return (rect.top + rect.bottom - metricsInt.top - metricsInt.bottom) / 2;
    }


    private int getColNum() {

        int oneRowWordCount = getColWordCount();
        int colNum = 0;
        if (cutChars != null) {
            String[] textArray = text.split(cutChars);
            for (int i = 0; i < textArray.length; i++) {
                List<String> textCache = dealStrWithEmoji(textArray[i]);
                int size = textCache.size();
                if (size > oneRowWordCount) {
                    colNum += size / oneRowWordCount;
                    if (size % oneRowWordCount > 0) {
                        colNum++;
                    }
                } else {
                    colNum++;
                }
            }
        } else {
            List<String> textCache = dealStrWithEmoji(text);
            int size = textCache.size();
            colNum =size / oneRowWordCount;
            if (size % oneRowWordCount > 0) {
                colNum++;
            }
        }


        return colNum;
    }

    private String testWord = "我";

    private float getOneWordWidth() {
        return paint.measureText(testWord) + textHorizontalMargin;
    }

    private float getOneWordHeight() {
       /* Paint.FontMetricsInt metricsInt = paint.getFontMetricsInt();
        return (metricsInt.bottom - metricsInt.top);*/
        Rect rect = new Rect();
        paint.getTextBounds(testWord, 0, 1, rect);
        return rect.height() + textVerticalMargin;
    }

    private int getColWordCount() {
        int oneLineWordCount = (int) (height / getOneWordHeight());
        return oneLineWordCount;
    }

    public int dip2px(Context context, float dpValue) {
        final float scale = context.getResources().getDisplayMetrics().density;
        return (int) (dpValue * scale + 0.5f);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int oneLineWordCount = getColWordCount();

        float w = getOneWordWidth();
        float h = getOneWordHeight();

        int colNum = getColNum();

        String[] cutCharArray = (cutChars == null ? null : cutChars.split("|"));
        if (cutCharArray != null) {
            String[] textArray = text.split(cutChars);
            int stepCol = 0;
            for (int n = 0; n < textArray.length; n++) {
                int currentCol = 0;
                List<String> dealStringList = dealStrWithEmoji(textArray[n]);
                for (int i = 0; i < dealStringList.size(); i++) {
                    String str = dealStringList.get(i);
                    int currentRow = i % oneLineWordCount;
                    if (colNum == 1) {
                        currentRow = i;
                    }
                    if (colNum > 1) {
                        currentCol = stepCol + (i / oneLineWordCount);
                    }
                    drawText(w, h, currentCol, currentRow, str, canvas);
                    if (i + 1 == dealStringList.size()) {
                        stepCol = currentCol + 1;
                    }
                }
            }
        } else {
            int currentCol = 0;
            List<String> dealStringList = dealStrWithEmoji(text);
            for (int i = 0; i < dealStringList.size(); i++) {
                String str = dealStringList.get(i);
                int currentRow = i % oneLineWordCount;
                if (colNum == 1) {
                    currentRow = i;
                }
                if (colNum > 1) {
                    currentCol = (i) / oneLineWordCount;
                }
                drawText(w, h, currentCol, currentRow, str, canvas);
            }
        }
    }

    /**
     * 把string转换为List集合，处理表情
     */
    private List<String> dealStrWithEmoji(String source) {
        if (TextUtils.isEmpty(source)) {
            return new ArrayList<>();
        }
        List<String> result = new ArrayList<>();
        StringBuilder cacheBuilder = new StringBuilder();
        int emojiCounter = 0;
        for (int i = 0; i < source.length(); i++) {
            char cacheChar = source.charAt(i);
            String str = String.valueOf(cacheChar);
            cacheBuilder.append(str);
            if (containsEmojiByChar(cacheChar)) {
                emojiCounter++;
                if (emojiCounter != 2) {
                    continue;
                } else {
                    emojiCounter = 0;
                }
            }
            result.add(cacheBuilder.toString());
            cacheBuilder = new StringBuilder();
        }
        return result;
    }

    private void drawText(float w, float h, int currentCol, int currentRow, String str, Canvas canvas) {
        RectF rectF;
        if (startOrientation == START_LEFT) {
            rectF = new RectF(currentCol * w, currentRow * h, currentCol * w + w, currentRow * h + h);
        } else {
            rectF = new RectF((width - (currentCol + 1) * w), currentRow * h, (width - (currentCol + 1) * w) + w, currentRow * h + h);
        }
        float baseline = getTextBaseLine(rectF);
        paint.setColor(textColor);
        paint.setStyle(Paint.Style.FILL);
        canvas.drawText(str, rectF.centerX(), baseline, paint);
        paint.setColor(lineColor);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(lineWidth);
        if (line2TextMargin == -1) {
            line2TextMargin = lineWidth * 1f / 2;
        }
        if (lineOrientation == RIGHT_LINE) {
            Path path = new Path();
            path.moveTo(rectF.right - line2TextMargin, rectF.top);
            path.lineTo(rectF.right - line2TextMargin, rectF.bottom);
            canvas.drawPath(path, paint);
        } else if (lineOrientation == LEFT_LINE) {
            Path path = new Path();
            path.moveTo(rectF.left + line2TextMargin, rectF.top);
            path.lineTo(rectF.left + line2TextMargin, rectF.bottom);
            canvas.drawPath(path, paint);
        }
    }

    /**
     * 设置字体样式
     */
    public void setTextFont(@NonNull Typeface typeface) {
        if (paint == null) {
            KtLogUtil.d("setTextFont paint is null");
            return;
        }
        paint.setTypeface(typeface);
    }

    /**
     * 是否包含表情
     */
    public boolean containsEmoji(String source) {
        int len = source.length();
        for (int i = 0; i < len; i++) {
            char codePoint = source.charAt(i);
            if (containsEmojiByChar(codePoint)) { // 如果不能匹配,则该字符是Emoji表情
                return true;
            }
        }
        return false;
    }

    private boolean containsEmojiByChar(char codePoint) {
        return !((codePoint == 0x0) || (codePoint == 0x9) || (codePoint == 0xA) || (codePoint == 0xD)
                || ((codePoint >= 0x20) && (codePoint <= 0xD7FF)) || ((codePoint >= 0xE000) && (codePoint <= 0xFFFD))
                || ((codePoint >= 0x10000) && (codePoint <= 0x10FFFF)));
    }
}
