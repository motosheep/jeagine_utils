package com.north.light.libmediatemplate.widget

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.util.AttributeSet
import android.view.Gravity
import android.widget.RelativeLayout
import android.widget.TextView
import com.north.light.libmediatemplate.R
import com.north.light.libmediatemplate.utils.KtLogUtil

/**
 * FileName: TemplateNormalTxView
 * Author: lzt
 * Date: 2023/6/29 11:11
 * Description:
 */
open class TemplateDefaultTextView @JvmOverloads constructor(
        context: Context, attrs: AttributeSet? = null
) : RelativeLayout(context, attrs) {

    private var tvContent: TextView = getTextView()


    init {
        //取消字体间距--设置三方字体的时候，这句话尤为重要
        tvContent.includeFontPadding = false
        addView(tvContent)
        initAttr(attrs)
    }

    protected open fun getTextView(): TextView {
        return TextView(context)
    }

    private fun initAttr(attrs: AttributeSet?) {
        if (attrs == null) {
            return
        }
        val ta: TypedArray =
                context.obtainStyledAttributes(attrs, R.styleable.TemplateDefaultTextView)

        val contentMarginStart =
                ta.getDimension(
                        R.styleable.TemplateDefaultTextView_temp_default_tv_content_margin_start,
                        0f
                )
        val contentMarginTop =
                ta.getDimension(
                        R.styleable.TemplateDefaultTextView_temp_default_tv_content_margin_top,
                        0f
                )
        val contentMarginEnd =
                ta.getDimension(
                        R.styleable.TemplateDefaultTextView_temp_default_tv_content_margin_end,
                        0f
                )
        val contentMarginBottom =
                ta.getDimension(
                        R.styleable.TemplateDefaultTextView_temp_default_tv_content_margin_bottom,
                        0f
                )

        val contentSize =
                ta.getInteger(R.styleable.TemplateDefaultTextView_temp_default_tv_content_size, 10)
        val txtColor =
                ta.getColor(
                        R.styleable.TemplateDefaultTextView_temp_default_tv_content_color, Color.BLACK
                )

        val txtGravity =
                ta.getInt(
                        R.styleable.TemplateDefaultTextView_temp_default_tv_content_gravity, 0
                )
        val txtInfo =
                ta.getString(R.styleable.TemplateDefaultTextView_temp_default_tv_content_text)
        val bold =
                ta.getBoolean(R.styleable.TemplateDefaultTextView_temp_default_tv_content_bold, false)

        setTextColor(txtColor)
        setText(txtInfo)
        setTextSize(contentSize.toFloat())
        setTextBold(bold)
        setTextGravity(txtGravity)

        tvContent.layoutParams = (tvContent.layoutParams as LayoutParams).apply {
            setMargins(
                    contentMarginStart.toInt(),
                    contentMarginTop.toInt(),
                    contentMarginEnd.toInt(),
                    contentMarginBottom.toInt()
            )
        }

        ta.recycle()

    }


    open fun setTextColor(resId: Int) {
        tvContent.setTextColor(resId)
    }

    open fun setText(info: String?) {
        tvContent.text = info
    }

    open fun setTextSize(size: Float) {
        tvContent.textSize = size
    }

    open fun setTextBold(bold: Boolean) {
        tvContent.typeface = Typeface.defaultFromStyle(if (bold) Typeface.BOLD else Typeface.NORMAL)
    }


    open fun setTextGravity(txtGravity: Int) {
        when (txtGravity) {
            0 -> {
                tvContent.gravity = Gravity.LEFT
            }
            1 -> {
                tvContent.gravity = Gravity.CENTER
            }
            2 -> {
                tvContent.gravity = Gravity.RIGHT
            }
        }
    }


    fun getContentView(): TextView {
        return tvContent
    }

    fun setTextFontByPath(path: String?) {
        if (path.isNullOrBlank()) {
            return
        }
        try {
            val typeFace = Typeface.createFromFile(path)
            getContentView().typeface = typeFace
        } catch (e: Exception) {
            e.printStackTrace()
            KtLogUtil.d("字体不存在")
        }
    }


}