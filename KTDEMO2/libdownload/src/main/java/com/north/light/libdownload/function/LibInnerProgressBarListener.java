package com.north.light.libdownload.function;

/**
 * author:li
 * date:2020/12/13
 * desc:下载监听接口
 */
interface LibInnerProgressBarListener {
    /**
     * 获取文件的长度
     *
     * @param length
     * @param fileName
     */
    void getMax(int length, String localPath,String fileName);

    /**
     * 获取每次下载的长度
     *
     * @param length
     * @param fileName
     */
    void getDownload(int length, String localPath,String fileName);

    /**
     * 错误
     */
    void error(String msg);

    /**
     * 设置进度--建议本地缓存
     */
    void setProgress(String key, long pro);

    /**
     * 获取进度--建议本地缓存
     */
    Long getProgress(String key);
}
