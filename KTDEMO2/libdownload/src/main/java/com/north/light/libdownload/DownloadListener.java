package com.north.light.libdownload;

/**
 * FileName: DownloadListener
 * Author: lizhengting
 * Date: 2021/9/19 11:09
 * Description:外部调用监听
 */
public interface DownloadListener {

    /**
     * 回调数据
     */
    void data(String link, long total, long current, float percent, boolean isFinish,
              String localPath, String fileName);

    /**
     * 错误
     */
    void error(String message);

    /**
     * 设置进度--建议本地缓存
     */
    void setProgress(String key, long pro);


    /**
     * 获取进度--建议本地缓存
     */
    Long getProgress(String key);

    /***
     * 没有权限
     **/
    void noPermission();

    /**
     * 检查资源是否需要下载
     */
    boolean checkNeedToDownload(String url, String localPath, String fileName);

}
