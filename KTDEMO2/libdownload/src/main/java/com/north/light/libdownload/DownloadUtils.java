package com.north.light.libdownload;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.MediaStore;
import android.text.TextUtils;

import com.north.light.libdownload.function.LibDownloadManager;

import java.io.File;

/**
 * FileName: DownloadUtils
 * Author: lizhengting
 * Date: 2021/9/19 11:09
 * Description:
 */
public class DownloadUtils extends LibDownloadManager {

    private static final class SingleHolder {
        static DownloadUtils mInstance = new DownloadUtils();
    }

    public static DownloadUtils getInstance() {
        return SingleHolder.mInstance;
    }

    /**
     * 需要在io线程调用
     */
    public void startDownLoad(boolean replaceHttps, Context context, String storagePath, String urlPath, DownloadListener listener) {
        try {
            //检查权限
            PackageManager pm = context.getPackageManager();
            boolean readStorage = pm.checkPermission(Manifest.permission.READ_EXTERNAL_STORAGE, context.getPackageName())
                    == PackageManager.PERMISSION_GRANTED;
            boolean writeStorage = pm.checkPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE, context.getPackageName())
                    == PackageManager.PERMISSION_GRANTED;
            if (!readStorage || !writeStorage) {
                if (listener != null) {
                    listener.noPermission();
                }
                return;
            }
            //创建本地目录
            new File(storagePath).mkdirs();
            super.start(replaceHttps, storagePath, urlPath, listener);
        } catch (Exception e) {
            if (listener != null) {
                listener.error(e.getMessage());
            }
        }

    }


    @Override
    public Boolean stop(String path) {
        return super.stop(path);
    }


    /**
     * 检查视频资源是否存在图库中
     */
    public boolean checkVideoExistGallery(Context context, String videoName) {
        if (TextUtils.isEmpty(videoName)) {
            return false;
        }
        try {
            Cursor videoCursor = context.getApplicationContext().getContentResolver().query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
            while (videoCursor.moveToNext()) {
                //获取视频的名称
                String displayName = videoCursor.getString(videoCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME));
                if (!TextUtils.isEmpty(displayName) && videoName.equals(displayName)) {
                    videoCursor.close();
                    return true;
                }
            }
            videoCursor.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 检查图片资源是否在图集中
     */
    public boolean checkPicExistGallery(Context context, String picName) {
        if (TextUtils.isEmpty(picName)) {
            return false;
        }
        try {
            Cursor picCursor = context.getApplicationContext().getContentResolver().query(
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI, null, null, null, null);
            while (picCursor.moveToNext()) {
                //获取视频的名称
                String displayName = picCursor.getString(picCursor.getColumnIndexOrThrow(MediaStore.Images.Media.DISPLAY_NAME));
                if (!TextUtils.isEmpty(displayName) && picName.equals(displayName)) {
                    picCursor.close();
                    return true;
                }
            }
            picCursor.close();
            return false;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 检查传入的图片and视频是否在资源库中
     */
    public boolean checkResourceExistGallery(Context context, String fileName) {
        return checkVideoExistGallery(context, fileName) || checkPicExistGallery(context, fileName);
    }
}
