package com.north.light.musiclibrary.control;

import android.os.RemoteCallbackList;
import android.os.RemoteException;
import android.support.v4.media.session.MediaSessionCompat;

import com.north.light.musiclibrary.service.LibPlayMusicService;
import com.north.light.musiclibrary.aidl.listener.LibPlayMusicNotifyContract;
import com.north.light.musiclibrary.aidl.model.LibPlayMusicSongInfo;
import com.north.light.musiclibrary.aidl.source.LibPlayMusicIOnPlayerEventListener;
import com.north.light.musiclibrary.aidl.source.LibPlayMusicIOnTimerTaskListener;
import com.north.light.musiclibrary.bus.LibPlayMusicSubscriber;
import com.north.light.musiclibrary.bus.tags.LibPlayMusicBusTags;
import com.north.light.musiclibrary.bus.tags.LibPlayMusicQueueIndexUpdated;
import com.north.light.musiclibrary.cache.LibPlayMusicCacheConfig;
import com.north.light.musiclibrary.constans.LibPlayMusicState;
import com.north.light.musiclibrary.helper.LibPlayMusicQueueHelper;
import com.north.light.musiclibrary.notification.LibPlayMusicNotificationCreater;
import com.north.light.musiclibrary.playback.player.LibPlayMusicExoPlayback;
import com.north.light.musiclibrary.playback.player.LibPlayMusicMediaPlayback;
import com.north.light.musiclibrary.playback.player.LibPlayMusicPlayback;

import java.util.List;

/**
 * Binder
 * Created by xian on 2018/1/28.
 */

public class LibPlayMusicPlayControl extends LibPlayMusicBasePlayControl {

    private RemoteCallbackList<LibPlayMusicIOnPlayerEventListener> mRemoteCallbackList;
    private RemoteCallbackList<LibPlayMusicIOnTimerTaskListener> mOnTimerTaskListenerList;
    private boolean isRegisterOnPlayerEventListener = false;
    private boolean isRegisterOnTimerTaskListener = false;

    private boolean isAsyncLoading = false;

    private LibPlayMusicPlayControl(Builder builder) {
        super();
        mMusicService = builder.mMusicService;
        isAutoPlayNext = builder.isAutoPlayNext;
        notificationCreater = builder.notificationCreater;
        mNotifyStatusChanged = new NotifyStatusChange();
        mNotifyMusicSwitch = new NotifyMusicSwitch();
        mNotifyTimerTask = new NotifyTimerTask();
        mRemoteCallbackList = new RemoteCallbackList<>();
        mOnTimerTaskListenerList = new RemoteCallbackList<>();
        mPlayback = builder.isUseMediaPlayer
                ? new LibPlayMusicMediaPlayback(mMusicService.getApplicationContext(), builder.cacheConfig, builder.isGiveUpAudioFocusManager)
                : new LibPlayMusicExoPlayback(mMusicService.getApplicationContext(), builder.cacheConfig, builder.isGiveUpAudioFocusManager);
        init();
    }

    public LibPlayMusicPlayControl getController() {
        return this;
    }

    public static class Builder {
        private LibPlayMusicService mMusicService;
        private boolean isUseMediaPlayer = false;
        private boolean isAutoPlayNext = true;
        private boolean isGiveUpAudioFocusManager = false;
        private LibPlayMusicNotificationCreater notificationCreater;
        private LibPlayMusicCacheConfig cacheConfig;

        public Builder(LibPlayMusicService mService) {
            mMusicService = mService;
        }

        public Builder setUseMediaPlayer(boolean useMediaPlayer) {
            isUseMediaPlayer = useMediaPlayer;
            return this;
        }

        public Builder setAutoPlayNext(boolean autoPlayNext) {
            isAutoPlayNext = autoPlayNext;
            return this;
        }

        public Builder setNotificationCreater(LibPlayMusicNotificationCreater notificationCreater) {
            this.notificationCreater = notificationCreater;
            return this;
        }

        public Builder setCacheConfig(LibPlayMusicCacheConfig cacheConfig) {
            this.cacheConfig = cacheConfig;
            return this;
        }

        public Builder setGiveUpAudioFocusManager(boolean giveUpAudioFocusManager) {
            isGiveUpAudioFocusManager = giveUpAudioFocusManager;
            return this;
        }

        public LibPlayMusicPlayControl build() {
            return new LibPlayMusicPlayControl(this);
        }
    }

    private class NotifyStatusChange implements LibPlayMusicNotifyContract.NotifyStatusChanged {

        @Override
        public void notify(LibPlayMusicSongInfo info, int index, int status, String errorMsg) {
            synchronized (LibPlayMusicPlayControl.class) {
                final int N = mRemoteCallbackList.beginBroadcast();
                for (int i = 0; i < N; i++) {
                    LibPlayMusicIOnPlayerEventListener listener = mRemoteCallbackList.getBroadcastItem(i);
                    if (listener != null) {
                        try {
                            switch (status) {
                                case LibPlayMusicState.STATE_IDLE:
                                    listener.onPlayCompletion(info);
                                    break;
                                case LibPlayMusicState.STATE_ASYNC_LOADING:
                                    isAsyncLoading = true;
                                    listener.onAsyncLoading(false);
                                    break;
                                case LibPlayMusicState.STATE_PLAYING:
                                    isAsyncLoading = false;
                                    listener.onAsyncLoading(true);
                                    listener.onPlayerStart();
                                    break;
                                case LibPlayMusicState.STATE_PAUSED:
                                    if (isAsyncLoading) {
                                        listener.onAsyncLoading(true);
                                        isAsyncLoading = false;
                                    }
                                    listener.onPlayerPause();
                                    break;
                                case LibPlayMusicState.STATE_STOP:
                                    listener.onPlayerStop();
                                    break;
                                case LibPlayMusicState.STATE_ERROR:
                                    listener.onError(errorMsg);
                                    break;
                            }
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mRemoteCallbackList.finishBroadcast();
            }
        }
    }

    private class NotifyMusicSwitch implements LibPlayMusicNotifyContract.NotifyMusicSwitch {

        @Override
        public void notify(LibPlayMusicSongInfo info) {
            synchronized (LibPlayMusicPlayControl.class) {
                final int N = mRemoteCallbackList.beginBroadcast();
                for (int i = 0; i < N; i++) {
                    LibPlayMusicIOnPlayerEventListener listener = mRemoteCallbackList.getBroadcastItem(i);
                    if (listener != null) {
                        try {
                            listener.onMusicSwitch(info);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mRemoteCallbackList.finishBroadcast();
            }
        }
    }

    private class NotifyTimerTask implements LibPlayMusicNotifyContract.NotifyTimerTask {

        @Override
        public void notifyTimerTasFinish() {
            synchronized (LibPlayMusicPlayControl.class) {
                final int N = mOnTimerTaskListenerList.beginBroadcast();
                for (int i = 0; i < N; i++) {
                    LibPlayMusicIOnTimerTaskListener listener = mOnTimerTaskListenerList.getBroadcastItem(i);
                    if (listener != null) {
                        try {
                            listener.onTimerFinish();
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mOnTimerTaskListenerList.finishBroadcast();
            }
        }

        @Override
        public void onTimerTick(long millisUntilFinished, long totalTime) {
            synchronized (LibPlayMusicPlayControl.class) {
                final int N = mOnTimerTaskListenerList.beginBroadcast();
                for (int i = 0; i < N; i++) {
                    LibPlayMusicIOnTimerTaskListener listener = mOnTimerTaskListenerList.getBroadcastItem(i);
                    if (listener != null) {
                        try {
                            listener.onTimerTick(millisUntilFinished, totalTime);
                        } catch (RemoteException e) {
                            e.printStackTrace();
                        }
                    }
                }
                mOnTimerTaskListenerList.finishBroadcast();
            }
        }
    }

    public LibPlayMusicPlayback getPlayback() {
        return mPlayback;
    }

    public void releaseMediaSession() {
        mMediaSessionManager.release();
    }

    @Override
    public void registerPlayerEventListener(LibPlayMusicIOnPlayerEventListener listener) {
        super.registerPlayerEventListener(listener);
        mRemoteCallbackList.register(listener);
        isRegisterOnPlayerEventListener = true;
    }

    @Override
    public void unregisterPlayerEventListener(LibPlayMusicIOnPlayerEventListener listener) {
        super.unregisterPlayerEventListener(listener);
        if (isRegisterOnPlayerEventListener) {
            mRemoteCallbackList.unregister(listener);
            isRegisterOnPlayerEventListener = false;
        }
    }

    @Override
    public void registerTimerTaskListener(LibPlayMusicIOnTimerTaskListener listener) {
        super.registerTimerTaskListener(listener);
        mOnTimerTaskListenerList.register(listener);
        isRegisterOnTimerTaskListener = true;
    }

    @Override
    public void unregisterTimerTaskListener(LibPlayMusicIOnTimerTaskListener listener) {
        super.unregisterTimerTaskListener(listener);
        if (isRegisterOnTimerTaskListener) {
            mOnTimerTaskListenerList.unregister(listener);
            isRegisterOnTimerTaskListener = false;
        }
    }

    @LibPlayMusicSubscriber(tag = LibPlayMusicBusTags.onMetadataChanged)
    public void onMetadataChanged(LibPlayMusicSongInfo songInfo) {
        mMediaSessionManager.updateMetaData(LibPlayMusicQueueHelper.fetchInfoWithMediaMetadata(songInfo));
    }

    @LibPlayMusicSubscriber(tag = LibPlayMusicBusTags.onMetadataRetrieveError)
    public void onMetadataRetrieveError() {
        mPlaybackManager.updatePlaybackState("Unable to retrieve metadata.",false);
    }

    @LibPlayMusicSubscriber(tag = LibPlayMusicBusTags.onCurrentQueueIndexUpdated)
    public void onCurrentQueueIndexUpdated(LibPlayMusicQueueIndexUpdated updated) {
        mPlaybackManager.handlePlayPauseRequest(updated.isJustPlay, updated.isSwitchMusic); //播放
    }

    @LibPlayMusicSubscriber(tag = LibPlayMusicBusTags.onQueueUpdated)
    public void onQueueUpdated(List<MediaSessionCompat.QueueItem> newQueue) {
        mMediaSessionManager.setQueue(newQueue);
    }

    @LibPlayMusicSubscriber(tag = LibPlayMusicBusTags.onPlayModeChange)
    public void onPlayModeChange(int playMode) {
        mPlayQueueManager.checkIndexForPlayMode(mPlayback.getCurrentMediaId());
    }
}
