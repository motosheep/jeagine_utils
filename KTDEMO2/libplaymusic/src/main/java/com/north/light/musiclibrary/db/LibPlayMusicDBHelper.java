package com.north.light.musiclibrary.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by xian on 2018/3/31.
 */

public class LibPlayMusicDBHelper extends SQLiteOpenHelper {

    private static final String name = "musiclibrary";
    private static final int version = 1;
    private static volatile LibPlayMusicDBHelper instance;

    public LibPlayMusicDBHelper(Context context) {
        super(context, name, null, version);
    }

    public static LibPlayMusicDBHelper getInstance(Context context) {
        if (instance == null) {
            synchronized (LibPlayMusicDBHelper.class) {
                if (instance == null) {
                    instance = new LibPlayMusicDBHelper(context.getApplicationContext());
                }
            }
        }
        return instance;
    }

    private final String TABLE_MUSIC_HISTORY = "create table "
            + LibPlayMusicSongHistoryManager.TABLE_HISTORY + " ( "
            + LibPlayMusicSongHistoryManager.SONG_ID + " text, "
            + LibPlayMusicSongHistoryManager.SONG_POSITION + " text);";

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_MUSIC_HISTORY);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.delete(TABLE_MUSIC_HISTORY, null, null);
    }
}
