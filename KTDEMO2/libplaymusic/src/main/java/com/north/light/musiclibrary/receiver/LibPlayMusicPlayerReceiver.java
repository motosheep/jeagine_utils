package com.north.light.musiclibrary.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;

import com.north.light.musiclibrary.service.LibPlayMusicService;
import com.north.light.musiclibrary.constans.LibPlayMusicState;
import com.north.light.musiclibrary.control.LibPlayMusicPlayControl;
import com.north.light.musiclibrary.control.LibPlayMusicBasePlayControl;
import com.north.light.musiclibrary.notification.LibPlayMusicIMediaNotification;

/**
 * Created by xian on 2018/2/18.
 */

public class LibPlayMusicPlayerReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if (TextUtils.isEmpty(action)) {
            return;
        }
        LibPlayMusicService musicService = LibPlayMusicService.getService();
        if (musicService == null) {
            return;
        }
        LibPlayMusicPlayControl binder = musicService.getBinder();
        if (binder == null) {
            return;
        }
        LibPlayMusicBasePlayControl controller = binder.getController();
        if (controller == null) {
            return;
        }

        Intent newIntent = new Intent();
        newIntent.setAction(action+"_ok");
        context.sendBroadcast(newIntent);
        switch (action) {
            case LibPlayMusicIMediaNotification.ACTION_CLOSE:
                controller.pauseMusic();
                controller.stopNotification();
                break;
            case LibPlayMusicIMediaNotification.ACTION_PLAY_PAUSE:
                if (controller.getStatus() == LibPlayMusicState.STATE_PLAYING) {
                    controller.pauseMusic();
                } else if (controller.getStatus() == LibPlayMusicState.STATE_PAUSED) {
                    controller.resumeMusic();
                }
                break;
            case LibPlayMusicIMediaNotification.ACTION_PREV:
                controller.playPre();
                break;
            case LibPlayMusicIMediaNotification.ACTION_NEXT:
                controller.playNext();
                break;
            default:
                break;
        }
    }
}
