package com.north.light.musiclibrary.playback;

import java.util.Observable;

/**
 * lzx
 * 2018/2/8
 */

public class LibPlayMusicPlayStateObservable extends Observable {

    public LibPlayMusicPlayStateObservable() {

    }

    public void stateChangeNotifyObservers(Object data) {
        setChanged();
        notifyObservers(data);
    }
}
