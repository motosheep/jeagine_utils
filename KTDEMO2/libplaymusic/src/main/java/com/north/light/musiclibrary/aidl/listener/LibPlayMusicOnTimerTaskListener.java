package com.north.light.musiclibrary.aidl.listener;

/**
 * lzx
 * 2018/2/26
 */

public interface LibPlayMusicOnTimerTaskListener {
    void onTimerFinish();

    void onTimerTick(long millisUntilFinished,long totalTime);
}
