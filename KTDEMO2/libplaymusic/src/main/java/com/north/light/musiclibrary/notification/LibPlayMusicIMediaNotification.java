package com.north.light.musiclibrary.notification;

import android.os.Bundle;

import com.north.light.musiclibrary.aidl.model.LibPlayMusicSongInfo;

/**
 * Created by xian on 2018/3/17.
 */

public interface LibPlayMusicIMediaNotification {

    String CHANNEL_ID = "com.north.light.libplaymusic.MUSIC_CHANNEL_ID";
    int NOTIFICATION_ID = 412;
    int REQUEST_CODE = 100;

    //action
    String ACTION_PLAY_PAUSE = "com.north.light.libplaymusic.play_pause";
    String ACTION_PAUSE = "com.north.light.libplaymusic.pause";
    String ACTION_PLAY = "com.north.light.libplaymusic.play";
    String ACTION_PREV = "com.north.light.libplaymusic.prev";
    String ACTION_NEXT = "com.north.light.libplaymusic.next";
    String ACTION_STOP = "com.north.light.libplaymusic.stop";
    String ACTION_CLOSE = "com.north.light.libplaymusic.close";
    String ACTION_FAVORITE = "com.north.light.libplaymusic.favorite";
    String ACTION_LYRICS = "com.north.light.libplaymusic.lyrics";
    String ACTION_DOWNLOAD = "com.north.light.libplaymusic.download";
    String ACTION_INTENT_CLICK = "com.north.light.libplaymusic.EXTRY_NOTIFICATION_TO_MAINACTIVITY";

    void startNotification(LibPlayMusicSongInfo songInfo);

    void stopNotification();

    void updateViewStateAtStart();

    void updateViewStateAtPause();

    void updateFavorite(boolean isFavorite);

    void updateLyrics(boolean isChecked);

    void updateModelDetail(LibPlayMusicSongInfo songInfo);

    void updateContentIntent(Bundle bundle, String targetClass);
}
