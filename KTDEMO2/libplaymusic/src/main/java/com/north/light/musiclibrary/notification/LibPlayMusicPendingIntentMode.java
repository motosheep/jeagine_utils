package com.north.light.musiclibrary.notification;

public class LibPlayMusicPendingIntentMode {
    public static final int MODE_ACTIVITY = 0;
    public static final int MODE_BROADCAST = 1;
    public static final int MODE_SERVICE = 2;
}
