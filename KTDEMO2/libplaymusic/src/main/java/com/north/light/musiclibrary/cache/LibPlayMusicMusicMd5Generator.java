package com.north.light.musiclibrary.cache;

import com.north.light.musiclibrary.videocache.ProxyCacheUtils;
import com.north.light.musiclibrary.videocache.file.FileNameGenerator;

/**
 * Created by xian on 2018/4/2.
 */

public class LibPlayMusicMusicMd5Generator implements FileNameGenerator {

    @Override
    public String generate(String url) {
        return ProxyCacheUtils.computeMD5(url);
    }
}
