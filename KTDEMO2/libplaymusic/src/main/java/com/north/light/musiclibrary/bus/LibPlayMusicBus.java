package com.north.light.musiclibrary.bus;

import android.os.Looper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * 简单实现一个EventBus
 * create by lzx
 * time:2018/11/2
 */
public class LibPlayMusicBus {

    private final Map<String, CopyOnWriteArrayList<LibPlayMusicSubscription>> subscriptionsByTag;
    private LibPlayMusicSubscriberMethodFinder subscriberMethodFinder;

    private final ThreadLocal<PostingThreadState> currentPostingThreadState = new ThreadLocal<PostingThreadState>() {
        @Override
        protected PostingThreadState initialValue() {
            return new PostingThreadState();
        }
    };

    private LibPlayMusicBus() {
        subscriptionsByTag = new HashMap<>();
        subscriberMethodFinder = new LibPlayMusicSubscriberMethodFinder();
    }

    public static LibPlayMusicBus getInstance() {
        return SingletonHolder.sInstance;
    }

    private static class SingletonHolder {
        private static final LibPlayMusicBus sInstance = new LibPlayMusicBus();
    }

    /**
     * 订阅
     */
    public void register(Object subscriber) {
        if (subscriber == null) {
            return;
        }
        Class<?> subscriberClass = subscriber.getClass();
        //找到所有订阅方法
        List<LibPlayMusicSubscriberMethod> subscriberMethods = subscriberMethodFinder.findSubscriberMethods(subscriberClass);
        synchronized (this) {
            for (LibPlayMusicSubscriberMethod subscriberMethod : subscriberMethods) {
                subscribe(subscriber, subscriberMethod);
            }
        }
    }

    /**
     * 遍历赋值到 subscriptionsByTag 中
     */
    private void subscribe(Object subscriber, LibPlayMusicSubscriberMethod subscriberMethod) {
        String eventTag = subscriberMethod.eventTag; //唯一标记
        LibPlayMusicSubscription newSubscription = new LibPlayMusicSubscription(subscriber, subscriberMethod);
        CopyOnWriteArrayList<LibPlayMusicSubscription> subscriptions = subscriptionsByTag.get(eventTag);
        if (subscriptions == null) {
            subscriptions = new CopyOnWriteArrayList<>();
            subscriptionsByTag.put(eventTag, subscriptions);
        }
        int size = subscriptions.size();
        for (int i = 0; i <= size; i++) {
            if (i == size) {
                subscriptions.add(i, newSubscription);
                break;
            }
        }
    }

    /**
     * 取消订阅
     */
    public synchronized void unregister(Object obj) {
        if (obj == null) {
            return;
        }
        subscriptionsByTag.clear();
        currentPostingThreadState.remove();
        subscriberMethodFinder.clearCache();
    }

    /**
     * 发送事件
     */
    public synchronized void post(Object event, String eventTag) {
        PostingThreadState postingState = currentPostingThreadState.get();
//        List<Object> eventQueue = postingState.eventQueue;
        List<Object> eventQueue = new ArrayList<>();
        eventQueue.add(event);
        if (!postingState.isPosting) {
            postingState.isMainThread = isMainThread();
            postingState.eventTag = eventTag;
            postingState.isPosting = true;
            if (postingState.canceled) {
                throw new RuntimeException("Internal error. Abort state was not reset");
            }
            try {
                while (!eventQueue.isEmpty()) {
                    postSingleEventForTag(eventQueue.remove(0), postingState);
                }
            } finally {
                postingState.isPosting = false;
                postingState.isMainThread = false;
                postingState.eventTag = null;
            }
        }

    }

    private  void postSingleEventForTag(Object event, PostingThreadState postingState) {
        CopyOnWriteArrayList<LibPlayMusicSubscription> subscriptions;
        synchronized (this) {
            subscriptions = subscriptionsByTag.get(postingState.eventTag);
        }
        if (subscriptions != null && !subscriptions.isEmpty()) {
            for (LibPlayMusicSubscription subscription : subscriptions) {
                postingState.event = event;
                postingState.subscription = subscription;
                boolean aborted;
                try {
                    subscription.postToSubscription(subscription, event, postingState.isMainThread);
                    aborted = postingState.canceled;
                } finally {
                    postingState.event = null;
                    postingState.subscription = null;
                    postingState.canceled = false;
                }
                if (aborted) {
                    break;
                }
            }
        }
    }

    final static class PostingThreadState {
        final List<Object> eventQueue = new ArrayList<>();
        boolean isPosting;
        boolean isMainThread;
        LibPlayMusicSubscription subscription;
        Object event;
        boolean canceled;
        String eventTag;
    }

    private boolean isMainThread() {
        return Looper.myLooper() == Looper.getMainLooper();
    }
}
