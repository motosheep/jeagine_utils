package com.north.light.musiclibrary.constans;

import android.content.Context;

import com.north.light.musiclibrary.bus.LibPlayMusicBus;
import com.north.light.musiclibrary.bus.tags.LibPlayMusicBusTags;
import com.north.light.musiclibrary.utils.LibPlayMusicSPUtils;

import static com.north.light.musiclibrary.constans.LibPlayMusicConstans.music_key_play_model;

/**
 * Created by xian on 2018/1/28.
 */

public class LibPlayMusicPlayMode {

    //单曲循环
    public static final int PLAY_IN_SINGLE_LOOP = 1;

    //随机播放
    public static final int PLAY_IN_RANDOM = 2;

    //列表循环
    public static final int PLAY_IN_LIST_LOOP = 3;

    //顺序播放
    public static final int PLAY_IN_ORDER = 4;

    //倒叙播放
    public static final int PLAY_IN_FLASHBACK = 5;

    private int currPlayMode = PLAY_IN_LIST_LOOP;

    private LibPlayMusicPlayMode() {
    }

    public static LibPlayMusicPlayMode getInstance() {
        return SingletonHolder.sInstance;
    }

    private static class SingletonHolder {
        private static final LibPlayMusicPlayMode sInstance = new LibPlayMusicPlayMode();
    }

    public int getCurrPlayMode(Context context) {
        currPlayMode = (int) LibPlayMusicSPUtils.get(context, music_key_play_model, PLAY_IN_LIST_LOOP);
        return currPlayMode;
    }

    public void setCurrPlayMode(Context context, int currPlayMode) {
        this.currPlayMode = currPlayMode;
        LibPlayMusicSPUtils.put(context, music_key_play_model, currPlayMode);
        LibPlayMusicBus.getInstance().post(currPlayMode, LibPlayMusicBusTags.onPlayModeChange); //通知播放模式变化
    }

}
