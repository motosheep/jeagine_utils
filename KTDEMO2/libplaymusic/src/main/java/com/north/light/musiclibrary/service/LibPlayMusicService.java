package com.north.light.musiclibrary.service;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.os.Build;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.north.light.musiclibrary.cache.LibPlayMusicCacheConfig;
import com.north.light.musiclibrary.control.LibPlayMusicPlayControl;
import com.north.light.musiclibrary.notification.LibPlayMusicNotificationCreater;
import com.north.light.musiclibrary.playback.player.LibPlayMusicExoPlayerHelper;
import com.north.light.musiclibrary.utils.LibPlayMusicLogUtil;

/**
 * Created by xian on 2018/1/20.
 */

public class LibPlayMusicService extends Service {

    private LibPlayMusicPlayControl mBinder;

    private static LibPlayMusicService mService;
    private NotificationManager mNotificationManager;

    @Override
    public void onCreate() {
        super.onCreate();
        mService = this;
        this.mNotificationManager = (NotificationManager) getSystemService(Service.NOTIFICATION_SERVICE);
        stopForeground(true);

        if (Build.VERSION.SDK_INT >= 26) {
            NotificationChannel channel = new NotificationChannel("com.north.light.musiclibrary", "播放通知栏", NotificationManager.IMPORTANCE_HIGH);
            channel.enableLights(false);
            channel.setShowBadge(false);
            channel.setSound(null, null);
            channel.enableVibration(false);
            this.mNotificationManager.createNotificationChannel(channel);
            //    Notification notification = new Notification.Builder(getApplicationContext(),channel.getId()).build();
            //    startForeground(1, notification);
        }

        LibPlayMusicExoPlayerHelper.getInstance().init(this);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        boolean isUseMediaPlayer = intent.getBooleanExtra("isUseMediaPlayer", false);
        boolean isAutoPlayNext = intent.getBooleanExtra("isAutoPlayNext", true);
        boolean isGiveUpAudioFocusManager = intent.getBooleanExtra("isGiveUpAudioFocusManager", false);
        LibPlayMusicNotificationCreater notificationCreater = intent.getParcelableExtra("notificationCreater");
        LibPlayMusicCacheConfig cacheConfig = intent.getParcelableExtra("cacheConfig");
        mBinder = new LibPlayMusicPlayControl
                .Builder(this)
                .setAutoPlayNext(isAutoPlayNext)
                .setUseMediaPlayer(isUseMediaPlayer)
                .setGiveUpAudioFocusManager(isGiveUpAudioFocusManager)
                .setNotificationCreater(notificationCreater)
                .setCacheConfig(cacheConfig)
                .build();
        return mBinder;
    }

    public LibPlayMusicPlayControl getBinder() {
        return mBinder;
    }

    public static LibPlayMusicService getService() {
        return mService;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {


        return START_STICKY;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
        stopSelf();
        LibPlayMusicLogUtil.i("服务关闭了。。。");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        LibPlayMusicLogUtil.i("服务关闭了。。。");
        if (mBinder != null) {
            //mBinder.stopMusic();
            mBinder.releaseMediaSession();
        }
    }
}
