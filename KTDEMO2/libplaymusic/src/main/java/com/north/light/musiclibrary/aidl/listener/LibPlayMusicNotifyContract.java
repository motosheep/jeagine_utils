package com.north.light.musiclibrary.aidl.listener;

import com.north.light.musiclibrary.aidl.model.LibPlayMusicSongInfo;

/**
 * lzx
 * 2018/2/3
 */

public interface LibPlayMusicNotifyContract {
    /**
     * 统一通知播放状态改变
     */
    interface NotifyStatusChanged {
        void notify(LibPlayMusicSongInfo info, int index, int status, String errorMsg);
    }

    /**
     * 切歌
     */
    interface NotifyMusicSwitch {
        void notify(LibPlayMusicSongInfo info);
    }

    interface NotifyTimerTask {
        void notifyTimerTasFinish();

        void onTimerTick(long millisUntilFinished, long totalTime);
    }
}
