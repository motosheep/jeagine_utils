package com.north.light.musiclibrary.control;

import android.os.Bundle;
import android.support.v4.media.session.PlaybackStateCompat;

import com.north.light.musiclibrary.service.LibPlayMusicService;
import com.north.light.musiclibrary.aidl.listener.LibPlayMusicNotifyContract;
import com.north.light.musiclibrary.aidl.model.LibPlayMusicSongInfo;
import com.north.light.musiclibrary.aidl.source.LibPlayMusicIOnPlayerEventListener;
import com.north.light.musiclibrary.aidl.source.LibPlayMusicIOnTimerTaskListener;
import com.north.light.musiclibrary.aidl.source.LibPlayMusicIPlayControl;
import com.north.light.musiclibrary.bus.LibPlayMusicBus;
import com.north.light.musiclibrary.constans.LibPlayMusicPlayMode;
import com.north.light.musiclibrary.constans.LibPlayMusicState;
import com.north.light.musiclibrary.helper.LibPlayMusicQueueHelper;
import com.north.light.musiclibrary.manager.LibPlayMusicMediaSessionManager;
import com.north.light.musiclibrary.manager.LibPlayMusicTimerTaskManager;
import com.north.light.musiclibrary.notification.LibPlayMusicCustomNotification;
import com.north.light.musiclibrary.notification.LibPlayMusicIMediaNotification;
import com.north.light.musiclibrary.notification.LibPlayMusicNotificationCreater;
import com.north.light.musiclibrary.notification.LibPlayMusicSystemNotification;
import com.north.light.musiclibrary.playback.LibPlayMusicPlaybackManager;
import com.north.light.musiclibrary.playback.player.LibPlayMusicPlayback;
import com.north.light.musiclibrary.queue.LibPlayMusicPlayQueueManager;
import com.north.light.musiclibrary.utils.LibPlayMusicSPUtils;

import java.util.List;

import static com.north.light.musiclibrary.constans.LibPlayMusicConstans.play_back_pitch;
import static com.north.light.musiclibrary.constans.LibPlayMusicConstans.play_back_speed;

/**
 * 运行在Remote端
 * <p>
 * lzx
 * 2018/2/8
 */

public class LibPlayMusicBasePlayControl extends LibPlayMusicIPlayControl.Stub implements LibPlayMusicPlaybackManager.PlaybackServiceCallback {

    LibPlayMusicService mMusicService;
    LibPlayMusicNotifyContract.NotifyStatusChanged mNotifyStatusChanged;
    LibPlayMusicNotifyContract.NotifyMusicSwitch mNotifyMusicSwitch;
    LibPlayMusicNotifyContract.NotifyTimerTask mNotifyTimerTask;
    LibPlayMusicPlayback mPlayback;
    boolean isAutoPlayNext;
    LibPlayMusicNotificationCreater notificationCreater;

    LibPlayMusicPlayQueueManager mPlayQueueManager;
    LibPlayMusicPlaybackManager mPlaybackManager;
    LibPlayMusicMediaSessionManager mMediaSessionManager;
    private LibPlayMusicTimerTaskManager mTimerTaskManager;
    private LibPlayMusicIMediaNotification mNotification;

    LibPlayMusicBasePlayControl() {
        LibPlayMusicBus.getInstance().register(this);
    }

    void init() {
        mTimerTaskManager = new LibPlayMusicTimerTaskManager();
        mPlayQueueManager = new LibPlayMusicPlayQueueManager(mMusicService.getApplicationContext());
        mPlaybackManager = new LibPlayMusicPlaybackManager(mPlayback, mPlayQueueManager, isAutoPlayNext);
        mPlaybackManager.setServiceCallback(this);
        mMediaSessionManager = new LibPlayMusicMediaSessionManager(this.mMusicService.getApplicationContext(), mPlaybackManager);
        mPlaybackManager.updatePlaybackState(null, false);
        updateNotificationCreater(notificationCreater);
    }

    public void unregisterBus() {
        LibPlayMusicBus.getInstance().unregister(this);
    }

    /**
     * 设置当前播放音频并通知播放
     */
    private void setCurrentQueueItem(LibPlayMusicSongInfo info, boolean isJustPlay) {
        if (info != null) {
            mPlayQueueManager.setCurrentQueueItem(info.getSongId(), isJustPlay, LibPlayMusicQueueHelper.isNeedToSwitchMusic(mPlaybackManager, info));
        }
    }

    @Override
    public void playMusic(List<LibPlayMusicSongInfo> list, int index, boolean isJustPlay) {
        if (!LibPlayMusicQueueHelper.isIndexPlayable(index, list)) {
            return;
        }
        mPlayQueueManager.setSongInfos(list, index);
        setCurrentQueueItem(list.get(index), isJustPlay);
    }

    @Override
    public void playMusicByInfo(LibPlayMusicSongInfo info, boolean isJustPlay) {
        mPlayQueueManager.addSongInfo(info);
        setCurrentQueueItem(info, isJustPlay);
    }

    @Override
    public void playMusicByIndex(int index, boolean isJustPlay) {
        if (mPlayQueueManager.getSongInfos().size() == 0) {
            return;
        }
        if (!LibPlayMusicQueueHelper.isIndexPlayable(index, mPlayQueueManager.getSongInfos())) {
            return;
        }
        LibPlayMusicSongInfo playInfo = mPlayQueueManager.getSongInfos().get(index);
        setCurrentQueueItem(playInfo, isJustPlay);
    }

    @Override
    public void pausePlayInMillis(final long time) {
        mTimerTaskManager.cancelCountDownTask();
        if (time != -1) {
            mTimerTaskManager.starCountDownTask(time, new LibPlayMusicTimerTaskManager.OnCountDownFinishListener() {
                @Override
                public void onFinish() {
                    if (mPlaybackManager.getPlayback().getState() == LibPlayMusicState.STATE_PLAYING) {
                        mPlaybackManager.handlePauseRequest();
                        mNotifyTimerTask.notifyTimerTasFinish();
                    }
                }

                @Override
                public void onTick(long millisUntilFinished) {
                    mNotifyTimerTask.onTimerTick(millisUntilFinished, time);
                }
            });
        }
    }

    @Override
    public int getCurrPlayingIndex() {
        return mPlayQueueManager.getCurrentIndex();
    }

    @Override
    public void pauseMusic() {
        mPlaybackManager.handlePauseRequest();
    }

    @Override
    public void resumeMusic() {
        mPlaybackManager.handlePlayRequest();
    }

    @Override
    public void stopMusic() {
        mPlaybackManager.handleStopRequest(null, false);
    }

    @Override
    public void setPlayList(List<LibPlayMusicSongInfo> list) {
        mPlayQueueManager.setSongInfos(list);
    }

    @Override
    public void setPlayListWithIndex(List<LibPlayMusicSongInfo> list, int index) {
        mPlayQueueManager.setSongInfos(list, index);
    }

    @Override
    public List<LibPlayMusicSongInfo> getPlayList() {
        return mPlayQueueManager.getSongInfos();
    }

    @Override
    public void deleteSongInfoOnPlayList(LibPlayMusicSongInfo info, boolean isNeedToPlayNext) {
        mPlayQueueManager.deleteSongInfo(info, isNeedToPlayNext);
    }

    @Override
    public int getStatus() {
        return mPlaybackManager.getPlayback().getState();
    }

    @Override
    public int getDuration() {
        return mPlaybackManager.getPlayback().getDuration();
    }

    @Override
    public void playNext() {
        LibPlayMusicSongInfo nextSongInfo = mPlayQueueManager.getNextMusicInfo(true);
        setCurrentQueueItem(nextSongInfo, true);
    }

    @Override
    public void playPre() {
        LibPlayMusicSongInfo preSongInfo = mPlayQueueManager.getPreMusicInfo(true);
        setCurrentQueueItem(preSongInfo, true);
    }

    @Override
    public boolean hasPre() {
        return mPlayQueueManager.hasPreSong();
    }

    @Override
    public boolean hasNext() {
        return mPlayQueueManager.hasNextSong();
    }

    @Override
    public LibPlayMusicSongInfo getPreMusic() {
        return mPlayQueueManager.getPreMusicInfo(false);
    }

    @Override
    public LibPlayMusicSongInfo getNextMusic() {
        return mPlayQueueManager.getNextMusicInfo(false);
    }

    @Override
    public LibPlayMusicSongInfo getCurrPlayingMusic() {
        return mPlayQueueManager.getCurrentSongInfo();
    }

    @Override
    public void setCurrMusic(int index) {
        mPlayQueueManager.setCurrentSong(index);
    }

    @Override
    public void setPlayMode(int mode) {
        LibPlayMusicPlayMode.getInstance().setCurrPlayMode(mMusicService, mode);
    }

    @Override
    public int getPlayMode() {
        return LibPlayMusicPlayMode.getInstance().getCurrPlayMode(mMusicService);
    }

    @Override
    public long getProgress() {
        return mPlaybackManager.getCurrentPosition();
    }

    @Override
    public void seekTo(int position) {
        mPlaybackManager.getPlayback().seekTo(position);
    }

    @Override
    public void reset() {
        mPlaybackManager.handleStopRequest(null, true);
        stopNotification();
    }

    @Override
    public void openCacheWhenPlaying(boolean isOpen) {
        mPlayback.openCacheWhenPlaying(isOpen);
    }

    @Override
    public void stopNotification() {
        if (mNotification != null) {
            mNotification.stopNotification();
        }
    }

    @Override
    public void setPlaybackParameters(float speed, float pitch) {
        LibPlayMusicSPUtils.put(mMusicService.getApplicationContext(), play_back_speed, speed);
        LibPlayMusicSPUtils.put(mMusicService.getApplicationContext(), play_back_pitch, pitch);
        mPlayback.setPlaybackParameters(speed, pitch);
    }

    @Override
    public long getBufferedPosition() {
        return mPlaybackManager.getBufferedPosition();
    }

    @Override
    public void setVolume(float audioVolume) {
        mPlayback.setVolume(audioVolume);
    }

    @Override
    public void updateNotificationCreater(LibPlayMusicNotificationCreater creater) {
        if (creater != null) {
            if (creater.isCreateSystemNotification()) {
                mNotification = new LibPlayMusicSystemNotification(mMusicService, creater, mPlaybackManager);
            } else {
                mNotification = new LibPlayMusicCustomNotification(mMusicService, creater, mPlaybackManager);
            }
        }
    }

    @Override
    public void updateNotificationFavorite(boolean isFavorite) {
        if (mNotification != null) {
            mNotification.updateFavorite(isFavorite);
        }
    }

    @Override
    public void updateNotificationLyrics(boolean isChecked) {
        if (mNotification != null) {
            mNotification.updateLyrics(isChecked);
        }
    }

    @Override
    public void updateNotificationContentIntent(Bundle bundle, String targetClass) {
        if (mNotification != null) {
            mNotification.updateContentIntent(bundle, targetClass);
        }
    }

    @Override
    public void registerPlayerEventListener(LibPlayMusicIOnPlayerEventListener listener) {

    }

    @Override
    public void unregisterPlayerEventListener(LibPlayMusicIOnPlayerEventListener listener) {

    }

    @Override
    public void registerTimerTaskListener(LibPlayMusicIOnTimerTaskListener listener) {

    }

    @Override
    public void unregisterTimerTaskListener(LibPlayMusicIOnTimerTaskListener listener) {

    }

    @Override
    public int getAudioSessionId() {
        return mPlaybackManager.getAudioSessionId();
    }

    @Override
    public float getPlaybackSpeed() {
        return mPlayback.getPlaybackSpeed();
    }

    @Override
    public float getPlaybackPitch() {
        return mPlayback.getPlaybackPitch();
    }

    @Override
    public void onPlaybackSwitch(LibPlayMusicSongInfo info) {
        mNotifyMusicSwitch.notify(info);
        if (mNotification != null) {
            mNotification.startNotification(info);
        }
    }

    @Override
    public void onPlaybackError(String errorMsg) {
        mNotifyStatusChanged.notify(mPlayQueueManager.getCurrentSongInfo(), mPlayQueueManager.getCurrentIndex(), LibPlayMusicState.STATE_ERROR, errorMsg);
    }

    @Override
    public void onPlaybackCompletion(LibPlayMusicSongInfo songInfo) {
        mNotifyStatusChanged.notify(songInfo, mPlayQueueManager.getCurrentIndex(), LibPlayMusicState.STATE_IDLE, null);
    }

    @Override
    public void onNotificationRequired() {

    }

    @Override
    public void onPlaybackStateUpdated(int state, PlaybackStateCompat newState) {
        //状态改变
        mNotifyStatusChanged.notify(mPlayQueueManager.getCurrentSongInfo(), mPlayQueueManager.getCurrentIndex(), state, null);
        mMediaSessionManager.setPlaybackState(newState);
        if (mNotification != null) {
            if (state == LibPlayMusicState.STATE_PLAYING) {
                mNotification.updateViewStateAtStart();
            } else {
                mNotification.updateViewStateAtPause();
            }
        }
    }
}
