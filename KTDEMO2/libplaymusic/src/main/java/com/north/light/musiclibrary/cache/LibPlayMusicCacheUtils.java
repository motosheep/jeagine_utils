package com.north.light.musiclibrary.cache;

import android.content.Context;
import android.os.Environment;
import android.text.TextUtils;

import com.north.light.musiclibrary.videocache.HttpProxyCacheServer;

import java.io.File;
import java.io.IOException;

/**
 * 音频缓存工具类
 * Created by xian on 2018/3/31.
 * change by lzt 20220920 修改为应用内部目录缓存
 */
public class LibPlayMusicCacheUtils {

    private static String defaultPath = "/musicLibrary_doexam/song-cache/";

    /**
     * change by lzt 20220920 修改缓存目录为应用私有目录
     */
    public static File getDefaultSongCacheDir(Context context) {
        try {
            String rootPath = context.getApplicationContext().getExternalCacheDir().getAbsolutePath();
            return getSongCacheDir(rootPath + getCachePath());
        } catch (Exception e) {
            return getSongCacheDir(getStorageDirectoryPath() + getCachePath());
        }
    }

    public static File getSongCacheDir(String path) {
        File filePath = new File(path);
        if (!filePath.exists()) {
            filePath.mkdirs();
        }
        return filePath;
    }

    public static void cleanSongCacheDir(Context context) throws IOException {
        File videoCacheDir = getDefaultSongCacheDir(context);
        cleanDirectory(videoCacheDir);
    }

    public static String getStorageDirectoryPath() {
        return Environment.getExternalStorageDirectory().getPath();
    }

    public static String getCachePath() {
        return defaultPath;
    }

    public static void setCachePath(String path) {
        defaultPath = path;
    }

    private static void cleanDirectory(File file) throws IOException {
        if (!file.exists()) {
            return;
        }
        File[] contentFiles = file.listFiles();
        if (contentFiles != null) {
            for (File contentFile : contentFiles) {
                delete(contentFile);
            }
        }
    }

    private static void delete(File file) throws IOException {
        if (file.isFile() && file.exists()) {
            deleteOrThrow(file);
        } else {
            cleanDirectory(file);
            deleteOrThrow(file);
        }
    }

    private static void deleteOrThrow(File file) throws IOException {
        if (file.exists()) {
            boolean isDeleted = file.delete();
            if (!isDeleted) {
                throw new IOException(String.format("File %s can't be deleted", file.getAbsolutePath()));
            }
        }
    }


    public static HttpProxyCacheServer.Builder createHttpProxyCacheServerBuilder(Context context, LibPlayMusicCacheConfig cacheConfig) {
        HttpProxyCacheServer.Builder builder = new HttpProxyCacheServer.Builder(context);
        if (cacheConfig == null) {
            builder.cacheDirectory(LibPlayMusicCacheUtils.getDefaultSongCacheDir(context))
                    .maxCacheSize(1024 * 1024 * 1024) //1G
                    .fileNameGenerator(new LibPlayMusicMusicMd5Generator());
        } else {
            String configCachePath = cacheConfig.getCachePath();
            int configMaxSize = cacheConfig.getMaxCacheSize();
            int configMaxFileCount = cacheConfig.getMaxCacheFilesCount();
            builder.cacheDirectory(
                    !TextUtils.isEmpty(configCachePath)
                            ? LibPlayMusicCacheUtils.getSongCacheDir(configCachePath)
                            : LibPlayMusicCacheUtils.getDefaultSongCacheDir(context));
            builder.maxCacheSize(configMaxSize != 0 ? configMaxSize : 1024 * 1024 * 1024);
            if (configMaxFileCount != 0) {
                builder.maxCacheFilesCount(configMaxFileCount);
            }
            builder.fileNameGenerator(new LibPlayMusicMusicMd5Generator());
        }
        return builder;
    }
}
