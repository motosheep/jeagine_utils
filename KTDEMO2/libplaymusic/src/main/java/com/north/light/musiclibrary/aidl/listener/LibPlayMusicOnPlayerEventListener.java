package com.north.light.musiclibrary.aidl.listener;

import com.north.light.musiclibrary.aidl.model.LibPlayMusicSongInfo;

/**
 * lzx
 * 2018/2/3
 */

public interface LibPlayMusicOnPlayerEventListener {
    void onMusicSwitch(LibPlayMusicSongInfo music);

    void onPlayerStart();

    void onPlayerPause();

    void onPlayCompletion(LibPlayMusicSongInfo songInfo);

    void onPlayerStop();

    void onError(String errorMsg);

    void onAsyncLoading(boolean isFinishLoading);
}
