package com.north.light.libmusicplay.permission;

import android.content.Context;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;


/**
 * author:li
 * date:2022/4/3
 * desc:权限检查Utils
 */
public class LibMusicPlayPermissionCheck {

    private static final class SingleHolder {
        static final LibMusicPlayPermissionCheck mInstance = new LibMusicPlayPermissionCheck();
    }

    public static LibMusicPlayPermissionCheck getInstance() {
        return LibMusicPlayPermissionCheck.SingleHolder.mInstance;
    }

    /**
     * 检查权限
     * Manifest.permission.CAMERA
     */
    public boolean check(Context context, String permission) {
        if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
            return false;
        }
        return true;
    }


}
