package com.north.light.libsliderecyclerview;

import android.graphics.Canvas;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.RecyclerView;

/**
 * author:li
 * date:2022/9/17
 * desc:
 */
public class SlideItemTouchHelperCallback<T extends SlideBaseAdapter<?,?>> extends ItemTouchHelper.Callback {

    private T mAdapter;

    public SlideItemTouchHelperCallback(T mAdapter) {
        this.mAdapter = mAdapter;
    }

    /**
     * makeMovementFlags(p1,p2)
     * p1:侧滑删除使用:0忽略手势；ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.UP | ItemTouchHelper.DOWN
     * p2：拖动滑动使用:0忽略手势；ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.UP | ItemTouchHelper.DOWN
     */
    @Override
    public int getMovementFlags(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder) {
//        return makeMovementFlags(0, ItemTouchHelper.LEFT | ItemTouchHelper.RIGHT | ItemTouchHelper.UP | ItemTouchHelper.DOWN);
        return makeMovementFlags(0, ItemTouchHelper.UP);
    }

    @Override
    public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder
            viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
        return false;
    }

    @Override
    public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        //通知adapter
        if (mAdapter != null) {
            int removePosition = viewHolder.getLayoutPosition();
            //由于是限定了永远只有三个，所以，该current position的值永远少于等于
            mAdapter.getDataList().remove(removePosition);
//        mAdapter.getDataList().add(0, s);
            mAdapter.notifyDataSetChanged();
            mAdapter.notifyOutSideRefresh();
            mAdapter.notifyPageChange(removePosition);
        }
    }



    @Override
    public void onChildDraw(@NonNull Canvas c, @NonNull RecyclerView
            recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, float dX, float dY,
                            int actionState, boolean isCurrentlyActive) {
        try {
            //计算移动距离
            float distance = (float) Math.hypot(dX, dY);
            float maxDistance = recyclerView.getWidth() / 2f;
            //比例
            float fraction = distance / maxDistance;
            if (fraction > 1) {
                fraction = 1;
            }
            int maxCount = SlideConfig.SHOW_MAX_COUNT;
            int trainY = SlideDpUtils.dp2px(SlideConfig.TRANSLATION_Y);
            //为每个child执行动画
            int count = recyclerView.getChildCount();
            int adapterCount = mAdapter.getDataList().size();
            for (int i = 0; i < count; i++) {
                if (i != count - 1) {
                    //不是第一层--且数量大于3
                    if (adapterCount > maxCount) {
                        View view = recyclerView.getChildAt(i);
                        view.setScaleX(1 - (maxCount - i - 1) * SlideConfig.SCALE + fraction * SlideConfig.SCALE);
                        view.setTranslationY((maxCount - i - 1) * trainY - fraction * trainY);
                    } else {
                        View view = recyclerView.getChildAt(i);
                        view.setScaleX(1 - (count - i - 1) * SlideConfig.SCALE + fraction * SlideConfig.SCALE);
                        view.setTranslationY((count - i - 1) * trainY - fraction * trainY);
                    }

                }


//            int level = SlideConfig.SHOW_MAX_COUNT - i - 1;
//            if (level != SlideConfig.SHOW_MAX_COUNT)
//            //获取的view从下层到上层
//            View view = recyclerView.getChildAt(i);
//            int level = SlideConfig.SHOW_MAX_COUNT - i - 1;
//            //level范围（SlideConfig.SHOW_MAX_COUNT-1）-0，每个child最大只移动一个SlideConfig.TRANSLATION_Y和放大SlideConfig.SCALE
//
//            if (level == SlideConfig.SHOW_MAX_COUNT - 1) { // 最下层的不动和最后第二层重叠
//                view.setTranslationY(SlideConfig.TRANSLATION_Y * (level - 1));
//                view.setScaleX(1 - SlideConfig.SCALE * (level - 1));
//                view.setScaleY(1 - SlideConfig.SCALE * (level - 1));
//            } else if (level > 0) {
//                view.setTranslationY(level * SlideConfig.TRANSLATION_Y - fraction * SlideConfig.TRANSLATION_Y);
//                view.setScaleX(1 - level * SlideConfig.SCALE + fraction * SlideConfig.SCALE);
//                view.setScaleY(1 - level * SlideConfig.SCALE + fraction * SlideConfig.SCALE);
//            }
            }
        } catch (Exception e) {

        }
        super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
    }

    //控制滑动距离生效灵敏度
    @Override
    public float getSwipeThreshold(@NonNull RecyclerView.ViewHolder viewHolder) {
        return 0.3f;
    }
}
