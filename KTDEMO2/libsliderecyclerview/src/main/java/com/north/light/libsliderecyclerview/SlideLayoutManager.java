package com.north.light.libsliderecyclerview;

import android.view.View;
import android.view.ViewGroup;

import androidx.recyclerview.widget.RecyclerView;

/**
 * author:li
 * date:2022/9/17
 */
public class SlideLayoutManager extends RecyclerView.LayoutManager {



    public SlideLayoutManager() {
    }


    @Override
    public RecyclerView.LayoutParams generateDefaultLayoutParams() {
        return new RecyclerView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
    }

    @Override
    public void onLayoutChildren(RecyclerView.Recycler recycler, RecyclerView.State state) {
        super.onLayoutChildren(recycler, state);

        detachAndScrapAttachedViews(recycler);
        int maxCount = SlideConfig.SHOW_MAX_COUNT;
        //获取所有item(包括不可见的)个数
        int count = getItemCount();
        //由于我们是倒序摆放，所以初始索引从后面开始
        int initIndex = count - maxCount;
        if (initIndex < 0) {
            initIndex = 0;
        }

        //当前顺序
        int currentIndex = 0;

        boolean enoughCount = count > maxCount;

        if (enoughCount) {
            for (int i = initIndex; i < count; i++) {
                //从缓存中获取view
                View view = recycler.getViewForPosition(i);
                //添加到recyclerView
                addView(view);
                //测量一下view
                measureChild(view, 0, 0);

                //居中摆放，getDecoratedMeasuredWidth方法是获取带分割线的宽度，比直接使用view.getWidth()精确
                int realWidth = getDecoratedMeasuredWidth(view);
                int realHeight = getDecoratedMeasuredHeight(view);
                int widthPadding = (int) ((getWidth() - realWidth) / 2f);
                int heightPadding = (int) ((getHeight() - realHeight) / 2f);

                //摆放child
                layoutDecorated(view, widthPadding, heightPadding,
                        widthPadding + realWidth, heightPadding + realHeight);
                //根据索引，来位移和缩放child
                int measureHeight = view.getMeasuredHeight();
                int trainY = SlideDpUtils.dp2px(SlideConfig.TRANSLATION_Y);


                RecyclerView.LayoutParams viewParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                int paramsHeight = viewParams.height;
                if (paramsHeight == -1) {
                    viewParams.height = (measureHeight - trainY * (maxCount + 1));
//                viewParams.height = (500);
                    view.setLayoutParams(viewParams);
                }

                float translationY = (maxCount - currentIndex - 1) * trainY;
                if (currentIndex != maxCount - 1) {
                    view.setTranslationY(translationY);
                }
                view.setScaleX(1 - (maxCount - currentIndex - 1) * SlideConfig.SCALE);
                currentIndex++;

//            view.setScaleY(1 - level * SlideConfig.SCALE);
            }
        } else {
            for (int i = initIndex; i < count; i++) {
                //从缓存中获取view
                View view = recycler.getViewForPosition(i);
                //添加到recyclerView
                addView(view);
                //测量一下view
                measureChild(view, 0, 0);

                //居中摆放，getDecoratedMeasuredWidth方法是获取带分割线的宽度，比直接使用view.getWidth()精确
                int realWidth = getDecoratedMeasuredWidth(view);
                int realHeight = getDecoratedMeasuredHeight(view);
                int widthPadding = (int) ((getWidth() - realWidth) / 2f);
                int heightPadding = (int) ((getHeight() - realHeight) / 2f);

                //摆放child
                layoutDecorated(view, widthPadding, heightPadding,
                        widthPadding + realWidth, heightPadding + realHeight);
                //根据索引，来位移和缩放child
                int measureHeight = view.getMeasuredHeight();
                int trainY = SlideDpUtils.dp2px(SlideConfig.TRANSLATION_Y);


                RecyclerView.LayoutParams viewParams = (RecyclerView.LayoutParams) view.getLayoutParams();
                int paramsHeight = viewParams.height;
                if (paramsHeight == -1) {
                    viewParams.height = (measureHeight - trainY * (maxCount + 1));
//                viewParams.height = (500);
                    view.setLayoutParams(viewParams);
                }

                float translationY = (count - currentIndex - 1) * trainY;
                if (currentIndex != count - 1) {
                    view.setTranslationY(translationY);
                }
                view.setScaleX(1 - (count - currentIndex - 1) * SlideConfig.SCALE);
                currentIndex++;

//            view.setScaleY(1 - level * SlideConfig.SCALE);
            }
        }



    }
}
