package com.north.light.libsliderecyclerview;

/**
 * author:li
 * date:2022/9/17
 * desc:
 */
public class SlideConfig {
    public static int SHOW_MAX_COUNT = 3;
    //Y轴偏移量 unit dp
    public static float TRANSLATION_Y = 8f;
    public static float SCALE = 0.1f;
}
