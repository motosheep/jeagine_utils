package com.north.light.libsliderecyclerview;

import android.content.Context;
import android.util.AttributeSet;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

/**
 * author:li
 * date:2022/9/17
 * desc:拖拽滑动recyclerview
 */
public class SlideRecyclerview extends RecyclerView {

    public SlideRecyclerview(@NonNull Context context) {
        super(context);
        init();
    }

    public SlideRecyclerview(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlideRecyclerview(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOverScrollMode(OVER_SCROLL_NEVER);
    }


}
