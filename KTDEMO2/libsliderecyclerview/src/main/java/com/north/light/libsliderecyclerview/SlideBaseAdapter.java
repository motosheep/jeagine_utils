package com.north.light.libsliderecyclerview;

import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * author:li
 * date:2022/9/18
 * desc:拖动组件滑动adapter
 */
public abstract class SlideBaseAdapter<T extends RecyclerView.ViewHolder, B extends Object> extends RecyclerView.Adapter<T> {
    private List<B> dataList = new ArrayList<>();
    //是否刷新中的标识
    private boolean mLoading = false;
    private OnLoadEventListener<B> mListener;

    public List<B> getDataList() {
        if (dataList == null || dataList.size() == 0) {
            return new ArrayList<>();
        }
        return dataList;
    }

    public void setDataList(List<B> dataList) {
        //由于layout manager的绘制方式，是从下往上绘制的，所以，需要把数据倒转
        if (dataList == null || dataList.size() == 0) {
            this.dataList = new ArrayList<>();
        } else {
            //倒序
            Collections.reverse(dataList);
            notifyPageChangeAfterRefresh(dataList);
            this.dataList = dataList;
        }
        notifyDataSetChanged();
    }

    public void addDataList(List<B> dataList) {
        //由于layout manager的绘制方式，是从下往上绘制的，所以，需要把数据倒转
        if (dataList != null && dataList.size() != 0) {
            Collections.reverse(dataList);
            notifyPageChangeAfterRefresh(dataList);
            this.dataList.addAll(0, dataList);
            notifyDataSetChanged();
        }
    }


    /**
     * 判断是否需要通知外部刷新
     */
    public void notifyOutSideRefresh() {
        if (mLoading) {
            return;
        }
        //当数据小于等于3的时候，则需要加载数据
        if (getDataList().size() <= SlideConfig.SHOW_MAX_COUNT) {
            mLoading = true;
            if (mListener != null) {
                mListener.load();
            }
        }
        if (getDataList().size() == 0) {
            mLoading = false;
            if (mListener != null) {
                mListener.noData();
            }
        }
    }

    /**
     * 页面切换通知
     *
     * @param removePosition 当前移除数据的位置
     */
    public void notifyPageChange(int removePosition) {
        if (removePosition == 0) {
            return;
        }
        if (mListener == null) {
            return;
        }
        int dataPosition = Math.max(0, removePosition - 1);
        B obj = getDataList().get(dataPosition);
        mListener.swipe(dataPosition, obj);
        mListener.swipe(obj);
    }

    /**
     * 判断旧数据是否有值，否则不刷新
     *
     * @param dataList 更新的数据集合
     */
    public void notifyPageChangeAfterRefresh(List<B> dataList) {
        if (dataList == null || dataList.size() == 0) {
            return;
        }
        if (!(getDataList() == null || getDataList().size() == 0)) {
            return;
        }
        if (mListener == null) {
            return;
        }
        B obj = dataList.get(dataList.size() - 1);
        mListener.swipe(dataList.size() - 1, obj);
        mListener.swipe(obj);
    }

    /**
     * 设置加载完成
     */
    public void setLoadFinish() {
        this.mLoading = false;
    }


    /**
     * 刷新监听
     */
    public void setOnLoadEventListener(OnLoadEventListener<B> listener) {
        mListener = listener;
    }


    public interface OnLoadEventListener<W> {
        void load();

        void noData();

        /**
         * @param currentPosition adapter中的数据位置，
         * @param obj             adapter中的数据对象
         */
        void swipe(int currentPosition, W obj);

        void swipe(W obj);
    }
}
