package com.north.light.libsliderecyclerview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.ItemTouchHelper;

/**
 * author:li
 * date:2022/9/18
 * desc:
 */
public class SlideRootView extends RelativeLayout {
    private SlideRecyclerview slideRecyclerview;

    public SlideRootView(Context context) {
        super(context);
        init();
    }

    public SlideRootView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public SlideRootView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        slideRecyclerview = new SlideRecyclerview(getContext());

        addView(getSlideRecyclerview());
        int trainY = SlideDpUtils.dp2px(SlideConfig.TRANSLATION_Y);
        int count = SlideConfig.SHOW_MAX_COUNT - 1;
        RelativeLayout.LayoutParams relativeLayoutParams = (LayoutParams) getSlideRecyclerview().getLayoutParams();
        relativeLayoutParams.setMargins(
                relativeLayoutParams.leftMargin,
                relativeLayoutParams.topMargin - trainY * count + 1,
                relativeLayoutParams.rightMargin,
                relativeLayoutParams.bottomMargin
        );
        relativeLayoutParams.height = LayoutParams.MATCH_PARENT;
        relativeLayoutParams.width = LayoutParams.MATCH_PARENT;
        getSlideRecyclerview().setLayoutParams(relativeLayoutParams);
        getSlideRecyclerview().setLayoutManager(new SlideLayoutManager());
    }


    public SlideRecyclerview getSlideRecyclerview() {
        return slideRecyclerview;
    }


    /**
     * 设置adapter并且关联数据
     */
    public void setAdapter(SlideBaseAdapter<?, ?> adapter) {
        getSlideRecyclerview().setAdapter(adapter);
        SlideItemTouchHelperCallback<?> itemTouchHelperCallback = new SlideItemTouchHelperCallback<SlideBaseAdapter<?, ?>>(adapter);
        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(itemTouchHelperCallback);
        itemTouchHelper.attachToRecyclerView(getSlideRecyclerview());
    }
}
